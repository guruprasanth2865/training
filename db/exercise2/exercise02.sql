CREATE DATABASE office;
USE office;

/*QUESTION 1*/
CREATE TABLE department(
  dept_id INT NOT NULL AUTO_INCREMENT
  ,PRIMARY KEY(dept_id)
  ,name VARCHAR(25)
  );
  
  CREATE TABLE employee(
    emp_id INT NOT NULL AUTO_INCREMENT
    ,PRIMARY KEY(emp_id)
    ,first_name VARCHAR(25) NOT NULL
    ,surname VARCHAR(25)
    ,dob DATE NOT NULL
    ,date_of_joining DATE NOT NULL
    ,annual_salary FLOAT NOT NULL
    ,dept INT NOT NULL
    ,FOREIGN KEY (dept) REFERENCES department(dept_id)
  );
  
  INSERT INTO department(name)
    VALUES ("ITDesk")
		  ,("Finance")
          ,("Engineering")
          ,("HR")
          ,("Recruitment")
          ,("Facility");
	
SELECT dept_id, name FROM department;
  
  
  /*question 2*/
  INSERT INTO employee (first_name, surname, dob, date_of_joining, annual_salary, dept)
    VALUES ('Vishnu', 'Sarath', '1980-01-19', '2000-06-06', 2500000, 1)
          ,('Guru', 'Prasanth', '1975-09-30', '2001-07-25', 1500000,1)
          ,('Naveen', 'Akash', '1999-07-15', '2019-06-18', 2000000, 1)
          ,('Sam', 'Ebinazer', '1995-02-28', '2018-06-23', 1200000, 1)
          ,('Pravin', 'Kumar', '1987-02-06', '2001-01-16', 1000000, 1)
          ,('Sundar', 'Raj', '1998-03-10', '2019-07-21', 500000, 2)
          ,('Mark', 'Zuk', '1995-08-04', '2017-08-26', 350000, 2)
          ,('Steve', 'Jobs', '1980-06-09', '1998-04-03', 1500000, 2)
          ,('Mukesh', 'Kumar', '1978-04-12', '1999-03-17', 2000000, 2)
          ,('Silverster', 'Stalone', '1965-07-08', '1995-03-06', 1300000, 2)
          ,('Chris', 'Evan', '1979-03-07', '2002-08-19', 1700000, 3)
          ,('Robert', 'Downy', '1986-01-10', '2005-02-15', 5000000, 3)
          ,('Dwane', 'Johnson', '1982-03-18', '2004-01-28', 4000000, 3)
          ,('Ram', 'Chandran', '2000-04-23', '2019-08-05', 450000, 3)
          ,('Kamal', 'Hasan', '1979-04-20', '2015-06-11', 600000, 3)
          ,('Rajini', 'Kanth', '1994-01-15', '2009-03-05', 1000000, 4)
          ,('Sushanth', 'Singh', '1988-04-06', '2010-09-11', 1200000, 4)
          ,('Bharath', 'Ram', '1977-12-17', '1998-10-23', 400000, 4)
          ,('Kygo', 'Drawn', '1985-11-28', '2016-06-06', 1500000, 4)
          ,('Jack', 'Ma', '2000-12-10', '2019-12-24', 650000, 4)
          ,('Karthi', 'Keyan', '1977-10-30', '1997-07-09', 700000, 5)
          ,('Saravana', 'Kumar', '1988-03-27', '2017-12-02', 850000, 5)
          ,('Abhishek', 'Varma', '1996-09-29', '2018-10-03', 900000, 5)
          ,('Naveen', 'Krishna', '1993-05-09', '2014-04-01', 560000, 5)
          ,('Bear', 'Gylls', '1990-08-24', '2011-12-04', 870000, 5)
          ,('Siva', 'Kumar', '1988-03-22', '2009-10-18', 650000, 6)
          ,('Tony', 'Stark', '1990-01-01', '2010-03-10', 1700000, 6)
          ,('David', 'Guetta', '1996-12-05', '2017-08-17', 300000, 6)
          ,('Martin', 'Garrix', '1985-11-21', '2005-09-15', 550000, 6)
          ,('Drake', 'Robin', '1977-09-18', '1997-12-28', 650000, 6);
  
SELECT emp_id, first_name, dob, date_of_joining, annual_salary, dept FROM employee;

/*QUESTION 3 --- SET PRIMARY KEY TO EMPLOYEE --- ALREADY SET*/

/*QUESTION 4  --  INCREMENT SALARY 10 PERCENT*/
UPDATE employee
  SET annual_salary = annual_salary + (annual_salary * (10/100))
    WHERE dept IN (SELECT dept_id FROM department);
  
SELECT emp_id, first_name, dob, date_of_joining, annual_salary, dept FROM employee;

/*QUESTION 5  -  FIND HIGHEST AND LEAST PAID IN EACH DEPARTMENT*/
/*High paid from IT-DESK*/
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 1;
  
SELECT emp_id, first_name, MAX(annual_salary) FROM employee;
/*Least paid from IT_DESK*/
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 1;
  
/*High paid from Finance*/
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 2;
/*Least paid from Finance*/
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 2;

/*High paid from Engineering*/
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 3;
/*Least paid from Engineering*/
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 3;

/*High paid from HR*/
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 4;
/*Least paid from HR*/
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 4;

/*High paid from Recruitment*/
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 5;
/*Least paid from Recruitment*/
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 5;

/*High paid from Facility*/
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 6;
/*Least paid from Facility*/
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 6;


/*QUESTION 6*/
SELECT AVG(annual_salary) FROM employee;

/*QUESTION 7*/
/*Self Join*/
CREATE TABLE working_day(
  id VARCHAR(3) NOT NULL DEFAULT 'Aug'
  ,emp_id INT NOT NULL
  ,FOREIGN KEY(emp_id) REFERENCES employee(emp_id)
  ,attendance INT NOT NULL
);

INSERT INTO working_day (emp_id, attendance)
  VALUES (1, 26)
		,(2, 30)
        ,(3, 25)
        ,(4, 20)
        ,(5, 26)
        ,(6, 26)
		,(7, 30)
        ,(8, 25)
        ,(9, 20)
        ,(10, 26)
        ,(11, 26)
		,(12, 30)
        ,(13, 25)
        ,(14, 20)
        ,(15, 26)
        ,(16, 26)
		,(17, 30)
        ,(18, 25)
        ,(19, 20)
        ,(20, 26)
        ,(21, 26)
		,(22, 30)
        ,(23, 25)
        ,(24, 20)
        ,(25, 26)
        ,(26, 26)
		,(27, 30)
        ,(28, 25)
        ,(29, 20)
        ,(30, 26)
        ,(31, 5);
        
        
SELECT emp.emp_id, emp.first_name, emp.annual_salary, dept.name, work.attendance
  FROM employee emp, department dept, working_day work
    WHERE emp.dept = dept.dept_id AND emp.emp_id = work.emp_id ;       
        
        
        
SELECT emp.emp_id, emp.first_name, emp.annual_salary, dept.name
  FROM employee emp, department dept, working_day
    WHERE emp.dept = dept.dept_id;
    
/*QUESTION 8*/
/*LIST OUT EMPLOYEE FROM SAME DEPARTMENT*/ 
ALTER TABLE employee
  ADD COLUMN area VARCHAR(25); 
  
  
SELECT emp.emp_id, emp.first_name, emp.annual_salary, dept.name
  FROM employee emp, department dept
    WHERE emp.dept = dept.dept_id AND emp.dept = 1 GROUP BY emp.area;


ALTER TABLE employee
  MODIFY dept Int;

INSERT INTO employee (first_name, surname, dob, date_of_joining, annual_salary)
  VALUES ('Ram', 'Charan', '2020-08-04', '2000-06-12', 2300000);
    
show columns from employee;


/*QUESTION 9*/

SELECT emp_id, first_name, dob FROM employee WHERE dob = date(now());


/*QUESTION 10*/
SELECT emp_id, first_name, dob FROM employee
  WHERE dept IS NULL;

/*QUESTION 11 */
SELECT emp.emp_id, emp.first_name, dept.name 
  FROM employee emp, department dept
    WHERE emp.dept = dept.dept_id;
    










