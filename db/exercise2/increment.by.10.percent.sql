
/*QUESTION 4  --  INCREMENT SALARY 10 PERCENT*/
UPDATE employee
  SET annual_salary = annual_salary + (annual_salary * (10/100))
    WHERE dept IN (SELECT dept_id FROM department);
  
SELECT emp_id, first_name, dob, date_of_joining, annual_salary, dept FROM employee;

