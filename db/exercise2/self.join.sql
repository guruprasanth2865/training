/*QUESTION 7*/
/*Self Join*/
CREATE TABLE working_day(
  id VARCHAR(3) NOT NULL DEFAULT 'Aug'
  ,emp_id INT NOT NULL
  ,FOREIGN KEY(emp_id) REFERENCES employee(emp_id)
  ,attendance INT NOT NULL
);

INSERT INTO working_day (emp_id, attendance)
  VALUES (1, 26)
		,(2, 30)
        ,(3, 25)
        ,(4, 20)
        ,(5, 26)
        ,(6, 26)
		,(7, 30)
        ,(8, 25)
        ,(9, 20)
        ,(10, 26)
        ,(11, 26)
		,(12, 30)
        ,(13, 25)
        ,(14, 20)
        ,(15, 26)
        ,(16, 26)
		,(17, 30)
        ,(18, 25)
        ,(19, 20)
        ,(20, 26)
        ,(21, 26)
		,(22, 30)
        ,(23, 25)
        ,(24, 20)
        ,(25, 26)
        ,(26, 26)
		,(27, 30)
        ,(28, 25)
        ,(29, 20)
        ,(30, 26)
        ,(31, 5);
        
        
SELECT emp.emp_id, emp.first_name, emp.annual_salary, dept.name, work.attendance
  FROM employee emp, department dept, working_day work
    WHERE emp.dept = dept.dept_id AND emp.emp_id = work.emp_id ;       
        
        
        
SELECT emp.emp_id, emp.first_name, emp.annual_salary, dept.name
  FROM employee emp, department dept, working_day
    WHERE emp.dept = dept.dept_id;
    