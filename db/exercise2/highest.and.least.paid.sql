
/*QUESTION 5  -  FIND HIGHEST AND LEAST PAID IN EACH DEPARTMENT*/
/*High paid from IT-DESK*/
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 1;
  
SELECT emp_id, first_name, MAX(annual_salary) FROM employee;
/*Least paid from IT_DESK*/
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 1;
  
/*High paid from Finance*/
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 2;
/*Least paid from Finance*/
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 2;

/*High paid from Engineering*/
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 3;
/*Least paid from Engineering*/
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 3;

/*High paid from HR*/
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 4;
/*Least paid from HR*/
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 4;

/*High paid from Recruitment*/
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 5;
/*Least paid from Recruitment*/
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 5;

/*High paid from Facility*/
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 6;
/*Least paid from Facility*/
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 6;

