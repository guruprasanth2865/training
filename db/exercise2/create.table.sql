
/*QUESTION 1*/
CREATE TABLE department(
  dept_id INT NOT NULL AUTO_INCREMENT
  ,PRIMARY KEY(dept_id)
  ,name VARCHAR(25)
  );
  
  CREATE TABLE employee(
    emp_id INT NOT NULL AUTO_INCREMENT
    ,PRIMARY KEY(emp_id)
    ,first_name VARCHAR(25) NOT NULL
    ,surname VARCHAR(25)
    ,dob DATE NOT NULL
    ,date_of_joining DATE NOT NULL
    ,annual_salary FLOAT NOT NULL
    ,dept INT NOT NULL
    ,FOREIGN KEY (dept) REFERENCES department(dept_id)
  );
  
  INSERT INTO department(name)
    VALUES ("ITDesk")
		  ,("Finance")
          ,("Engineering")
          ,("HR")
          ,("Recruitment")
          ,("Facility");
	
SELECT dept_id, name FROM department;
  