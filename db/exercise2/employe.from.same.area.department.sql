    
/*QUESTION 8*/
/*LIST OUT EMPLOYEE FROM SAME DEPARTMENT*/ 
ALTER TABLE employee
  ADD COLUMN area VARCHAR(25); 
  
  
SELECT emp.emp_id, emp.first_name, emp.annual_salary, dept.name
  FROM employee emp, department dept
    WHERE emp.dept = dept.dept_id AND emp.dept = 1 GROUP BY emp.area;


ALTER TABLE employee
  MODIFY dept Int;

INSERT INTO employee (first_name, surname, dob, date_of_joining, annual_salary)
  VALUES ('Ram', 'Charan', '2020-08-04', '2000-06-12', 2300000);
    
show columns from employee;
