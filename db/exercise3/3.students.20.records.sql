/* Exercise 3 - Question 3*/
/**/
USE college;

SELECT std.roll_number, 
       std.name AS student_name, 
	   std.gender, 
	   std.dob, 
	   std.email, 
	   std.phone, 
	   std.address, 
       clg.name AS college_name, 
       dept.dept_name AS department_name
      FROM student std
          LEFT JOIN college clg 
                    ON clg.id = std.college_id
	      LEFT JOIN college_department clg_dpt
                    ON clg.id = clg_dpt.college_id
	      LEFT JOIN department dept
                    ON clg_dpt.udept_code = dept.dept_code;