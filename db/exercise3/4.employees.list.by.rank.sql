/*Exercise 3 - question 4*/

SELECT emp.name, 
       clg.name, 
	   dept.dept_name, 
	   desig.name FROM employee emp
	  LEFT JOIN college clg
                ON emp.college_id = clg.id
      LEFT JOIN college_department clg_dpt
                USING (cdept_id)
      LEFT JOIN department dept
                ON dept.dept_code = clg_dpt.udept_code
      LEFT JOIN university univ
                ON  clg.univ_code =  univ.univ_code 
                  AND univ.university_name = "Anna University"
      LEFT JOIN designation desig
                ON desig.id = emp.desig_id
      ORDER BY clg.name, desig.d_rank;
    
    
