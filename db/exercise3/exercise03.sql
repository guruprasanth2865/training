/*CREATE DATABASE COLLEGE*/
CREATE DATABASE college;

/*Use database college*/
USE college;

/*Create university table*/
CREATE TABLE university (
  univ_code CHAR(4) NOT NULL PRIMARY KEY
  ,university_name VARCHAR(100) NOT NULL
);

SHOW COLUMNS FROM university;
DROP TABLE university;

CREATE TABLE college (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  ,code CHAR(4) NOT NULL
  ,name VARCHAR(100) NOT NULL
  ,univ_code CHAR(4) NOT NULL
  ,FOREIGN KEY(univ_code) REFERENCES university(univ_code)
  ,city VARCHAR(50) NOT NULL
  ,state VARCHAR(50) NOT NULL
  ,year_opened YEAR NOT NULL
);


SHOW COLUMNS FROM college;
DROP TABLE college; 

/*CREATE DEPARTMENT*/
CREATE TABLE department (
  dept_code CHAR(4) NOT NULL PRIMARY KEY
  ,dept_name VARCHAR(50) NOT NULL
  ,univ_code CHAR(4) NOT NULL
  ,FOREIGN KEY(univ_code) REFERENCES university(univ_code)
);

SHOW COLUMNS FROM department;
DROP TABLE department;

/*CREATE TABLE COLLEGE_DEPARTMENT*/
CREATE TABLE college_department (
  cdept_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  ,udept_code CHAR(4) NOT NULL
  ,FOREIGN KEY(udept_code) REFERENCES department(dept_code)
  ,college_id INT NOT NULL
  ,FOREIGN KEY(college_id) REFERENCES college(id)
);


CREATE TABLE syllabus (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  ,cdept_id INT NOT NULL
  ,FOREIGN KEY(cdept_id) REFERENCES college_department(cdept_id)
  ,syllabus_code CHAR(4) NOT NULL
  ,syllabus_name VARCHAR(100) NOT NULL

);

/*designation table*/
CREATE TABLE designation(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
  ,name VARCHAR(30) NOT NULL
  ,d_rank CHAR(1) NOT NULL /*here rank field is cahnged to d_rank as Rank is a function*/
);

CREATE TABLE employee(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
  ,name VARCHAR(100) NOT NULL
  ,dob DATE NOT NULL
  ,email VARCHAR(50) NOT NULL
  ,phone BIGINT NOT NULL
  ,college_id INT NOT NULL
  ,FOREIGN KEY(college_id) REFERENCES college(id)
  ,cdept_id INT NOT NULL 
  ,FOREIGN KEY(cdept_id) REFERENCES college_department(cdept_id)
  ,desig_id INT 
  ,FOREIGN KEY(desig_id) REFERENCES designation(id)
);



CREATE TABLE professor_syllabus(
  emp_id INT 
  ,FOREIGN KEY (emp_id) REFERENCES employee(id)
  ,syllabus_id INT NOT NULL UNIQUE
  ,FOREIGN KEY (syllabus_id) REFERENCES syllabus(id)
  ,semester TINYINT NOT NULL
);



CREATE TABLE student(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
  ,roll_number CHAR(8) NOT NULL
  ,name VARCHAR(100) NOT NULL
  ,dob DATE NOT NULL
  ,gender CHAR(1) NOT NULL
  ,email VARCHAR(50) NOT NULL
  ,phone BIGINT NOT NULL
  ,address VARCHAR(200) NOT NULL
  ,academic_year YEAR NOT NULL
  ,cdept_id INT NOT NULL
  ,FOREIGN KEY (cdept_id) REFERENCES college_department(cdept_id)
  ,college_id INT NOT NULL
  ,FOREIGN KEY (college_id) REFERENCES college(id)
);


CREATE TABLE semester_fee(
  cdept_id INT NOT NULL
  ,FOREIGN KEY (cdept_id) REFERENCES college_department(cdept_id)
  ,stud_id INT NOT NULL
  ,FOREIGN KEY (stud_id) REFERENCES student(id)
  ,semester TINYINT NOT NULL
  ,amount DOUBLE(18,2) 
  ,paid_year YEAR 
  ,paid_status VARCHAR(10) NOT NULL
);





CREATE TABLE semester_result(
  stud_id INT NOT NULL
  ,FOREIGN KEY(stud_id) REFERENCES student(id)
  ,syllabus_id INT NOT NULL
  ,FOREIGN KEY(syllabus_id) REFERENCES syllabus(id)
  ,semester TINYINT NOT NULL
  ,grade VARCHAR(2) NOT NULL
  ,credits FLOAT NOT NULL
  ,result_date DATE NOT NULL
);


INSERT INTO university(univ_code, university_name)
  VALUES ('UN01', 'Anna University');


INSERT INTO university(univ_code, university_name)
  VALUES ('UN02', 'Birla University');

INSERT INTO college( code, name, univ_code, city, state, year_opened)
  VALUES ('CG01', 'KPR college', 'UN01', 'Coimbatore', 'Tamil Nadu', 2010)
        ,('CG02', 'Eshwar college', 'UN01', 'Coimbatore', 'Tamil Nadu', 2009)
        ,('CG03', 'Sri Sakthi college', 'UN01', 'Coimbatore', 'Tamil Nadu', 2009);


INSERT INTO department(dept_code, dept_name, univ_code)
  VALUES ('DP01','Electronics Engineering','UN01')
        ,('DP02','Computer Science Engineering','UN01');

INSERT INTO college_department(udept_code, college_id)
  VALUES ('DP01', 1)
        ,('DP02', 1)
        ,('DP01', 2)
        ,('DP02', 2);

select * from college_department;

INSERT INTO syllabus(cdept_id, syllabus_code, syllabus_name)
  VALUES (1, 'EC01', 'Electronic Devices')
		,(1, 'EC02', 'Circuit Analysis')
        ,(1, 'EC03', 'Digital Signal Processing')
		,(1, 'EC04', 'Electric Motors')
        ,(1, 'EC05', 'Signals and Systems')
        ,(1, 'EC06', 'Environment Systems')
        ,(2, 'CS01', 'Introduction to Progeamming')
		,(2, 'CS02', 'Learn Java')
        ,(2, 'CS03', 'DBMS')
		,(2, 'CS04', 'Basic Electrics')
        ,(2, 'CS05', 'Mathematics for Data Science')
        ,(2, 'CS06', 'Neural Networks');
        
INSERT INTO syllabus(cdept_id, syllabus_code, syllabus_name)
  VALUES (3, 'EC01', 'Electronic Devices')
		,(3, 'EC02', 'Circuit Analysis')
        ,(3, 'EC03', 'Digital Signal Processing')
		,(3, 'EC04', 'Electric Motors')
        ,(3, 'EC05', 'Signals and Systems')
        ,(3, 'EC06', 'Environment Systems')
        ,(4, 'CS01', 'Introduction to Progeamming')
		,(4, 'CS02', 'Learn Java')
        ,(4, 'CS03', 'DBMS')
		,(4, 'CS04', 'Basic Electrics')
        ,(4, 'CS05', 'Mathematics for Data Science')
        ,(4, 'CS06', 'Neural Networks');
        
        

select * from syllabus;

INSERT INTO designation(name, d_rank)
  VALUES ('HOD', 'a')
        ,('Senior Professor', 'b')
        ,('Assistant Professor', 'c')
        ,('Junior Professor', 'd')
        ,('Lab Assistant', 'e');


INSERT INTO employee(name, dob, email, phone, college_id, cdept_id, desig_id )
  VALUES ( 'Rajesh', '1975-12-09', 'rajesh@gmail.com', 9874563210, 1, 1, 1 )
        ,( 'Ramu', '1970-04-05', 'ramu@gmail.com', 9836521470, 1, 1, 2 )
        ,( 'Kumar', '1980-01-26', 'kumar@gmail.com', 9012345789, 1, 1, 3 )
        ,( 'Senthil', '1985-06-12', 'senthil@gmail.com', 9897765644, 1, 1, 3 )
        ,( 'Steve', '1984-07-30', 'steve@gmail.com', 9632548710, 1, 1, 4 )
        ,( 'Martin', '1983-04-20', 'martin@gmail.com', 9873248452, 1, 1, 5 )
        ,( 'Babu', '1978-09-10', 'babu@gmail.com', 9502134547, 1, 2, 1 )
        ,( 'John', '1973-03-21', 'john@gmail.com', 9879549621, 1, 2, 2 )
        ,( 'Doe', '1979-05-18', 'doe@gmail.com', 9632012365, 1, 2,  3)
        ,( 'Dhanush', '1980-08-06', 'dhanush@gmail.com', 9875455696, 1, 2, 3 )
        ,( 'Vijay', '1982-03-12', 'vijay@gmail.com', 9898989875, 1, 2, 4 )
        ,( 'Adharva', '1980-12-06', 'adharva@gmail.com', 9875389823, 1, 2, 5 );

INSERT INTO professor_syllabus(emp_id, syllabus_id, semester)
  VALUES ( 1, 1, 2 )
        ,( 1, 2, 2 )
        ,( 2, 3, 2 )
        ,( 2, 4, 2 )
        ,( 3, 5, 2 )
        ,( 3, 6, 2 )
        ,( 4, 7, 2 )
        ,( 4, 8, 2 )
        ,( 5, 9, 2 )
        ,( 5, 10, 2 )
        ,( 6, 11, 2 )
        ,( 6, 12, 2 );
        

INSERT INTO student(roll_number, name, dob, gender, email, phone, address, academic_year, cdept_id, college_id)
  VALUES ('KPR10001', 'Arjun', '2000-01-02', 'M', 'arjun@gmail.com', 9879879870, '54-er, abc nagar, covai-641030', 2021, 1, 1 )
        ,('KPR10002', 'Saravanan', '1998-05-03', 'M', 'saravanan@gmail.com', 9323226565, '87/d, raju nagar, kanyakumari-641641', 2021, 1, 1 )
        ,('KPR10003', 'Ram', '1999-07-21', 'M', 'ram34@gmail.com', 8787989852, '65/h, kumarapalayam, tirupur-658658', 2021, 1, 1)
        ,('KPR10004', 'Kumar', '1999-12-13', 'M', 'kumar34@gmail.com', 9121245456, '90, bose nagar, palani-645325', 2020, 1, 1 )
        ,('KPR10005', 'Guru', '2000-09-30', 'M', 'gurugp@gmail.com', 9876546549, '32/a, venkat nagar, coimbatore-641032', 2020, 1, 1)
        ,('KPR10006', 'Vishnu', '2001-01-10', 'M', 'vishnu@gmail.com', 9123321123, '43/n, gandhi street, karur-641980', 2021, 2, 1)
        ,('KPR10007', 'Sarath', '1999-11-15', 'M', 'sarath45@gmail.com', 9870303032, '68/l, kgf nagar, erode-640123', 2021, 2, 1 )
        ,('KPR10008', 'Sam', '1997-06-29', 'M', 'Sam98@gmail.com', 9654532122, '7/b, Subash Street, pondicherry-641325', 2021, 2 , 1)
        ,('KPR10009', 'Akash', '2000-08-28', 'M', 'akash7@gmail.com', 9878998789, '9/a, AGH nagar, theni-641587', 2020, 2, 1)
        ,('KPR10010', 'praveen', '2000-07-19', 'M', 'praveen4@gmail.com', 9871236542, '5/f, Chandran Nagar, coimbatore-641146', 2020, 2, 1)
        ,('KPR10011', 'Sundar', '1999-02-01', 'M', 'sundar@gmail.com', 9879887878, '65-k, efg nagar, Trichy-641330', 2020, 1, 1 )
        ,('KPR10012', 'Manu', '1999-12-13', 'M', 'manu8@gmail.com', 9879826565, '7/e, KYC nagar, Thiruvannamalai-641161', 2020, 1, 1 )
        ,('KPR10013', 'Naresh', '1998-11-30', 'M', 'naresh21@gmail.com', 9514989824, '5/e, k palayam, Mysur-658784', 2021, 1, 1)
        ,('KPR10014', 'Saran', '1998-09-19', 'M', 'saran@gmail.com', 9871245123, '90, LMC nagar, Theni-645002', 2021, 1, 1 )
        ,('KPR10015', 'Mahesh', '1999-03-03', 'M', 'mahesh@gmail.com', 9514546954, '21/c, Sushanth nagar, coimbatore-651320', 2021, 1, 1)
        ,('KPR10016', 'Aravind', '2001-01-16', 'M', 'aravind8@gmail.com', 9456323215, '87/H, Jawaharlal street, karur-641129', 2021, 2, 1)
        ,('KPR10017', 'Sidhyesh', '1999-07-05', 'M', 'sidh@gmail.com', 9120303320, '6/b, DTP nagar, erode-640784', 2021, 2, 1 )
        ,('KPR10018', 'Sai', '1997-08-04', 'M', 'sai45@gmail.com', 9888532777, '7/b, SRM Street, karaikal-641985', 2021, 2 , 1)
        ,('KPR10019', 'Santhosh', '2000-05-05', 'M', 'santhosh03@gmail.com', 9123991239, '78/a, PRK nagar, theni-641587', 2020, 2, 1)
        ,('KPR10020', 'Varun', '2000-09-14', 'M', 'varun@gmail.com', 9456236103, '3/c, Suryan Nagar, coimbatore-641146', 2020, 2, 1 );
truncate table student;
drop table semester_fee; 


INSERT INTO semester_result(stud_id, syllabus_id, semester, grade, credits, Result_date )
  VALUES (1, 1, 6, 'O', 3.0, '2021-01-02' )
        ,(2, 2, 6, 'A+', 3.0, '2021-01-02' )
        ,(3, 3, 6, 'B+' , 2.0, '2021-01-02' )
        ,(4, 4, 6, 'B', 2.0, '2021-01-02' )
        ,(5, 5, 6, 'C+', 3.0, '2021-01-02')
        ,(6, 6, 6, 'A+', 3.0, '2021-01-02')
        ,(7, 1, 6, 'O', 3.0, '2021-01-02' )
        ,(8, 2, 6, 'A+', 3.0, '2021-01-02' )
        ,(9, 3, 6, 'B+' , 2.0, '2021-01-02' )
        ,(10, 4, 6, 'B', 2.0, '2021-01-02' );

ALTER TABLE semester_result 
  ADD COLUMN GPA INT;


SELECT std.name,  sum(res.credits)  FROM student std, semester_result res
  WHERE std.id = res.stud_id;

    
    
    
    
    
/*Question 1*/

CREATE INDEX idx_name ON designation (name);



show indexes from designation;
show indexes from university;

select * from designation where name = "HOD";

ALTER TABLE university ADD INDEX (university_name, univ_code);

CREATE INDEX idx_name ON university (university_name);
CREATE INDEX idx_id ON university (univ_code);


show columns from university;
    
SELECT clg.code AS college_code, 
       clg.name AS college_name, 
	   univ.university_name AS university_name, 
	   clg.city, 
	   clg.state, 
	   clg.year_opened, 
	   dept.dept_name AS department_name, 
	   emp.name AS employee_name, 
	   desig.name AS designation
      FROM university univ
          LEFT JOIN college clg
					ON clg.univ_code = univ.univ_code
		  LEFT JOIN department dept
					ON dept.univ_code = univ.univ_code
		  LEFT JOIN college_department clg_dpt
                    ON clg_dpt.udept_code = dept.dept_code AND clg_dpt.college_id = clg.id
	      LEFT JOIN employee emp
                    ON emp.college_id = clg.id AND emp.cdept_id = clg_dpt.cdept_id
	      INNER JOIN designation desig
                    ON desig.id = emp.desig_id AND desig.name = 'HOD';

SELECT clg.code , 
       clg.name AS college_name, 
	   univ.university_name , 
	   clg.city, clg.state, 
	   clg.year_opened, 
	   dept.dept_name, 
	   emp.name
  FROM college clg, 
       niversity univ, 
	   department dept, 
	   employee emp, 
	   college_department, 
	   designation dsg
   WHERE clg.univ_code = univ.univ_code 
    AND college_department.udept_code = dept.dept_code 
      AND college_department.college_id = clg.id 
        AND dsg.name = 'HOD'
          AND emp.desig_id = dsg.id
            AND emp.college_id = clg.id
              AND emp.cdept_id = college_department.cdept_id;


/*Question 2*/

CREATE INDEX idx_name ON college (city);
CREATE INDEX idx_academic_year ON student (academic_year);
CREATE INDEX idx_dept_name ON department (dept_name);
CREATE INDEX idx_dept_code ON department (dept_code);
CREATE INDEX idx_udept_code ON college_department (udept_code);


SELECT std.roll_number, 
       std.name, 
	   std.gender, 
	   std.academic_year, 
	   std.dob, 
	   std.email, 
	   std.phone, 
	   std.address, 
	   clg.name, 
	   dept.dept_name
  FROM student std
    LEFT JOIN college clg
      ON std.college_id = clg.id 
	    AND clg.city = 'coimbatore' 
		AND std.academic_year = 2020
	LEFT JOIN college_department clg_dpt
      USING (cdept_id)
	LEFT JOIN department dept
      ON clg_dpt.udept_code = dept.dept_code
	INNER JOIN university univ
      ON univ.university_name = 'Anna University' AND univ.univ_code = clg.univ_code;




/*QUESTION 3*/


CREATE INDEX idx_college_id ON student (college_id);


SELECT std.roll_number, 
       std.name, 
	   std.gender, 
	   std.dob, 
	   std.email, 
	   std.phone, 
	   std.address, 
       clg.name, dept.dept_name
    FROM student std
      LEFT JOIN college clg 
        ON clg.id = std.college_id
	  LEFT JOIN college_department clg_dpt
        ON clg.id = clg_dpt.college_id AND std.cdept_id = clg_dpt.cdept_id
	  LEFT JOIN department dept
        ON clg_dpt.udept_code = dept.dept_code;

SELECT std.id, 
       std.name, 
	   clg.name, 
	   dept.dept_name FROM student std, 
	   college clg, 
	   department dept, 
	   college_department clg_dpt
  WHERE std.college_id = clg.id 
    AND clg_dpt.cdept_id = std.cdept_id 
      AND dept.dept_code = clg_dpt.udept_code;

/*Question 4*/


CREATE INDEX idx_id ON employee (id);
CREATE INDEX idx_dept_code ON department (dept_code);

drop index idx_dept_doce on department;

SELECT emp.name, 
       clg.name, 
	   dept.dept_name, 
	   desig.name FROM employee emp
  LEFT JOIN college clg
    ON emp.college_id = clg.id
  LEFT JOIN college_department clg_dpt
    using (cdept_id)
  LEFT JOIN department dept
    ON dept.dept_code = clg_dpt.udept_code
  LEFT JOIN university univ
    ON  clg.univ_code =  univ.univ_code AND univ.university_name = "Anna University"
  LEFT JOIN designation desig
    ON desig.id = emp.desig_id
  ORDER BY clg.name, desig.d_rank;
    
    


/*Question 5*/

UPDATE semester_result
  SET GPA = 
    (CASE stud_id WHEN 1 THEN 8
                  WHEN 3 THEN 10
                  WHEN 5 THEN 9
                  WHEN 7 THEN 8
                  WHEN 9 THEN 7
                  WHEN 2 THEN 9
                  WHEN 4 THEN 10
                  WHEN 6 THEN 9
                  WHEN 8 THEN 8
                  WHEN 10 THEN 6
    END)
    WHERE stud_id in (1,3,5,7,9,2,4,6,8,10);
    

use college;

SELECT std.name AS student_name, 
       std.roll_number, 
	   clg.name AS college_name, 
	   sem_res.credits, 
	   sem_res.grade, 
	   sem_res.GPA
  FROM student std, semester_result sem_res, college clg
    WHERE std.id = sem_res.stud_id
      AND std.college_id = clg.id;

SELECT std.name AS student_name, 
       std.roll_number, 
	   clg.name AS college_name, 
	   sem_res.credits, 
	   sem_res.grade, 
	   sem_res.GPA
  FROM student std
      INNER JOIN semester_result sem_res
                ON sem_res.stud_id = std.id
	  INNER JOIN college clg
                ON clg.id = std.college_id;



/*Question 6*/

 INSERT INTO semester_fee(cdept_id, stud_id, semester, amount, paid_year, paid_status )
  VALUES (1, 1, 6, 50000, NULL, 'Unpaid')
		,(1, 2, 6, 60000, NULL , 'Unpaid')
        ,(1, 3, 6, 70500, NULL, 'Unpaid')
        ,(1, 4, 8, 75000, NULL , 'Unpaid')
        ,(1, 5, 8, 89000, NULL, 'Unpaid')
        ,(1, 11, 8, 45000, NULL , 'Unpaid')
        ,(1, 12, 8, 47000, NULL, 'Unpaid')
        ,(1, 13, 6, 52000, NULL, 'Unpaid')
        ,(1, 14, 6, 65000, NULL, 'Unpaid')
        ,(1, 15, 6, 62000, NULL , 'Unpaid')
        ,(2, 6, 6, 55000, NULL, 'Unpaid')
		,(2, 7, 6, 65000, NULL , 'Unpaid')
        ,(2, 8, 6, 40500, NULL, 'Unpaid')
        ,(2, 9, 8, 58000, NULL , 'Unpaid')
        ,(2, 10, 8, 23000, NULL, 'Unpaid')
        ,(2, 16, 6, 90000, NULL , 'Unpaid')
        ,(2, 17, 6, 75000, NULL, 'Unpaid')
        ,(2, 18, 6, 46000, NULL, 'Unpaid')
        ,(2, 19, 8, 76000, NULL, 'Unpaid')
        ,(2, 20, 8, 58000, NULL , 'Unpaid');

select sum(fee.amount) from semester_fee fee 
where fee.paid_status = "Paid" and fee.semester = 8;

select sum(fee.amount) from semester_fee fee 
where fee.paid_status = "Unpaid" and fee.semester = 8;


/*Question 7*/

UPDATE semester_fee
  SET paid_year = 2016
    WHERE stud_id = 3;
UPDATE semester_fee
  SET paid_status = 'Paid'
    WHERE stud_id = 3;




UPDATE semester_fee 
  SET paid_year = 
    (CASE stud_id WHEN 5 THEN 2018 
                  WHEN 7 THEN 2016
                  WHEN 9 THEN 2018
                  WHEN 19 THEN 2017
                  WHEN 20 THEN 2016
    END)
    , paid_status = 'Paid'
      WHERE stud_id in (5, 7, 9, 19, 20 );


   
 /*Question 8*/
 select * from professor_syllabus;
 
select desig.name, 
       desig.d_rank, 
	   clg.name, 
	   dept.dept_name, 
	   univ.university_name
       FROM designation desig, 
	        university univ, 
			department dept, 
			college_department clg_dpt, 
			college clg, 
			employee emp
         WHERE dept.univ_code = univ.univ_code
           AND clg_dpt.udept_code = dept.dept_code
             AND clg_dpt.college_id = clg.id
               AND emp.cdept_id = clg_dpt.cdept_id
                 AND emp.college_id IS NULL;
 
 
SELECT desig.name AS designation, 
       desig.d_rank AS designation_rank, 
	   clg.name AS college_name, 
	   dept.dept_name AS department_name, 
	   univ.university_name
       FROM department dept
           INNER JOIN university univ 
                     ON univ.univ_code = dept.univ_code
		   INNER JOIN college_department clg_dpt 
                     ON  clg_dpt.udept_code = dept.dept_code
		   INNER JOIN college clg 
                     ON clg.id = clg_dpt.college_id
		   INNER JOIN employee emp 
                     ON emp.cdept_id = clg_dpt.cdept_id
                       AND emp.college_id IS NULL
			INNER JOIN designation desig
					 ON emp.desig_id = desig.id;
 

 
 

/*Question 9*/
SELECT  clg.name AS college_name, 
        fee.semester, 
		sum(fee.amount) FROM student std
        INNER JOIN college clg
          ON clg.id = std.college_id
	    INNER JOIN semester_fee fee
          ON fee.paid_status = "Paid" AND std.id = fee.stud_id
        GROUP BY fee.semester;

SELECT  clg.name, 
        fee.semester, 
		sum(fee.amount) FROM student std
        INNER JOIN college clg
          ON clg.id = std.college_id
	    INNER JOIN semester_fee fee
          ON fee.paid_status = "Unpaid" AND std.id = fee.stud_id
        GROUP BY fee.semester;
        
        
/*	QUESTION 10*/

SELECT std.name, 
       std.roll_number, 
	   clg.name, 
	   sem_res.credits, 
	   sem_res.grade, 
	   sem_res.GPA
  FROM student std, 
       semester_result sem_res, 
	   college clg
    WHERE std.id = sem_res.stud_id
      AND std.college_id = clg.id
        AND sem_res.GPA > 8;
SELECT std.name, 
       std.roll_number, 
	   clg.name, 
	   sem_res.credits, 
	   sem_res.grade, 
	   sem_res.GPA
      FROM student std
          INNER JOIN semester_result sem_res
                    ON sem_res.stud_id = std.id 
                      AND sem_res.GPA > 8
		  INNER JOIN college clg 
					ON clg.id = std.college_id;
      
        
SELECT std.name, 
       std.roll_number, 
	   clg.name, 
	   sem_res.credits, 
	   sem_res.grade, 
	   sem_res.GPA
  FROM student std, semester_result sem_res, college clg
    WHERE std.id = sem_res.stud_id
      AND std.college_id = clg.id
        AND sem_res.GPA > 5;

SELECT std.name, 
       std.roll_number, 
	   clg.name, 
	   sem_res.credits, 
	   m_res.grade, 
	   sem_res.GPA
  FROM student std, semester_result sem_res, college clg
    WHERE std.id = sem_res.stud_id
      AND std.college_id = clg.id
        AND sem_res.GPA > 5
          AND sem_res.GPA <= 8;
        
        
        
        
        
        