/*Exercis 3 question 10*/



SELECT std.name AS student_name, 
       std.roll_number, 
	   clg.name AS college_name, 
	   sem_res.credits, 
	   sem_res.grade, 
	   sem_res.GPA
      FROM student std
          INNER JOIN semester_result sem_res
                    ON sem_res.stud_id = std.id 
                      AND sem_res.GPA > 5
		  LEFT JOIN college clg 
					ON clg.id = std.college_id;