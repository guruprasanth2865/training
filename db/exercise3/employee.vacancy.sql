/*Exeercise 3 question 8*/


SELECT desig.name AS designation, 
       desig.d_rank AS designation_rank, 
	   clg.name AS college_name, 
	   dept.dept_name AS department_name, 
	   univ.university_name
       FROM department dept
           LEFT JOIN university univ 
                     ON univ.univ_code = dept.univ_code
		   LEFT JOIN college_department clg_dpt 
                     ON  clg_dpt.udept_code = dept.dept_code
		   LEFT JOIN college clg 
                     ON clg.id = clg_dpt.college_id
		   LEFT JOIN employee emp 
                     ON emp.cdept_id = clg_dpt.cdept_id
                       AND emp.college_id IS NULL
		   INNER JOIN designation desig
					 ON emp.desig_id = desig.id;
 
