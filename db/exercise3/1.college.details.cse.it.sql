/*Exercis 3 - Question 1*/

/*Use database college*/
USE college;

SELECT clg.code AS college_code, 
       clg.name AS college_name, 
	   univ.university_name AS university_name, 
	   clg.city, 
	   clg.state, 
	   clg.year_opened, 
	   dept.dept_name AS department_name, 
	   emp.name AS employee_name, 
	   desig.name AS designation
      FROM university univ
          LEFT JOIN college clg
					ON clg.univ_code = univ.univ_code
		  LEFT JOIN department dept
					ON dept.univ_code = univ.univ_code
		  LEFT JOIN college_department clg_dpt
                    ON clg_dpt.udept_code = dept.dept_code 
                      AND clg_dpt.college_id = clg.id
	      LEFT JOIN employee emp
                    ON emp.college_id = clg.id 
                      AND emp.cdept_id = clg_dpt.cdept_id
	      INNER JOIN designation desig
                    ON desig.id = emp.desig_id 
                      AND desig.name = 'HOD';
