/*  Exercis 3 - question 7 */

UPDATE semester_fee 
     SET paid_year = 
       (CASE stud_id WHEN 5 THEN 2018 
                  WHEN 7 THEN 2016
                  WHEN 9 THEN 2018
                  WHEN 19 THEN 2017
                  WHEN 20 THEN 2016
       END)
       ,paid_status = 'Paid'
           WHERE stud_id in (5, 7, 9, 19, 20 );
