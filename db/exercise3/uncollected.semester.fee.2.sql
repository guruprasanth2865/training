SELECT clg.name, 
	   fee.semester, 
       sum(fee.amount) 
       FROM student std
           LEFT JOIN college clg
                     ON clg.id = std.college_id
	       LEFT JOIN semester_fee fee
                     ON fee.paid_status = "Unpaid" AND std.id = fee.stud_id
           GROUP BY fee.semester;
        