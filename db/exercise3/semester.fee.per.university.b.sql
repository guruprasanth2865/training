/* Exercise 3 - Question 9*/

SELECT univ.univ_code
      ,univ.university_name
      ,sum(sem_fee.amount) As collected_fees
      ,sem_fee.paid_year
      FROM university univ
          INNER JOIN college clg
                    ON univ.univ_code = clg.univ_code
          INNER JOIN student std
                    ON clg.id = std.college_id 
          INNER JOIN semester_fee sem_fee
                    ON std.id = sem_fee.stud_id
          WHERE paid_status = 'Paid'
              AND paid_year = '2020'
                 GROUP BY univ.univ_code
                         ,univ.university_name;