/*Exercise 3 - Question 9*/

SELECT clg.name AS college_name, 
	   fee.semester, 
	   sum(fee.amount) AS total_paid_amount 
	  FROM student std
          LEFT JOIN college clg
                    ON clg.id = std.college_id
	      LEFT JOIN semester_fee fee
                    ON fee.paid_status = "Paid" AND std.id = fee.stud_id
		  GROUP BY fee.semester;