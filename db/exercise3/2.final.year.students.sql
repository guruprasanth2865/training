/*Exercise 3 - Question 2*/
/*Select final year students (Assuming that universities have engineering deps only)
 details who are Studying under a particular university and selected cities alone*/

SELECT std.roll_number, 
       std.name AS student_name, 
	   std.gender, 
	   std.academic_year, 
	   std.dob AS date_of_birth, 
	   std.email, 
	   std.phone, 
	   std.address, 
	   clg.name AS college_name, 
	   dept.dept_name AS department_name
       FROM student std
           LEFT JOIN college clg
                     ON std.college_id = clg.id 
	                   AND clg.city = 'coimbatore' 
						  AND std.academic_year = 2020
	       LEFT JOIN college_department clg_dpt
                     USING (cdept_id)
		   LEFT JOIN department dept
                     ON clg_dpt.udept_code = dept.dept_code
	       INNER JOIN university univ
                     ON univ.university_name = 'Anna University' AND univ.univ_code = clg.univ_code;

