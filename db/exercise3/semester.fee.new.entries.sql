/*Exercis 3 - question 6*/

 INSERT INTO 
            semester_fee ( cdept_id, 
                           stud_id, semester, 
                           amount, 
                           paid_year, 
                           paid_status )
			VALUES (1, 1, 6, 50000, NULL, 'Unpaid')
		          ,(1, 2, 6, 60000, NULL , 'Unpaid')
                  ,(1, 3, 6, 70500, NULL, 'Unpaid')
                  ,(1, 4, 8, 75000, NULL , 'Unpaid')
                  ,(1, 5, 8, 89000, NULL, 'Unpaid')
                  ,(1, 11, 8, 45000, NULL , 'Unpaid')
                  ,(1, 12, 8, 47000, NULL, 'Unpaid')
                  ,(1, 13, 6, 52000, NULL, 'Unpaid')
                  ,(1, 14, 6, 65000, NULL, 'Unpaid')
                  ,(1, 15, 6, 62000, NULL , 'Unpaid')
                  ,(2, 6, 6, 55000, NULL, 'Unpaid')
				  ,(2, 7, 6, 65000, NULL , 'Unpaid')
                  ,(2, 8, 6, 40500, NULL, 'Unpaid')
                  ,(2, 9, 8, 58000, NULL , 'Unpaid')
                  ,(2, 10, 8, 23000, NULL, 'Unpaid')
                  ,(2, 16, 6, 90000, NULL , 'Unpaid')
                  ,(2, 17, 6, 75000, NULL, 'Unpaid')
                  ,(2, 18, 6, 46000, NULL, 'Unpaid')
                  ,(2, 19, 8, 76000, NULL, 'Unpaid')
                  ,(2, 20, 8, 58000, NULL , 'Unpaid');
