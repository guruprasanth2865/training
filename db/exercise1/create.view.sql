       
/*Question 14*/
/*Create view*/
CREATE VIEW manage_order AS
SELECT ord.id AS order_id, usr.id AS user_id, usr.name AS user_name, prd.id AS product_id, prd.name AS product_name, prd.price
  FROM product_order ord 
    INNER JOIN user usr
      ON ord.user_id = usr.id
	INNER JOIN product prd
      ON prd.id = ord.product_id;


SELECT order_id, user_id, user_name, product_id, product_name, price FROM manage_order
         