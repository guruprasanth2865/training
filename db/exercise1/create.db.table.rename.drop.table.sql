/*Question 1*/
/*Create database*/
CREATE DATABASE ecommerce;
/*select database*/
USE ecommerce;
/*Create Table*/
CREATE TABLE user(
  id INT NOT NULL AUTO_INCREMENT
  ,PRIMARY KEY(id)
  ,name VARCHAR(25) NOT NULL
  ,mobile_no INT NOT NULL
  ,address VARCHAR(100) NOT NULL
);
/*Alter table name*/
ALTER TABLE user RENAME TO userdata;
/*drop table*/
DROP TABLE userdata;