/* QUESTION 2*/
CREATE TABLE user(
  id INT NOT NULL AUTO_INCREMENT
  ,PRIMARY KEY(id)
  ,name VARCHAR(25) NOT NULL
  ,mobile_no INT NOT NULL
  ,address VARCHAR(100) NOT NULL
);

/*create column*/
ALTER TABLE user
  ADD age INT NOT NULL;
  
SHOW COLUMNS FROM user;

/*MODIFY COLUMN FROM TABLE*/
ALTER TABLE user 
   MODIFY age VARCHAR(5) NOT NULL;
  
SHOW COLUMNS FROM user;

/*RENAME - Change column name */
ALTER TABLE user 
  CHANGE COLUMN age user_age INT NOT NULL;
  
SHOW COLUMNS FROM user;

/*DROP column from table*/
ALTER TABLE user 
  DROP COLUMN user_age;
  
SHOW COLUMNS FROM user;