 
/*QUESTION 9 FILTER RECORDS*/
SELECT * FROM product;

INSERT INTO product(name, detail, price)
  VALUES ("t-shirt", " A nike branded t-shirt", 1000)
        ,("bag", "American tourister bag", 5050)
        ,("chair", "nice polymer material", 1500)
        ,("tablet", "A samsung tablet", 20000)
        ,("Refrigirator", "A whirlpool Refrigirator", 25000)
        ,("AC", "A samsung AC", 45000);
        
        
/*OR*/
SELECT id, name, detail, price FROM product
  WHERE price < 5000 OR price > 50000;
  
/*AND*/
SELECT id, name, detail, price FROM product 
  WHERE price <10000 AND price >1000;
  
/*LIKE*/
SELECT id, name, detail, price FROM product 
  WHERE name LIKE '%et%';
  
/*IN*/
SELECT id, name, detail, price FROM product
  WHERE name IN ("tablet");
  
/*NOT */
SELECT id, name, detail, price FROM product 
  WHERE name NOT LIKE '%et%';
  
  
CREATE TABLE product_order(
  id  INT NOT NULL AUTO_INCREMENT
  ,PRIMARY KEY (id)
  ,product_id INT NOT NULL
  ,FOREIGN KEY (product_id) REFERENCES product(id)
  ,user_id INT NOT NULL
  ,FOREIGN KEY (user_id) REFERENCES user(id)
);

SHOW COLUMNS FROM product_order;

INSERT INTO product_order(product_id, user_id)
  VALUES (3, 7)
        ,(1, 3)
        ,(5, 2)
        ,(4, 1)
        ,(3, 9)
        ,(3, 2);

/*ANY*/        
SELECT prod.id, prod.name 
  FROM product prod
    WHERE prod.id = ANY ( 
      SELECT product_id 
        FROM product_order 
          GROUP BY product_id
            HAVING COUNT(id) > 0
	);
