    
/*QUESTION 12 */
/*Join 3 tables */

/*Inner join*/
SELECT ord.id, usr.id, usr.name, prd.id, prd.name, prd.price
  FROM product_order ord 
    INNER JOIN user usr
      ON ord.user_id = usr.id
	INNER JOIN product prd
      ON prd.id = ord.product_id;

/*LEFT*/
SELECT usr.id, usr.name, ord.id AS order_id, prd.name
  FROM user usr 
    LEFT JOIN product_order ord
      ON ord.user_id = usr.id
	LEFT JOIN product prd
      ON ord.product_id = prd.id;
	
/*RIGHT*/
SELECT usr.id, usr.name, ord.id AS order_id, prd.name AS product_name
  FROM user usr 
    RIGHT JOIN product_order ord
      ON ord.user_id = usr.id
	RIGHT JOIN product prd
      ON ord.product_id = prd.id;
      
/*CROSS*/
SELECT usr.id, usr.name, ord.id AS order_id, prd.name AS product_name
  FROM user usr 
    CROSS JOIN product_order ord
      ON ord.user_id = usr.id
	CROSS JOIN product prd
      ON ord.product_id = prd.id;
      
/*INNER*/
SELECT usr.id, usr.name, ord.id AS order_id, prd.name AS product_name
  FROM user usr 
    INNER JOIN product_order ord
      ON ord.user_id = usr.id
	INNER JOIN product prd
      ON ord.product_id = prd.id;
      
