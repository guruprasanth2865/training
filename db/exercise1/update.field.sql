
/*QUESTION 6 UPDATE FIELD VALUES*/

INSERT INTO PRODUCT(name, detail, price)
  VALUES ("Samsung s20", "Nice mobile", 55000)
		,("HP i3 laptop", "quality laptop with good specs", 35000)
        ,("Sandisk Pen drive", "A 16gb pendrive", 600)
        ,("Sony headset", "A bass head set", 6500)
        ,("LG TV", " A premium T.V", 40000);
        
SELECT * FROM product;

ALTER TABLE product 
  MODIFY price FLOAT NOT NULL;

SHOW COLUMNS FROM product;

/*update with while*/
UPDATE product 
  SET price = 5000
    WHERE id = 2;
  
UPDATE product 
  SET price = 5000;
  
  