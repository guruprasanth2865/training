/*Question 1*/
/*Create database*/
CREATE DATABASE ecommerce;
/*select database*/
USE ecommerce;
/*Create Table*/
CREATE TABLE user(
  id INT NOT NULL AUTO_INCREMENT
  ,PRIMARY KEY(id)
  ,name VARCHAR(25) NOT NULL
  ,mobile_no INT NOT NULL
  ,address VARCHAR(100) NOT NULL
);
/*Alter table name*/
ALTER TABLE user RENAME TO userdata;
/*drop table*/
DROP TABLE userdata;


/* QUESTION 2*/
CREATE TABLE user(
  id INT NOT NULL AUTO_INCREMENT
  ,PRIMARY KEY(id)
  ,name VARCHAR(25) NOT NULL
  ,mobile_no INT NOT NULL
  ,address VARCHAR(100) NOT NULL
);

/*create column*/
ALTER TABLE user
  ADD age INT NOT NULL;
  
SHOW COLUMNS FROM user;

/*MODIFY COLUMN FROM TABLE*/
ALTER TABLE user 
   MODIFY age VARCHAR(5) NOT NULL;
  
SHOW COLUMNS FROM user;

/*RENAME - Change column name */
ALTER TABLE user 
  CHANGE COLUMN age user_age INT NOT NULL;
  
SHOW COLUMNS FROM user;

/*DROP column from table*/
ALTER TABLE user 
  DROP COLUMN user_age;
  
SHOW COLUMNS FROM user;

/*QUESTION 3  ---  USE CONSTRAINS*/
DROP TABLE user;

CREATE TABLE product(
  id INT NOT NULL AUTO_INCREMENT
  ,PRIMARY KEY(id)
  ,name VARCHAR(25) NOT NULL
  ,detail VARCHAR(100) 
  ,price INT NOT NULL
);

CREATE TABLE user(
	id INT NOT NULL AUTO_INCREMENT
    ,PRIMARY KEY(id)
    ,name VARCHAR(25) NOT NULL
    ,product_count INT DEFAULT 0
    ,age INT NOT NULL CHECK (age>=18)
    ,address VARCHAR(100)
);

CREATE TABLE product_order (
	id INT NOT NULL AUTO_INCREMENT
    ,PRIMARY KEY(id)
    ,product_id INT NOT NULL
    ,FOREIGN KEY(product_id) REFERENCES product(id)
    ,user_id INT NOT NULL
    ,FOREIGN KEY(user_id) REFERENCES user(id)
);


/*QUESTION 4*/

DROP TABLE product_order;

INSERT INTO user(name, age, address) 
  VALUES ('GURU', 19, '2-b, bose nagar, covai-641050')
		,('Prasanth', 20, '44-h, voc nagar, coimbatore-641120');
        
SELECT id, name,product_count, age, address FROM user;

SHOW COLUMNS FROM user;
  
TRUNCATE TABLE user;

SELECT id, name,product_count, age, address FROM user;


/*QUESTION 5 --- INSERT RECORDS*/
/*Insert record into Tables*/
INSERT INTO user(name, age, address) 
  VALUES ('GURU', 19, '2-b, bose nagar, covai-641050')
		,('Prasanth', 20, '44-h, voc nagar, coimbatore-641120')
        ,('Vishnu', 21, '855-i, ak-tp nagar, selam-641120')
        ,('Sarath', 25, '412-j, voc nagar, coimbatore-641120')
        ,('Sam', 80, '87-b, ABC street, Madurai-600658')
        ,('Naveen', 22, '76, Race Course, coimbatore-541120')
        ,('Akash', 35, '45-w, 100ft road, coimbatore-641240')
        ,('Ram', 30, '189-m, Gandhi nagar, Tirupur-647850')
        ,('Kumar', 41, '70, ram street, coimbatore-646464')
        ,('Samyu', 20, '21-g, SG nagar, coimbatore-641120');
        
SELECT id, name, product_count, age, address FROM user;


/*QUESTION 6 UPDATE FIELD VALUES*/

INSERT INTO PRODUCT(name, detail, price)
  VALUES ("Samsung s20", "Nice mobile", 55000)
		,("HP i3 laptop", "quality laptop with good specs", 35000)
        ,("Sandisk Pen drive", "A 16gb pendrive", 600)
        ,("Sony headset", "A bass head set", 6500)
        ,("LG TV", " A premium T.V", 40000);
        
SELECT * FROM product;

ALTER TABLE product 
  MODIFY price FLOAT NOT NULL;

SHOW COLUMNS FROM product;

/*update with while*/
UPDATE product 
  SET price = 5000
    WHERE id = 2;
  
UPDATE product 
  SET price = 5000;
  
  
/*QUESTION 7 DELETE */
/*DELETE WITH WHILE*/
DELETE FROM product
  WHERE id = 2;
  
SELECT * FROM product;

/*QUESTION 8 FETCH RECORD*/
/*fetch all data*/
SELECT * FROM product;

SELECT id, name, detail, price FROM product;

/*fetch particular record*/
SELECT id, name, detail, price FROM product
  WHERE id=4;
  
SELECT name FROM  product;
  
/*QUESTION 9 FILTER RECORDS*/
SELECT * FROM product;

INSERT INTO product(name, detail, price)
  VALUES ("t-shirt", " A nike branded t-shirt", 1000)
        ,("bag", "American tourister bag", 5050)
        ,("chair", "nice polymer material", 1500)
        ,("tablet", "A samsung tablet", 20000)
        ,("Refrigirator", "A whirlpool Refrigirator", 25000)
        ,("AC", "A samsung AC", 45000);
        
        
/*OR*/
SELECT id, name, detail, price FROM product
  WHERE price < 5000 OR price > 50000;
  
/*AND*/
SELECT id, name, detail, price FROM product 
  WHERE price <10000 AND price >1000;
  
/*LIKE*/
SELECT id, name, detail, price FROM product 
  WHERE name LIKE '%et%';
  
/*IN*/
SELECT id, name, detail, price FROM product
  WHERE name IN ("tablet");
  
/*NOT */
SELECT id, name, detail, price FROM product 
  WHERE name NOT LIKE '%et%';
  
  
CREATE TABLE product_order(
  id  INT NOT NULL AUTO_INCREMENT
  ,PRIMARY KEY (id)
  ,product_id INT NOT NULL
  ,FOREIGN KEY (product_id) REFERENCES product(id)
  ,user_id INT NOT NULL
  ,FOREIGN KEY (user_id) REFERENCES user(id)
);

SHOW COLUMNS FROM product_order;

INSERT INTO product_order(product_id, user_id)
  VALUES (3, 7)
        ,(1, 3)
        ,(5, 2)
        ,(4, 1)
        ,(3, 9)
        ,(3, 2);

/*ANY*/        
SELECT prod.id, prod.name 
  FROM product prod
    WHERE prod.id = ANY ( 
      SELECT product_id 
        FROM product_order 
          GROUP BY product_id
            HAVING COUNT(id) > 0
	);

/*QUESTION 10*/
/*MAX*/
SELECT MAX(price) FROM product;

/*MIN*/
SELECT MIN(price) AS 'Minimum Price' FROM product
  WHERE price > 6000;
  
/*AVG*/
SELECT AVG(price) AS 'Average Price' FROM product;

/*COUNT*/
SELECT COUNT(DISTINCT(user_id)) as 'Regular customers COUNT' FROM product_order;

/*SUM*/	
SELECT SUM(price) FROM  product;


/*FIRST*/
SELECT id, name FROM user 
  ORDER BY 	name ASC LIMIT 2;
  
/*LAST*/
SELECT id, name FROM user 
  ORDER BY name DESC LIMIT 3;

/*date now*/
ALTER TABLE produCt_order 
  ADD order_date DATETIME DEFAULT now();

SELECT * FROM product_order;

/*QUESTION 11*/
/*RECORDS of Two tables are read by using FOREIGN KEY*/

SELECT usr.id, usr.name FROM user usr
  WHERE usr.id IN (
    SELECT ord.user_id FROM product_order ord 
    );
    
/*QUESTION 12 */
/*Join 3 tables */

/*Inner join*/
SELECT ord.id, usr.id, usr.name, prd.id, prd.name, prd.price
  FROM product_order ord 
    INNER JOIN user usr
      ON ord.user_id = usr.id
	INNER JOIN product prd
      ON prd.id = ord.product_id;

/*LEFT*/
SELECT usr.id, usr.name, ord.id AS order_id, prd.name
  FROM user usr 
    LEFT JOIN product_order ord
      ON ord.user_id = usr.id
	LEFT JOIN product prd
      ON ord.product_id = prd.id;
	
/*RIGHT*/
SELECT usr.id, usr.name, ord.id AS order_id, prd.name AS product_name
  FROM user usr 
    RIGHT JOIN product_order ord
      ON ord.user_id = usr.id
	RIGHT JOIN product prd
      ON ord.product_id = prd.id;
      
/*CROSS*/
SELECT usr.id, usr.name, ord.id AS order_id, prd.name AS product_name
  FROM user usr 
    CROSS JOIN product_order ord
      ON ord.user_id = usr.id
	CROSS JOIN product prd
      ON ord.product_id = prd.id;
      
/*INNER*/
SELECT usr.id, usr.name, ord.id AS order_id, prd.name AS product_name
  FROM user usr 
    INNER JOIN product_order ord
      ON ord.user_id = usr.id
	INNER JOIN product prd
      ON ord.product_id = prd.id;
      
      
/*question 13*/
CREATE TABLE new_product(
  id INT NOT NULL AUTO_INCREMENT
  ,PRIMARY KEY(id)
  ,name VARCHAR(25) NOT NULL
  ,detail VARCHAR(100) 
  ,price FLOAT NOT NULL
);

CREATE TABLE old_product(
  id INT NOT NULL AUTO_INCREMENT
  ,PRIMARY KEY(id)
  ,name VARCHAR(25) NOT NULL
  ,detail VARCHAR(100) 
  ,price FLOAT NOT NULL
);

INSERT INTO old_product(name, detail, price)
  VALUES ("fan", "bajaj fan. Nice product", 5000)
		,("book", "nice book written by R.K.Nrayanan", 500)
        ,("Tea pot" , "ceramic product", 3500)
        ,("Speaker", "A BOSS product", 2750)
        ,("sofa", "Sofa from wood pecker", 65000);
        
INSERT INTO new_product(name, detail, price)
  VALUES ("Grinder", "bajaj grinder. Nice product", 4500)
		,("Camera", "A premium quality camera", 49000)
        ,("Vegetable Cutter" , "sharp Cutter", 3500)
        ,("Monitor", "Crystal clear monitor", 27500)
        ,("gas stove", "ISO certified gas stove", 7500);
        
/*QUESTION 13 Union 2 Different table with same Column name (use Distinct,ALL)*/

/*ALL*/
SELECT name , detail , price 
  FROM new_product
    UNION ALL
      SELECT name , detail , price 
        FROM old_product;


/*DISTINCT*/
INSERT INTO old_product(name, detail, price)
  VALUES ("Grinder", "bajaj grinder. Nice product", 4500);
  
SELECT name, detail, price 
  FROM new_product
    UNION DISTINCT
      SELECT name, detail, price
       FROM old_product;
         
/*Question 14*/
/*Create view*/
CREATE VIEW manage_order AS
SELECT ord.id AS order_id, usr.id AS user_id, usr.name AS user_name, prd.id AS product_id, prd.name AS product_name, prd.price
  FROM product_order ord 
    INNER JOIN user usr
      ON ord.user_id = usr.id
	INNER JOIN product prd
      ON prd.id = ord.product_id;


SELECT order_id, user_id, user_name, product_id, product_name, price FROM manage_order
         