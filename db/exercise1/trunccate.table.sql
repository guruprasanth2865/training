
/*QUESTION 4*/

DROP TABLE product_order;

INSERT INTO user(name, age, address) 
  VALUES ('GURU', 19, '2-b, bose nagar, covai-641050')
		,('Prasanth', 20, '44-h, voc nagar, coimbatore-641120');
        
SELECT id, name,product_count, age, address FROM user;

SHOW COLUMNS FROM user;
  
TRUNCATE TABLE user;

SELECT id, name,product_count, age, address FROM user;

