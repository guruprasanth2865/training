/*QUESTION 3  ---  USE CONSTRAINS*/
DROP TABLE user;

CREATE TABLE product(
  id INT NOT NULL AUTO_INCREMENT
  ,PRIMARY KEY(id)
  ,name VARCHAR(25) NOT NULL
  ,detail VARCHAR(100) 
  ,price INT NOT NULL
);

CREATE TABLE user(
	id INT NOT NULL AUTO_INCREMENT
    ,PRIMARY KEY(id)
    ,name VARCHAR(25) NOT NULL
    ,product_count INT DEFAULT 0
    ,age INT NOT NULL CHECK (age>=18)
    ,address VARCHAR(100)
);

CREATE TABLE product_order (
	id INT NOT NULL AUTO_INCREMENT
    ,PRIMARY KEY(id)
    ,product_id INT NOT NULL
    ,FOREIGN KEY(product_id) REFERENCES product(id)
    ,user_id INT NOT NULL
    ,FOREIGN KEY(user_id) REFERENCES user(id)
);