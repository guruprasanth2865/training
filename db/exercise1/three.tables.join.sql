/*question 13*/
CREATE TABLE new_product(
  id INT NOT NULL AUTO_INCREMENT
  ,PRIMARY KEY(id)
  ,name VARCHAR(25) NOT NULL
  ,detail VARCHAR(100) 
  ,price FLOAT NOT NULL
);

CREATE TABLE old_product(
  id INT NOT NULL AUTO_INCREMENT
  ,PRIMARY KEY(id)
  ,name VARCHAR(25) NOT NULL
  ,detail VARCHAR(100) 
  ,price FLOAT NOT NULL
);

INSERT INTO old_product(name, detail, price)
  VALUES ("fan", "bajaj fan. Nice product", 5000)
		,("book", "nice book written by R.K.Nrayanan", 500)
        ,("Tea pot" , "ceramic product", 3500)
        ,("Speaker", "A BOSS product", 2750)
        ,("sofa", "Sofa from wood pecker", 65000);
        
INSERT INTO new_product(name, detail, price)
  VALUES ("Grinder", "bajaj grinder. Nice product", 4500)
		,("Camera", "A premium quality camera", 49000)
        ,("Vegetable Cutter" , "sharp Cutter", 3500)
        ,("Monitor", "Crystal clear monitor", 27500)
        ,("gas stove", "ISO certified gas stove", 7500);
        
/*QUESTION 13 Union 2 Different table with same Column name (use Distinct,ALL)*/

/*ALL*/
SELECT name , detail , price 
  FROM new_product
    UNION ALL
      SELECT name , detail , price 
        FROM old_product;


/*DISTINCT*/
INSERT INTO old_product(name, detail, price)
  VALUES ("Grinder", "bajaj grinder. Nice product", 4500);
  
SELECT name, detail, price 
  FROM new_product
    UNION DISTINCT
      SELECT name, detail, price
       FROM old_product;
 