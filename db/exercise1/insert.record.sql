
/*QUESTION 5 --- INSERT RECORDS*/
/*Insert record into Tables*/
INSERT INTO user(name, age, address) 
  VALUES ('GURU', 19, '2-b, bose nagar, covai-641050')
		,('Prasanth', 20, '44-h, voc nagar, coimbatore-641120')
        ,('Vishnu', 21, '855-i, ak-tp nagar, selam-641120')
        ,('Sarath', 25, '412-j, voc nagar, coimbatore-641120')
        ,('Sam', 80, '87-b, ABC street, Madurai-600658')
        ,('Naveen', 22, '76, Race Course, coimbatore-541120')
        ,('Akash', 35, '45-w, 100ft road, coimbatore-641240')
        ,('Ram', 30, '189-m, Gandhi nagar, Tirupur-647850')
        ,('Kumar', 41, '70, ram street, coimbatore-646464')
        ,('Samyu', 20, '21-g, SG nagar, coimbatore-641120');
        
SELECT id, name, product_count, age, address FROM user;

