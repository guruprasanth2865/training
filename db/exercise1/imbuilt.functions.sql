/*QUESTION 10*/
/*MAX*/
SELECT MAX(price) FROM product;

/*MIN*/
SELECT MIN(price) AS 'Minimum Price' FROM product
  WHERE price > 6000;
  
/*AVG*/
SELECT AVG(price) AS 'Average Price' FROM product;

/*COUNT*/
SELECT COUNT(DISTINCT(user_id)) as 'Regular customers COUNT' FROM product_order;

/*SUM*/	
SELECT SUM(price) FROM  product;


/*FIRST*/
SELECT id, name FROM user 
  ORDER BY 	name ASC LIMIT 2;
  
/*LAST*/
SELECT id, name FROM user 
  ORDER BY name DESC LIMIT 3;

/*date now*/
ALTER TABLE produCt_order 
  ADD order_date DATETIME DEFAULT now();

SELECT * FROM product_order;
