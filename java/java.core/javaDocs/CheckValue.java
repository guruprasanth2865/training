/*
Requirement:
    + What Integer method would you use to convert a string expressed in base 5 into the equivalent int?
        For example, how would you convert the string "230" into the integer value 65? Show the code you would use to accomplish this task.
		
Entities:
    - 
	
Function Declaration:
    -
	
Job to be done:
    - valueOf

		
*/

package com.kpriet.training.java.javaDocs;

class CheckValue {

    public static void main(String[] args) {
	    String word = "230";
		System.out.println(Integer.valueOf(word, 5));
	}
}