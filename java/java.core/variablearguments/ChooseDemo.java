/*
Requirements:
    + demonstrate overloading with varArgs

Entities:
    ChooseDemo
	
Function Declaration:
    - public static void main(String[] args)
	- listItems(int ...lst)
	- listItems(String ...lst)
	
Job to be done:
    - Create class ChooseDemo
	- Create function listItems(int ...lst) to get integers and print them
	- Create function listItems(String ...lst) to get strings and print them
	- Create main function 
	- Invoke function listItems(int ...lst)
	- Invoke function listItems(String ...lst)
*/

package com.kpriet.training.java.variablearguments;

class ChooseDemo {
    
	public static void listItems(int ...lst) {
	    int sum = 0;
		for (int i:lst) {
		    sum+=i;
		}
		System.out.println("The sum of list = " + sum);
	}
	
	public static void listItems(String ...lst) {
		System.out.println("\nWords in the list are ");
	    for (String word:lst) {
		    System.out.println(word);
		}
	}
	
	public static void main(String[] args) {
	    listItems(1,2,3,4,5);
		listItems("Hello", "World");
	}
}











