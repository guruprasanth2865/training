/*
Requirement:
    + demonstrate object equality using Object.equals() vs ==, using String objects
	
Entities:
    - EqualCheckDemo
	
Function Declaration:
    - public static void main(String[] args)
	- equals()
	
Job to be done:
    - Create class EqualCheckDemo
	- Declare two strings named wordOne and wordTwo
	- Compare them using == operator and equals() method and print their output
	
Explanation:
    Output:
    false
    true

*/

package com.kpriet.training.java.javalang;

public class EqualCheckDemo { 

    public static void main(String[] args) 
    { 
        String wordOne = new String("Program"); 
        String wordTwo = "Program"; 
        System.out.println(wordOne == wordTwo); 
        System.out.println(wordOne.equals(wordTwo)); 
    } 
} 

