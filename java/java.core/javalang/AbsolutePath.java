/*
Requirement:
    + print the absolute path of the .class file of the current class
	
Entities:
    AbsolutePath
	
Function Declaration 
    - public static void main(String[] args)
	- AbsolutePath()
	
Job to be done:
    - Create class AbsolutePath
	- Declare a constructor 
	- Declare a path object using getClass() and getClassLoader and load the path
	- Print the path of file 
	- Create main method and create an object of type AbsolutePath
*/

//package com.kpriet.training.java.javalang;

package com.kpriet.training.java.javalang;

import java.net.*;

class AbsolutePath {
   public AbsolutePath() {
      ClassLoader path = this.getClass().getClassLoader();
      URL current = path.getResource("AbsolutePath.class");
      System.err.println("Url=" + current);
      System.out.println(System.getProperty("user.dir"));
   }
   public static void main( String[] args ) {
      AbsolutePath path = new AbsolutePath();
   }
}