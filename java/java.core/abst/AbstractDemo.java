/*
Requirement:
    + Demonstrate abstract classes using Shape class.
        - Shape class should have methods to calculate area and perimeter
        - Define two more class Circle and Square by extending Shape class and implement the calculation for each class respectively

Entities: 
    - AbstractDemo
	- Shape
	- Square
	- Circle

Function Declaration:
    - public static void main(String[] args)
	- area()
	- perimeter()
	
Job to be done:
    - Create a abstract class Shape and declare two functions - area and perimeter
	- Create two child classes - Square and Circle for class Shape and define functions - area and primeter in it.
	- Create two methods - getArea(), getPerimeter() to get the values of area and perimeter.
	- Create AbstractDemo class
	- Create two objects at the instance of class square and circle
	- Invoke their methods - area() and perimeter() 

Explanation : 
    Area of square with length = 4.0 is 16.0
    Perimeter of square with length = 4.0 is 16.0
    Area of circle with length = 7.0 is 153.93804002589985
    Perimeter of circle with length = 7.0 is 43.982297150257104
*/
package com.kpriet.training.java.abst;

import java.lang.Math.*;
import java.util.Scanner;

abstract class Shape {
    
    public abstract void area();
    public abstract void perimeter();
}

class Square extends Shape {
	
    float value;
	float area;
	float perimeter;

    public void area() {
        area = value * value;
		System.out.println("Area of square with length = " + value + " is " + getArea());
    }

    public void perimeter() {
		perimeter = 4.00f * value;
		System.out.println("Perimeter of square with length = " + value + " is " + getPerimeter());
	}
	
	public float getArea(){
	    return area;
	}
	
	public float getPerimeter() {
	    return perimeter;
    }
}

class Circle extends Shape {
	
    double value;
	double area;
	double perimeter;

    public void area() {
        area = value * value * Math.PI;
		System.out.println("Area of circle with length = " + value + " is " +getArea());
    }

    public void perimeter() {
		perimeter = 2.0d * Math.PI * value;
		System.out.println("Perimeter of circle with length = " + value + " is " +getPerimeter());
	}
	
	public double getArea() {
		return area;
	}
	
	public double getPerimeter() {
        return perimeter;
    }	   
}


class AbstractDemo {
	
	public static void main(String[] args) {
	    Square box = new Square();
        Circle round = new Circle();
        box.value = 4.0f;
		box.area();
		box.perimeter();
        round.value = 7.0d;
        round.area();
        round.perimeter();		
	}
	
}
