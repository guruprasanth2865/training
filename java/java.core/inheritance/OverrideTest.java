/*
Requirement:
    + Consider the following two classes:
    a. Which method overrides a method in the superclass?
    b. Which method hides a method in the superclass?
    c. What do the other methods do?
	
Entities:
    - ClassA
	- ClassB
	- OverrideTest
	
Function Declaration:
    public void methodOne(int i)
	public void methodTwo(int i)
	public static void methodThree(int i)
	public static void methodFour(int i)
	public static void methodOne(int i)
	public void methodThree(int i)
	
Job to be done:
    - Create class Parent
	- Declare function methodOne(int i)
	- Declare function methodTwo(int i)
	- Declare a static function methodThree(int i)
    - Declare a static function methodFour(int i)
    - Create a class Child extends Parent class
    - Declare a static function methodOne(int i)
    - Declare function methodTwo(int i)
    - Declare function methodThree(int i)
    - Declare a static function methodFour(int i) 
    - In all these functions, print their class name and function name	
	
Explanation:

a) Which method overrides a method in the superclass?
   - methodTwo, methodFour

b) Which method hides a method in the superclass?
   - 

c) What do the other methods do?
   - Other methods cause compile-time errors.

*/

package com.kpriet.training.java.inheritance;

class Parent {

    /*public void methodOne(int i) {
	    System.out.println("Parent Class - methodOne " + i);
    }*/
	
    public void methodTwo(int i) {
	    System.out.println("Parent Class - methodTwo " + i);
    }
	
    /*public static void methodThree(int i) {
	    System.out.println("Parent Class - methodThree " + i);
    }*/
	
    public static void methodFour(int i) {
	    System.out.println("Parent Class - methodFour " + i);
    }
}

class Child extends Parent {

    /*public static void methodOne(int i) {
	    System.out.println("Child Class - methodOne " + i);
    }*/
	
    public void methodTwo(int i) {
	    System.out.println("Child Class - methodTwo " + i);
    }
	
    /*public void methodThree(int i) {
	    System.out.println("Child Class - methodThree " + i);
    }*/
	
    public static void methodFour(int i) {
	    System.out.println("Child Class - methodFour " + i);
    }
}

class OverrideTest {

    public static void main(String[] args) {
	    Child obj = new Child();
		//obj.methodOne(1);
		obj.methodTwo(2);
		//obj.methodThree(3);
		obj.methodFour(4);
	}
}