/*
Requirements:
    + demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of objects
	
Entities:
    InheritDemo
	Animal
	Dog
	Cat
	Snake
	
Function Declaration:


Job to be done:
    - Create class Animal
	- Declare function sound() and print sentence
	- Declare function setBreed() and set breed = "na"
	- Declare function setBreed(String breed) to get a string and set it to breed
	- Create class Dog extends Animal
	- Declare function sound() and print sentence
	- Create class Cat extends Animal
	- Declare function sound() and print sentence
	- Create class Snake extends Animal
	- Declare function sound() and print sentence
	- Create class InheritDemo
    

*/

package com.kpriet.training.java.inheritance;

class Animal {
	
	String name;
	String breed;
	
	public void sound() {
	    System.out.println("Animal Makes sound");
	}
	
	public void setBreed() {
	    breed =	"na";
	}
	
	public void setBreed(String breed) {
	    this.breed = breed;
	}
}

class Dog extends Animal {
	
	public void sound() {
	    System.out.println("Dog Barks");	
	}
}

class Cat extends Animal {
	
	public void sound() {
	    System.out.println("Cat Meows");	
	}
}

class Snake extends Animal {
	
	public void sound() {
	    System.out.println("Snake hisses");	
	}
}

class InheritDemo {
	
	public static void main(String[] args) {
		Dog tom = new Dog();
		tom.name = "Tom Cruise";
		tom.setBreed("Doberman Pincher");
		System.out.println("\nDog name = " + tom.name + "\nDog breed = " + tom.breed);
		tom.sound();
		
		Cat kitty = new Cat();
		kitty.name = "Kitty";
		kitty.setBreed();
		System.out.println("\nCat name = " + kitty.name + "\nCat breed = " + kitty.breed);
		kitty.sound();
		
		Snake cobra = new Snake();
		cobra.name = "Fluffy";
		cobra.setBreed("King Cobra");
		System.out.println("\nSnake name = " + cobra.name + "\nSnake breed = " + cobra.breed);
		cobra.sound();
	}
}










