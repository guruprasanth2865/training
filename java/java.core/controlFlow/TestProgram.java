/*
Requirements:
    + Consider the following code snippet.
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

        - What output do you think the code will produce if aNumber is 3?

        Write a test program containing the previous code snippet
        - and make aNumber 3. What is the output of the program?
        - Using only spaces and line breaks, reformat the code snippet to make the control flow easier to understand.
        - Use braces, { and }, to further clarify the code.

Entities:
    TestProgram
	
Function Declaration:
    public static void main(String[] args)
	
Job to be done:
    - Create class TestProgram
	- Declare and define main method init
	- Declare a variable aNumber of type integer
	- Provide clarity to the if else conditions provided 
    
*/

package com.kpriet.training.java.controlFlow;

class TestProgram {

    public static void main(String[] args){
        int aNumber = 3;
               
        if (aNumber >= 0) {
            if (aNumber == 0) {
                System.out.println("first string");       
            }
        } else {
            System.out.println("second string");
        } 
        System.out.println("third string");
    }
}

