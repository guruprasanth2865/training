/*
+ print fibinocci using for loop, while loop and recursion

For loop:

import java.util.Scanner;

public class FibbonacciForLoop{

    public static void main(String[] args){
	  
        Scanner sc = new Scanner(System.in);
	int a = 0, b = 1, n, temp;
	System.out.println("Enter the number : ");
	n = sc.nextInt();
	for( int i = 0; i < n; i++){
            System.out.println(a);
	    temp = a + b;
	    a = b;
	    b = temp;
	}
    }
}

1) Requirements:
    fibbonacci series using - For loop

2) Entities 
    FibbonacciForLoop

3) Function Declaration
    public class FibbonacciForLoop
    
4) Jobs to be done
    - Create a class 
    - Declare main methos 
    - In main method, create four variables
    - Get a value from user 
    - Using for loop, print the fibbonacci series 
    

While Loop:

import java.util.Scanner;

public class FibbonacciWhileLoop{

    public static void main(String[] args){
	    
        Scanner sc = new Scanner(System.in);
       	int a = 0, b = 1, n, temp, i=0;
	System.out.println("Enter the number : ");
	n = sc.nextInt();
	while(i<n){
	    System.out.println(a);
            temp = a + b;
	    a = b;
            b = temp;
	    i++;
        }
    }
}

1) Requirements:
    fibbonacci series using - While loop

2) Entities 
    FibbonacciForLoop

3) Function Declaration
    public class FibbonacciWhileLoop
    
4) Jobs to be done
    - Create a class 
    - Declare main methos 
    - In main method, create four variables
    - Get a value from user 
    - Using while loop, print the fibbonacci series 
    



Recursion

import java.util.Scanner;

class FibbonacciRecursion{

    public static int numberOne=0, numberTwo=1, sum=0;
	
	static void calculate(int count){
	    if(count>0){
	        sum = numberOne + numberTwo;
		numberOne = numberTwo;
		numberTwo = sum;
		System.out.println(numberTwo);
		calculate(count-1);
	    }
	}
    
    public static void main(String[] args){
        
        Scanner sc = new Scanner(System.in);
	System.out.println("Enter the number : ");
        int n = sc.nextInt();		
	if(n>0){
	    System.out.println(0 + " ");
	}
	if(n>1){
            System.out.println(1 + " ");
	}
	calculate(n-2);
    }
}

1) Requirements:
    fibbonacci series using - Recursion

2) Entities 
    FibbonacciRecursion

3) Function Declaration
    public class FibbonacciRecursion
    
4) Jobs to be done
    - Create a class 
    - Declare  instance variables start, next,  temp, count
    - Declare a method with a parameter to calculate fibbonacci series
    - Declare main methos 
    - In main method, get the value from user 
    - Invoke function - calculate   to print fibbonacci series.
    

*/

/*
Requirements:
    + print fibinocci using for loop, while loop and recursion
	
Entities:
    FibbonacciDemo
	
Function Declaration:
    public void 
    public static void main(String[] args)
	
Job to be done:
    - Create class FibbonacciDemo
	- Declare five instance variables start, next, end, temp, count
	- Declare function whileProcess(int n)
	    - In while loop, check wheteher count is less than or equal to end and do calculation for       recursion and print them
	- Declare function forProcess(int n)
	    - In For loop,check wheteher count is less than or equal to end and do calculation for recursion and print them
	- Declare function recursiveProcess(int n)
	    - In this function, check wheteher count is less than or equal to end and do calculation for recursion and print them and call function by itself with recursion process
	- Declare main function
	- Get a number from user
	- Invoke all functions with that number
	
Explanation:
 
Enter the number :
10

Fibonaci using While loop :
0
1
1
2
3
5
8
13
21
34

Fibonaci using For loop :
0
1
1
2
3
5
8
13
21
34

Fibonaci using Recursion :
0
1
1
2
3
5
8
13
21
34


*/

package com.kpriet.training.java.controlFlow;

import java.util.Scanner;

class FibbonacciDemo {
	
	int start = 0;
	int next = 1;
	int count = 1;
	int temp;
	int n;
	
	public void whileProcess() {
		while (count <= n) {
			System.out.println(start);
			temp = start + next;
			start = next;
			next = temp;
			count++;
		}
		count = 1;
		start = 0;
		next = 1;
	}
	
	public void forProcess() {
		for (count = 1; count <= n; count++) {
			System.out.println(start);
			temp = start + next;
			start = next;
			next = temp;
		}
		start = 0;
		next = 1;
		count = 1;
	}
	
	public void recursiveProcess(int count) {
		if (count <= n) {
		    System.out.println(start);
		    temp = start + next;
		    start = next;
		    next = temp;
		    recursiveProcess(count+1);
		}
		start = 0;
		next = 1;
		count = 1;
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		FibbonacciDemo number = new FibbonacciDemo();
		System.out.println("Enter the number : ");
		number.n = sc.nextInt();
		System.out.println("\nFibonaci using While loop : ");
		number.whileProcess();
		System.out.println("\nFibonaci using For loop : ");
		number.forProcess();
		System.out.println("\nFibonaci using Recursion : ");
		number.recursiveProcess(number.count);
	}
}













