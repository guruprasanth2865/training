/*
Requirements:
    + print the type of the result value of following expressions
        - 100 / 24
        - 100.10 / 10
        - 'Z' / 2
        - 10.5 / 0.5
        - 12.4 % 5.5
        - 100 % 56
	
Entities:
    TypePrinterDemo
	
Function Declaration:
    public static void main(String[] args)
	
Job to be done:
    

Explanation:
    - 4
    - 10.01
    - 45
    - 21.0
    - 1.4000000000000004
    - 44

*/

package com.kpriet.training.java.dataType;

class TypePrinterDemo {

    public static void main(String[]  args) {
	    Integer ansOne = 100/ 24;
		Double ansTwo = 100.10 / 10;
		Integer ansThree = 'z' / 2;
		Double ansFour = 10.5 / 0.5;
		Double ansFive = 12.4 % 5.5;
		Integer ansSix = 100 % 56;
	    System.out.println(ansOne + " is of type " + ansOne.getClass().getName());
		System.out.println(ansTwo +  " is of type " + ansTwo.getClass().getName());
		System.out.println(ansThree +  " is of type " + ansThree.getClass().getName());
		System.out.println(ansFour +  " is of type " + ansFour.getClass().getName());
		System.out.println(ansFive +  " is of type " + ansFive.getClass().getName());
		System.out.println(ansSix +  " is of type " + ansSix.getClass().getName());
	}
}



