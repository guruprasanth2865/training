/*
Requirements:
    + What is the value of the following expression, and why?
     Integer.valueOf(1).equals(Long.valueOf(1))

Entities:
    EqualComparatorDemo
	
Function Declaration:
    public static void main(String[]args)
    equals()
	valueOf()
	
Job to be done:
    - Create class EqualComparatorDemo
	- Declare main method init
	- Compare two variables of type Integer ans Long using equals() method

Explanation:
    Output:
	false
	
	The values used to compare are of different datatypes and so the output is false.
*/

package com.kpriet.training.java.dataType;

class EqualComparatorDemo {

    public static void main(String[] args) {
	    System.out.println(Integer.valueOf(1).equals(Long.valueOf(1)));
	}
}







