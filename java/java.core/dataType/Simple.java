/*
Requirements:
+ Create a program that reads an unspecified number of integer arguments from the command line and adds them together.
    For example, suppose that you enter the following: java Adder 1 3 2 10
    The program should display 16 and then exit. The program should display an error message if the user enters only one argument.

Entities:
    Simple

Function Declaration:
    public static void main(String[] args)

Job to be done:
   - Create class Simple
   - Declare main method init.
   - Declare three variables n, count and ans o type integer
   - Get an input from user and execute while loop
   - Add the input got from user and add to the variable-ans.
   - Again get another input from user and check whether it is not equal to zero
   - If it is not equal to zero, continue the same process in while loop
   - When the input given is equal to zero, print the ans
*/

package com.kpriet.training.java.dataType;

import java.util.Scanner;

public class Simple {
	
    public static void main(String args[]){
        int n;
		int ans=0;
		int count=0;
        Scanner sc = new Scanner(System.in);
	    System.out.println("Print the numbers : ");
        n = sc.nextInt();
        while(n!=0) {
            ans += n;
            n = sc.nextInt();
	    	count++;
        }
        if(count==1) {
	    System.out.println(n);
        } else {
	    System.out.println(ans);
        }
    }
}
