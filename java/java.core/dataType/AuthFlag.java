/*
Requirement:
    + program to store and retrieve four boolean flags using a single int
	
Entities:
    AuthFlag
	
Function Declaration:
    public static void main(String[] args)

Job to be done:
    - Create class AuthFlag
	- Declare four variables of type integer
    - Declare another variable auth of type int
	- Use comparison operator and print output

*/

package com.kpriet.training.java.dataType;

class AuthFlag {

    public static void main(String[] args) {
	
	    int user = 1;
		int superUser = 2;
		int admin = 3;
		int proprietor = 4;
		int authorization = admin | proprietor;
		
		System.out.println((user & authorization) > 2 ? "Access Granted" : "No access" );
		System.out.println((superUser & authorization) > 2 ? "Access Granted" : "No access");
		System.out.println((admin & authorization) > 2 ?  "Access Granted" : "No access");
		System.out.println((proprietor & authorization) > 2 ? "Access Granted" : "No access");
	}
}
		
		
		
		