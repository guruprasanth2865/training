/*
Requirements:
    + To invert the value of a boolean, which operator would you use?
	
Entities:
    InvertValue

Function Declaration:
    public static void main(String[] args)
	
Job to be done:
    - Create class InvertValue
	- Create a variable of type Boolean
	- Print the value of variable
	- Print it by using a exclamation symbol

Explanation:
    ! - can be used to invert a boolean value

*/

package com.kpriet.training.java.dataType;

class InvertValue{
    
	public static void main(String[] args) {
	    boolean value = true;
		System.out.println(value);
		System.out.println(!value);
	}
}

