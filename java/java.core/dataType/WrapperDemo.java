/*
Requirement:
    + demonstrate what happens when mixing primitive with respective Wrapper types for the above operations
	
Entities:
    - WrapperDemo
	
Function Declaration:
    - public static void main(String[] args)
	- intWrap()
	- charWrap()
	- doubleWrap()
	
Job to be done:
    - Create class WrapperDemo
	- Declare method intWrap(int value)
	- Declare method charWrap(char value)
	- Declare method doubleWrap(double value)
	- Declare and define main method
	- Declare a variable intValue of type Integer
	- Declare a vaariablr of type Character
	- Declare a variable of type Double
	- Invoke function intWrap(intValue) with parameter 
	- Invoke function charWrap(charValue) with parameter
	- Invoke function doubleWrap(doubleValue) with parameter
    

Explanation:
    intValue is converted to Integer 117
    charValue is converted to Character G
    doubleValue is converted to Double 883.8


*/

package com.kpriet.training.java.dataType;

class WrapperDemo {

    void intWrap(int value) {
        System.out.println("intValue is converted to Integer "+value);
    }
    
    void charWrap(char value) {
        System.out.println("charValue is converted to Character "+value);
    }
    
    void doubleWrap(double value) {
        System.out.println("doubleValue is converted to Double "+value);
    }
	
	public static void main(String[] args) {
	    Integer intValue = 117;
		Character charValue = 'G';
		Double doubleValue = 883.8;
	    WrapperDemo number = new WrapperDemo();
		number.intWrap(intValue);
		number.charWrap(charValue);
		number.doubleWrap(doubleValue);
	}
	
}

