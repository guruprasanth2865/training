/*
Requirement:
    + print the classname of all the primitive data types (Note: not the wrapper types)
	
Entities:
    PrimitiveName
	
Function Declaration: 
    public static void main(String[] args)
	
Job to be done:
    - Create class PrimitiveName
	- Declare and define main method init
	- Get the class names of initeger, float, double, char using class.getName() method init
	- Print their returned name
	
Explanation:
ClassName of Int : int
ClassName of Char : char
ClassName of double : double
ClassName of float : float
	
*/

package com.kpriet.training.java.dataType;

public class PrimitiveName {

    public static void main(String[] args) {

        System.out.println("ClassName of Int : " + int.class.getName());
        System.out.println("ClassName of Char : " + char.class.getName());
        System.out.println("ClassName of double : " + double.class.getName());
        System.out.println("ClassName of float : " + float.class.getName());
    }
}