/*
Requirement:
    Write a class that implements the CharSequence interface found in the java.lang package.
    Your implementation should return the string backwards. Write a small main method to test your class; make sure to call all four methods.
	
Entities:
    - CharSeqDemo
	
Function Declaration:
    - public static void main(String[] args)
	- public CharSequenceDemo(String s)
	- public CharSequence subSequence(int start, int end)
    - private int fromEnd(int i)
    - public char charAt(int i)
    - public int length()
    - public String toString()
    - private static int random(int max)
	
Job to be done:
    - Create class CharSequenceDemo that impolements CharSequence
	- Create a instanve vartiable of type string
	- Create a Constructor to get a string and set it as the value of instance variable
	- Declare a method getIndex with an integer argument and 
	   return word.length() - 1 - int_value
	- Declare a method charAt(int n) and return letter from the word at that index
	- Declare a method length() to return the length of the word
	- Declare a method subSequence to get first and last integer values. After checking whether thhose values are valid, Reverse of the string is returned with the string builder
	- Declare method toString() to change other datatypes to strings
	- Declare method rendom() and return a random number
	- Declare main method
	- Create a object word of type CharSeqDemo
	- Invoke the functions of CharSeqDemo() and print the output
*/

package com.kpriet.training.java.interfc;

public class CharSeqDemo implements CharSequence {
    private String word;

    public CharSeqDemo(String word) {
        this.word = word;
    }

    private int getIndex(int n) {
        return word.length() - 1 - n;
    }

    public char charAt(int n) {
        if ((n < 0) || (n >= word.length())) {
            throw new StringIndexOutOfBoundsException(n);
        }
        return word.charAt(getIndex(n));
    }

    public int length() {
        return word.length();
    }

    public CharSequence subSequence(int first, int last) {
        if (first < 0) {
            throw new StringIndexOutOfBoundsException(first);
        }
        if (last > word.length()) {
            throw new StringIndexOutOfBoundsException(last);
        }
        if (first > last) {
            throw new StringIndexOutOfBoundsException(first - last);
        }
        StringBuilder subString = 
            new StringBuilder(word.subSequence(getIndex(last), getIndex(first)));
        return subString.reverse();
    }

    public String toString() {
        StringBuilder word = new StringBuilder(this.word);
        return word.reverse().toString();
    }

    private static int random(int max) {
        return (int) Math.round(Math.random() * (max+1));
    }

    public static void main(String[] args) {
        CharSeqDemo word =
            new CharSeqDemo("Hello Everyone");

        for (int i = 0; i < word.length(); i++) {
            System.out.print(word.charAt(i));
        }
        
        int start = random(word.length() - 1);
        int end = random(word.length() - 1 - start) + start;
        System.out.println("\n"+word.subSequence(start, end));

        System.out.println(word);
    }
}