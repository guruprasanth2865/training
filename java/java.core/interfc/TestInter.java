/*
Requirement:
    + Is the following interface valid?
        public interface Marker {}
		
Entities: 
    - Marker
    - TestInter
	
Function Declaration:
    public static void main(String[] args)
	
Job to be done:
    - Create an interface Marker
	- Create a class TestInter which implements 
	- Declare and define a main class and print text
		
Explanation:
    Interface Marker created is valid. But it cannot be used as there is no function defined in it.
	Interface can be used efficiently only if there is any function declared init.
	Here, there is no function declared in it. But it don't create any compilation errors.

*/

package com.kpriet.training.java.interfc;

interface Marker {}

class TestInter implements Marker {
	
	public static void main(String[] args) {
		System.out.println("Hello World");
	}
}