/*
Requirment:
    + compare the enum values using equal method and == operator
	
Entities:
    EnumCheckerDemo
	
Function Declaration:
    public static void main(String[] args)
	
Job to be done:
    - Create class EnumCheckerDemo
	- create two enum class - First ans Second,   with their values
	- Declare main method in EnumCheckerDemo
	- Comparte the declared enum values
	
Explanation:
    true
    true
    false
    false

*/

package com.kpriet.training.java.enumChecker;

class EnumCheckerDemo { 

    public enum First { ONE, TWO, THREE } 
	
    public enum Second { ONE, FOUR, FIVE } 
	
    public static void main(String[] args) {
	
        First f = First.ONE;
        //System.out.println(First.ONE == Second.ONE);
        System.out.println(f == First.ONE);
        System.out.println(f.equals(First.ONE)); 
		
		System.out.println(f.equals(Second.ONE));
        System.out.println(First.ONE.equals(Second.ONE)); 
    }
}
