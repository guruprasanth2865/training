/*
Requirements:
   + In the following program, explain why the value "6" is printed twice in a row:
       class PrePostDemo {
           public static void main(String[] args){
               int i = 3;
               i++;
               System.out.println(i);    // "4"
               ++i;
               System.out.println(i);    // "5"
               System.out.println(++i);  // "6"
               System.out.println(i++);  // "6"
               System.out.println(i);    // "7"
           }
       }

Entities:
    PrePostDemo
	
Function Declaration:
    public static void main(String[] args)
	
Job to be done:
    - Create class PrePostDemo
	- Declare and define main function
	- Declare a variable i of type int with value 3
	- Use post-increment operator to variable i and print its value
	- Use pre-increment operator to variablre i and print its value
	- Print the value of i 
	- Use pre-increment operator to variablre i and print its value
	- Use post-increment operator to variable i and print its value
	- Print the value of i

Explanation:
    - At line - 7 , value of 'i' variable is printed as '5'. 
   
    - At line - 8 , pre-increment operator is used for 'i' variable to increment its value 
      before using it in an expression, it becomes 6. 

    - At line - 9 , post-increment operator is used for 'i' variable to increment its value after using it in expression, so it prints the value 6. After printing, its value is incremented to 7
	
    - At line -10 , value of 'i' is printed ( value of 'i' is 7, which was incremented by 
     the previous line ).
*/

package com.kpriet.training.java.operator;

class PrePostDemo {
    public static void main(String[] args){
        int i = 3;
        i++;
        System.out.println(i);    // "4"
        ++i;
        System.out.println(i);    // "5"
        System.out.println(++i);  // "6"
        System.out.println(i++);  // "6"
        System.out.println(i);    // "7"
    }
}


