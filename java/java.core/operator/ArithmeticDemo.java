/*
Requirement:
    + Change the following program to use compound assignments:
       class ArithmeticDemo {

            public static void main (String[] args){

                 int result = 1 + 2; // result is now 3
                 System.out.println(result);

                 result = result - 1; // result is now 2
                 System.out.println(result);

                 result = result * 2; // result is now 4
                 System.out.println(result);

                 result = result / 2; // result is now 2
                 System.out.println(result);

                 result = result + 8; // result is now 10
                 result = result % 7; // result is now 3
                 System.out.println(result);
            }
       }
	   
Entities:
    ArithmeticDemo
	
Function Declaration:
    public static void main(String[] args)
	
Job to be done:
    - Create class ArithmeticDemo
	- Declare result of type int and set value as 1;
	- Use compound assignment to do math operations
	- Increse the value of result ( result +=2 ) and print it
	- Decrease the value of result by 1 ( result -=1 )( compound assignment )
	- Multily the value of result with 2 ( result *= 2)
	
Explanation:
Output:
    3
    2
    4
    2
    3

*/

package com.kpriet.training.java.operator;

class ArithmeticDemo {

    public static void main (String[] args){

        int result = 1;
        result += 2; // result is now 3
        System.out.println(result);

        result -= 1; // result is now 2
        System.out.println(result);

        result *= 2; // result is now 4
        System.out.println(result);

        result /= 2; // result is now 2
        System.out.println(result);

        result += 8; // result is now 10
        result %= 7; // result is now 3
        System.out.println(result);
    }
}

