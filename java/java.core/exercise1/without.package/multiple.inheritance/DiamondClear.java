interface HeadInterface
{  
    default void show() 
    { 
        System.out.println("Default Function Show()"); 
    } 
} 
  
interface SubInterfaceOne extends HeadInterface { 
    
	default void run(){
	    System.out.println("Default Function of SubInterfaceOne");
	}
} 
  
interface SubInterfaceTwo extends HeadInterface { 

    default void run(){
	    System.out.println("Default Function of SubInterfaceTwo");
	}
} 
  
class DiamondClear implements SubInterfaceOne, SubInterfaceTwo 
{ 
    public void run(){
	    SubInterfaceOne.super.run();
        SubInterfaceTwo.super.run();	
	}
	
    public static void main(String args[]) 
    { 
        DiamondClear solutionOne = new DiamondClear(); 
        solutionOne.show(); 
		solutionOne.run();
    } 
}