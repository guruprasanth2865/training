abstract class PaymentGateway{

    public abstract void basicGateway1();
	
	public String GatewayG1(){
	    return "GatewayG1";
	}
}

abstract class SubGatewayTwo extends PaymentGateway{

    public void basicGateway1(){
	    System.out.println("This is gateway 1 of Grant parent class");
	}
	
	public abstract void subGateway2();
	
	public String GatewayG2(){
	    return "GatewayG2";
	}
}

class SubGatewayThree extends SubGatewayTwo{

    public void subGateway2(){
	    System.out.println("This is gateway 2 - Abstract method of parent class");
	}
	
	public String GatewayG3(){
	    return "GatewayG3 - function in child class";
	}
}

public class PaymentClass{
    
	public static void main(String[] args){
	    SubGatewayThree paymentOne = new SubGatewayThree();
	    paymentOne.basicGateway1();
	    paymentOne.subGateway2();	
	}
}