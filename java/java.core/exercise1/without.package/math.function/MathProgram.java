public class MathProgram{
    
	float a, b;
	
	public MathProgram(float num1, float num2){
		a = num1;
		b = num2;
	}
	
	public MathProgram(){
		a = 0.00f;
		b = 0.00f;
	}
	
	public float add(){
	    return a+b;
	}
	
	public float sub(){
	    return a-b;
	}
	
	public float mul(){
	    return a*b;
	}
	
	public float div(){
	    return a/b;
	}

    public static void main(String[] args){
	    MathProgram calc1 = new MathProgram(5.0f, 2.0f);
		float ans = calc1.add();
		System.out.println("Addition : " + ans);
		System.out.println("Subraction : " + calc1.sub());
		System.out.println("Multiplication : " + calc1.mul());
		System.out.println("Division : " + calc1.div());
	}
}