package com.amazon.PaymentGateways;

import com.amazon.PaymentGateways.*;

abstract class SubGatewayTwo extends PaymentGateway{

    public void basicGateway1(){
	    System.out.println("This is gateway 1 of Grant parent class");
	}
	
	public abstract void subGateway2();
	
	public String GatewayG2(){
	    return "GatewayG2";
	}
}