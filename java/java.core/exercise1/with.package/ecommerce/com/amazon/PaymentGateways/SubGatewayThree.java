package com.amazon.PaymentGateways;

import com.amazon.PaymentGateways.*;

public class SubGatewayThree extends SubGatewayTwo{

    public void subGateway2(){
	    System.out.println("This is gateway 2 - Abstract method of parent class");
	}
	
	public String GatewayG3(){
	    return "GatewayG3 - function in child class";
	}
}