package com.amazon.People;

import com.amazon.People.*;


public class CreateUser{
    public static void main(String[] args){
	    User ramesh = new User(1000001L, "Ramesh", "30-09-2000", "ramesh23@gmail.com", "ramesh123", 19, 9874563210L);
		
		ramesh.publicArea();
		ramesh.userExperiance();
		ramesh.address.doorNo = "22-B";
		ramesh.address.streetname = "ABC Nahar";
		ramesh.address.locality = "Coimbatore";
		ramesh.address.state = "Tamil Nadu";
		ramesh.address.country = "India";
		ramesh.address.pincode = 641407L;
		ramesh.address.displayAddress();
		ramesh.orderProduct(123456789L);
		ramesh.orderProduct(123456583L);
		ramesh.cancelOrder(123456789L);
		
		System.out.println("\n" + "Special price for "+ ramesh.name + " is " + ramesh.calculatePrice(100f));
		
		System.out.println("Inheritance created successfully");
		
		ramesh.makePayment(1000001L, "ramesh123");
		ramesh.makePayment("ramesh23@gmail.com", "ramesh123");
	}
}