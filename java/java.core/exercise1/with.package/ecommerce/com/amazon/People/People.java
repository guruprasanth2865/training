package com.amazon.People;

import com.amazon.People.Address;

public class People{
    
    public Long id;
	public String name;
    public String dob;
	public String email;
	public String password;
	public int age;
	private Long[] order;
	public Long mobile;
	public Address address;
	
	public People(Long idno, String n, String db, String em, String pass, int ag, Long mob){
		id = idno;
		name = n;
		dob = db;
		email = em;
		password = pass;
		age = ag;
		mobile = mob;
		address = new Address();
	}
	
	public People(){
		id = 0L;
		name = "na";
		dob = "na";
		email = "na";
		password = "na";
		age = 0;
		mobile = 0L;
		address = new Address();
	}
	
	
	public void orderProduct(Long productId){
		if(order == null){
			Long[] dummyArray = new Long[1];
			dummyArray[0] = productId;
			order = dummyArray.clone();
			System.out.println(order.length);
		} else {
			Long[] dummyArray = new Long[order.length + 1];
			for( int j = 0; j < order.length; j++){
				dummyArray[j] = order[j];
			}
		    dummyArray[order.length] = productId;
			order = dummyArray.clone();
		}
        System.out.println("\n"+"Product Ordered");	
		System.out.println("Product details");
		for( int j = 0; j<order.length ; j++){
		    System.out.println(order[j]);	
		}
	    
	}
	
	public void cancelOrder(Long productId){
		if( order.length > 0){
		    Long[] dummyArray = new Long[order.length-1];
		    int condition = 0;
		    int di = 0;
			System.out.println();
		    for (int i = 0; i<order.length; i++){
		        if (order[i].equals(productId)){
			        condition = 1;
				    System.out.println("Product removed : "+productId);
			    } else {
			    dummyArray[di] = order[i];
				di+=1;
			    }
		    }
		    if (condition == 1){
			    order = dummyArray.clone();
			    System.out.println("Order Cancelled");
                for(int i = 0; i<order.length; i++){
                    System.out.println(order[i]);
                }					
		    } 
			if (condition == 0){
				System.out.println("Product not ofund");
			}
		} else {
			System.out.println("Order list is empty");
		} 
	}
	
	public float calculatePrice(float price){
	    return (price - (price * (0.05f)));	
	}
	
	public String createUsername(String name, Long id){
	    return name+Long.toString(id);
	}
	
	public String createUsername(String email){
	    return email;
	}
	
	public void makePayment(Long id, String password){
	    if(password.equals(this.password) && id.equals(this.id)){
			System.out.println("\nPayment is Success" + "\n" + "Payment by " + this.id);
		} else {
		    System.out.println("Check your pasword for payment");
		}    			
	}
	
	public void makePayment(String email, String password){
	    if(password.equals(this.password) && email.equals(this.email)){
			System.out.println("\nPayment is Success" + "\n" + "Payment by " + this.email);
		} else {
		    System.out.println("Check your pasword for payment");
		}    			
	}
	
	public void returnOrder(){
	    System.out.println("Order Returned");	
	}
	
	public void publicArea(){
		System.out.println("parent Class");
	}
}



