package com.amazon.People;

public class Address{

    public String doorNo;
	public String streetname;
	public String locality;
	public String state;
	public String country;
	public Long pincode;
	
	public Address(String dn, String sn, String ly, String st, String cy, Long pnc){
	    doorNo = dn;
		streetname = sn;
		locality = ly;
		state = st;
		country = cy;
		pincode = pnc;
	}
	
	public Address(){
	    doorNo = "na";
		streetname = "na";
		locality = "na";
		state = "na";
		country = "na";
		pincode = 0L;
	}
	
	public void displayAddress(){
	    System.out.println("\n" + "Address details : " 
		    + "\n" + doorNo + "," +  streetname + "," +  locality + "," + state + "-" + pincode + "," + country);
	}
	
}