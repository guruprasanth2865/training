package com.kpr.training.multiple_inheritnce;

import com.kpr.training.multiple_inheritnce.*;

class DiamondClear implements SubInterfaceOne, SubInterfaceTwo 
{ 
    public void run(){
	    SubInterfaceOne.super.run();
        SubInterfaceTwo.super.run();	
	}
	
    public static void main(String args[]) 
    { 
        DiamondClear solutionOne = new DiamondClear(); 
        solutionOne.show(); 
		solutionOne.run();
    } 
}