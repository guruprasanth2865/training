package com.kpr.training.multiple_inheritnce;

interface HeadInterface
{  
    default void show() 
    { 
        System.out.println("Default Function Show()"); 
    } 
} 