package com.kpr.training.multiple_inheritnce;

import com.kpr.training.multiple_inheritnce.HeadInterface;

interface SubInterfaceTwo extends HeadInterface{
    
	default void run(){
	    System.out.println("Default Function of SubInterfaceTwo");
	}
}