/*
Requirement:
    + The following code creates one array and one string object. How many references to those objects exist after the code executes?
     Is either object eligible for garbage collection?
    
      String[] students = new String[10];
      String studentName = "Peter Parker";
      students[0] = studentName;
      studentName = null;
	  
Entities:
    ReferanceDemo

Function Declaration:
    public static void main
	
Job to be done:
    - Create class ReferanceDemo
	- Create a String Array - students
	- Create a string - studentName and assign value
    - Assign studentName string to the index[0] of the array students  
    - now set the string studentName to null
    - Now print the value of array - students at index[0]	
	
Explanation:
    How many references to those objects exist after the code executes?
	    There only one referances to the 
*/
package com.kpriet.training.java.string;

class ReferanceDemo {
	public static void main(String[] args){
		String[] students = new String[10];
		String studentName = "Peter Parker";
		System.out.println("At begining : \n" + students[0] + "\n" + studentName); 
		students[0] = studentName;
		System.out.println("\nAfter Assigning string value: \n" + students[0] + "\n" + studentName); 
		studentName = null;
        System.out.println("\nAfter Assigning null value : \n" + students[0] + "\n" + studentName); 
	}
}
	
	
	
	
	
	
	
	
	
	