/*
Requirement: 
    + In the following program, called ComputeResult, what is the value of result after each numbered line executes?
    public class ComputeResult {
        public static void main(String[] args) {
            String original = "software";
            StringBuilder result = new StringBuilder("hi");
            int index = original.indexOf('a');

            result.setCharAt(0, original.charAt(0));
            result.setCharAt(1, original.charAt(original.length()-1));
            result.insert(1, original.charAt(4));
            result.append(original.substring(1,4));
            result.insert(3, (original.substring(index, index+2) + " "));

            System.out.println(result);
        }
    }
	
Entities:
    ComputeResult
	
Function Declaration:
    public static void main(String[] args)
	
Job to be done:
    - Create a Class - ComputeResult
	- In the main function, declare a string - original and assign value "software".
	- Create a StringBuilder - result
	- 1) Use setCharAt and assign value to - result 
	- 2) Use setCharAt and assign value to - result 
	- 3) Use insert to insert string in result
	- 4) Use append to append the string result
	- 5) Use insert to insert string in result
	
Explanation:
    si
    se
    swe
    sweoft
    swear oft
*/

package com.kpriet.training.java.string;

public class ComputeResult {

    public static void main(String[] args) {
        String original = "software";
        StringBuilder result = new StringBuilder("hi");
        int index = original.indexOf('a');

        result.setCharAt(0, original.charAt(0));                       /*1*/
		System.out.println(result);
        result.setCharAt(1, original.charAt(original.length()-1));     /*2*/
		System.out.println(result);
        result.insert(1, original.charAt(4));                          /*3*/
		System.out.println(result);
        result.append(original.substring(1,4));                        /*4*/
		System.out.println(result);
        result.insert(3, (original.substring(index, index+2) + " "));  /*5*/
        System.out.println(result);
    }
}







