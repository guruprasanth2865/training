/*
Requirements:
    + sort and print following String[] alphabetically ignoring case. Also convert and print even indexed Strings into uppercase { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }

Entities:
    District
	
Function Declaration:
    public static void main
	
Jobs to be done:
    - Create class District
	- Initialise array with district name
	- Declare a function - sortString to get an array of strings and print in sorted form
	- Declare a function - changeUppercase and print their case changed array
	- pass the array to function - sortStrings
	- pass each string from array to function - changeUppercase using for loop
    
*/

package com.kpriet.training.java.string;

class District {
    
	public void sortString(String[] dists) {
		String temp;
		for (int i = 0; i < dists.length; i++) {
			for (int j = i+1; j < dists.length; j++) {
				if (dists[i].compareTo(dists[j]) > 0) {
					temp = dists[i];
					dists[i] = dists[j];
					dists[j] = temp;
				}
			}
			System.out.println(dists[i]);
		}
	}
	
	public String changeUppercase(String dists) {
		String temp;
		for (int i = 0; i < dists.length(); i+=2) {
			temp = dists.charAt(i);
			dists.setCharAt(i, temp.toUppercase());
		}
		return dists;
	}
	
	public static void main(String[] args) {
		String[] arr = new String[]{ "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
		String changedString;
		String temp;
		sortString(arr);
		for (int i = 0; i < arr.length; i++) {
			temp = arr[i].toLowercase();
			changedString = changeUppercase(temp);
			arr[i] = changedString;
			System.out.println(arr[i]);
		}
	}
}
			
			
			
			
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		