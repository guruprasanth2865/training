set path = C:\Program Files\Java\jdk1.8.0_261\bin

javac -d . *.java

java com.kpriet.training.java.string.SubStrDemo
java com.kpriet.training.java.string.ReferDemo
java com.kpriet.training.java.string.ReferanceDemo
java com.kpriet.training.java.string.NameInitial
java com.kpriet.training.java.string.District
java com.kpriet.training.java.string.ConcatDemo
java com.kpriet.training.java.string.ComputeResult
java com.kpriet.training.java.string.CapacityCounter

pause