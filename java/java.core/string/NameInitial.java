/*
Requirement:
    + Write a program that computes your initials from your full name and displays them.
	
Entities:
    NameInitial
	
Function Declaration:
    public static void main(String[] args) 
	
Job to be done:
    - Create class NameInitial
	- Create a static function - initialValue to get the name and return only the initial from that string
	- Create main method 
	- Declare a string - name
	- get input from user
	- Invoke function initialValue(name)
	- Print the returned value

Explanation:
    Mention the initial at first ->
    S.Guru Prasanth
    Initial is S
	
	
	Mention the initial at first ->
    Ramu
    No Initial specified
*/

package com.kpriet.training.java.string;

import java.util.Scanner;

class NameInitial {
    
	public static String initialValue(String name) {
	    if (name.indexOf('.') > 0) {
		    return "Initial is " + name.substring(0, name.indexOf('.'));
		} else {
		    return "No Initial specified";
		}
	}
	
	public static void main(String[] args) {
	    Scanner sc = new Scanner(System.in);
		System.out.println("Mention the initial at first -> ");
	    String name = sc.nextLine();
		System.out.println(initialValue(name));
	}
}
		
		
		
		
		
		
		