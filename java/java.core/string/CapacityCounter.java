/*
Requirement:
    + What is the initial capacity of the following string builder?
    StringBuilder sb = new StringBuilder("Able was I ere I saw Elba.");
	
Entities:
    CapacityCounter
	
Function Declaration:
    public static void main()
	capacity()
	
Job to be done:
    - Create a class CapacityCounter
	- Create a object word of type StringBuilder  
	- Print the capacity of the object - word using function capacity()
	
Explanation:
    - 16
	
*/
package com.kpriet.training.java.string;

class CapacityCounter {
	public static void main(String[] args) {
		StringBuilder word = new StringBuilder();
		System.out.println(word.capacity());
	}
}