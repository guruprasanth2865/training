/*
Requirement:
    + Show two ways to concatenate the following two strings together to get the string "Hi, mom.":
        String hi = "Hi, ";
        String mom = "mom.";
		
Entities:
    ConcatDemo
	
Function Declaration:
    public static void main(String[] args)
	
Job to be done:
    - Create a class ConcatDemo
	- Create a function - symbolConcat(String str1, String str2) to get two strings and return their concatinated  string using + operator
	- Create a function - functionConcat(String str1, String str2)  to get two strings and return their concatinated  string using concat() method
	- Create a main method in that class
	- Create 2 string varibles.
	
Explanation: 
    Concatination using '+' operator = Hi, mom.
    Concatination using concat() method = Hi, mom.
*/

package com.kpriet.training.java.string;

class ConcatDemo {
    
	public static String symbolConcat(String str1, String str2) {
	    return str1 + str2;
	}
	
	public static String functionConcat(String str1, String str2) {
	    return str1.concat(str2);
	}
	
	public static void main(String[] args) {
	    String str1 = "Hi, ";
		String str2 = "mom.";
		System.out.println("Concatination using '+' operator = " + symbolConcat(str1, str2));
		System.out.println("Concatination using concat() method = " + functionConcat(str1, str2));
	}
}
		
		
		
		
		
		
		