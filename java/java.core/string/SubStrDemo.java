/*
Requirement:
    + How long is the string returned by the following expression? What is the string?
      "Was it a car or a cat I saw?".substring(9, 12)
	  
Entities:
    SubStrDemo
	
Function Declaration:
    public static void main(String[] args)
	
Job to be done:
    - Create class SubStrDemo
	- Declare a string - statement with the given value
	- execute the method substring() and print their returned value and their count
	
Expanation:
statement.substring(9, 12)
Returned value  = car
Length of returned value = 3

*/

package com.kpriet.training.java.string;

class SubStrDemo {
    
	public static void main(String[] args) {
		String statement = "Was it a car or a cat I saw?";
		System.out.println("statement.substring(9, 12)\nReturned value  = " + statement.substring(9, 12) + "\nLength of returned value = " + statement.substring(9, 12).length());
	}
}