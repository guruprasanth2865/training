/*
Requirement:
    + Consider the following string:
    String hannah = "Did Hannah see bees? Hannah did.";
    - What is the value displayed by the expression hannah.length()?
    - What is the value returned by the method call hannah.charAt(12)?
    - Write an expression that refers to the letter b in the string referred to by hannah.
	
Entities:
    ReferDemo
	
Function Declaration:
    public static void main(String[] args)
	
Job to be done:
    - Create a class ReferDemo
    - Create a string statement
    - Print the value of the expression displayed by - statement.length() 	
    - Print the value of the expression returned by - statement.charAt(12)
	- Write a expression to refer to the letter 'b' from - statement
	
Explanation:
    32
	e 
    Statement for refering b is - statement.charAt(statement.indexOf('b'))	
*/

package com.kpriet.training.java.string;

class ReferDemo {
	
	public static void main(String[] args) {
		String statement = "Did Hannah see bees? Hannah did.";
		System.out.println(statement.length());
		System.out.println(statement.charAt(12));
		System.out.println("Statement for refering "+ statement.charAt(statement.indexOf('b')) + " is - statement.charAt(statement.indexOf('b'))");
	}
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	