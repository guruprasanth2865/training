/*
Requirements:
    + What is the output from the following code:
	
Entities:
    IdentifyMyParts
	
Function Declaration:
    public static void main(String[] args)
	
Job to be done:
    - Create class IdentifyMyParts
	- Declare a static variable x of type int.
	- Declare a instance variable of type int.
	- Declare and define a main method.
	- Create a object obj1 of type IdentifyMyParts
    - Create another object obj2 of	type IdentifyMyParts
	- Print the values of static and instance variables of objects obj1 and obj2
Explanation :

    Output:-
    obj1.y = 5
    obj2.y = 6
    obj1.x = 2
    obj2.x = 2
    IdentifyMyParts.x = 2
*/

package com.kpriet.training.java.classObjects;

public class IdentifyMyParts {
    
	public static int x = 7;
    public int y = 3;
    
	public static void main(String args[]){
        IdentifyMyParts obj1 = new IdentifyMyParts();
        IdentifyMyParts obj2 = new IdentifyMyParts();
        obj1.y = 5;
        obj2.y = 6;
        obj1.x = 1;
        obj2.x = 2;
        System.out.println("obj1.y = " + obj1.y);
        System.out.println("obj2.y = " + obj2.y);
        System.out.println("obj1.x = " + obj1.x);
        System.out.println("obj2.x = " + obj2.x);
        System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);
    }
}




    