/*	
Requirements:
    + What's wrong with the following program? And fix it.

    public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }
	
Entities:
    Rectangle 
	
Function Declaration:
    public static void main(String[] args)
    public int area()
	
Job to be done:
    - Create class
    - Declare instance variable instde class
    - Declare method to get two parameters and rturn their product
    - In main methos, create a object and set the values of the instance variables of oblect
    - Invoke function with arguments and print the returned value.

*/

package com.kpriet.training.java.classObjects;

public class Rectangle { //Class Rectangle
    
    public int width; //Initialized variable width 
    public int height;  //Initialized variable height

    public int area(int width,int height) { //Method declared with parameters
        return width*height; // return product of width and height
    }

    public static void main(String[] args) {
        Rectangle myRect = new Rectangle(); //Create object myRect 
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area(myRect.width,myRect.height)); // Invoke method - area()
    }
}
