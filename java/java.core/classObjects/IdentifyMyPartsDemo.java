/*
Requirements:
    + Consider the following class:
    public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }
    - What are the class variables?
    - What are the instance variables?
	
Entities:
    IdentifyMyParts
	
Function Declaration:


Job to be done:
    - Create class IdentifyMyParts
	- Declare a static variable and instance variable
	
Explanation :
    - What are the class variables?
        Class variables - Class variables are the variablres in a class declared with static modifier. 
        A class variable have only a single copy, regardless of instance of classes(objects) created 


    - What are the instance variables?
        Instance variables - Instance vartiables are the variables created in the individually in the instance of classes.
        These variables are not declared with static modifier.
*/

package com.kpriet.training.java.classObjects;

public class IdentifyMyPartsDemo {
    public static int x = 7;
    public int y = 3;
}

