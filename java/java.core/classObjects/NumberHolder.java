/*
Requirements:
    + Given the following class, called NumberHolder,
    write some code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.
    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }

Entities:
    NumberHolder
	
Function Declaration:
    public void display(int anInt,float aFloat)
	public static void main(String args[])
	
Job to be done:
    - Create class NumberHolder
    - Declare instance variable - anInt amd aFloat, inside class
    - Declare method display(int anInt,float aFloat) to get two parameters and return their product
    - In main methos, create a object and set the values to their instances
    - Invoke function display(int anInt,float aFloat) with arguments and print the returned value.

*/

package com.kpriet.training.java.classObjects;

public class NumberHolder {

    public int anInt;
    public float aFloat;

    public void display(int anInt,float aFloat) {
        System.out.println("anInt value = " + anInt);
		System.out.println("aFloat value = " + aFloat);
    }

    public static void main(String args[]) {
        NumberHolder numberHoldOne = new NumberHolder();
        numberHoldOne.anInt = 10;
        numberHoldOne.aFloat = 8.3f;
        numberHoldOne.display(numberHoldOne.anInt,numberHoldOne.aFloat);
    }
}

