import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

class SetDemo {
	
	public static void main(String[] args) {
		//Create set without genrics
        //Set cars = new HashSet();	
        Set<String> cars = new HashSet<>();
		Set<String> oldCars = new HashSet<>();
		Set<String> newCars = new HashSet<>();
		

        //Add elements in set - add() method
        cars.add("Lamboghini");
        cars.add("Ford");
		cars.add("Mahindra");
		cars.add("GMC");
		cars.add("AstonMatin");
		cars.add("Ferrari");
		cars.add("Benz");
		cars.add("Honda");
		cars.add("BMW");
 
        /* Set can be Iterated by : 
		    * Using Iterator 
            * Using for-each loop.*/ 
			
		System.out.println("\nIteration using Iterator : ");
		Iterator traverse = cars.iterator();
		while(traverse.hasNext()) {
			System.out.println(traverse.next());
		}
		
		System.out.println("\nIteration using For Loop");
		for( String car : cars) {
			System.out.println(car);
		}
		
		cars.remove("Ferrari");
		System.out.println("\nAfter removing \"Ferrari\" from cars : ");
		for( String car : cars ) {
		    System.out.println(car);	
		}
		
		//Remove All Elements From Set - cars.clear()
		
		//Check if set is empty isEmpty()
		if(cars.isEmpty()) {
			System.out.println("\nCars set is empty");
		} else {
			//Size of set - size() method
			System.out.println("\nCars size is - " + cars.size());
		}
		
		//Check if set contains a elements
		if(cars.contains("Lamboghini")) {
			System.out.println("\nCars set contains - \"Lamboghini\"");
		} else {
			System.out.println("\nCars set dont contain - \"Lamboghini\"");
		}
		
		//removeAll()
		oldCars.add("Mahindra");
		oldCars.add("Ford");
		
		cars.removeAll(oldCars);
		
		System.out.println("\nAfter removeAll() method");
		for(String car : cars) {
			System.out.println(car);
		}
		
		//retainAll()
		newCars.add("Lamboghini");
		newCars.add("GMC");
		
		cars.retainAll(newCars);
		
		System.out.println("\nAfter retainAll() method");
		for(String car : cars) {
			System.out.println(car);
		}
	}
}
