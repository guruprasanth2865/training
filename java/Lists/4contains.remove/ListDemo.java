import java.util.List;
import java.util.ArrayList;

class ListDemo {
	
	public static void main(String[] args) {
		//Create List - dogs
		List<String> dogs = new ArrayList<>(); 
		boolean present;
		
		//Insert Elements in a Java List - add()
		dogs.add("Golden Retriver");
		dogs.add("Doberman Pincher");
		dogs.add("German Shepherd");
		// dogs[ "Golden Retriver" , "Doberman Pincher" , "German Shepherd" ]
		
		//check whether list contains element - contains()
		System.out.println("Contains() - method");
		present = dogs.contains("Old English Bulldog");
		if(present == true) {
			System.out.println("Bulldog is Present");
		} else {
			System.out.println("Bulldog is Extinct");
		}
		
		System.out.println("\nBefore Removing : ");
		for(String dog : dogs) {
			System.out.println(dog);
		}
		
		System.out.println("\nRemove method()");
        //Remove element from dogs - remove()
		dogs.remove(0); // or dogs.remove("Golden Retriver);
		
		System.out.println("\nAfter Removing one element : "); 
        for(String dog: dogs) {
			System.out.println(dog);
		}	
		
		dogs.clear();
		System.out.println("\nAfter Clearing, length of List is : " + dogs.size());
	}
}