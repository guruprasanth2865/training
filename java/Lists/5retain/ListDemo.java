import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.lang.Comparable;

class ListDemo {
	
	public static void main(String[] args) {
		//Create List - dogs
		List<String> dogs = new ArrayList<>(); 
		List<String> newDogs = new ArrayList<>();
		List<String> oldDogs = new ArrayList<>();
		
		//Insert Elements in a Java List - add()
		dogs.add("Golden Retriver");
		dogs.add("Doberman Pincher");
		dogs.add("German Shepherd");
		dogs.add("Pomeranian");
		dogs.add("Bulldog");
		/* dogs[ "Golden Retriver" , "Doberman Pincher" , "German Shepherd", "Pomeranian", "Bulldog" ]*/
		
		newDogs.add("Golden Retriver");
		newDogs.add("Doberman Pincher");
		newDogs.add("German Shepherd");
		newDogs.add("Pomeranian");
		/*dogs[ "Golden Retriver", "Doberman Pincher", "German Shepherd", "Pomeranian"] */
		
		// retainAll() method
		dogs.retainAll(newDogs);
		
		System.out.println("Retained breeds : ");
		for(String dog : dogs) {
			System.out.println(dog);
		}
		
		//Sublist of List
		oldDogs = dogs.subList(1, 3);
		
		System.out.println("\nValues in List - oldDogs are ");
		for(String dog : oldDogs) {
			System.out.println(dog);
		}
		
		//sort list using sort() method
		Collections.sort(dogs);
		System.out.println("\nSorted List - dogs :");
		for(String dog : dogs) {
			System.out.println(dog);
		}
	}
}