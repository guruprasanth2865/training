import java.util.List;
import java.util.ArrayList;

class ListDemo {
	
	public static void main(String[] args) {
		//Create List - dogs
		List<String> dogs = new ArrayList<>(); 
		
		//Insert Elements in a Java List - add()
		dogs.add("Golden Retriver");
		
		//Adding null values also possile
		//dogs.add(null);
		
		dogs.add(1, "Doberman Pincher"); //same as dogs.add("Doberman Pincher")
		
		try {
			dogs.add(5, "German Shepherd");
		} catch (IndexOutOfBoundsException err) {
			System.out.println("dog.add(5, \"German Shepherd\") throws error - Index is out of range");
		}
		
		System.out.println("Elements in list are : ");
		for(String dog : dogs) {
			System.out.println(dog);
		}
	}
}
		