/*

java.util.ArrayList
java.util.LinkedList
java.util.Vector
java.util.Stack

List listA = new ArrayList();
List listB = new LinkedList();
List listC = new Vector();
List listD = new Stack();

*/

import java.util.List;
import java.util.ArrayList;

public class ListDemo {
    
	public static void main(String[] args) {
		
	    //List cars = new ArrayList();  
        //list.add(new String("First String"));		
		
		//Create list
		List<String> cars = new ArrayList<>();
		List<String> newCars = new ArrayList<>();
		Set<String> uniqueCars = new HashSet();

        //add
        cars.add("Swift");
        cars.add("Ambasidor");
        cars.add("BMW");
		//cars.add(null);
		//add index
        cars.add(3, "Ferrari");
		
		
        newCars.addAll(cars);
        
        //get value
		String element = cars.get(0);
		System.out.println(element);
		
		//Find elements
		//indexOf()
        //lastIndexOf()
		
		int location = cars.indexOf("BMW");
		System.out.println("Location of BMW is " + location);
		
		//Check if contains
		if (cars.contains("BMW")) {
			System.out.println("Cars List contains BMW");
		}
		
		//Remove
		cars.remove("BMW");
		
		//Remove with index
		//cars.remove(0);
		
		
		//Clear list 
		//list.clear();
		
		//Retain all
		cars.retainAll(newCars);
		
		//convert list to set
		uniqueCars.addAll(cars);
		
		
		//Convert list to array
		String[] repairedCars = cars.toArray(new String[0]);
		
		
		//Convert Array to string
		List<String> oldCars = (List<String>)Array.asList(repairedCars);
		
		//Size 
		System.out.println("Total number of cars are : " + cars.size());

        for(String car : cars){
            String carBrand = (String) car;
			System.out.println("Car Brand is : " + carBrand);
		}
	}
}