import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;

class Converter {
	
	public static void main(String[] args) {
        List<String> cars = new ArrayList<>();
        Set<String> uniqueCars = new HashSet<>();
		String[] allCars;
		List<String> oldCars = new ArrayList<>();
		
		//Insert elements in List
		cars.add("Honda");
		cars.add("BMW");
		cars.add("Renault");
		
		//Convert list to set
		uniqueCars.addAll(cars);
		
		System.out.println("After converting List to Set : ");
		for(String car : cars) {
		    System.out.println(car);	
		}
		
		//Convert list to array
		allCars = cars.toArray(new String[0]);
		System.out.println("\nList is converted to array : ");
		for( int i = 0; i < allCars.length; i++) {
		    System.out.println(allCars[i]);	
		}
		
		//convert this array back to list
		oldCars = (List<String>) Arrays.asList(allCars);
        System.out.println("\nArray is converted to list : ");	
        for( String car : cars ) {
            System.out.println(car);
        }	

        //Clear cars
        cars.clear();
        System.out.println("cars is cleared");	

        //Convert set- uniqueCars to List-cars
        cars.addAll(uniqueCars);
        System.out.println("\nSet = \"uniqueCars\" is converted to List");
        for(String car : cars) {
			System.out.println(car);
		}
	}
}