import java.util.List;
import java.util.ArrayList;

class ListDemo {
	
	public static void main(String[] args) {
		//Create List - dogs
		List<String> dogs = new ArrayList<>(); 
		//Create list - newDogs
		List<String> newDogs = new ArrayList<>();
		
		//Insert Elements in a Java List - add()
		dogs.add("Golden Retriver");
		dogs.add("Doberman Pincher");
		dogs.add("German Shepherd");
		// dogs[ "Golden Retriver" , "Doberman Pincher" , "German Shepherd" ]
		
		//Insert All Elements From One List Into Another
		newDogs.addAll(dogs);
		//  newDogs[ "Golden Retriver" , "Doberman Pincher" , "German Shepherd" ]
		
		//Print the values after inserting
		System.out.println("After inserting \nnewDogs List : ");
		for(String dog: dogs) {
			System.out.println(dog);
		}
		
		//Get Element in List - get()
		System.out.println("\nget() method : \nElement at index-0 is " + newDogs.get(0));
		
		/*Get element from the index values
	    You can get it with the use of two methods
		   * indexOf()
		   * lastIndexOf()
		For this another "GOlden Retriver" is added in the list-dogs */
		System.out.println("\nGet element from index : ");
		dogs.add("Golden Retriver");
		/* dogs[ "Golden Retriver" , "Doberman Pincher" , "German Shepherd", "Golden Retriver"  ]*/
		System.out.println("index of \"Golden Retriver\" from first - " + dogs.indexOf("Golden Retriver"));
		System.out.println("index of \"Golden Retriver\" from last - " + dogs.lastIndexOf("Golden Retriver"));
	}
}