import java.util.List;
import java.util.ArrayList;

public class ListDemo {
	
	public static void main(String[] args) {
		//Create a list without object specifier
		//List cars = new ArrayList()
		//Type Casting problem
		
		//Create a Generic list 
		// List<MyObject> list = new ArrayList<MyObject>();
		List<String> cars = new ArrayList<>();                                      
		
		//Print statement
		System.out.println("List named cars is created");
	}
}