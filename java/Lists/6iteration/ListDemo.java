import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.*;

class ListDemo {
	
	public static void main(String[] args) {
		//Create List - dogs
		List<String> dogs = new ArrayList<>(); 
		Iterator<String> iterator;
		Stream<String> stream;
		
		
		//Insert Elements in a Java List - add()
		dogs.add("Golden Retriver");
		dogs.add("Doberman Pincher");
		dogs.add("German Shepherd");
		dogs.add("Pomeranian");
		dogs.add("Bulldog");
		
		iterator = dogs.iterator();
		System.out.println("Iteration using Iterator : ");
		while(iterator.hasNext()) {
			String str = iterator.next();
			System.out.println(str);
		}
		
		System.out.println("\nIteration using forEach loop : ");
		for(String dog : dogs) {
			System.out.println(dog);
		}
		
		System.out.println("\nIteration using For loop");
		for( int i = 0; i < dogs.size(); i++) {
			System.out.println(dogs.get(i));
		}
		
		//Done using stream() method.
		System.out.println("\nIteration Using Stream API");
		stream = dogs.stream();
		stream.forEach(element -> {
			System.out.println(element);
		});
	}
}