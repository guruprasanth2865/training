/*
Requiremetns:
    - Create a File, get the Following fields as input from the user 
    1.Name
    2.studentId
    3.Address
    4.Phone Number
    now store the input in the File and serialize it, and again de serialize the File and print the 
    content. 
    
Entities:
    - SerializeDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Crate a class Student and declare variables
    - Create a class SerializeDemo 
    - Declare and define main function init
    - Create a Student and set values to it from reading a .ser file
    - Print the values from object-student
    
Psudocode:
public class DeserializeDemo {

    public static void main(String[] args) {
    	Student student = null;
    	try {
    		ObjectInputStream file = new ObjectInputStream(new FileInputStream("E:\\test\\student.ser"));
    		student = (Student) file.readObject();
    		file.close();
    	} catch (IOException e) { 
    		e.printStackTrace();
    	} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
    	System.out.println("Decentralization executes ");
	    System.out.println("name : " + student.name);
	    System.out.println("id : " + student.studentId);
	    System.out.println("sddress : " + student.address);
	    System.out.println("phone : " + student.phonenumber);
    }
}

 
 * */

package serialization;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializeDemo {

    public static void main(String[] args) {
    	Student student = null;
    	try {
    		ObjectInputStream file = new ObjectInputStream(new FileInputStream("E:\\test\\student.ser"));
    		student = (Student) file.readObject();
    		file.close();
    	} catch (IOException e) { 
    		e.printStackTrace();
    	} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
    	System.out.println("Decentralization executes ");
	    System.out.println("name : " + student.name);
	    System.out.println("id : " + student.studentId);
	    System.out.println("sddress : " + student.address);
	    System.out.println("phone : " + student.phonenumber);
    }
}
