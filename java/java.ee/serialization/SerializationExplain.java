/*
Requirements:
    - What is serialization? 
    - What is need of serialization?
    - What will happen if one of the member in the class doesn�t implement serializable interface?
    
What is serialization? 
    - Serialization in Java is the process of converting the Java code Object into a Byte Stream, 
      to transfer the Object Code from one Java Virtual machine to another and recreate it using 
      the process of Deserialization.    
      
What is need of serialization?
    - We need Serialization for the following reasons:
        Communication: Serialization involves the procedure of object serialization and transmission. 
        This enables multiple computer systems to design, share and execute objects simultaneously.

        Caching: The time consumed in building an object is more compared to the time required for de-serializing it. 
        Serialization minimizes time consumption by caching the giant objects.

        Deep Copy: Cloning process is made simple by using Serialization. 
        An exact replica of an object is obtained by serializing the object to a byte array, and then de-serializing it.

        Cross JVM Synchronization: The major advantage of Serialization is that it works across different JVMs that 
        might be running on different architectures or Operating Systems
        
What will happen if one of the member in the class doesn�t implement serializable interface?
    - When a object is serialized which implements serializable interface, if the object referances too a non-serializable interface,
      it throws a NotSerializableException 

Persistence: The State of any object can be directly stored by applying Serialization on to it and stored in a database so that it can be retrieved later.
 
 * */

package serialization;

public class SerializationExplain {

}
