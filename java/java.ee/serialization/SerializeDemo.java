/*
Requirements:
    - Create a File, get the Following fields as input from the user 
    1.Name
    2.studentId
    3.Address
    4.Phone Number
    now store the input in the File and serialize it, and again de serialize the File and print the 
    content. 
    
Entities:
    - SerializeDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Crate a class Student and declare variables
    - Create class SerizlizeDemo
    - Declare and define main method
    - Create a object of type Student and set values
    - Write the object student in a file of type-.ser with ObjectOutputStream and FileOutputStream

Psudocode:
public class SerializeDemo {

    public static void main(String[] args) {
    	Scanner sc = new Scanner(System.in);
    	Student student = new Student();
    	System.out.println("Enter Student name : ");
    	student.name = sc.next();
    	System.out.println("Enter Student Id : ");
    	student.studentId = sc.next();
    	System.out.println("Enter Student Address : ");
    	student.address = sc.next();
    	System.out.println("Enter Student Number : ");
    	student.phonenumber = sc.nextLong();
    	try {
    		ObjectOutputStream fileWriter = new ObjectOutputStream(new FileOutputStream("E:\\test\\student.ser"));
    		fileWriter.writeObject(student);
    		fileWriter.close();
	        System.out.println("Data saved in student.ser file ...");
    	} catch (IOException i) {
	        i.printStackTrace();
	    }
    }
}
 
 * */

package serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class SerializeDemo {

    public static void main(String[] args) {
    	Scanner sc = new Scanner(System.in);
    	Student student = new Student();
    	System.out.println("Enter Student name : ");
    	student.name = sc.next();
    	System.out.println("Enter Student Id : ");
    	student.studentId = sc.next();
    	System.out.println("Enter Student Address : ");
    	student.address = sc.next();
    	System.out.println("Enter Student Number : ");
    	student.phonenumber = sc.nextLong();
    	try {
    		ObjectOutputStream fileWriter = new ObjectOutputStream(new FileOutputStream("E:\\test\\student.ser"));
    		fileWriter.writeObject(student);
    		fileWriter.close();
	        System.out.println("Data saved in student.ser file ...");
    	} catch (IOException i) {
	        i.printStackTrace();
	    }
    }
}
