/*
Requirements:
    - 2. To print the following pattern of the Date and Time using SimpleDateFormat.
      "yyyy-MM-dd HH:mm:ssZ" 
      
Entities:
    - PatternDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class PatternDemo
    - Declare and define main method
    - Create a object of type SimpleDateFormat with pattern
    - Print the date with format() method
    
Psudocode:
public class PatternDemo {

    public static void main(String[] args) {
    	SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
    	System.out.println(date.format(new Date()));
    }
}

 
 * */


package internationalization;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PatternDemo {

    public static void main(String[] args) {
    	SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
    	System.out.println(date.format(new Date()));
    }
}
