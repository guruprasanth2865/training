/*
Requirements:
    - 1. To print the following Date Formats in Date using locale.
        DEFAULT, MEDIUM, LONG, SHORT, FULL. 
        
Entities:
    - DateFormatDemo
    
Function Declaration:
    - public static void main(String[] args)

Job to be done:
    - Create class DateFormatDemo
    - Declare and define main method
    - Create s object of type-Date
    - Create Locale class to get specified language and country time
    - Use DateFormat class and its getTimeInstance() method and pass parameters 
      => DEFAULT
      => MEDIUM
      => LONG
      => SHORT
      => FULL
     and print date formats
     
Psudocode:
public class DateFormatDemo {

    public static void main(String[] args) {
    	Date date = new Date();
    	System.out.println("Current Date : " + date);
    	Locale locale = new Locale("English", "IN");
    	System.out.println("Date format with DEFAULT : " + DateFormat.getTimeInstance(DateFormat.DEFAULT, locale).format(date));
    	System.out.println("Date format with MEDIUM : " + DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(date));
    	System.out.println("Date format with LONG : " + DateFormat.getDateInstance(DateFormat.LONG, locale).format(date));
    	System.out.println("Date Format with SHORT : " + DateFormat.getDateInstance(DateFormat.SHORT, locale).format(date));
    	System.out.println("Date format with FULL : " + DateFormat.getTimeInstance(DateFormat.FULL, locale).format(date));
    }
}

     
 * */


package internationalization;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatDemo {

    public static void main(String[] args) {
    	Date date = new Date();
    	System.out.println("Current Date : " + date);
    	Locale locale = new Locale("English", "IN");
    	System.out.println("Date format with DEFAULT : " + DateFormat.getTimeInstance(DateFormat.DEFAULT, locale).format(date));
    	System.out.println("Date format with MEDIUM : " + DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(date));
    	System.out.println("Date format with LONG : " + DateFormat.getDateInstance(DateFormat.LONG, locale).format(date));
    	System.out.println("Date Format with SHORT : " + DateFormat.getDateInstance(DateFormat.SHORT, locale).format(date));
    	System.out.println("Date format with FULL : " + DateFormat.getTimeInstance(DateFormat.FULL, locale).format(date));
    }
}
