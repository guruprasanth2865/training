/*
Requirements:
    - 1.Write a code to change the number format to Denmark number format .
    
Entities:
    - NumberFormatDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class NumberFormatDemo
    - Declare and define main method
    - Invoke getInstance() method with parameter as a new Locale("Denmark") and print it with format() method. 

Psudocode:
public class NumberFormatDemo {

    public static void main(String[] args) {
    	System.out.println("Denmark : " + NumberFormat.getInstance(new Locale("Denmark")).format(6749312));
    }
}

 
 * */

package internationalization;

import java.text.NumberFormat;
import java.util.Locale;

public class NumberFormatDemo {

    public static void main(String[] args) {
    	System.out.println("Denmark : " + NumberFormat.getInstance(new Locale("Denmark")).format(6749312));
    }
}
