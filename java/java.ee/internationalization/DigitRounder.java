/*
Requirements:
     - 2.Write a code to rounding the value to maximum of 3 digits by setting maximum and minimum Fraction digits.
     
Entities:
    - DigitRounder
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class DigitRounder
    - Declare and deploy main method
    - Create a object of type NumberFormat and set maximum value with method setMaximumFractionDigits()
    - Print the formatted number with format() method
    
Psudocode:
public class DigitRounder {

    public static void main(String[] args) {
    	NumberFormat value = NumberFormat.getInstance();
    	value.setMaximumFractionDigits(3);
    	value.setMinimumFractionDigits(0);
    	System.out.println(value.format(451.32654f));
    }
}

 
 * */

package internationalization;

import java.text.NumberFormat;

public class DigitRounder {

    public static void main(String[] args) {
    	NumberFormat value = NumberFormat.getInstance();
    	value.setMaximumFractionDigits(3);
    	value.setMinimumFractionDigits(0);
    	System.out.println(value.format(451.32654f));
    }
}
