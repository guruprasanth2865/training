/*
Requirements:
    - 2) Write a program to add 5 elements to a xml file and print the elements in the xml file using list. 
         Remove the 3rd element from the xml file and print the xml file.
 
Entities:
    - PropertyXmlDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create  class PropertyXmlDemo
    - Declare and define main method
    - Create a object of type-Property and add 5 values to it
    - Create a object of type-FileOutputStream for a xml file
    - Use storeToXML() method to store the property to the xml file
    - Read the xml file with FileInputStream
    - Create a new object of type Properties and store the content from xml file to this property
    - Convert those properties-object to a list and print it.

Psudocode:
class User{
	String key;
	String value;
}

public class PropertyXmlDemo {

    public static void main(String[] args) {
    	Properties user = new Properties();
    	Properties newUser = new Properties();
    	List<User> listUser = new ArrayList<>();
    	user.setProperty("first_name", "Guru");
    	user.setProperty("last_name", "Prasanth");
    	user.setProperty("age", "20");
    	user.setProperty("Degree", "B.E - ECE");
    	user.setProperty("year", "3");
    	try {
			FileOutputStream writeFile = new FileOutputStream("E:\\test\\user.xml");
			user.storeToXML(writeFile, "User Details");
			writeFile.flush();
			writeFile.close();
			FileInputStream readFile = new FileInputStream("E:\\test\\user.xml");
			newUser.loadFromXML(readFile);
			newUser.forEach((key, value) -> {
				User temp = new User();
				temp.key = (String) key;
				temp.value = (String) value;
				listUser.add(temp);
			});
			for(User usr : listUser) {
				System.out.println(usr.key + " - " + usr.value);
			}
			newUser.remove("age");
			System.out.println("\nafter removing 3rd element : ");
			newUser.entrySet().stream().forEach(System.out::println);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
}

 
 * */

package properties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

class User{
	String key;
	String value;
}

public class PropertyXmlDemo {

    public static void main(String[] args) {
    	Properties user = new Properties();
    	Properties newUser = new Properties();
    	List<User> listUser = new ArrayList<>();
    	user.setProperty("first_name", "Guru");
    	user.setProperty("last_name", "Prasanth");
    	user.setProperty("age", "20");
    	user.setProperty("Degree", "B.E - ECE");
    	user.setProperty("year", "3");
    	try {
			FileOutputStream writeFile = new FileOutputStream("E:\\test\\user.xml");
			user.storeToXML(writeFile, "User Details");
			writeFile.flush();
			writeFile.close();
			FileInputStream readFile = new FileInputStream("E:\\test\\user.xml");
			newUser.loadFromXML(readFile);
			newUser.forEach((key, value) -> {
				User temp = new User();
				temp.key = (String) key;
				temp.value = (String) value;
				listUser.add(temp);
			});
			for(User usr : listUser) {
				System.out.println(usr.key + " - " + usr.value);
			}
			newUser.remove("age");
			System.out.println("\nafter removing 3rd element : ");
			newUser.entrySet().stream().forEach(System.out::println);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
}
