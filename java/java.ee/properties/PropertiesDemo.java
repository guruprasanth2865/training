/*
Requirements:
    - 1.Write a program to perform the following operations in Properties.
    i) add some elements to the properties file.
    ii) print all the elements in the properties file using iterator.
    iii) print all the elements in the properties file using list method.
    
Entities:
    - PropertiesDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class PropertiesDemo
    - Declare and define main method
    - Create a object of type-Properties
    - Set some properties to it
    - Print the elements in Properties with Iterator
    - Print the Properties using list method
    
Psudocode:
public class PropertiesDemo {

    public static void main(String[] args) {
    	Properties user = new Properties();
    	user.setProperty("first_name", "Guru");
    	user.setProperty("last_name", "Prasanth");
    	user.setProperty("language", "Tamil");
    	Iterator iterator = user.keySet().iterator();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            System.out.println(key + " : " + user.get(key));
        }
        user.entrySet().stream().forEach(System.out::println);
    }
}

    
 * */

package properties;

import java.util.Iterator;
import java.util.Properties;

public class PropertiesDemo {

    public static void main(String[] args) {
    	Properties user = new Properties();
    	user.setProperty("first_name", "Guru");
    	user.setProperty("last_name", "Prasanth");
    	user.setProperty("language", "Tamil");
    	Iterator iterator = user.keySet().iterator();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            System.out.println(key + " : " + user.get(key));
        }
        user.entrySet().stream().forEach(System.out::println);
    }
}
