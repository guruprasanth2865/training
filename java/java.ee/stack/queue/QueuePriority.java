/*
Requirement:
    * Create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
        -> add atleast 5 elements
        -> remove the front element
        -> search a element in stack using contains key word and print boolean value value
        -> print the size of stack
        -> print the elements using Stream 
        
Entities:
    QueueLinkedList
    
Function Declaration:
    public static void main(String[] args)
    
Job to be done:
    -> Create class QueueLinkedList
    -> Create main function
    -> Add five elements
    -> Remove the peek element from queue
    -> Use Stream API to print the element
    
Pseudocode:
public class QueuePriority {

    public static void main(String[] args) {
        Queue<String> cars = new PriorityQueue<>();
        Stream<String> stream = cars.stream();
        String checkValue;
        cars.add("BMW");
        cars.add("Tesla");
        cars.add("Honda");
        cars.add("Benz");
        cars.add("Suzuki");
        cars.add("Mahindra");
        cars.add("Hummer");
        checkValue = cars.contains("BMW")==true ? "Cars contains BMW" : "Cars does not contains BMW";
        System.out.println(checkValue);
        System.out.println("Size of cars is " + cars.size());
        stream.forEach((element) -> {
        	System.out.println(element);
        });
    }
}
    
 * */


package stack.queue;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.*;

public class QueuePriority {

    public static void main(String[] args) {
        Queue<String> cars = new PriorityQueue<>();
        Stream<String> stream = cars.stream();
        String checkValue;
        cars.add("BMW");
        cars.add("Tesla");
        cars.add("Honda");
        cars.add("Benz");
        cars.add("Suzuki");
        cars.add("Mahindra");
        cars.add("Hummer");
        checkValue = cars.contains("BMW") == true ? "Cars contains BMW" : "Cars does not contains BMW";
        System.out.println(checkValue);
        System.out.println("Size of cars is " + cars.size());
        stream.forEach((element) -> {
        	System.out.println(element);
        });
    }
}
/*
 Output:
 Cars contains BMW
 Size of cars is 7
 BMW
 Tesla
 Honda
 Benz
 Suzuki
 Mahindra
 Hummer
 * */
