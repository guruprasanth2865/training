/*
Requirements:
* Create a stack using generic type and implement
  -> Push at least 5 elements
  -> Pop the peek element
  -> search a element in stack and print index value
  -> print the size of stack
  -> print the elements using Stream
  
Entities:
  StackDemo
  
Function Declaration:
  public static void main(String[] args)
  
Job to be done:
  -> Create a class StckDemo
  -> Create main function
  -> Push five strings in a stack
  -> Pop the element with pop() method
  -> Search a element with search() method
  -> Print the size of stack
  -> Use stream API to print the elements
  
  
Pseudocode :
public class StackDemo {
    
	public static void main(String[] args) {
		Stack<String> cars = new Stack<>();
		Stream<String> stream = cars.stream();
		cars.push("BMW");
		cars.push("Benz");
		cars.push("Honda");
		cars.push("Tesla");
		cars.push("Honda");
		cars.pop();
		int index = cars.search("BMW");
		int size = cars.size();
		System.out.println("Size is " + size);
		System.out.println("Index of BMW is " + index);
		stream.forEach((element) -> {
			System.out.println(element);
		});
	}
}
  
  */


package stack.queue;

import java.util.Stack;
import java.util.stream.*;

public class StackDemo {
    
	public static void main(String[] args) {
		Stack<String> cars = new Stack<>();
		Stream<String> stream = cars.stream();
		cars.push("BMW");
		cars.push("Benz");
		cars.push("Honda");
		cars.push("Tesla");
		cars.push("Honda");
		cars.pop();
		int index = cars.search("BMW");
		int size = cars.size();
		System.out.println("Size is " + size);
		System.out.println("Index of BMW is " + index);
		stream.forEach((element) -> {
			System.out.println(element);
		});
	}
}

/*
 Output:
 Size is 4
 Index of BMW is 4
 BMW
 Benz
 Honda
 Tesla

 */
