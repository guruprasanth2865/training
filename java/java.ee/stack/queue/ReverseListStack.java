/*
Requirement:
    * Reverse List Using Stack with minimum 7 elements in list.

Entities:
    ReverseListStack
    
Function Declaration:
    public static void main(String[] args)
    
Job to be done:
    -> Create a class ReverseListStack
    -> Declare main function
    -> Create List named cars
    -> Add 7 elements to it
    -> Create a stack and push the elements from list by removing them from list
    
Pseudocode:
public class ReverseListStack {

	public static void main(String[] args) {
		List<String> cars = new ArrayList<>();
		Stack<String> stack = new Stack<>();
		cars.add("BMW");
		cars.add("Ford");
		cars.add("Benz");
		cars.add("Ferrari");
		cars.add("Range Rover");
		cars.add("Hummer");
		cars.add("JEEP");
		System.out.println("Before Reversing : ");
		System.out.println(cars);
		while(cars.size() > 0) {
			stack.push(cars.remove(0));
		}
		while(stack.size() > 0) {
			cars.add(stack.pop());
		}
		System.out.println("After Reversing : ");
		System.out.println(cars);
	}
}
 
 * */


package stack.queue;

import java.util.List;
import java.util.ArrayList;
import java.util.Stack;

public class ReverseListStack {

	public static void main(String[] args) {
		List<String> cars = new ArrayList<>();
		Stack<String> stack = new Stack<>();
		cars.add("BMW");
		cars.add("Ford");
		cars.add("Benz");
		cars.add("Ferrari");
		cars.add("Range Rover");
		cars.add("Hummer");
		cars.add("JEEP");
		System.out.println("Before Reversing : ");
		System.out.println(cars);
		while (cars.size() > 0) {
			stack.push(cars.remove(0));
		}
		while (stack.size() > 0) {
			cars.add(stack.pop());
		}
		System.out.println("After Reversing : ");
		System.out.println(cars);
	}
}

