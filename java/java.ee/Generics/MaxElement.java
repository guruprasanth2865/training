/*
Requirements:
    - 4. Write a generic method to find the maximal element in the range [begin, end) of a list.
    
Entities:
    - MaxElement
    
Function Declaration:
    - public static void main()String[] args)
    - findMax(list, start, end)
     
Job to be done:
    - Create a class MaxElement
    - Create a function findMax with 3 parameters-List, start, end
    - In that function, Declare a temp variable and set the value as the value in the list at index-start
    - In a for loop from start to end, 
        1.1) check whether value is greater then the temp value
        1.2) If it is greater, then replace the temp value to that greater value
    - return the maximum value
    - Declare main method
    - Declare a list of element-type as Integer and add 5 elements
    - Invoke the function findMax with arguments - List, startValue, endValue and print the returned value
    
Pseudocode:
public class MaxElement {

	public static<T extends Object & Comparable<? super T>> T findMax(List<T> cars, int start, int end) {
		T max = cars.get(start);
		for( int i=start; i<=end; i++) {
			if(max.compareTo(cars.get(i)) < 0) {
				max = cars.get(i);
			}
		}
		return max;
	}
	
	public static void main(String[] args) {
		List<Integer> cars = Arrays.asList(45,2,3,87,64,78,32,21,4,64,21,12);
		System.out.println(findMax(cars, 2, 7));
	}
}
 
 * */

package Generics;
import java.util.List;
import java.util.Arrays;

public class MaxElement {

	public static<T extends Object & Comparable<? super T>> T findMax(List<T> cars, int start, int end) {
		T max = cars.get(start);
		for (int i=start; i<=end; i++) {
			if(max.compareTo(cars.get(i)) < 0) {
				max = cars.get(i);
			}
		}
		return max;
	}
	
	public static void main(String[] args) {
		List<Integer> cars = Arrays.asList(45,2,3,87,64,78,32,21,4,64,21,12);
		System.out.println(findMax(cars, 2, 7));
	}
}
