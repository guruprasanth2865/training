/*
Requirements:
    Will the following class compile? If not, why?
   	    public final class Algorithm {
               public static <T> T max(T x, T y) {
                   return x > y ? x : y;
               }
        }
        
Entities:
    - Algorithm
    
Function Declaration:
    - public static <T> T max(T x, T y)
    
Job to be done:
    - Check whether code compiles
    
Answer : 
    The code does not compile because, greater-than (>) comparator works only for primitive numeric types
    
public class Algorithm {

	public static <T> T max(T x, T y) {
        return (x > y) ? x : y;
    }
}
    
*/