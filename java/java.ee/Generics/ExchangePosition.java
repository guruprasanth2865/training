/*
Requirements:
    - 3. Write a generic method to exchange the positions of two different elements in an array.
    
Entities:
    ExchangePosition
    
Function Declaration:
    - public static void main(String[] args)
    - exchange
    
Job to be done:
    - Create class ExcnhangePosition
    - Create function exchange() with 3 parameters - an array, firstIndex, secondIndex
    - Swap the elements in those two indices with a temporary variable
    - return the exchanged array and print it
    
Pseudocode:
public class ExchangePosition {

	public static <T> T[] exchange(T[] arr, int firstOne, int secondOne) {
		T[] cars = arr;
		T temp = cars[firstOne];
		cars[firstOne] = cars[secondOne];
		cars[secondOne] = temp;
		return  cars;
	}
	
	public static void main(String[] args) {
		String[] cars = {"BMW", "Benz", "Lamboghini", "Mazda", "Toyota"};
		System.out.println(Arrays.asList(cars));
	    cars = exchange(cars, 0, 4);
	    System.out.println(Arrays.asList(cars));
	}
}
 
 * */

package Generics;
import java.util.Arrays;

public class ExchangePosition {

	public static <T> T[] exchange(T[] arr, int firstOne, int secondOne) {
		T[] cars = arr;
		T temp = cars[firstOne];
		cars[firstOne] = cars[secondOne];
		cars[secondOne] = temp;
		return  cars;
	}
	
	public static void main(String[] args) {
		String[] cars = {"BMW", "Benz", "Lamboghini", "Mazda", "Toyota"};
		System.out.println(Arrays.asList(cars));
	    cars = exchange(cars, 0, 4);
	    System.out.println(Arrays.asList(cars));
	}
}
