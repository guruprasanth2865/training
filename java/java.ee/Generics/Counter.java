/*
Requirements:
    - Write a generic method to count the number of elements in a collection that have a specific property 
      (for example, odd integers, prime numbers, palindromes). 
  
Entities:
    - Counter
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class Counter
    - In it, declare a main method
    - Create a list and add 10 integers to it
    - In the list,
        1.1) check each elements are odd
        1.2) If they are odd, print them

Pseudocode:
public class Counter {

    public static void main(String[] args) {
    	List<Integer> scores = new ArrayList<>();
    	scores.add(1);
    	scores.add(2);
    	scores.add(3);
    	scores.add(4);
    	scores.add(5);
    	scores.add(6);
    	scores.add(7);
    	scores.add(8);
    	scores.add(9);
    	scores.add(10);
    	for(Integer value : scores) {
    		if(value%2 != 0) {
    			System.out.println(value);
    		}
    	}
    }
}



 * */

package Generics;
import java.util.List;
import java.util.ArrayList;

public class Counter {

    public static void main(String[] args) {
    	List<Integer> scores = new ArrayList<>();
    	scores.add(1);
    	scores.add(2);
    	scores.add(3);
    	scores.add(4);
    	scores.add(5);
    	scores.add(6);
    	scores.add(7);
    	scores.add(8);
    	scores.add(9);
    	scores.add(10);
    	for(Integer value : scores) {
    		if(value%2 != 0) {
    			System.out.println(value);
    		}
    	}
    }
}
