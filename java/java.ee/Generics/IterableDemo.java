/*
Requirement:
    - 1. Write a program to print employees name list by implementing iterable interface.
    
Entities:
    - IterableDemo
    - Person

Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a Person class and Define a private List named members 
    - Declare and define Iterate Interface to return iterator function
    - Create class IterableDemo
    - Create IterableDemo
    - Declare main method
    - Create a arrayList names and add 3 strings 
    - Create a object employee instance of Person and pass the array names to it
    
Pseudocode:
class Person<T> implements Iterable<T> {
	List<T> members = new ArrayList<>();
	
	public Person(List<T> members) {
		this.members.addAll(members);
	}
	
	public Iterator<T> iterator() {
		return members.iterator();
	}
}

public class IterableDemo {

    public static void main(String[] args) {
    	List<String> names = new ArrayList<>();
    	names.add("Guru");
    	names.add("Vishnu");
    	names.add("Sarath");
    	Person<String> employees = new Person<>(names);
    	for(String employee : employees) {
    		System.out.println(employee);
    	}
    }
}


 * */

package Generics;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

class Person<T> implements Iterable<T> {
	List<T> members = new ArrayList<>();
	
	public Person(List<T> members) {
		this.members.addAll(members);
	}
	
	public Iterator<T> iterator() {
		return members.iterator();
	}
}

public class IterableDemo {

    public static void main(String[] args) {
    	List<String> names = new ArrayList<>();
    	names.add("Guru");
    	names.add("Vishnu");
    	names.add("Sarath");
    	Person<String> employees = new Person<>(names);
    	for (String employee : employees) {
    		System.out.println(employee);
    	}
    }
}
