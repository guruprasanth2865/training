/*
Requirements:
    - Generics-Class literals:
        1. Write a program to demonstrate generics - class objects as type literals. 
        
Entities:
    - GenericObjectDemo
    
Function Declaration:
    - public static void main(String[] args)
    - public static <T> T setInfo(Class<T> cars)

Jobs to be done:
    - Create a class Car and declare instance variables
    - Declare setInfo method
        1) Get information from user
        2) Sets the value with this keyword
    - Declare main method
    - Create a object of type Car
    - Invoke function setInfo with argument as the object
    
Pseudocode:
public class Car {

	String brand;
	String model;
	int price;
	
	public static <T> T setInfo(Class<T> car) throws IllegalAccessException, InstantiationException {
		T carDetail = car.newInstance();
		return carDetail;
	}
	
	public static void main(String[] args) throws IllegalAccessException, InstantiationException {
		Car benz = setInfo(Car.class);
		benz.brand = "Benz";
		benz.model = "Series A";
		benz.price = 3500000;
		System.out.println(benz.brand + "\n" + benz.model + "\n" + benz.price);
	}
	
}
 
 * */

package Generics;

public class Car {

	String brand;
	String model;
	int price;
	
	public static <T> T setInfo(Class<T> car) throws IllegalAccessException, InstantiationException {
		T carDetail = car.newInstance();
		return carDetail;
	}
	
	public static void main(String[] args) throws IllegalAccessException, InstantiationException {
		Car benz = setInfo(Car.class);
		benz.brand = "Benz";
		benz.model = "Series A";
		benz.price = 3500000;
		System.out.println(benz.brand + "\n" + benz.model + "\n" + benz.price);
	}
	
}
