/*
Requirements:
    5. What will be the output of the following program?

public class UseGenerics {
    public static void main(String args[]){  
        MyGen<Integer> m = new MyGen<Integer>();  
        m.set("merit");
        System.out.println(m.get());
    }
}
class MyGen<T>
{
    T var;
    void  set(T var)
    {
        this.var = var;
    }
    T get()
    {
        return var;
    }
} 


Output:
Error:
     Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
	 Type mismatch: cannot convert from String to T
 
 * */

package Generics;

public class UseGenerics {
    public static void main(String args[]){  
        MyGen<Integer> m = new MyGen<Integer>();  
        m.set(5);
        System.out.println(m.get());
    }
}
class MyGen<T>
{
    T var;
    void  set(T var)
    {
        this.var = var;
    }
    T get()
    {
        return var;
    }
}