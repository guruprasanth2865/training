/*
Requirement:
    - 2. Write a program to demonstrate generics - for loop, for list, set and map.
    
Entities:
    - GenericDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class GenericDemo
    - Declare a main method
    - Create a List, set and a map to add only Strings as elements
    - Add elements to them and print them
    - Print the List, Set and Map
    
Pseudocode:
public class GenericDemo {

    public static void main(String[] args) {
    	List<String> cars = new ArrayList<>();
    	Set<String> newCars = new HashSet<>();
    	Map<String, String> carShed = new HashMap<>();
    	cars.add("BMW");
    	cars.add("Mazda");
    	cars.add("Lexus");
    	newCars.add("Ford");
    	newCars.add("Jaguar");
    	newCars.add("Toyota");
    	carShed.put("place-one", cars.get(0));
    	carShed.put("place-two", cars.get(1));
    	carShed.put("place-three", cars.get(2));
    	System.out.println(cars);
    	System.out.println(newCars);
    	System.out.println(carShed);
    }
}

 
 * */

package Generics;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

public class GenericDemo {

    public static void main(String[] args) {
    	List<String> cars = new ArrayList<>();
    	Set<String> newCars = new HashSet<>();
    	Map<String, String> carShed = new HashMap<>();
    	cars.add("BMW");
    	cars.add("Mazda");
    	cars.add("Lexus");
    	newCars.add("Ford");
    	newCars.add("Jaguar");
    	newCars.add("Toyota");
    	carShed.put("place-one", cars.get(0));
    	carShed.put("place-two", cars.get(1));
    	carShed.put("place-three", cars.get(2));
    	System.out.println(cars);
    	System.out.println(newCars);
    	System.out.println(carShed);
    }
}
