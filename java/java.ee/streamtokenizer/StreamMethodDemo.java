/*
Requirements:
    - 5) Write java programs for stream tokenizer methods :-
       5.1) commentChar() method
       5.2) eollsSignificant() method
       5.3) lineno() method
       5.4) lowerCaseMode() method 
       
Entities:
    StreamMethodDemo
    
Function Declaration: 
    - public static void main(String[] args)
    
Job to be done:
    - Create class StreamMethodDemo
    - Declare and define commentCharExecute()
    - In commentCharExecute function, read a file with FileReader and BufferedReader and execute commentChar() method
    - After executing, print the content of the file
    - Declare and define eollsSignificantExecute()
    - In eollsSignificantExecute function, read a file with FileReader and BufferedReader and execute eolIsSignificant() method
    - After executing, print the content of the file
    - Declare and define linenoExecute()
    - In linenoExecute function, read a file with FileReader and BufferedReader and execute lineno() method
    - After executing, print the content of the file
    - Declare and define lowerCaseModeExecute()
    - In lowerCaseModeExecute function, read a file with FileReader and BufferedReader and execute lowerCaseMode() method
    - After executing, print the content of the file
    - Create main method and invoke all the methods
    
Psudocode:
public class StreamMethodDemo {
	
	public void commentCharExecute() throws IOException {
		StreamTokenizer fileToken = new StreamTokenizer(new BufferedReader(new FileReader("E:\\test\\token.txt")));
		fileToken.commentChar('g');
		int checker = -1;
		while ((checker = fileToken.nextToken()) != StreamTokenizer.TT_EOF) {
			switch (checker) {
			    case StreamTokenizer.TT_NUMBER: {
				    System.out.println("Number : " + fileToken.nval);
				    break;
			    }
			    case StreamTokenizer.TT_WORD: {
				    System.out.println("Word : " + fileToken.sval);
				    break;
			    }
			}
		}
	}
	
	public void eollsSignificantExecute() throws IOException {
        StreamTokenizer fileToken = new StreamTokenizer(new BufferedReader(new FileReader("E:\\test\\token.txt"))); 
        fileToken.eolIsSignificant(true); 
        int checker; 
        while ((checker = fileToken.nextToken()) != StreamTokenizer.TT_EOF) { 
            switch (checker) { 
                case StreamTokenizer.TT_EOL: {
                	System.out.println("End of Line encountered."); 
                    break; 
                }
                case StreamTokenizer.TT_NUMBER: {
            	    System.out.println("Number : " + fileToken.nval); 
                    break; 
                }
                case StreamTokenizer.TT_WORD: {
            	    System.out.println("Word : " + fileToken.sval); 
                    break; 
                }
            } 
        }
	}
	
	public void linenoExecute() throws IOException {
        StreamTokenizer fileToken = new StreamTokenizer(new BufferedReader(new FileReader("E:\\test\\token.txt"))); 
        fileToken.eolIsSignificant(true); 
        System.out.println("Line : " + fileToken.lineno()); 
        fileToken.commentChar('g'); 
        int checker = -1; 
        while ((checker = fileToken.nextToken()) != StreamTokenizer.TT_EOF) { 
            switch (checker) { 
                case StreamTokenizer.TT_EOL: {
                	System.out.println(""); 
                    System.out.println("Line No. : " + fileToken.lineno()); 
                    break; 
                }
                case StreamTokenizer.TT_WORD: {
                	System.out.println("Word : " + fileToken.sval); 
                    break; 
                }
                case StreamTokenizer.TT_NUMBER: {
                	System.out.println("Number : " + fileToken.nval); 
                    break; 
                }
            } 
        } 
    } 
	
	public void lowerCaseModeExecute() throws IOException {
        StreamTokenizer fileToken = new StreamTokenizer(new BufferedReader(new FileReader("E:\\test\\token.txt"))); 
        fileToken.lowerCaseMode(true); 
        int checker = -1; 
        while ((checker = fileToken.nextToken()) != StreamTokenizer.TT_EOF) { 
            switch (checker) { 
                case StreamTokenizer.TT_NUMBER: {
                	System.out.println("Number : " + fileToken.nval); 
                    break; 
                }
                case StreamTokenizer.TT_WORD: {
            	    System.out.println("Word : " + fileToken.sval); 
                    break; 
                }
            } 
        } 

	}
	
	public static void main(String[] args) {
		StreamMethodDemo tokenizer = null;
		try {
			tokenizer = new StreamMethodDemo();
			System.out.println("\nExecution of commentChar() method : ");
			tokenizer.commentCharExecute();
			System.out.println("\nExecution of eolIsSignificant() method : ");
			tokenizer.eollsSignificantExecute();
			System.out.println("\nExecution of lineno() method : ");
			tokenizer.linenoExecute();
			System.out.println("\nExecution of lowerCaseMode() method : ");
			tokenizer.lowerCaseModeExecute();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
    
 
 * */

package streamtokenizer;

import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.BufferedReader;
import java.io.FileReader;

public class StreamMethodDemo {
	
	public void commentCharExecute() throws IOException {
		StreamTokenizer fileToken = new StreamTokenizer(new BufferedReader(new FileReader("E:\\test\\token.txt")));
		fileToken.commentChar('g');
		int checker = -1;
		while ((checker = fileToken.nextToken()) != StreamTokenizer.TT_EOF) {
			switch (checker) {
			    case StreamTokenizer.TT_NUMBER: {
				    System.out.println("Number : " + fileToken.nval);
				    break;
			    }
			    case StreamTokenizer.TT_WORD: {
				    System.out.println("Word : " + fileToken.sval);
				    break;
			    }
			}
		}
	}
	
	public void eollsSignificantExecute() throws IOException {
        StreamTokenizer fileToken = new StreamTokenizer(new BufferedReader(new FileReader("E:\\test\\token.txt"))); 
        fileToken.eolIsSignificant(true); 
        int checker; 
        while ((checker = fileToken.nextToken()) != StreamTokenizer.TT_EOF) { 
            switch (checker) { 
                case StreamTokenizer.TT_EOL: {
                	System.out.println("End of Line encountered."); 
                    break; 
                }
                case StreamTokenizer.TT_NUMBER: {
            	    System.out.println("Number : " + fileToken.nval); 
                    break; 
                }
                case StreamTokenizer.TT_WORD: {
            	    System.out.println("Word : " + fileToken.sval); 
                    break; 
                }
            } 
        }
	}
	
	public void linenoExecute() throws IOException {
        StreamTokenizer fileToken = new StreamTokenizer(new BufferedReader(new FileReader("E:\\test\\token.txt"))); 
        fileToken.eolIsSignificant(true); 
        System.out.println("Line : " + fileToken.lineno()); 
        fileToken.commentChar('g'); 
        int checker = -1; 
        while ((checker = fileToken.nextToken()) != StreamTokenizer.TT_EOF) { 
            switch (checker) { 
                case StreamTokenizer.TT_EOL: {
                	System.out.println(""); 
                    System.out.println("Line No. : " + fileToken.lineno()); 
                    break; 
                }
                case StreamTokenizer.TT_WORD: {
                	System.out.println("Word : " + fileToken.sval); 
                    break; 
                }
                case StreamTokenizer.TT_NUMBER: {
                	System.out.println("Number : " + fileToken.nval); 
                    break; 
                }
            } 
        } 
    } 
	
	public void lowerCaseModeExecute() throws IOException {
        StreamTokenizer fileToken = new StreamTokenizer(new BufferedReader(new FileReader("E:\\test\\token.txt"))); 
        fileToken.lowerCaseMode(true); 
        int checker = -1; 
        while ((checker = fileToken.nextToken()) != StreamTokenizer.TT_EOF) { 
            switch (checker) { 
                case StreamTokenizer.TT_NUMBER: {
                	System.out.println("Number : " + fileToken.nval); 
                    break; 
                }
                case StreamTokenizer.TT_WORD: {
            	    System.out.println("Word : " + fileToken.sval); 
                    break; 
                }
            } 
        } 

	}
	
	public static void main(String[] args) {
		StreamMethodDemo tokenizer = null;
		try {
			tokenizer = new StreamMethodDemo();
			System.out.println("\nExecution of commentChar() method : ");
			tokenizer.commentCharExecute();
			System.out.println("\nExecution of eolIsSignificant() method : ");
			tokenizer.eollsSignificantExecute();
			System.out.println("\nExecution of lineno() method : ");
			tokenizer.linenoExecute();
			System.out.println("\nExecution of lowerCaseMode() method : ");
			tokenizer.lowerCaseModeExecute();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}