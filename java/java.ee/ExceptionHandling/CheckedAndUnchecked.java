/*
Requirements:
    - 3)Compare the checked and unchecked exception.
    
Entities:
    - CheckedAndUnchecked
    
Checked Exception:
    - Checked exceptions are the exceptions checked in the compile time
    - If a method is throwing a checked exception then it should handle 
      the exception using try-catch block or it should declare the exception using throws keyword,
      otherwise the program will give a compilation error.
    
Unchecked Exceptions:
    - Unchecked Exceptions are the exceptions checked in the runtime
    - If your program is throwing an unchecked exception and even 
      if you didn�t handle/declare that exception, the program won�t
      give a compilation error.

 * */

package ExceptionHandling;

public class CheckedAndUnchecked {

}
