/*
Requirements:
    - 6)Handle and give the reason for the exception in the following code: 
        PROGRAM:
         public class Exception {  
             public static void main(String[] args) {
  	             int arr[] ={1,2,3,4,5};
	             System.out.println(arr[7]);
             }
         }
        //Display the output.

Entities;
    - Exception
    
Function Declaration:
    - public static void main(String[] args)

Job to be done:
   - Create a class - Exception
   - Declare and Define main method
   - Declare an array with 5 elements
   - Print the element at 7th index of array inside a try block
   - Add a catch block to catch ArrayIndexOutOfBoundsException .
 
Output:
    Thread [main] (Suspended (uncaught exception ArrayIndexOutOfBoundsException))	
	Exception.main(String[]) line: 33	

Psudo code:
public class ArrayException {

	public static void main(String[] args) {
        int arr[] ={1,2,3,4,5};
        try {
        	System.out.println(arr[7]);
        } catch(ArrayIndexOutOfBoundsException e) {
        	System.out.println("Index is out of range");
        }
        
    }
}
   
 * */

package ExceptionHandling;

public class ArrayException {

	public static void main(String[] args) {
        int arr[] ={1,2,3,4,5};
        try {
        	System.out.println(arr[7]);
        } catch (ArrayIndexOutOfBoundsException e) {
        	System.out.println("Index is out of range");
        }
        
    }
}
