/*
Requirements:
    - 4)Can we write only try block without catch and finally blocks?why?
    
Entities:
    - ExceptionExplanation
    
Explanation:
    - It is possible to have a try block without a catch block by using a final block. 
      As we know, a final block will always execute even there is an exception occurred in a try block, 
      except System.exit() it will execute always.
    - Finally block is an optional block. This block just executes after try and catch block.
 
 * */

package ExceptionHandling;

public class ExceptionExplanation {

}
