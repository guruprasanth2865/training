/*
7.a)Why validation is important?
      When receiving input that needs to be validated before it can be used. This is done to avoid leaving the application 
in a half valid state and avoid processing invalid data.
   Example:
           check if user already exists
           validate user
           validate address

           insert user
           insert address  
           
b)The following code is possible. This is because, data are validated before inserting
              check if user already exists
              validate user
              validate address

              insert user
              
 * */


package ExceptionHandling;

public class ValidationDemo {

}
