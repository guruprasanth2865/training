/*
Requiremetns:
    - 5)i)Mutiple catch block -  
          a]Explain with Example
          b]It is possible to have more than one try block? - Reason.
       ii)Difference between catching multiple exceptions and Mutiple catch blocks.
       
Entities:
    - MultipleCatchDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a array 
    - open a try block and try to print the element of array with higher value of index
    - Add catch blocks to catch the errors
    - Print the error
    
Pseudocode:
public class MultipleCatchDemo {

	public static void main(String[] args){
		int[] arr = new int[10];
		try {
	        System.out.println("value : " + arr[15]);
	    } catch (ArithmeticException e) {
	        System.out.println("Number cannot be divided by zero");
	    } catch (ArrayIndexOutOfBoundsException e){
	        System.out.println("Array index is out of range");
	    } catch (Exception e) {
	        System.out.println("Please check the program");
	    }
    }
}



5)
ii)Difference between catching multiple exceptions and Mutiple catch blocks.

Explanation:

Catching multiple exceptions:
    This is also known as multi catch.
    We could separate different exceptions using pipe ( | ).
    Eg: catch(IOException | SQLException ex)
    This syntax does not looks clumsy.

Mutiple catch blocks
    For catching different exceptions need to write different catch blocks.
    Dedicated messages can be conveyed for different exceptions
    try{

    }catch(ArrayIndexOutOfBoundsException e){

    } catch(Exception e){

    }

 * */


package ExceptionHandling;

public class MultipleCatchDemo {

	public static void main(String[] args){
		int[] arr = new int[10];
		try {
	        System.out.println("value : " + arr[15]);
	    } catch (ArithmeticException e) {
	        System.out.println("Number cannot be divided by zero");
	    } catch (ArrayIndexOutOfBoundsException e){
	        System.out.println("Array index is out of range");
	    } catch (Exception e) {
	        System.out.println("Please check the program");
	    }
    }
}
