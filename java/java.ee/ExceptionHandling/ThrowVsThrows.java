/*
Requirements:
    - 9)Difference between throws and throw , Explain with simple example.
    
Entities:
    - ThrowVsThrows
    
Function Declaration:
    - public static void main(String[] args)
    - throwsDivide
    - throwDivide
    
Job to be done:
    - Create class ThrowVsThrows
    - Declare function throwDivide(int divisor, int dividend)
    - it return (divisor/dividend)
    - Declare function throwsDivide(int divisor, int dividend)
    - it return (divisor/dividend)
    - Declare and define main function
    - Invoke the function throwDivide and throwsDivide and print their returned values
    
Explanation:
    
Throw:
    - Throw is a keyword which is used to throw an exception explicitly in the program inside
      a function or inside a block of code.
    
    - Internally throw is implemented as it is allowed to throw only single exception at a time 
      i.e we cannot throw multiple exception with throw keyword.
      
    - With throw keyword we can propagate only unchecked exception i.e checked exception cannot 
      be propagated using throw.
      
    - Syntax wise throw keyword is followed by the instance variable.
    
    - In order to use throw keyword we should know that throw keyword is used within the method.
      
      
Throws:
    - Throws is a keyword used in the method signature used to declare an exception which might 
      get thrown by the function while executing the code.
      
    - On other hand we can declare multiple exceptions with throws keyword that could get thrown 
      by the function where throws keyword is used.
      
    - On other hand with throws keyword both checked and unchecked exceptions can be declared and 
      for the propagation checked exception must use throws keyword followed by specific exception 
      class name.
      
    - On other hand syntax wise throws keyword is followed by exception class names.
    
    - On other hand throws keyword is used with the method signature.
    

Psudocode:
public class ThrowVsThrows {
	
	public void throwDivide(int divisor, int dividend) {
		if(dividend == 0) {
			throw new ArithmeticException("Cannot divide by Zero");
		} else {
			System.out.println(divisor/dividend);
		}
	}
	
	public int throwsDivide(int divisor, int dividend) throws ArithmeticException {
		return divisor/dividend;
	}
	
	public static void main(String[] args) {
		ThrowVsThrows checkValue = new ThrowVsThrows();
		checkValue.throwDivide(6, 2);
		try {
			System.out.println(checkValue.throwsDivide(5, 0));
		} catch(ArithmeticException e) {
			System.out.println(e);
		}
	}

}

 
 * */

package ExceptionHandling;

public class ThrowVsThrows {
	
	public void throwDivide(int divisor, int dividend) {
		if (dividend == 0) {
			throw new ArithmeticException("Cannot divide by Zero");
		} else {
			System.out.println(divisor/dividend);
		}
	}
	
	public int throwsDivide(int divisor, int dividend) throws ArithmeticException {
		return divisor/dividend;
	}
	
	public static void main(String[] args) {
		ThrowVsThrows checkValue = new ThrowVsThrows();
		checkValue.throwDivide(6, 2);
		try {
			System.out.println(checkValue.throwsDivide(5, 0));
		} catch (ArithmeticException e) {
			System.out.println(e);
		}
	}

}
