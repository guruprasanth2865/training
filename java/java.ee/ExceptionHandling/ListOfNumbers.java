/*
Requirements:
    -  2)Write a program ListOfNumbers (using try and catch block).
    
Entities:
    - ListOfNumbers
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class ListOfNumbers
    - Create a main method
    - Declare and define a List-values with 10 integers
    - Create a try block and write a for loop in it to print the elements in list
    - Add catch blocks followed by try block
    - Add catch block for IndexOutOfBoundsException 
    - Add catch block for all other exceptions - (Commonly as Exceptions)
    - Add a finally block  
  
Psudocode:
public class ListOfNumbers {

    public static void main(String[] args) {
    	List<Integer> values = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
    	try {
    		for(int i=0; i<=values.size(); i++) {
    			System.out.println(values.get(i));
    		}
    	} catch(IndexOutOfBoundsException ie) {
    		System.out.println("Error : " + ie);
    	} catch(Exception e) {
    		System.out.println("Error : " + e);
    	} finally {
    		System.out.println("Program ends...");
    	}
    }
}

 * */

package ExceptionHandling;

import java.util.List;
import java.util.Arrays;

public class ListOfNumbers {

    public static void main(String[] args) {
    	List<Integer> values = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
    	try {
    		for (int i = 0; i <= values.size(); i++) {
    			System.out.println(values.get(i));
    		}
    	} catch (IndexOutOfBoundsException ie) {
    		System.out.println("Error : " + ie);
    	} catch (Exception e) {
    		System.out.println("Error : " + e);
    	} finally {
    		System.out.println("Program ends...");
    	}
    }
}
