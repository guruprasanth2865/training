/*
Requirements:
    - Demonstrate the catching multiple exception with example. 
    
Entities:
    ExceptionDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class - ExceptionDemo
    - Declare and define main function
    - Declare a integer value with value 5
    - Inside  a try block, get a number-divider and divide value with that divider
    - Add a catch block to catch ArithmeticException 
    - Add a catch block to catch IOException
    - Add a catch block to catch other exceptions
    
psudo code:
public class ExceptionDemo {

    public static void main(String[] args) throws IOException {
    	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    	int value = 5;
    	int divider = 0;
    	System.out.println("Enter a value : \n");
    	try {
    		divider = Integer.parseInt(reader.readLine());
    		System.out.println(value/divider);
    	} catch (ArithmeticException ae) {
    		System.out.println("Error : " + ae);
    	} catch (IOException ie) {
    		System.out.println("Error : " + ie);
    	} catch (Exception e) {
    		System.out.println("Error : " + e);
    	} finally {
    		
    		System.out.println("Program ends...");
    	}
    }
}
 
 * */

package ExceptionHandling;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;  

public class ExceptionDemo {

    public static void main(String[] args) throws IOException {
    	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    	int value = 5;
    	int divider = 0;
    	System.out.println("Enter a value : \n");
    	try {
    		divider = Integer.parseInt(reader.readLine());
    		System.out.println(value/divider);
    	} catch (ArithmeticException ae) {
    		System.out.println("Error : " + ae);
    	} catch (IOException ie) {
    		System.out.println("Error : " + ie);
    	} catch (Exception e) {
    		System.out.println("Error : " + e);
    	} finally {
    		
    		System.out.println("Program ends...");
    	}
    }
}
