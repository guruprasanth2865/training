/*
Requirements:
    - 8)Explain the below program.
         try{
          dao.readPerson();
        } catch (SQLException sqlException) {
        throw new MyException("wrong", sqlException);
      }
      
Entities:
    - SqlExceptionDemo
    
Explanation:
    - There is a try block. In that try block, function-readPerson() method is invoked which is of object-dao.
    - Then try block ends, followed by a catch block to catch SQLException 
    - Inside that catch block, it throw a new Exception-"wrong" of SQLException type
 
 * */

package ExceptionHandling;

public class SqlExceptionDemo {

}
