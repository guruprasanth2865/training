/*
Requirements:
    7.  8 districts are shown below
    Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy   to be converted to UPPERCASE.
    
Entities:
    DistrictCaseConverter
    
Function Declaration:
    public static void main(String[] args)

Job to be done:
    - Create a class DistrictCaseConverter
    - Declare main method
    - Create a list-place and add 8 district names
    - Use for loop to traverse and change them with Upper Case strings

pseudocode:
public static void main(String[] args) {
    	List<String> districts = new ArrayList<>();
    	districts.add("Madurai");
    	districts.add("Coimbatore");
    	districts.add("Theni");
    	districts.add("Chennai");
    	districts.add("Karur");
    	districts.add("Salem");
    	districts.add("Erode");
    	districts.add("Trichy");
    	System.out.println("");
    	for(int i = 0; i<districts.size(); i++) {
    		districts.set(i, districts.get(i).toUpperCase());
    		System.out.println(districts.get(i));
    	}
    }
    
 * */

package Collections;
import java.util.List;
import java.util.ArrayList;


public class DistrictCaseConverter {

    public static void main(String[] args) {
    	List<String> districts = new ArrayList<>();
    	districts.add("Madurai");
    	districts.add("Coimbatore");
    	districts.add("Theni");
    	districts.add("Chennai");
    	districts.add("Karur");
    	districts.add("Salem");
    	districts.add("Erode");
    	districts.add("Trichy");
    	System.out.println("");
    	for (int i = 0; i < districts.size(); i++) {
    		districts.set(i, districts.get(i).toUpperCase());
    		System.out.println(districts.get(i));
    	}
    }
}
