/*
Requirement:
    - 4.use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),pollFirst(),poolLast() methods 
      to store and retrieve elements in ArrayDequeue.
      
Entities:
    DequeTest
    
Function Declaration:
    public static void main(String[] args)

Job to be done:
    - Create a class DequeTest
    - Declare main method
    - Create a Deque-cars and add 5 strings in it with add() method
    - add elements with addFirst() and addLast()
    - remove elements with removeFirst() and removeLast() method
    - Peek the elements with peekFirst() and peekLast()
    - poll the elements with pollFirst() and pollLast() method
    
Pseudocode:
public class DequeTest {

    public static void main(String[] args) {
    	Deque<String> cars = new ArrayDeque<>();
    	cars.add("BMW");
    	cars.add("Benz");
    	cars.add("Aston Martin");
    	cars.add("Hummer");
    	cars.add("Ferrari");
    	System.out.println("Deque - cars : ");
    	for(String car : cars) {
    		System.out.println(car);
    	}
    	cars.addFirst("Suzuki");
    	cars.addLast("Ford");
    	System.out.println("\nAfter adding elements with addFirst() and addLast()");
    	for(String car : cars) {
    		System.out.println(car);
    	}
    	System.out.println("\nRemoved first element with - removeFirst() is - " + cars.removeFirst());
    	System.out.println("Removed first element with - removeLast() is - " + cars.removeLast());
    	System.out.println("After Removing - cars : ");
    	for(String car : cars) {
    		System.out.println(car);
    	}
    	System.out.println("\npeeking first element with peekFirst() - " + cars.peekFirst());
    	System.out.println("peeking last element with peekLast() - " + cars.peekLast());
    	System.out.println("\nRemoving first element with pollFirst() - " + cars.pollFirst());
    	System.out.println("Removing last element with pollLast() - " + cars.pollLast());
    	System.out.println("After removing with poll method - cars");
    	for(String car : cars) {
    		System.out.println(car);
    	}
    }
}
      
 * */

package Collections;
import java.util.ArrayDeque;
import java.util.Deque;

public class DequeTest {

    public static void main(String[] args) {
    	Deque<String> cars = new ArrayDeque<>();
    	cars.add("BMW");
    	cars.add("Benz");
    	cars.add("Aston Martin");
    	cars.add("Hummer");
    	cars.add("Ferrari");
    	System.out.println("Deque - cars : ");
    	for (String car : cars) {
    		System.out.println(car);
    	}
    	cars.addFirst("Suzuki");
    	cars.addLast("Ford");
    	System.out.println("\nAfter adding elements with addFirst() and addLast()");
    	for (String car : cars) {
    		System.out.println(car);
    	}
    	System.out.println("\nRemoved first element with - removeFirst() is - " + cars.removeFirst());
    	System.out.println("Removed first element with - removeLast() is - " + cars.removeLast());
    	System.out.println("After Removing - cars : ");
    	for (String car : cars) {
    		System.out.println(car);
    	}
    	System.out.println("\npeeking first element with peekFirst() - " + cars.peekFirst());
    	System.out.println("peeking last element with peekLast() - " + cars.peekLast());
    	System.out.println("\nRemoving first element with pollFirst() - " + cars.pollFirst());
    	System.out.println("Removing last element with pollLast() - " + cars.pollLast());
    	System.out.println("After removing with poll method - cars");
    	for (String car : cars) {
    		System.out.println(car);
    	}
    }
}
