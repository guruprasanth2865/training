/*
Requirement:
    5.add and remove the elements in stack
    
Entities:
    StackDemo
    
Function Declaration:
    public static void main(String[] args)
  
Job to be done:
    - Create a Class StackDemo
    - Declare main method in it
    - Declare a Stack-cars 
    - Add element with push() method 
    - remove element with pop() method
    
Pseudocode:
public class StackDemo {

    public static void main(String[] args) {
    	Stack<String> cars = new Stack<>();
    	cars.push("Mazda");
    	cars.push("Honda");
    	cars.push("Renault");
    	for(String car : cars) {
    		System.out.println(car);
    	}
    	System.out.println("\nRemoved element with pop() method - " + cars.pop() + "\nAfter Removing - cars : ");
    	for(String car : cars) {
    		System.out.println(car);
    	}
    }
}
    
 * */

package Collections;
import java.util.Stack;

public class StackDemo {

    public static void main(String[] args) {
    	Stack<String> cars = new Stack<>();
    	cars.push("Mazda");
    	cars.push("Honda");
    	cars.push("Renault");
    	for (String car : cars) {
    		System.out.println(car);
    	}
    	System.out.println("\nRemoved element with pop() method - " + cars.pop() + "\nAfter Removing - cars : ");
    	for (String car : cars) {
    		System.out.println(car);
    	}
    }
}
