/*
Requirement:
    3.code for sorting vector list in descending order.
    
Entities:
    VectorSort
    
Function Declaration:
    public static void main(String[] args)

Job to be done:
    - Create a vectorList-cars
    - Add 5 strings in that list 
    - use sort() method to sort them with Collections.sort(vectorList, Collections.reverseOrder()); 
    - print the sorted vectorList

Pseudocode:
public class VectorSort {

    public static void main(String[] args) {
    	Vector<String> cars = new Vector<>();
    	cars.add("Aston Martin");
    	cars.add("BMW");
    	cars.add("Honda");
    	cars.add("Suzuki");
    	cars.add("Benz");
    	System.out.println("Before Sorting ");
    	for(String car : cars) {
    		System.out.println(car);
    	}
    	Collections.sort(cars, Collections.reverseOrder());
    	System.out.println("\nAfter Sorting in Descending order : ");
    	for(String car : cars) {
    		System.out.println(car);
    	}
    }
}

 * */

package Collections;
import java.util.Collections;
import java.util.Vector;

public class VectorSort {

    public static void main(String[] args) {
    	Vector<String> cars = new Vector<>();
    	cars.add("Aston Martin");
    	cars.add("BMW");
    	cars.add("Honda");
    	cars.add("Suzuki");
    	cars.add("Benz");
    	System.out.println("Before Sorting ");
    	for (String car : cars) {
    		System.out.println(car);
    	}
    	Collections.sort(cars, Collections.reverseOrder());
    	System.out.println("\nAfter Sorting in Descending order : ");
    	for (String car : cars) {
    		System.out.println(car);
    	}
    }
}
