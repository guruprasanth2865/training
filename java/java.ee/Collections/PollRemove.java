/*
Requirement:
    - 2.what is the difference between poll() and remove() method of queue interface?

Entities:
    - PollRemove

Function Declaration:
    - public static void main(String[] args)

Job to be done:
    - Create a class PollRemove
    - Declare a main method
    - Declare a queue-cars 
    - Try removing a element with remove() and catch error "NoSuchElementException"
    - try removing head element with poll()

poll():
    Retrieves and removes the head of this queue,or returns null if this queue is empty.
    
remove():
    If the queue is null, it throws an error.
    NullPointerException - if the specified element is null and thiscollection does not permit null elements(optional)
    
Pseudocode:
public class PollRemove {

    public static void main(String[] args) {
    	Queue<String> cars = new LinkedList<>();
    	try {
    		cars.remove();
    	} catch(Exception e) {
    		System.out.println("Output from remove() - "+e);
    	}
    	System.out.println("Output from poll() - "+cars.poll());
    }
}

    
*/


package Collections;
import java.util.Queue;
import java.util.LinkedList;

public class PollRemove {

    public static void main(String[] args) {
    	Queue<String> cars = new LinkedList<>();
    	try {
    		cars.remove();
    	} catch (Exception e) {
    		System.out.println("Output from remove() - "+e);
    	}
    	System.out.println("Output from poll() - " + cars.poll());
    }
}
