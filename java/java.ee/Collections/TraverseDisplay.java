/*
Requirement:
    - 1.create an array list with 7 elements, and create an empty linked list add all elements 
      of the array list to linked list ,traverse the elements and display the result
      
Entities:
    - TraverseDisplay
    
Function Declaration:
    - public static void main(String[] args)

Job to be done:
    - Create a Class TraverseDisplay
    - Declare main method in it.
    - Declare a ArrayList-cars and add 7 names of cars in it.
    - Declare a LinkedList-newCars and add all values from cars
    - Display the values in the LinkedList using a for loop
    
Pseudocode:
public class TraverseDisplay {

	public static void main(String[] args) {
		ArrayList<String> cars = new ArrayList<>();
		LinkedList<String> newCars = new LinkedList<>();
		cars.add("BMW");
		cars.add("Benz");
		cars.add("Ambasidor");
		cars.add("Suzuki");
		cars.add("Ford");
		cars.add("TATA");
		cars.add("Renault");
		newCars.addAll(cars);
		for(String car: newCars) {
			System.out.println(car);
		}
	}
}

 * */
package Collections;
import java.util.LinkedList;
import java.util.ArrayList;

public class TraverseDisplay {

	public static void main(String[] args) {
		ArrayList<String> cars = new ArrayList<>();
		LinkedList<String> newCars = new LinkedList<>();
		cars.add("BMW");
		cars.add("Benz");
		cars.add("Ambasidor");
		cars.add("Suzuki");
		cars.add("Ford");
		cars.add("TATA");
		cars.add("Renault");
		newCars.addAll(cars);
		for(String car: newCars) {
			System.out.println(car);
		}
	}
}
