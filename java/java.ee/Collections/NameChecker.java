/*
Requirements:
    - 6) LIST CONTAINS 10 STUDENT NAMES
      krishnan, abishek, arun,vignesh, kiruthiga, murugan,adhithya,balaji,vicky, priya and display only names starting with 'A'.

Entities:
    NameChecker
    
Function Declaration:
    public static void main(String[] args)
   
Job to be done:
    - Create a  class-NameChecker
    - Declare a method
    - Create a List-cars
    - Add 10 elements in it
    - Create a for loop to traverse the elements in the list
    - Check whether each element starts with "A" with startsWith() method
    - if startsWith() method returns true, then print the same element
    
Pseudocode:
public class NameChecker {

    public static void main(String[] args) {
    	List<String> students = new ArrayList<>();
    	students.add("krishnan");
    	students.add("abishek");
    	students.add("arun");
    	students.add("vignesh");
    	students.add("kiruthiga");
    	students.add("murugan");
    	students.add("adhithya");
    	students.add("balaji");
    	students.add("vicky");
    	students.add("priya");
    	System.out.println("\nNames starting with \"A\" or \"a\"");
    	for(String student : students) {
    		if(student.startsWith("A") || student.startsWith("a")) {
    			System.out.println(student);
    		}
    	}
    }
}

 * */

package Collections;
import java.util.List;
import java.util.ArrayList;

public class NameChecker {

    public static void main(String[] args) {
    	List<String> students = new ArrayList<>();
    	students.add("krishnan");
    	students.add("abishek");
    	students.add("arun");
    	students.add("vignesh");
    	students.add("kiruthiga");
    	students.add("murugan");
    	students.add("adhithya");
    	students.add("balaji");
    	students.add("vicky");
    	students.add("priya");
    	System.out.println("\nNames starting with \"A\" or \"a\"");
    	for (String student : students) {
    		if (student.startsWith("A") || student.startsWith("a")) {
    			System.out.println(student);
    		}
    	}
    }
}
