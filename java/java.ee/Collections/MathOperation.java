/*
Requirement:
    8.Addition,Substraction,Multiplication and Division concepts are achieved using Lambda expression and functional interface.
    
Entities:
    MathOperation
    
Function Declaration:
    public static void main(String[] args)

Job to be done:
    - - Create a functional interface with a method declaration with two arguments
    - Create class MathOperation
    - Declare and define main method
    - Create instance for the functional interface for addintion, subtractio, multiplication, division assigned with lambda expression
    - Perform the lambda expression and print the result
    
PseudoCode:
interface Calculator {
	public int perform(int a, int b);
}

public class MathOperation {

    public static void main(String[] args) {
    	Calculator addition = (int a, int b) -> a + b;
    	Calculator subraction = (int a, int b) -> a - b;
    	Calculator multiplication = (int a, int b) -> a * b;
    	Calculator division = (int a, int b) -> a / b;
    	System.out.println("Add : " + addition.perform(2, 2));
    	System.out.println("Subract : " + subraction.perform(5,  3));
    	System.out.println("Multiply : " + multiplication.perform(2, 2));
    	System.out.println("Division : " + division.perform(10, 2));
    }
}
    
    - 
    
 * */

package Collections;

interface Calculator {
	public int perform(int a, int b);
}

public class MathOperation {

    public static void main(String[] args) {
    	Calculator addition = (int a, int b) -> a + b;
    	Calculator subraction = (int a, int b) -> a - b;
    	Calculator multiplication = (int a, int b) -> a * b;
    	Calculator division = (int a, int b) -> a / b;
    	System.out.println("Add : " + addition.perform(2, 2));
    	System.out.println("Subract : " + subraction.perform(5,  3));
    	System.out.println("Multiply : " + multiplication.perform(2, 2));
    	System.out.println("Division : " + division.perform(10, 2));
    }
}
