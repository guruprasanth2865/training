/* Consider a following code snippet
Queue bike = new PriorityQueue();    
bike.poll();
System.out.println(bike.peek());    

what will be output and complete the code.


Output:
RE
*/

package QueueDemo;
import java.util.Queue;
import java.util.PriorityQueue;

public class QueueCodeSnippet {
    
	public static void main(String[] args) {
		Queue bike= new PriorityQueue();
		bike.add("RE");
		bike.add("Yamaha");
		bike.add("Kawasiki");
		bike.poll();
		System.out.println(bike.peek());  
	}
}
