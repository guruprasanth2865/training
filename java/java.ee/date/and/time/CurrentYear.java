/*
Requirements:
    - 5. Write a Java program to get the information of current/given year
       5.1)check the given year is leap year or not? 
       5.2)Find the Length of the give year? 
       
Entities:
    - CurrentYear
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class CurrentYear
    - Declare and define main method
    - Create a instance of Year-class and store the current year
    - Check whether current year is leap year or not with isLeap() method
    - Print the length of the year with length() method

Psudocode:
public class CurrentYear {

    public static void main(String[] args) {
    	Year currentYear = Year.now();
    	if (currentYear.isLeap() == true) {
    		System.out.println(currentYear + " is a Leap year");
    	} else {
    		System.out.println(currentYear + " is not a Leap year");
    	}
    	System.out.println("Length of year : " + currentYear.length() +" days");
    }
}
 
 * */

package date.and.time;

import java.time.Year;

public class CurrentYear {

    public static void main(String[] args) {
    	Year currentYear = Year.now();
    	if (currentYear.isLeap() == true) {
    		System.out.println(currentYear + " is a Leap year");
    	} else {
    		System.out.println(currentYear + " is not a Leap year");
    	}
    	System.out.println("Length of year : " + currentYear.length() +" days");
    }
}
