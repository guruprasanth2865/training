/*
Requirements:
    -  1. Given a random date, how would you find the date of the previous Friday?
    
Entities:
    - PreviousFridayFinder
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class PreviousFridayFinder
    - Declare and define main method
    - Get the current date with now() method
    - Use TemporalAdjusters and DayOfWeek to find the friday
    
Psudocode:
public class PreviousFridayFinder {

    public static void main(String[] args) {
    	LocalDate currentDate = LocalDate.now();
    	LocalDate lastFriday = currentDate.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY));
    	System.out.println("Last friday : " + lastFriday);
    }
}
 
 * */

package date.and.time;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

public class PreviousFridayFinder {

    public static void main(String[] args) {
    	LocalDate currentDate = LocalDate.now();
    	LocalDate lastFriday = currentDate.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY));
    	System.out.println("Last friday : " + lastFriday);
    }
}
