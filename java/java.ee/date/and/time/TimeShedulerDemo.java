/*
Requirements:
    -  3. Write a Java program to demonstrate schedule method calls of Timer class ?

Entities:
    - TimeShedulerDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a Runner class that extends TimerTask
    - Declare and define run() method
    - Create a class TimerShedularDemo
    - Declare and define main method
    - Create a Runner object, TimerShedulerDemo object and a Date object
    - Set the time with sheduleAtFixedRate() method
    - Invoke synchronized() method 
    - Invoke wait()method and cancel() method inside synchronized() method
    
Psudocode:
class Runner extends TimerTask {
	
	public static int count = 0;

	public void run() {
		System.out.println("Timer count : " + ++count);
		if (count == 10) {
			synchronized (TimeShedulerDemo .obj) {
				TimeShedulerDemo .obj.notify();
			}
		}
	}
}

public class TimeShedulerDemo  {
	
	protected static TimeShedulerDemo  obj;

	public static void main(String[] args) throws InterruptedException {
		obj = new TimeShedulerDemo ();
		Timer timer = new Timer();
		TimerTask task = new Runner();
		Date date = new Date();
		System.out.println("Timer Started");
		timer.scheduleAtFixedRate(task, date, 1000);
		synchronized (obj) {
			obj.wait();
			timer.cancel();
			System.out.println(timer.purge());
		}
	}
}
 
 * */


package date.and.time;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

class Runner extends TimerTask {
	
	public static int count = 0;

	public void run() {
		System.out.println("Timer count : " + ++count);
		if (count == 10) {
			synchronized (TimeShedulerDemo .obj) {
				TimeShedulerDemo .obj.notify();
			}
		}
	}
}

public class TimeShedulerDemo  {
	
	protected static TimeShedulerDemo  obj;

	public static void main(String[] args) throws InterruptedException {
		obj = new TimeShedulerDemo ();
		Timer timer = new Timer();
		TimerTask task = new Runner();
		Date date = new Date();
		System.out.println("Timer Started");
		timer.scheduleAtFixedRate(task, date, 1000);
		synchronized (obj) {
			obj.wait();
			timer.cancel();
			System.out.println(timer.purge());
		}
	}
}