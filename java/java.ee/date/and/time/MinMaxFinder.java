/*
Requirements:
    - 8. Write a Java program to get the maximum and minimum value  of the year, month, week, date 
      from the current date of a default calendar. 

Entities:
    - MinMaxFinder

Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class MinMaxFinder
    - Declare and define main method
    - Declare a variable of type-Calender and set value with getInstance() method
    - Get maximum values with getActualMaximum() method
    - Get maximum values with getActualMinimum() method
    - print the values
    
Psudocode:
public class MinMaxFinder {

    public static void main(String[] args) {
    	Calendar calendar = Calendar.getInstance();
    	System.out.println("Current time : " + calendar.getTime());
    	System.out.println("\nMaximum value : \n" + "Maximum Year : " + calendar.getActualMaximum(Calendar.YEAR)
    	                  + "\nMaximum Month : " + calendar.getActualMaximum(Calendar.MONTH)
    	                  + "\nMaximum Week of year : " + calendar.getActualMaximum(Calendar.WEEK_OF_YEAR)
    	                  + "\nMaximum Date : " + calendar.getActualMaximum(Calendar.DATE));
    	System.out.println("\nMinimum value : \n" + "Maximum Year : " + calendar.getActualMinimum(Calendar.YEAR)
                          + "\nMinimum Month : " + calendar.getActualMinimum(Calendar.MONTH)
                          + "\nMinimum Week of year : " + calendar.getActualMinimum(Calendar.WEEK_OF_YEAR)
                          + "\nMinimum Date : " + calendar.getActualMinimum(Calendar.DATE));
    }
}
 
 * */

package date.and.time;

import java.util.Calendar;

public class MinMaxFinder {

    public static void main(String[] args) {
    	Calendar calendar = Calendar.getInstance();
    	System.out.println("Current time : " + calendar.getTime());
    	System.out.println("\nMaximum value : \n" + "Maximum Year : " + calendar.getActualMaximum(Calendar.YEAR)
    	                  + "\nMaximum Month : " + calendar.getActualMaximum(Calendar.MONTH)
    	                  + "\nMaximum Week of year : " + calendar.getActualMaximum(Calendar.WEEK_OF_YEAR)
    	                  + "\nMaximum Date : " + calendar.getActualMaximum(Calendar.DATE));
    	System.out.println("\nMinimum value : \n" + "Maximum Year : " + calendar.getActualMinimum(Calendar.YEAR)
                          + "\nMinimum Month : " + calendar.getActualMinimum(Calendar.MONTH)
                          + "\nMinimum Week of year : " + calendar.getActualMinimum(Calendar.WEEK_OF_YEAR)
                          + "\nMinimum Date : " + calendar.getActualMinimum(Calendar.DATE));
    }
}
