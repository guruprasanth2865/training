/*
Requirements:
    - 6. Write a Java program to get the dates 10 days before and after today.
        6.1)using local date 
        6.2)using calendar 
        
Entities:
    - TenDaysFinder
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class TenDaysFinder
    - Declare and define main method
    - Get the current date with now() method. Get before and after days with plusDays() method
    - Get the current days with getInstance() method. Get before and after days with add() method
    
Psudocode:
public class TenDaysFinder {

    public static void main(String[] args) {
    	LocalDate date = LocalDate.now();
    	Calendar calendar = Calendar.getInstance();
    	System.out.println("Date : " + date + "\nBefore : " + date.plusDays(-10) + "\nAfter : " + date.plusDays(10));
    	System.out.println("Date : " + calendar.getTime());
    	calendar.add(Calendar.DATE, -10);
    	System.out.println("Before : " +  calendar.getTime());
    	calendar = Calendar.getInstance();
    	calendar.add(Calendar.DATE, 10);
    	System.out.println("After : " +  calendar.getTime());
    }
}
 
 * */

package date.and.time;

import java.time.LocalDate;
import java.util.Calendar;

public class TenDaysFinder {

    public static void main(String[] args) {
    	LocalDate date = LocalDate.now();
    	Calendar calendar = Calendar.getInstance();
    	System.out.println("Date : " + date + "\nBefore : " + date.plusDays(-10) + "\nAfter : " + date.plusDays(10));
    	System.out.println("Date : " + calendar.getTime());
    	calendar.add(Calendar.DATE, -10);
    	System.out.println("Before : " +  calendar.getTime());
    	calendar = Calendar.getInstance();
    	calendar.add(Calendar.DATE, 10);
    	System.out.println("After : " +  calendar.getTime());
    }
}
