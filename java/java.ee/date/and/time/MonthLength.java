/*
Requirements:
    - 5. Write an example that, for a given year, reports the length of each month within that particular year.
    
Entities:
    - MonthLength
    
Function Declaration:
    - public staic void main(String[] args)
    
Jobs to be done:
    - Create class MonthLength
    - Declare and define main method
    - Get the year and month from the user 
    - Get the length with method-lengthOfMonth() and print the returned value 
    
Psudocode:
public class MonthLength {

    public static void main(String[] args) {
    	Scanner sc = new Scanner(System.in);
    	System.out.println("Enter year value : ");
    	int year = sc.nextInt();
    	System.out.println("Enter month value : ");
    	int month = sc.nextInt();
    	YearMonth value = YearMonth.of(year, month);
    	System.out.println("Month length : " + value.lengthOfMonth());
    }
}
 
 * */

package date.and.time;


import java.time.YearMonth;
import java.util.Scanner;

public class MonthLength {

    public static void main(String[] args) {
    	Scanner sc = new Scanner(System.in);
    	System.out.println("Enter year value : ");
    	int year = sc.nextInt();
    	System.out.println("Enter month value : ");
    	int month = sc.nextInt();
    	YearMonth value = YearMonth.of(year, month);
    	System.out.println("Month length : " + value.lengthOfMonth());
    }
}
