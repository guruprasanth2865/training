/*
Requirements:
    -  10. Write a Java program to get a date before and after 1 year compares to the current date. 
    
Entities:
    - DateFinder
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class DateFinder
    - Declare and define main method
    - Declare a object of type-Calendar and its getInstance() method
    - Get the current date with getTime() method
    - Get previous year by value of -1 in add() method
    - Get the next year with the value of 2 in add() method
    - Print the values
    
Psudocode:
public class DateFinder {

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		System.out.println("Current Date : " + calendar.getTime());
		calendar.add(Calendar.YEAR, -1);
		System.out.println("Previous year : " + calendar.getTime());
		calendar.add(Calendar.YEAR, 2);
		System.out.println("Next year : " + calendar.getTime());
	}
}
 
 * */


package date.and.time;

import java.util.Calendar;
import java.util.Date;

public class DateFinder {

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		System.out.println("Current Date : " + calendar.getTime());
		calendar.add(Calendar.YEAR, -1);
		System.out.println("Previous year : " + calendar.getTime());
		calendar.add(Calendar.YEAR, 2);
		System.out.println("Next year : " + calendar.getTime());
	}
}
