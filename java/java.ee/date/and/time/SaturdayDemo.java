/*
Requirements:
    - 3. Write an example that, for a given month of the current year, lists all of the Saturdays in that month.

Entities:
    - SaturdayDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class- SaturdayDemo
    - Declare and define main method
    - Get the month input from user
    - Invoke function of Month class and set the input month
    - Set the LocalDate in a LocalDate-object and print the saturdays date with the given month
    
Psudocode:
public class SaturdayDemo {
	    public static void main(String[] args) {
	        Scanner sc = new Scanner(System.in);
	        System.out.println("Enter month : \n");
	        String monthValue = sc.next();
	        Month month = Month.valueOf(monthValue.toUpperCase());
	        System.out.printf("Month : \n", month);
	        LocalDate date = Year.now()
	        		             .atMonth(month)
	        		             .atDay(1)
	        		             .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
	        Month persentValue = date.getMonth();
	        while (persentValue == month) {
	            System.out.println(date);
	            date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
	            persentValue = date.getMonth();
	        }
	        sc.close();
	    }
}
 
 * */

package date.and.time;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

public class SaturdayDemo {
	    public static void main(String[] args) {
	        Scanner sc = new Scanner(System.in);
	        System.out.println("Enter month : \n");
	        String monthValue = sc.next();
	        Month month = Month.valueOf(monthValue.toUpperCase());
	        System.out.printf("Month : \n", month);
	        LocalDate date = Year.now()
	        		             .atMonth(month)
	        		             .atDay(1)
	        		             .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
	        Month persentValue = date.getMonth();
	        while (persentValue == month) {
	            System.out.println(date);
	            date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
	            persentValue = date.getMonth();
	        }
	        sc.close();
	    }
}
