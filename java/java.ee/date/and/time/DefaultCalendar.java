/*
Requiremetns:
    -7. Write a Java program to get and display information (year, month, day, hour, minute) of a default calendar 
    
Entities:
    - DefaultCalendar
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class DefaultCalendar
    - Declare and define main method
    - Create a instance of Calendar class with its getInstance() method
    - Use get() method with different parameters (Calender.YEAR, Calender.MONTH, Calender.DATE, Calender.HOUR, 
      Calender.MINUTE)
      
psudocode:
public class DefaultCalendar {

    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        System.out.println("Year => " + calendar.get(Calendar.YEAR));
        System.out.println("Month => " + calendar.get(Calendar.MONTH));
        System.out.println("Date => " + calendar.get(Calendar.DATE));
        System.out.println("Hour => " + calendar.get(Calendar.HOUR));
        System.out.println("Minute => " + calendar.get(Calendar.MINUTE));
    }
}
 
 * */


package date.and.time;

import java.util.Calendar;

public class DefaultCalendar {

    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        System.out.println("Year => " + calendar.get(Calendar.YEAR));
        System.out.println("Month => " + calendar.get(Calendar.MONTH));
        System.out.println("Date => " + calendar.get(Calendar.DATE));
        System.out.println("Hour => " + calendar.get(Calendar.HOUR));
        System.out.println("Minute => " + calendar.get(Calendar.MINUTE));
    }
}
