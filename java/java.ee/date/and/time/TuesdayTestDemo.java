/*
 Requirements:
     - 4. Write an example that tests whether a given date occurs on Tuesday the 11th.
     
Entities:
    - TuesdayTestDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class TuesdayTestDemo
    - Declare and deploy main method
    - Get input date with scanner and store it in a LocalDate object
    - Invoke the class CheckTuesday
    - In CheckTuesday class, queryFrom() method gets a date and return boolean values
    
Psudocode:
class CheckTuesday implements TemporalQuery<Boolean> {
    public Boolean queryFrom(TemporalAccessor date) {
        return ((date.get(ChronoField.DAY_OF_MONTH) == 11) && (date.get(ChronoField.DAY_OF_WEEK) == 2));
    }
}

public class TuesdayTestDemo {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter date in format (YYYY-MM-DD)");
        LocalDate date = LocalDate.parse(scanner.next());
        System.out.println(date.query(new CheckTuesday()));
        scanner.close();
    }

}
 
 * */


package date.and.time;

import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalQuery;
import java.time.temporal.ChronoField;
import java.util.Scanner;
import java.time.LocalDate;
import java.lang.Boolean;

class CheckTuesday implements TemporalQuery<Boolean> {
    public Boolean queryFrom(TemporalAccessor date) {
        return ((date.get(ChronoField.DAY_OF_MONTH) == 11) && (date.get(ChronoField.DAY_OF_WEEK) == 2));
    }
}

public class TuesdayTestDemo {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter date in format (YYYY-MM-DD)");
        LocalDate date = LocalDate.parse(scanner.next());
        System.out.println(date.query(new CheckTuesday()));
        scanner.close();
    }

}