/*
Requirements:
    -  2. Find the time difference using timer class ?
    
Entities:
    - TimerDemo
    
Function Definition:
    - public static void main(String[] args)
    
Job to be done:
    - Create class TimerDemo
    - Declare and define main method
    - Create a Tiimer object
    - Create another class RunTask extends TimerTask
    - In RunTask class, dclare and define run() method
    - Invoke the run() method with time delay of 3seconds
    
Psudocode:
public class TimerDemo {
	Timer timer;
	
	class RunTask extends TimerTask {
		public void run() {
		    System.out.println("Work Ends...");
		    timer.cancel();
	    }
	}

    public static void main(String[] args) {
    	TimerDemo time = new TimerDemo();
    	time.timer = new Timer();
    	time.timer.schedule(time.new RunTask(), 3000);
    	System.out.println("Work starts...");
    }
}
 
 * */


package date.and.time;

import java.util.Timer;
import java.util.TimerTask;

public class TimerDemo {
	Timer timer;
	
	class RunTask extends TimerTask {
		public void run() {
		    System.out.println("Work Ends...");
		    timer.cancel();
	    }
	}

    public static void main(String[] args) {
    	TimerDemo time = new TimerDemo();
    	time.timer = new Timer();
    	time.timer.schedule(time.new RunTask(), 3000);
    	System.out.println("Work starts...");
    }
}
