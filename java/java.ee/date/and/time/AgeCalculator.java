/*
Requirements:
    - 4. Write a Java program to calculate your age 
    
Entities:
    - AgeCalculator
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class- AgeCalculator
    - Declare and define main method
    - Create a LocalDate object - dob and set date of birth
    - Create another LocalDate object and set the current date
    - Use Period class and  its method between() method to find the difference
    - Print the year, month and day difference 
    
PsudoCode:
public class AgeCalculator {

    public static void main(String[] args) {
    	LocalDate dob = LocalDate.of(2000, 9, 30);
    	LocalDate currentDate = LocalDate.now();
    	Period differ = Period.between(dob, currentDate);
    	System.out.println("Age is " + differ.getYears() + " years, " + differ.getMonths() + " months, " + differ.getDays() +
    			" days old");
    }
}
 
 * */

package date.and.time;

import java.time.LocalDate;
import java.time.Period;

public class AgeCalculator {

    public static void main(String[] args) {
    	LocalDate dob = LocalDate.of(2000, 9, 30);
    	LocalDate currentDate = LocalDate.now();
    	Period differ = Period.between(dob, currentDate);
    	System.out.println("Age is " + differ.getYears() + " years, " + differ.getMonths() + " months, " + differ.getDays() +
    			" days old");
    }
}
