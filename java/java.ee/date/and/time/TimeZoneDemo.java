/*
Requirements:
    - 9 Display the default time zone and Any three particular time zone as mentioned below.
     Sample output: 
        Displaying current of the particular TimeZones
        GMT-27-09-2020 Sunday 08:36:28 AM
        Europe/Copenhagen-27-09-2020 Sunday 10:36:28 AM
        Australia/Perth-27-09-2020 Sunday 04:36:28 PM
        America/Los_Angeles-27-09-2020 Sunday 01:36:28 AM
        
Entities:
    - TimeZoneDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
   - Create class TimeZoneDemo
   - Declare and define main method
   - Create a TimeZone with its getTieZone() method
   - Create a instance of SimpleDateFormat and its setTimeZone() method
   - Invoke date() and format() method to Print the time zone 
   - Invoke getTimeZone() to get time zone and set them with setTimeZone(). Print them
   
Psudocode:
public class TimeZoneDemo {

    public static void main(String[] args) {
    	TimeZone zone = TimeZone.getTimeZone("GMT");
    	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy EEEE hh:mm:ss a");
    	dateFormat.setTimeZone(zone);
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		System.out.println("GMT-" + currentDate);
		zone = TimeZone.getTimeZone("US/Hawaii");
		dateFormat.setTimeZone(zone);
		currentDate = dateFormat.format(date);
		System.out.println("US/Hawaii : " + currentDate);
		zone = TimeZone.getTimeZone("Canada/Pacific");
		dateFormat.setTimeZone(zone);
		currentDate = dateFormat.format(date);
		System.out.println("Canada/Pacific : " + currentDate);
    }

 
 * */


package date.and.time;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimeZoneDemo {

    public static void main(String[] args) {
    	TimeZone zone = TimeZone.getTimeZone("GMT");
    	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy EEEE hh:mm:ss a");
    	dateFormat.setTimeZone(zone);
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		System.out.println("GMT-" + currentDate);
		zone = TimeZone.getTimeZone("US/Hawaii");
		dateFormat.setTimeZone(zone);
		currentDate = dateFormat.format(date);
		System.out.println("US/Hawaii : " + currentDate);
		zone = TimeZone.getTimeZone("Canada/Pacific");
		dateFormat.setTimeZone(zone);
		currentDate = dateFormat.format(date);
		System.out.println("Canada/Pacific : " + currentDate);
    }
}
