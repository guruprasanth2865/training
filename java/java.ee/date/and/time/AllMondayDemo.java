/*
Requiremetns:
    -  11. Write a Java program for a given month of the current year, lists all of the Mondays in that month.
    
Entities:
    - AllMondayDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class AllMondayDemo
    - Declare and define main method
    - Create a instance of Month class with parameter
    - Invoke TemporalAdjusters() method to get to get first monday andprint 
    - Invoke atMonth() and atDay() method to get current date
    - Invoke getMonth() method to get the month and check whether equals to given month
      1.1) Print the date and change the next monday with TemporalAdjusters() method 
      1.2) Get the month using getMonth()

Psudocode:
public class AllMondayDemo {

    public static void main(String[] args) {
    	Month month = Month.valueOf("OCTOBER");
    	System.out.println("For month " + month + " => ");
    	LocalDate date = Year.now().atMonth(month).atDay(1).with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
    	Month checker = date.getMonth();
    	while (checker == month) {
    		System.out.println(date);
    		date = date.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
            checker = date.getMonth();
    	}
    }
}
 
 * */


package date.and.time;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;

public class AllMondayDemo {

    public static void main(String[] args) {
    	Month month = Month.valueOf("OCTOBER");
    	System.out.println("For month " + month + " => ");
    	LocalDate date = Year.now().atMonth(month).atDay(1).with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
    	Month checker = date.getMonth();
    	while (checker == month) {
    		System.out.println(date);
    		date = date.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
            checker = date.getMonth();
    	}
    }
}
