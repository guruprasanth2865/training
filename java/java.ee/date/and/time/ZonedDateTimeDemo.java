/*
Requirements:
    -  12. Write a Java program to convert ZonedDateTime to Date.
    
Entities:
    - ZonedDateTimeDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class ZonedDateTimeDemo
    - Declare and define main method
    - Store the default timezone in a object of type-ZoneId
    - Get local date time with LocalDateTime class and its method now()
    - Invoke atZone() method to convert the zone time and print it
    - Get the current timezone with from() method and print it
    
Psudocode:
public class ZonedDateTimeDemo {

    public static void main(String[] args) {
    	ZoneId defaultZone = ZoneId.systemDefault();
    	LocalDateTime localTime = LocalDateTime.now();
    	ZonedDateTime zoneTime = localTime.atZone(defaultZone);
    	System.out.println("Zone Date Time : " + zoneTime);
    	Instant zoneInstant = zoneTime.toInstant();
    	Date date = Date.from(zoneInstant);
		System.out.println("Date : " + date);
    }
}
 
 * */


package date.and.time;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class ZonedDateTimeDemo {

    public static void main(String[] args) {
    	ZoneId defaultZone = ZoneId.systemDefault();
    	LocalDateTime localTime = LocalDateTime.now();
    	ZonedDateTime zoneTime = localTime.atZone(defaultZone);
    	System.out.println("Zone Date Time : " + zoneTime);
    	Instant zoneInstant = zoneTime.toInstant();
    	Date date = Date.from(zoneInstant);
		System.out.println("Date : " + date);
    }
}
