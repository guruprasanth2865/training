/*
Requirements:
    - 1. create a pattern for password which contains
        8 to 15 characters in length
        Must have at least one uppercase letter
        Must have at least one lower case letter
        Must have at least one digit
        
Entities:
    - PasswordValidatorDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class PasswordValidatorDemo
    - Declare and define main method
    - Create a regex pattern and store it in a string
    - Create a pattern object and compile the regex pattern
    - Create a matcher object and match a sentence with the compiled pattern 
    - Check whether the sentence matches the regex pattern
    - Print success message if the sentence matches the regexx, else print failure message
    
Pseudocode:
public class PasswordValidatorDemo {

	public static void main(String[] args) {
		String regexPattern = "((?=.*\\d)(?=.*[A-Z])(?=.*[a-z]).{8,15})";
		String password = "BMW03series";
		Pattern pattern = Pattern.compile(regexPattern);
		Matcher match = pattern.matcher(password);
		if (match.matches() == true) {
		    System.out.println("Password - " + password + " is acceptable.");	
		} else {
			System.out.println("Password - " + password + " is not acceptable.");
		}
	}
}
 
 * */

package Regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidatorDemo {

	public static void main(String[] args) {
		String regexPattern = "((?=.*\\d)(?=.*[A-Z])(?=.*[a-z]).{8,15})";
		String password = "BMW03series";
		Pattern pattern = Pattern.compile(regexPattern);
		Matcher match = pattern.matcher(password);
		if (match.matches() == true) {
		    System.out.println("Password - " + password + " is acceptable.");	
		} else {
			System.out.println("Password - " + password + " is not acceptable.");
		}
	}
}
