/*
Requirements:
    - 3.)create a username for login website which contains
        8-12 characters in length
        Must have at least one uppercase letter
        Must have at least one lower case letter
        Must have at least one digit
    
Entities:
    - UserValidation
    
Function Declaration: 
    - public static void main(String[] args)
    
Job to be done:
    - Create class UserValidation
    - Declare and define main method
    - Declare a string and define the regex pattern
    - Declare a string and give a username as value
    - Compile the regex pattern and store in pattern
    - Create a matcher object with the compiled regex pattern and sentence
    - check whether the matcher matches or not and print result
 
Pseudocode:
public class UserValidation {

    public static void main(String[] args) {
        String userName = "Guru2865";
        String regexPattern = "(?=.*[0-9])(?=:*[A-Z])(?=.*[a-z]).{8,12}";
        Pattern pattern = Pattern.compile(regexPattern);
        Matcher match = pattern.matcher(userName);
        if(match.matches()) {
        	System.out.println("Username is valid");
        } else {
        	System.out.println("Username is not valid");
        }
    }
}
 
 
 * */

package Regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidation {

    public static void main(String[] args) {
        String userName = "Guru2865";
        String regexPattern = "(?=.*[0-9])(?=:*[A-Z])(?=.*[a-z]).{8,12}";
        Pattern pattern = Pattern.compile(regexPattern);
        Matcher match = pattern.matcher(userName);
        if(match.matches()) {
        	System.out.println("Username is valid");
        } else {
        	System.out.println("Username is not valid");
        }
    }
}



