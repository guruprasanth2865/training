/*
Requirements:
    - Find the no of occurrence of a pattern in the text and also fetch the start and 
end index of the the occurrence.

Entities:
    - PatternFinder
    
Function Declaration:
    - public static void main(String[] args)

Job to be done:
    - Create a class
    - declare and define main method
    - Declare a sentence of type string
    - Create a pattern and compile it
    - Create a matcher object and match the sentence with compiled pattern
    - then find the groups with find() and group() method and print the starting and ending index with start() and end() method 
    
Pseudocode:
public class PatternFinder {

    public static void main(String[] args) {
    	String sentence = "Ramu is playing cricket.";
    	Pattern pattern = Pattern.compile("a");
    	Matcher match = pattern.matcher(sentence);
    	while (match.find()) {
    		System.out.println(match.group() + " starts at " + match.start() + " and ends at " + match.end());
    	}
    }
}
 
 * */

package Regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternFinder {

    public static void main(String[] args) {
    	String sentence = "Ramu is playing cricket.";
    	Pattern pattern = Pattern.compile("a");
    	Matcher match = pattern.matcher(sentence);
    	while (match.find()) {
    		System.out.println(match.group() + " starts at " + match.start() + " and ends at " + match.end());
    	}
    }
}
