/*
Requirements:
    -  split any random text using any pattern you desire.
    
Entities:
    - RandomSpliter
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class RandomSpliter
    - Declare and define a sentence of type string
    - Declare a sentence of type string
    - Split the string with white space
    - print the split string with a for loop
    
Pseudocode:
public class RandomSpliter {

    public static void main(String[] args) {
    	String sentence = "Ramu is playing football.";
    	for (String str : sentence.split("\\s")) {
    		System.out.println(str);
    	}
    }
}
 
 * */

package Regex;

public class RandomSpliter {

    public static void main(String[] args) {
    	String sentence = "Ramu is playing football.";
    	for (String str : sentence.split("\\s")) {
    		System.out.println(str);
    	}
    }
}
