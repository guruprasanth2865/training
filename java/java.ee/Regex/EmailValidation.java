/*
Requirements:
    - 2.)write a program for email-validation?
    
Entities:
    - EmailValidation
    
Function Declaration: 
    - public static void main(String[] args)
    
Job to be done:
    - Create class EmailValidation
    - Declare and define main method
    - Declare a string and define the regex pattern
    - Declare a string and give a mail address as value
    - Compile the regex pattern and store in pattern
    - Create a matcher object with the compiled regex pattern and sentence
    - check whether the matcher matches or not and print result
 
 * */

package Regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidation {

    public static void main(String[] args) {
        String mailId = "18ec042@kpriet.ac.in";
        String regexPattern = "[a-z0-9]{7}[@][a-z]{4,8}[.](com|in|ac.in)";
        Pattern pattern = Pattern.compile(regexPattern);
        Matcher match = pattern.matcher(mailId);
        if(match.matches()) {
        	System.out.println("Email is valid");
        } else {
        	System.out.println("Email is not valid");
        }
    }
}
