/*
Requirements:
    - 2.) For the following code use the split method() and print in sentence
          String website = "https-www-google-com";
          
Entities:
    - WebSpliter
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class WebSpliter
    - Declare and define main method
    - Declare a variable of type string and set the given value
    - Split it with split("-") method and print the result with for loop
    
Pseudocode:
public class WebSpliter {

    public static void main(String[] args) {
    	String website = "https-www-google-com";
    	for (String str : website.split("-")) {
    		System.out.println(str);
    	}
    }
}

 
 * */

package Regex;

public class WebSpliter {

    public static void main(String[] args) {
    	String website = "https-www-google-com";
    	for (String str : website.split("-")) {
    		System.out.println(str);
    	}
    }
}
