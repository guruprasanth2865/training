/*
Requirements:
    - 1.)Write a Java program to calculate the revenue from a sale based on the unit price and quantity of a product input by the user.
         The discount rate is 10% for the quantity purchased between 100 and 120 units, and 15% for the quantity purchased greater than 
         120 units. If the quantity purchased is less than 100 units, the discount rate is 0%. See the example output as shown below:
         Enter unit price: 25
         Enter quantity: 110
         The revenue from sale: 2475.0$
         After discount: 275.0$(10.0%)
 
Entities:
    - RegexCalculator
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class Calculator
    - Define a method calculate(float unitValue, int quantity)
    - Check if quantity is less than 100, then return product of quantity and unitValue
    - check if quantity is between 100 and 120, return the product with 10% discount
    - check if quantity is greater than 120 and return the product with 15%
    - Declare and define main method
    - Get the values of unit and quantity and invoke calculate(float unitValue, int quantity) method and print the returned value
    
Pseudocode:
public class Calculator {
	
	public static void calculate(float unitValue, int quantity) {
		if (quantity < 100) {
			System.out.println("The revenue from sale : "+unitValue * ((float) quantity));
			System.out.println("After discount: 0.0$(0%)");
		} else if (quantity >= 100 & quantity <=120) {
			System.out.println("The revenue from sale : " + ((unitValue * ((float) quantity )) - (0.1f * unitValue * ((float) quantity))));
			System.out.println("After discount: " + (0.1f * unitValue * ((float) quantity)) + "$ (" + (0.1f * 100f) + "%)" );
		} else if (quantity > 120) {
			System.out.println("The revenue from sale : " + ((unitValue * ((float) quantity )) - (0.15f * unitValue * ((float) quantity))));
			System.out.println("After discount: " + (0.15f * unitValue * ((float) quantity)) + "$ (" + (0.15f * 100f) + "%)" );
		} else {
			System.out.println("Enter proper values");
		}
	}

    public static void main(String[] args) {
    	Scanner sc = new Scanner(System.in);
    	float unitValue = 0f;
    	int quantity = 0;
    	System.out.println("Enter Unit value (in float) : ");
    	unitValue = sc.nextFloat();
    	System.out.println("Enter quantity value : ");
    	quantity = sc.nextInt();
    	calculate(unitValue, quantity);
    }
}
 
 * */

package Regex;

import java.util.Scanner;

public class Calculator {
	
	public static void calculate(float unitValue, int quantity) {
		if (quantity < 100) {
			System.out.println("The revenue from sale : "+unitValue * ((float) quantity));
			System.out.println("After discount: 0.0$(0%)");
		} else if (quantity >= 100 & quantity <=120) {
			System.out.println("The revenue from sale : " + ((unitValue * ((float) quantity )) - (0.1f * unitValue * ((float) quantity))));
			System.out.println("After discount: " + (0.1f * unitValue * ((float) quantity)) + "$ (" + (0.1f * 100f) + "%)" );
		} else if (quantity > 120) {
			System.out.println("The revenue from sale : " + ((unitValue * ((float) quantity )) - (0.15f * unitValue * ((float) quantity))));
			System.out.println("After discount: " + (0.15f * unitValue * ((float) quantity)) + "$ (" + (0.15f * 100f) + "%)" );
		} else {
			System.out.println("Enter proper values");
		}
	}

    public static void main(String[] args) {
    	Scanner sc = new Scanner(System.in);
    	float unitValue = 0f;
    	int quantity = 0;
    	System.out.println("Enter Unit value (in float) : ");
    	unitValue = sc.nextFloat();
    	System.out.println("Enter quantity value : ");
    	quantity = sc.nextInt();
    	calculate(unitValue, quantity);
    }
}
