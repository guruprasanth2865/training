/*
Requirements:
    - 1.)write a program for java regex quantifier?
    
Entities:
    - QuantifierDemo
    
Function Declaration:
    - public static void main(String[] args)

Job to be done:
    - Create a class QuantifierDemo
    - Declare and define main method
    - Declare a variable of type string and store a value
    - Create a compiled pattern for matching regex
    - Create a matcher object to match the string and compiled pattern
    - Find the matches with find() and group(). Find their index with start() and end() method
 
Pseudocode:

public class QuantifierDemo {

    public static void main(String[] args) {
    	String sentence = "Ramu is a team member of football and volleyball.";
    	Pattern pattern = Pattern.compile("l+");
    	Matcher match = pattern.matcher(sentence);
    	while(match.find()) {
    		System.out.println(match.group() + " starts at " + match.start() + " and ends with " + match.end());
    	}
    }
}
 
 * */

package Regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QuantifierDemo {

    public static void main(String[] args) {
    	String sentence = "Ramu is a team member of football and volleyball.";
    	Pattern pattern = Pattern.compile("l+");
    	Matcher match = pattern.matcher(sentence);
    	while (match.find()) {
    		System.out.println(match.group() + " starts at " + match.start() + " and ends with " + match.end());
    	}
    }
}
