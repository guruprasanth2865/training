/*
Requirements:
    - 2.)write a program to display the following using escape sequence in java
          A. My favorite book is "Twilight" by Stephanie Meyer
          B.She walks in beauty, like the night, 
            Of cloudless climes and starry skies 
            And all that's best of dark and bright 
            Meet in her aspect and her eyes�
          C."Escaping characters", � 2019 Java 

Entities:
    - EscapeSequentceDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class EscapeSequenceDemo
    - declare and define main method
    - Print -  My favorite book is "Twilight" by Stephanie Meyer
    - Print the 
            She walks in beauty, like the night, 
            Of cloudless climes and starry skies 
            And all that's best of dark and bright 
            Meet in her aspect and her eyes�
            using "\n"
    - print "Escaping characters", � 2019 Java" using \u00A9
    
Pseudocode:
public class EscapeSequenceDemo {
    
	public static void main(String[] args) {
		System.out.println("My favorite book is \"Twilight\" by Stephanie Meyer");
		System.out.println("She walks in beauty, like the night, \nOf cloudless climes and starry skies \n"
				+ "And all that's best of dark and bright \nMeet in her aspect and her eyes�");
		System.out.println("\"Escaping characters\", \u00A9 2019 Java ");
	}
}

 * */

package Regex;

public class EscapeSequenceDemo {
    
	public static void main(String[] args) {
		System.out.println("My favorite book is \"Twilight\" by Stephanie Meyer");
		System.out.println("She walks in beauty, like the night, \nOf cloudless climes and starry skies \n"
				+ "And all that's best of dark and bright \nMeet in her aspect and her eyes�");
		System.out.println("\"Escaping characters\", \u00A9 2019 Java ");
	}
}
