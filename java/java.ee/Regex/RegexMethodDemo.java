/*
Requirements:
    - 1.) write a program for Java String Regex Methods?
    
Entities:
    - RegexMethodDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Methods:
    matches()
    split()
    replaceFirst()
    replaceAll()
    
Job to be done:
    - Create class RegexMethodDemo
    - Declare and define main method
    - Execute the regex methods and print the results

Pseudocode:
public class RegexMethodDemo {

    public static void main(String[] args) {
    	String sentence = "Ramu like space. Ramu is learning about space and research";
    	System.out.println("Sentence : " + sentence);
    	System.out.println("matches() method : ");
    	System.out.println(sentence.matches(".*research"));
    	System.out.println("split() method : ");
    	for (String str : sentence.split(" ")) {
    		System.out.println(str);
    	}
    	System.out.println("replaceFirst() : ");
    	System.out.println(sentence.replaceFirst("space", "geology"));
    	System.out.println("replaceAll() : ");
    	System.out.println(sentence.replaceAll("space", "medical"));
    }
}

 
 * */

package Regex;

public class RegexMethodDemo {

    public static void main(String[] args) {
    	String sentence = "Ramu like space. Ramu is learning about space and research";
    	System.out.println("Sentence : " + sentence);
    	System.out.println("matches() method : ");
    	System.out.println(sentence.matches(".*research"));
    	System.out.println("split() method : ");
    	for (String str : sentence.split(" ")) {
    		System.out.println(str);
    	}
    	System.out.println("replaceFirst() : ");
    	System.out.println(sentence.replaceFirst("space", "geology"));
    	System.out.println("replaceAll() : ");
    	System.out.println(sentence.replaceAll("space", "medical"));
    }
}
