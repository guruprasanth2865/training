/*
Requirements:
    - + Write a program which implement multithreading using sleep(), setPriority(), getPriorty(), Name(), getId() methods.
    
Entities:
    - ThreadDemo

Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class ThreadDemo that extends Thread
    - Declare and define main method
    - Create a thread.
    - Invoke method sleep()
    - In threadDemo class, Declare and define a run method
    - Invoke currentThread(), setPriority(), getId() method
    
Pseudocode:
public class ThreadDemo extends Thread {
	
	public void run() {
		for (int i=10; i<15; i++) {
			System.out.println(Thread.currentThread().getName() + "-" + i);
			try {
				System.out.println ("Thread " +  Thread.currentThread().getId() + " is running...");
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

    public static void main(String[] args) {
    	Thread thread = new Thread(new ThreadDemo());
    	thread.start();
    	Thread.currentThread().setPriority(10);
    	System.out.println("main thread - priority : " + Thread.currentThread().getPriority()); 
    }
}
 
 * */

package Multithreading;

public class ThreadDemo extends Thread {
	
	public void run() {
		for (int i=10; i<15; i++) {
			System.out.println(Thread.currentThread().getName() + "-" + i);
			try {
				System.out.println ("Thread " +  Thread.currentThread().getId() + " is running...");
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

    public static void main(String[] args) {
    	Thread thread = new Thread(new ThreadDemo());
    	thread.start();
    	Thread.currentThread().setPriority(10);
    	System.out.println("main thread - priority : " + Thread.currentThread().getPriority()); 
    }
}
