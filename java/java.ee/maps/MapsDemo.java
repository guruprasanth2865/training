/*
Requirements:
    1) Write a Java program to copy all of the mappings from the specified map to another map?
    2) Write a Java program to test if a map contains a mapping for the specified key?
    3) Count the size of mappings in a map?
    4) Write a Java program to get the portion of a map whose keys range from a given key to another key?
    
Entities:
    MapsDemo
    
Function Declaration:
    public static void main(String[] args)
   
Job to be done:
    - Create class - MapsDemo
    - Declare main method
    - Create a map - cars with key and value of type string
    - Add 5 values into the map - cars, with keys
    - Create another map - newCars  with key and value of type string
    - Copy the values from cars to newCars with for loop
    - Check whether the map-cars contains key-"BMW"
    - Count the size of the map
    - Get the portion of a map whose keys range from a given key to another key

Pseudo code:
public class MapsDemo {
    
	public static void main(String[] args) {
		Map<String, String> cars = new HashMap<>();
		Map<String, String> newCars = new HashMap<>();
		TreeMap<String, String> oldCars = new TreeMap<String, String>();
		cars.put("BMW", "Series A1");
		cars.put("Benz", "Class A");
		cars.put("Mazda", "Model M1");
		cars.put("Toyota", "Inovva");
		cars.put("Suzuki", "Swift");
		newCars.putAll(cars);
		oldCars.putAll(cars);
		System.out.println("cas is copyed to newCars map : ");
		for(String key : cars.keySet()) {
			System.out.println(key + " - " + cars.get(key));
		}
		if(cars.containsKey("Benz")) {
			System.out.println("Cars contain cars - Benz");
		} else {
			System.out.println("Cars does not contains - Benz");
		}
		System.out.println("Size of map-cars is " + cars.size());
		System.out.println(oldCars.subMap("Benz", "Toyota"));
	}
}

 */

package maps;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapsDemo {

	public static void main(String[] args) {
		Map<String, String> cars = new HashMap<>();
		Map<String, String> newCars = new HashMap<>();
		TreeMap<String, String> oldCars = new TreeMap<String, String>();
		cars.put("BMW", "Series A1");
		cars.put("Benz", "Class A");
		cars.put("Mazda", "Model M1");
		cars.put("Toyota", "Inovva");
		cars.put("Suzuki", "Swift");
		newCars.putAll(cars);
		oldCars.putAll(cars);
		System.out.println("cas is copyed to newCars map : ");
		for(String key : cars.keySet()) {
			System.out.println(key + " - " + cars.get(key));
		}
		if(cars.containsKey("Benz")) {
			System.out.println("Cars contain cars - Benz");
		} else {
			System.out.println("Cars does not contains - Benz");
		}
		System.out.println("Size of map-cars is " + cars.size());
		System.out.println(oldCars.subMap("Benz", "Toyota"));
	}
}
