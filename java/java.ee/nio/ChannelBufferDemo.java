/*
Requirements:
    -  1) Write a program to write and read the string to a channel using buffer(to demonstrate Pipe)
    
Entities:
    - ChannelBufferDemo
    
Function Declaration:
    - public static void main(String[] args)

Job to be done:
    - Create a class ChannelBufferDemo
    - Declare and definemain method
    - Create a object of type-File and set the filepath
    - Set the output-stream for the file
    - Create a FileChannel with getChannel() method
    - Use ByteBuffer and its allocate() method t execute
    
Psudocode:
public class ChannelBufferDemo {

    public static void main(String[] args) {
    	File file = new File("E:\\test\\niodemo.txt"); 
    	String content = "Hello World...";
    	FileOutputStream outputFile = null;
    	try {
    	    outputFile = new FileOutputStream(file, true);
    	    System.out.println("File stream is created");
        } catch (FileNotFoundException e) {
    	    e.printStackTrace(System.err);
    	}
    	FileChannel output = outputFile.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        for (char ch : content.toCharArray()) {
          buffer.putChar(ch);
        }
        buffer.flip(); 
        try {
        	output.write(buffer);
			outputFile.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        System.out.println("Content is written to file");
    }
}

 
 * */

package nio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class ChannelBufferDemo {

    public static void main(String[] args) {
    	File file = new File("E:\\test\\niodemo.txt"); 
    	String content = "Hello World...";
    	FileOutputStream outputFile = null;
    	try {
    	    outputFile = new FileOutputStream(file, true);
    	    System.out.println("File stream is created");
        } catch (FileNotFoundException e) {
    	    e.printStackTrace(System.err);
    	}
    	FileChannel output = outputFile.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        for (char ch : content.toCharArray()) {
          buffer.putChar(ch);
        }
        buffer.flip(); 
        try {
        	output.write(buffer);
			outputFile.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        System.out.println("Content is written to file");
    }
}
