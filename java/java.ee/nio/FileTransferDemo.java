/*
Requirements:
    - 1.Create two files named source and destination .Transfer the data from source file to destination file using nio filechannel. 
    
Entities:
    - FileTransferDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class FileTransferDemo
    - Declare and define main method
    - Create a input file path and output file path with FileOutputStream and FileInputStream. 
    - with the help of transferTo() method, transfer the content.
    
Psudocode:
public class FileTransferDemo {

    public static void main(String[] args) {
    	try {
			FileOutputStream outputFile = new FileOutputStream(new File("E:\\test\\destination.txt"));
			WritableByteChannel writer = outputFile.getChannel();
			FileInputStream inputFile = new FileInputStream(new File("E:\\test\\source.txt"));
			FileChannel reader = inputFile.getChannel();
			reader.transferTo(0, reader.size(), writer);
			outputFile.flush();
			outputFile.close();
			inputFile.close();
			reader.close();
			writer.close();
			System.out.println("File copied ...");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
    
 * */

package nio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;

public class FileTransferDemo {

    public static void main(String[] args) {
    	try {
			FileOutputStream outputFile = new FileOutputStream(new File("E:\\test\\destination.txt"));
			WritableByteChannel writer = outputFile.getChannel();
			FileInputStream inputFile = new FileInputStream(new File("E:\\test\\source.txt"));
			FileChannel reader = inputFile.getChannel();
			reader.transferTo(0, reader.size(), writer);
			outputFile.flush();
			outputFile.close();
			inputFile.close();
			reader.close();
			writer.close();
			System.out.println("File copied ...");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
