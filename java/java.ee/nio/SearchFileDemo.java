/*
Requirements:
    - 2) Search any file using walkFileTree() method with enum instance(SKIP_SIBLINGS,SKIP_SUBTREE).

Entities:
    - SearchFileDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Jobs to be done:
    - Create class SearchFileDemo
    - Declare and define main method
    - Create a object-FileVisitor 
    - Invoke a walkFileTree() 
    - Invoke postVisitDirectory() to return enum SKIP_SUBTREE and SKIP_SIBLINGS.
    - visitFile method to return the directory containing all file paths
    - Invoke visitFile method to check exists file or not.
    
Psudocode:

public class SearchFileDemo {

	public static void main(String[] args) {
		try {
			FileVisitor<Path> fv = new SimpleFileVisitor<Path>() {
				@SuppressWarnings("static-access")
				public FileVisitResult postVisitDirectory(Path dir, IOException e ) throws IOException {
					return FileVisitResult.SKIP_SUBTREE.SKIP_SIBLINGS;
				}
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					System.out.println(file);
					return super.visitFile(file, attrs);
				}
			};
			Path path = Paths.get("E:\\test\\nio");
			Files.walkFileTree(path, fv);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
 
 * */


package nio;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;


public class SearchFileDemo {

	public static void main(String[] args) {
		try {
			FileVisitor<Path> fv = new SimpleFileVisitor<Path>() {
				@SuppressWarnings("static-access")
				public FileVisitResult postVisitDirectory(Path dir, IOException e ) throws IOException {
					return FileVisitResult.SKIP_SUBTREE.SKIP_SIBLINGS;
				}
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					System.out.println(file);
					return super.visitFile(file, attrs);
				}
			};
			Path path = Paths.get("E:\\test\\nio");
			Files.walkFileTree(path, fv);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
