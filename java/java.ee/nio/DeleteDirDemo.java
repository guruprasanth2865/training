/*
Requirements:
    - 4) Delete the directory along with the files recursively.
    
Entities:
    - DeleteDirDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class DeleteDirDemo
    - Declare and define main method
    - Create a filepath of type-File
    - check if the file exists 
       1.1) Check whether the given filepath is a directory
       1.2) Check if the files are present in the directory.  
       1.3) Get all files in the directory and delete them

Psudocode:
public class DeleteDirDemo {

    public static void main(String[] args) {
    	File file = new File("E:\\test\\nio\\delete.txt");
		if(file.exists() == true) {
			if (file.isDirectory() == true) {
				if (file.list().length == 0) {
					file.delete();
					System.out.println("Directory deleted =>  " + file.getAbsolutePath());
				} else {
					File files[] = file.listFiles();
					for (File fileDelete : files) {
						file.delete();
						System.out.println("Directory deleted =>  " + file.getAbsolutePath());
					}
					if (file.list().length == 0) {
						file.delete();
						System.out.println("Directory deleted =>  " + file.getAbsolutePath());
					}
				}
			} else {
				file.delete();
				System.out.println("Directory deleted =>  " + file.getAbsolutePath());
			}
		}
    	
    }
}
 
 * */


package nio;

import java.io.File;

public class DeleteDirDemo {

    public static void main(String[] args) {
    	File file = new File("E:\\test\\nio\\delete.txt");
		if(file.exists() == true) {
			if (file.isDirectory() == true) {
				if (file.list().length == 0) {
					file.delete();
					System.out.println("Directory deleted =>  " + file.getAbsolutePath());
				} else {
					File files[] = file.listFiles();
					for (File fileDelete : files) {
						file.delete();
						System.out.println("Directory deleted =>  " + file.getAbsolutePath());
					}
					if (file.list().length == 0) {
						file.delete();
						System.out.println("Directory deleted =>  " + file.getAbsolutePath());
					}
				}
			} else {
				file.delete();
				System.out.println("Directory deleted =>  " + file.getAbsolutePath());
			}
		}
    	
    }
}
