/*
Requirements:
    - 1) Write a program to demonstrate absolute and relative path.
    
Entities:
    - RelativeAbsoluteDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class RelativeAbsoluteDemo
    - Declare and define main method
    - Create a object of type FileOutputStream with the pathfile for absolute file
    - Create a PrintWriter object with the filepath and close
    - Create a object of type FileOutputStream with the file name for relative file
    - Create a PrintWriter object for relative file
    
Psudocode:
public class RelativeAbsoluteDemo {

    public static void main(String[] args) {
    	try {
			FileOutputStream abosluteFile = new FileOutputStream("E:\\test\\absolute.txt", true);
			PrintWriter aboslutePrinter = new PrintWriter(abosluteFile);
			aboslutePrinter.println("File For Absolute - absolute.txt");
			aboslutePrinter.close();
			System.out.println("Absolute file is created");
			FileOutputStream relativeFile = new FileOutputStream("relative.txt", true);
			PrintWriter relativePrinter = new PrintWriter(relativeFile);
			relativePrinter.println("Sample File For Relative");
			relativePrinter.close();
			System.out.println("Rlative file is created");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}

 
 * */


package nio;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class RelativeAbsoluteDemo {

    public static void main(String[] args) {
    	try {
			FileOutputStream abosluteFile = new FileOutputStream("E:\\test\\absolute.txt", true);
			PrintWriter aboslutePrinter = new PrintWriter(abosluteFile);
			aboslutePrinter.println("File For Absolute - absolute.txt");
			aboslutePrinter.close();
			System.out.println("Absolute file is created");
			FileOutputStream relativeFile = new FileOutputStream("relative.txt", true);
			PrintWriter relativePrinter = new PrintWriter(relativeFile);
			relativePrinter.println("Sample File For Relative");
			relativePrinter.close();
			System.out.println("Rlative file is created");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
