/*
Requirements:
     - 1) Perform file operations using exists(), createDirectory(), copy(), move() and delete() methods.
     
Entities:
    - FileOperationDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class FileOperationDemo
    - Declare and define main method
    - Create a text filoe with method createTempFile()
    - Print whether file is present
    - Use copy() method to copy the content from one file to another file and print the status
    - Use createDirectory() method to create a directory
    - Delete the file using delete() method
    - Create two file path to move one to one another file.
    
Psudocode:
public class FileOperationDemo {

    public static void main(String[] args) {
    	//exist method
        Path filepath = null;
		try {
			filepath = Files.createTempFile("demo", ".txt");
			System.out.println("File to check : " + filepath);
	        boolean exists = Files.exists(filepath);
	        System.out.println("File to check exits: " + exists);
	        Path copy = Files.createTempFile("copy-sample", ".txt");
	        System.out.println("file to copy: " + filepath);
	        Path copiedFile = Files.copy(
	        		copy,
	                Paths.get("E:\\test\\nio" + copy.getFileName()),
	                StandardCopyOption.REPLACE_EXISTING
	        );
	        System.out.println("file copied: " + copiedFile);
	        Path tempPath = Files.createTempDirectory("test");
	        Path dirToCreate = tempPath.resolve("test1");
	        Path directory = Files.createDirectory(dirToCreate);
	        System.out.println("directory created: " + directory);
	        System.out.println("dir created exits: " + Files.exists(directory));
	        Files.delete(filepath);
	        boolean existsNow = Files.exists(filepath);
	        System.out.println("File exits after deleting: " + existsNow);
	        Path sourcePath = Paths.get("E:\\test\\source.txt");
	        Path destinationPath = Paths.get("E:\\test\\destination.txt");
	        Files.move(sourcePath, destinationPath,
                    StandardCopyOption.REPLACE_EXISTING);
            System.out.println("Successfully Moved");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    }
}

 
 * */


package nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class FileOperationDemo {

    public static void main(String[] args) {
    	//exist method
        Path filepath = null;
		try {
			filepath = Files.createTempFile("demo", ".txt");
			System.out.println("File to check : " + filepath);
	        boolean exists = Files.exists(filepath);
	        System.out.println("File to check exits: " + exists);
	        Path copy = Files.createTempFile("copy-sample", ".txt");
	        System.out.println("file to copy: " + filepath);
	        Path copiedFile = Files.copy(
	        		copy,
	                Paths.get("E:\\test\\nio" + copy.getFileName()),
	                StandardCopyOption.REPLACE_EXISTING
	        );
	        System.out.println("file copied: " + copiedFile);
	        Path tempPath = Files.createTempDirectory("test");
	        Path dirToCreate = tempPath.resolve("test1");
	        Path directory = Files.createDirectory(dirToCreate);
	        System.out.println("directory created: " + directory);
	        System.out.println("dir created exits: " + Files.exists(directory));
	        Files.delete(filepath);
	        boolean existsNow = Files.exists(filepath);
	        System.out.println("File exits after deleting: " + existsNow);
	        Path sourcePath = Paths.get("E:\\test\\source.txt");
	        Path destinationPath = Paths.get("E:\\test\\destination.txt");
	        Files.move(sourcePath, destinationPath,
                    StandardCopyOption.REPLACE_EXISTING);
            System.out.println("Successfully Moved");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    }
}
