/*
Requiremetns:
    -  3) Delete a file using visitFile() method.
    
Entities:
    - VisitFileDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class VisitFileDemo
    - Declare and deploy main method
    - Create a object of type-FileVisitor 
    - Check file get file Name using getFileName()
    - convert to String using toString() , contains method to file exists or not.
    - Invoke Files class delete method to delete file
    - Return the FileVisitResult class CONTINUE to stop process.
    
Psudocode:
public class VisitFileDemo {

    public static void main(String[] args) {
    	FileVisitor<Path> file = new SimpleFileVisitor<Path>() {
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				if(file.getFileName().toString().contains("deletefile.txt")) {
					Files.delete(file);
				}
				return FileVisitResult.CONTINUE ;
			}
		};
		try {
			Files.walkFileTree(Paths.get("E:\\test\\nio"), file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
 
 * */


package nio;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class VisitFileDemo {

    public static void main(String[] args) {
    	FileVisitor<Path> file = new SimpleFileVisitor<Path>() {
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				if(file.getFileName().toString().contains("deletefile.txt")) {
					Files.delete(file);
				}
				return FileVisitResult.CONTINUE ;
			}
		};
		try {
			Files.walkFileTree(Paths.get("E:\\test\\nio"), file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
