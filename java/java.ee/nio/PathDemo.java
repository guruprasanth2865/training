/*
Requirements:
    -  1) Write a program to demonstrate toAbsolutePath(), normalize(), getName(), getFileName() and getFileCount()
    
Entities:
    - PathDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class PathDemo
    - Declare and define main method
    - Create a path and toAbsolutePath()
    - Create a normalized path with normalize() method
    - Get the file path using Paths class get method pass file path.
    - Get get index file Name using getName method and print the index.
    - Get get file Name using getFileName method and print the filepath.
    - Get get the no of in directory using size method with length and print the number of file
    
Psudocode:
public class PathDemo {

    public static void main(String[] args) {
    	Path filepath = Paths.get("PathDemo.java");
    	System.out.println("Absolute : " + filepath.toAbsolutePath());
    	Path fullPath = Paths.get("C:\\Users\\gurup\\eclipse-workspace\\javaee-demo\\src\\nio\\PathDemo.java");
    	System.out.println("Normalized path : " + fullPath.normalize());
    	System.out.println("Index : " + fullPath.getName(0));
    	System.out.println("Filename : " + fullPath.getFileName().toString());
    	File fileDirectory = new File("C:\\Users\\gurup\\eclipse-workspace\\javaee-demo\\src\\nio");
		System.out.println("File Count:" + fileDirectory.list().length);
    }
}
 
 * */

package nio;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathDemo {

    public static void main(String[] args) {
    	Path filepath = Paths.get("PathDemo.java");
    	System.out.println("Absolute : " + filepath.toAbsolutePath());
    	Path fullPath = Paths.get("C:\\Users\\gurup\\eclipse-workspace\\javaee-demo\\src\\nio\\PathDemo.java");
    	System.out.println("Normalized path : " + fullPath.normalize());
    	System.out.println("Index : " + fullPath.getName(0));
    	System.out.println("Filename : " + fullPath.getFileName().toString());
    	File fileDirectory = new File("C:\\Users\\gurup\\eclipse-workspace\\javaee-demo\\src\\nio");
		System.out.println("File Count:" + fileDirectory.list().length);
    }
}
