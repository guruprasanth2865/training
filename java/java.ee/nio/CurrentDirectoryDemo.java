/*
Requirements:
    -1) Write a program to obtain current directory and parent directory 
    
Entities:
    - CurrentDirectoryDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class CurrentDirectoryDemo
    - declare and define main method
    - Use method-toAbsolutePath()
    - Create  java file in the same path
    - Print the current and parent direactory
    
public class CurrentDirectoryDemo {

    public static void main(String[] args) {
    	String currentPath = Paths.get("").toAbsolutePath().toString();
    	File file = new File(currentPath, "CurrentDir.java");
    	System.out.println("\nFile = " + file + "\nParent directory =  " + file.getParent());
    }
}
 
 * */


package nio;

import java.io.File;
import java.nio.file.Paths;

public class CurrentDirectoryDemo {

    public static void main(String[] args) {
    	String currentPath = Paths.get("").toAbsolutePath().toString();
    	File file = new File(currentPath, "CurrentDir.java");
    	System.out.println("\nFile = " + file + "\nParent directory =  " + file.getParent());
    }
}
