/*
Requirement:
    - 3) demonstrate linked hash set to array() method in java 
 
Entities:
    SetToArray
     
Function Declaration:
    - public static void main(String[] args)
    
Job to be done
    - Create a class SetToArray
    - Declare main function
    - Declare a set-cars
    - add five elements to set-cars
    - Declare a Array and copy all elements from set to array
    - Print the elements from array with a for loop
    
Pseudocode:
public class SetToArray {

    public static void main(String[] args) {
    	Set<String> cars = new LinkedHashSet<>();
    	cars.add("Honda");
    	cars.add("Mazda");
    	cars.add("BMW");
    	cars.add("Benz");
    	cars.add("Hummer");
    	String[] newCars = new String[cars.size()];
    	cars.toArray(newCars);
    	System.out.println("Elements converted to array : ");
    	for(int i = 0; i<newCars.length; i++) {
    		System.out.println(newCars[i]);
    	}
    }
}
    
 * */

package SetAndMap;
import java.util.Set;
import java.util.LinkedHashSet;

public class SetToArray {

    public static void main(String[] args) {
    	Set<String> cars = new LinkedHashSet<>();
    	cars.add("Honda");
    	cars.add("Mazda");
    	cars.add("BMW");
    	cars.add("Benz");
    	cars.add("Hummer");
    	String[] newCars = new String[cars.size()];
    	cars.toArray(newCars);
    	System.out.println("Elements converted to array : ");
    	for (int i = 0; i < newCars.length; i++) {
    		System.out.println(newCars[i]);
    	}
    }
}
