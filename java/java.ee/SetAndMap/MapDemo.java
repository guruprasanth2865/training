/*
Requirement:
    demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() ) 
    
Entities:
    - MapDemo
    
Function Declaration:
    public static void main(String[] args)
    
Job to be done:
    - Create class-MapDemo
    - Declare main method
    - Declare a map and add elements with put() method
    - Remove elements from maps with remove() method
    - Replace element of map with replace() method
    - Check whether a element presents in the map with containsValue()
    
Pseudocode:
public class MapDemo {

    public static void main(String[] args) {
    	Map<Integer, String> cars = new HashMap<>();
    	cars.put(1, "BMW");
    	cars.put(2, "Benz");
    	cars.put(3, "Renault");
    	cars.put(4, "Hummer");
    	cars.put(5, "Honda");
    	for(Integer key : cars.keySet()) {
    		System.out.println(key + " - " + cars.get(key));
    	}
    	System.out.println("Element removed with remove() method : " + cars.remove(1));
    	if(cars.containsValue("BMW")) {
    		System.out.println("Cars contains BMW");
    	} else {
    		System.out.println("Cars does not contain BMW");
    	}
    	cars.replace(2, "Bugati");
    	System.out.println("Value at key 2 is changed fromm Benz to Bugati :- ");
    	for(Integer key : cars.keySet()) {
    		System.out.println(key + " - " + cars.get(key));
    	}
    }
}

 
 * */

package SetAndMap;
import java.util.Map;
import java.util.HashMap;


public class MapDemo {

    public static void main(String[] args) {
    	Map<Integer, String> cars = new HashMap<>();
    	cars.put(1, "BMW");
    	cars.put(2, "Benz");
    	cars.put(3, "Renault");
    	cars.put(4, "Hummer");
    	cars.put(5, "Honda");
    	for (Integer key : cars.keySet()) {
    		System.out.println(key + " - " + cars.get(key));
    	}
    	System.out.println("Element removed with remove() method : " + cars.remove(1));
    	if (cars.containsValue("BMW")) {
    		System.out.println("Cars contains BMW");
    	} else {
    		System.out.println("Cars does not contain BMW");
    	}
    	cars.replace(2, "Bugati");
    	System.out.println("Value at key 2 is changed fromm Benz to Bugati :- ");
    	for (Integer key : cars.keySet()) {
    		System.out.println(key + " - " + cars.get(key));
    	}
    }
}
