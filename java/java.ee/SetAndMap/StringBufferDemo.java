/*
Requirement:
    - 4) java program to demonstrate insertions and string buffer in tree set
    
Entities:
    - StringBufferDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class StringBufferDemo
    - Declare main method
    - Declare a TreeSet-cars 
    - Insert 5 string buffer elements with add() method 
    
Pseudocode:
public class StringBufferDemo implements Comparator<StringBuffer>{
	
	@Override public int compare(StringBuffer s1, StringBuffer s2) 
    { 
        return s1.toString().compareTo(s2.toString()); 
    }

    public static void main(String[] args) {
	    Set<StringBuffer> cars = new TreeSet<>(new StringBufferDemo());
	    cars.add(new StringBuffer("BMW"));
	    cars.add(new StringBuffer("Benz"));
	    cars.add(new StringBuffer("Mazda"));
	    cars.add(new StringBuffer("Honda"));
	    cars.add(new StringBuffer("Hummer"));
	    System.out.println(cars);
    }
}
 
 * */

package SetAndMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.Comparator;

public class StringBufferDemo implements Comparator<StringBuffer>{
	
	@Override public int compare(StringBuffer s1, StringBuffer s2) 
    { 
        return s1.toString().compareTo(s2.toString()); 
    }

    public static void main(String[] args) {
	    Set<StringBuffer> cars = new TreeSet<>(new StringBufferDemo());
	    cars.add(new StringBuffer("BMW"));
	    cars.add(new StringBuffer("Benz"));
	    cars.add(new StringBuffer("Mazda"));
	    cars.add(new StringBuffer("Honda"));
	    cars.add(new StringBuffer("Hummer"));
	    System.out.println(cars);
    }
}
