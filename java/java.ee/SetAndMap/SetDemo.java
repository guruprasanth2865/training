/*
Requirements:
    - 1) java program to demonstrate adding elements, displaying, removing, and iterating in hash set
    
Entities:
    SetDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class SetDemo
    - Declare main method
    - Declare a set-cars
    - add five elements with add() method
    - Display the set
    - remove elements from set with remove() method
    - Traverse the set and print them with for loop
    
Pseudocode:
public class SetDemo {
	
    public static void main(String[] args) {
    	Set<String> cars = new HashSet<>();
    	cars.add("Mazda");
    	cars.add("Honda");
    	cars.add("Lambo");
    	cars.add("BMW");
    	cars.add("Benz");
    	System.out.println(cars);
    	cars.remove("BMW");
    	System.out.println("\nAfter removing BMW" + " - cars => " + cars);
    	System.out.println("\nAll elements from set-cars ");
    	for(String car : cars) {
    		System.out.println(car);
    	}
    }
}
 
 * */

package SetAndMap;
import java.util.Set;
import java.util.HashSet;

public class SetDemo {
	
    public static void main(String[] args) {
    	Set<String> cars = new HashSet<>();
    	cars.add("Mazda");
    	cars.add("Honda");
    	cars.add("Lambo");
    	cars.add("BMW");
    	cars.add("Benz");
    	System.out.println(cars);
    	cars.remove("BMW");
    	System.out.println("\nAfter removing BMW" + " - cars => " + cars);
    	System.out.println("\nAll elements from set-cars ");
    	for (String car : cars) {
    		System.out.println(car);
    	}
    }
}
