/*
Requirement:
    - 2) demonstrate program explaining basic add and traversal operation of linked hash set
    
Entities:
    LinkedHashSetDemo
    
Function Declaration:
    public static void main(String[] args)
    
Job to be done:
    - Create a Class LinkedHashSetDemo
    - Declare main method
    - Declare a LinkedHashSet-cars and add 5 elements in it
    - Traverse the LinkedHashSet-cars with for loop 
    
Pseudocode:
public class LinkedHashSetDemo {
    
    public static void main(String[] args) {
    	LinkedHashSet<String> cars = new LinkedHashSet<>();
    	cars.add("BMW");
    	cars.add("Aston Martin");
    	cars.add("Bugati");
    	cars.add("Jaguar");
    	cars.add("Ford");
    	for(String car : cars) {
    		System.out.println(car);
    	}
    }
}

 
 * */

package SetAndMap;
import java.util.LinkedHashSet;

public class LinkedHashSetDemo {
    
    public static void main(String[] args) {
    	LinkedHashSet<String> cars = new LinkedHashSet<>();
    	cars.add("BMW");
    	cars.add("Aston Martin");
    	cars.add("Bugati");
    	cars.add("Jaguar");
    	cars.add("Ford");
    	for (String car : cars) {
    		System.out.println(car);
    	}
    }
}
