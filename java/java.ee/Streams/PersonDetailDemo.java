/*
Requirements:
    - 2. Write a program to print minimal person with name and email address from the Person
      class using java.util.Stream<T>#map API
      
Entities:
    - PersonDetailDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class PersonDetailDemo
    - Declare and define main method
    - Get the persons list from class Person and its method createRoster()
    - Declare a map-personDetails
    - Add the email address as key and name as value in the map
    - Invoke collect() method to collect the minimal persons from list
    
Pseudocode:
public class PersonDetailDemo {

	 public static void main(String[] args) {
	     List<Person> persons = Person.createRoster();
	     Map<String, String> personDetails = new HashMap<>();
	     for (Person person : persons) {
	         personDetails.put(person.emailAddress, person.name);
	     }
	     Map<String, String> rosterMap = persons.stream()
	                                            .collect(Collectors.toMap(Person::getEmailAddress, Person::getName));
	     rosterMap.entrySet().forEach(System.out::println);
	}
}

 * */


package Streams;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PersonDetailDemo {

	 public static void main(String[] args) {
	     List<Person> persons = Person.createRoster();
	     Map<String, String> personDetails = new HashMap<>();
	     for (Person person : persons) {
	         personDetails.put(person.emailAddress, person.name);
	     }
	     Map<String, String> rosterMap = persons.stream()
	                                            .collect(Collectors.toMap(Person::getEmailAddress, Person::getName));
	     rosterMap.entrySet().forEach(System.out::println);
	}
}
