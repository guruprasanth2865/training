/*
Requirements:
    - 5. Write a program to collect the minimal person with name and email address from the Person class using 
        java.util.Stream<T> API as List

Entities:
    - MinimalPerson

Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Get the list of person from Person class and its createRoster() method
    - Person the stream method min() to get the person with minimal name and address
    - Print the filtered persons details

 * */

package Streams;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.*;

public class MinimalPerson {

	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		Optional<Person> smallPerson = persons.stream()
				                              .min((first, second) -> first.getName().compareTo(second.getName()) 
				                            		                & first.getEmailAddress().compareTo(second.getEmailAddress()));
		System.out.println(smallPerson.get().getName());
	}
}
