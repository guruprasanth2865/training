/*
Requirements:
    -  Print all the persons in the roster using java.util.Stream<T>#forEach 
    
Entities:
    - PersonDetail
    - Person
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class PersonDetail
    - Declare and define main method
    - Invoke createRoster() method and store the returned value in a list-persons
    - Use streams forEach() method to print each element from the list
    
Pseudocode:
public class PersonDetail {

    public static void main(String[] args) {
    	List<Person> persons = Person.createRoster();
    	System.out.println("Employee Details: ");
    	persons.stream().forEach((person) -> System.out.println(person.getName()));
    }
}
 
 * */

package Streams;

import java.util.stream.*;
import java.util.List;

public class PersonDetail {

    public static void main(String[] args) {
    	List<Person> persons = Person.createRoster();
    	System.out.println("Employee Details: ");
    	persons.stream().forEach((person) -> System.out.println(person.getName()));
    }
}
