/*
Requirements:
    -     11. sort the roster list based on the person's age in descending order using java.util.Stream
    
Entities:
    - AgeSortDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class AgeSortDemo
    - Declare and define main method
    - Get the person list from Person class and its method createRoster() method
    - Invoke sorted() method from stream to sort the person list 
    - Print the sorted list
 
 * */


package Streams;

import java.util.List;

public class AgeSortDemo {
    
	public static void main(String[] args) {
        List<Person> persons = Person.createRoster();
        persons.stream()
               .sorted(Person::compareByAge)
               .forEach(Person::printPerson);
    }

}
