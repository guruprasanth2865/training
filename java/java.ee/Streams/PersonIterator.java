/*
Requirements:
    - 10) Iterate the roster list in Persons class and and print the person without using forLoop/Stream  
    
Entities:
    - PersonIterator
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class PersonIterator
    - Declare and define main function
    - Invoke the createRoster() method and store the returned value in a list
    - Use Iterator to print the element
    
Pseudocode:
public class PersonIterator {

    public static void main(String[] args) {
    	List<Person> persons = Person.createRoster();
    	Iterator<Person> iterator = persons.iterator();
    	while(iterator.hasNext()) {
    		System.out.println(iterator.next().getName());
    	}
    }
}
 
 * */

package Streams;

import java.util.Iterator;
import java.util.List;

public class PersonIterator {

    public static void main(String[] args) {
    	List<Person> persons = Person.createRoster();
    	Iterator<Person> iterator = persons.iterator();
    	while (iterator.hasNext()) {
    		System.out.println(iterator.next().getName());
    	}
    }
}
