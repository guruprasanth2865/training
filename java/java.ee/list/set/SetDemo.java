/*
Requirements:
    -  + Create a set
        => Add 10 values
        => Perform addAll() and removeAll() to the set.
        => Iterate the set using 
            - Iterator
            - for-each
       + Explain the working of contains(), isEmpty() and give example.
       
Entities:
    - SetDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class SetDemo
    - Declare and define main method
    - Create a set and add 10 elements
    - Perform addAll() and removeAll() with a  new set
    - Print the elements from the set with
        => Iterator
        => for-each method
    - Check whether a element contains in the set with contains() method
    - Check whether a set is empty or not ( with isEmpty() method)
    
Pseudocode:

public class SetDemo {

    public static void main(String[] args) {
        Set<String> cars = new HashSet<>();
        Set<String> newCars = new HashSet<>();
        cars.add("BMW");
    	cars.add("Porche");
    	cars.add("Ford");
    	cars.add("TATA");
    	cars.add("Jaguar");
    	cars.add("Benz");
    	cars.add("Honda");
    	cars.add("Lamboghini");
    	cars.add("Hummer");
    	cars.add("Mahindra");
    	newCars.addAll(cars);
    	newCars.removeAll(cars);
    	Iterator<String> iterator = cars.iterator();
    	System.out.println("\nPrinted using Iterator : ");
    	while (iterator.hasNext()) {
    		System.out.println(iterator.next());
    	}
    	System.out.println("\nPrinted using For each : ");
    	for (String car : cars) {
    		System.out.println(car);
    	}
    	if (newCars.contains("BMW")) {
    		System.out.println("\nnewCars-set contains BMW car");
    	} else {
    		System.out.println("\nnewCars-set does not contain BMW car");
    	}
    	if (newCars.isEmpty() == true) {
    		System.out.println("\nnewCars-set is empty");
    	}
    }
}
 
 * */


package list.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {

    public static void main(String[] args) {
        Set<String> cars = new HashSet<>();
        Set<String> newCars = new HashSet<>();
        cars.add("BMW");
    	cars.add("Porche");
    	cars.add("Ford");
    	cars.add("TATA");
    	cars.add("Jaguar");
    	cars.add("Benz");
    	cars.add("Honda");
    	cars.add("Lamboghini");
    	cars.add("Hummer");
    	cars.add("Mahindra");
    	newCars.addAll(cars);
    	newCars.removeAll(cars);
    	Iterator<String> iterator = cars.iterator();
    	System.out.println("\nPrinted using Iterator : ");
    	while (iterator.hasNext()) {
    		System.out.println(iterator.next());
    	}
    	System.out.println("\nPrinted using For each : ");
    	for (String car : cars) {
    		System.out.println(car);
    	}
    	if (newCars.contains("BMW")) {
    		System.out.println("\nnewCars-set contains BMW car");
    	} else {
    		System.out.println("\nnewCars-set does not contain BMW car");
    	}
    	if (newCars.isEmpty() == true) {
    		System.out.println("\nnewCars-set is empty");
    	}
    }
}
