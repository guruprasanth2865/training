/*
Requiremetns:
    - + Create a list
        => Add 10 values in the list
        => Create another list and perform addAll() method with it
        => Find the index of some value with indexOf() and lastIndexOf()
        => Print the values in the list using 
            - For loop
            - For Each
            - Iterator
            - Stream API
        => Convert the list to a set
        => Convert the list to a array
        + Explain about contains(), subList(), retainAll() and give example 
        
Entities:
    - ListDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class ListDemo
    - Declare and define main method
    - Create a list and add 10 elements to it
    - Create another list and invoke addAll() method to copy the elements
    - Find an element with indexOf() and lastIndexOf() method
    - Traverse the list and print the elements with
        => for loop
        => For Each
        => Iterator
        => Stream API
    - Convert the list to a set
    - Convert the list to an array
    - Invoke contains() method to check whether a list contains an element
    - Invoke subList() method to to make a sub-list from previous list.
    - Invoke retainAll() method to retain only the similar elements of two list and remaining will be removed
    
Pseudocode:
    - public class ListDemo {

    public static void main(String[] args) {
    	List<String> cars = new ArrayList<>();
    	List<String> newCars = new ArrayList<>();
    	Set<String> oldCars = new HashSet<>();
    	String[] newArivals;
    	cars.add("BMW");
    	cars.add("Porche");
    	cars.add("Ford");
    	cars.add("TATA");
    	cars.add("Jaguar");
    	cars.add("Benz");
    	cars.add("Honda");
    	cars.add("Lamboghini");
    	cars.add("Hummer");
    	cars.add("Mahindra");
    	newCars.addAll(cars);
    	System.out.println("Index of car-BMW is " + cars.indexOf("BMW"));
    	System.out.println("Index of car-Hummer is " + cars.lastIndexOf("Hummer"));
    	System.out.println("\nPrinting list using for loop : ");
    	for (int i = 0; i < cars.size(); i++) {
    		System.out.println(cars.get(i));
    	}
    	System.out.println("\nPrinting elements using FOr Each : ");
    	for (String car : cars) {
    		System.out.println(car);
    	}
    	System.out.println("\nPrinting elements with Iterator : ");
    	Iterator<String> iterator = cars.iterator();
    	while (iterator.hasNext()) {
    		System.out.println(iterator.next());
    	}
    	System.out.println("\nPrinting elements with Stream API : ");
    	cars.stream().forEach((car) -> System.out.println(car));
    	oldCars.addAll(cars);
    	newArivals = cars.toArray(new String[0]);
    	if (cars.contains("BMW")) {
    		System.out.println("\nCars-List contains BMW\n");
    	}
    	newCars.clear();
    	newCars = cars.subList(4, 8);
    	cars.retainAll(newCars);
    	for (String car : cars) {
    		System.out.println(car);
    	}
    }
}
 
 * */


package list.set;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ListDemo {

    public static void main(String[] args) {
    	List<String> cars = new ArrayList<>();
    	List<String> newCars = new ArrayList<>();
    	Set<String> oldCars = new HashSet<>();
    	String[] newArivals;
    	cars.add("BMW");
    	cars.add("Porche");
    	cars.add("Ford");
    	cars.add("TATA");
    	cars.add("Jaguar");
    	cars.add("Benz");
    	cars.add("Honda");
    	cars.add("Lamboghini");
    	cars.add("Hummer");
    	cars.add("Mahindra");
    	newCars.addAll(cars);
    	System.out.println("Index of car-BMW is " + cars.indexOf("BMW"));
    	System.out.println("Index of car-Hummer is " + cars.lastIndexOf("Hummer"));
    	System.out.println("\nPrinting list using for loop : ");
    	for (int i = 0; i < cars.size(); i++) {
    		System.out.println(cars.get(i));
    	}
    	System.out.println("\nPrinting elements using FOr Each : ");
    	for (String car : cars) {
    		System.out.println(car);
    	}
    	System.out.println("\nPrinting elements with Iterator : ");
    	Iterator<String> iterator = cars.iterator();
    	while (iterator.hasNext()) {
    		System.out.println(iterator.next());
    	}
    	System.out.println("\nPrinting elements with Stream API : ");
    	cars.stream().forEach((car) -> System.out.println(car));
    	oldCars.addAll(cars);
    	newArivals = cars.toArray(new String[0]);
    	if (cars.contains("BMW")) {
    		System.out.println("\nCars-List contains BMW\n");
    	}
    	newCars.clear();
    	newCars = cars.subList(4, 8);
    	cars.retainAll(newCars);
    	for (String car : cars) {
    		System.out.println(car);
    	}
    }
}
