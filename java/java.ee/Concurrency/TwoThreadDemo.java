/*
Requirements:
    -  + Write a program of performing two tasks by two threads that implements Runnable interface.
    
Entities:
    - TwoThreadDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create Two classes FirstThread and SecondThread that implements Runnable
    - Inside these classes, Write a run() method
    - Create class TwoThreadDemo
    
Psudocode:
class FirstThread implements Runnable {
	public void run() {
		for (int i = 0; i <= 3; i++) {
			System.out.println("First Thread");
		}
	}
}

class SecondThread implements Runnable {
	public void run() {
		for (int i = 0; i <= 3; i++) {
			System.out.println("Second Thread");
		}
	}
}

public class TwoThreadDemo {

    public static void main(String[] args) {
    	Thread firstThread = new Thread(new FirstThread());
    	Thread secondThread = new Thread(new SecondThread());
    	firstThread.start();
    	secondThread.start();
    }
}

 
 * */

package Concurrency;

class FirstThread implements Runnable {
	public void run() {
		for (int i = 0; i <= 3; i++) {
			System.out.println("First Thread");
		}
	}
}

class SecondThread implements Runnable {
	public void run() {
		for (int i = 0; i <= 3; i++) {
			System.out.println("Second Thread");
		}
	}
}

public class TwoThreadDemo {

    public static void main(String[] args) {
    	Thread firstThread = new Thread(new FirstThread());
    	Thread secondThread = new Thread(new SecondThread());
    	firstThread.start();
    	secondThread.start();
    }
}
