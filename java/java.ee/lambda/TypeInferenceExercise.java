/*
Requirements:
    - What's wrong with the following program? And fix it using Type Reference

        public interface BiFunction{
            int print(int number1, int number2);
        }

        public class TypeInferenceExercise {
            public static void main(String[] args) {
    
                BiFunction function = (int number1, int number2) ->  { 
                    return number1 + number2;
                };
        
            int print = function.print(int 23,int 32);
            System.out.println(print);
            }
        }

Error: 
    At line 16, type of the value shound not be mentioned when we pass the value directly.
 
 * */

package lambda;

interface BiFunction{
    int print(int number1, int number2);
}

public class TypeInferenceExercise {
    public static void main(String[] args) {

        BiFunction function = (int number1, int number2) ->  { 
            return number1 + number2;
        };

    int print = function.print(23, 32);
    System.out.println(print);
    }
}