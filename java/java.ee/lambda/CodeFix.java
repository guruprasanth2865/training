/*
Requirements:
    - What is wrong with the following interface? and fix it.
        (int x, y) -> x + y;
        
Job to be done:
    - Fix the code

Correct Code:
    
    (x, y) -> x + y;
    
    Here, argumentrs are not defined with data-type.  
 
 * */

package lambda;

public class CodeFix {

}
