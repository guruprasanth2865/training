/*
Requirements:
    - Write a program to print the volume of  a Rectangle using lambda expression

Entities:
    - VolumeCalculator
    
Function Declaration:
    - public static void main(String[] args)
    - calculate(int length, int width, int height)
    
Job to be done:
    - Create a interface Calculator
    - Declare a function calculate(int length, int width, int height)
    - Create a class VolumeCalculator
    - Declare and define main method
    - Create object for interface-Calculator
    - Invoke the calculate() function to calculate the volume of rectangle

Pseudocode:
interface Calculator{
	public int calculate(int length, int width, int height);
}

public class VolumeCalculator {

    public static void main(String[] args) {
    	Calculator rectangle = (length, width, height) -> length*width*height;
    	System.out.println("Volume of rectangle with \nLength:5 \nWidth:5 \nHeight:5 "
    			+ "\nAnswer : "+rectangle.calculate(5, 5, 5));
    }
}
 
 * */

package lambda;

interface Calculator{
	public int calculate(int length, int width, int height);
}

public class VolumeCalculator {

    public static void main(String[] args) {
    	Calculator rectangle = (length, width, height) -> length*width*height;
    	System.out.println("Volume of rectangle with \nLength:5 \nWidth:5 \nHeight:5 "
    			+ "\nAnswer : "+rectangle.calculate(5, 5, 5));
    }
}
