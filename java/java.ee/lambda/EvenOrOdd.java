/*
Requirements:
    + Convert the following anonymous class into lambda expression. 
        interface CheckNumber {
        public boolean isEven(int value);
        } 
        
        public class EvenOrOdd {
            public static void main(String[] args) {
                CheckNumber number = new CheckNumber() {
                    public boolean isEven(int value) {
                    if (value % 2 == 0) {
                    return true;
                    } else return false;
                    }
                };
            System.out.println(number.isEven(11));
            }
        }

        
Entities:
    LambdaExp
    
Function Declaration :
    - public static void main(String[] args)
    
Job to be done:
    - Create class LambdaExp
    - Declare and define main method
    - Create a object for interface-CheckNumber and set a lambda function to get a value and perform value % 2
       and return a boolean value
    - Invoke the function isEven
    
Pseudocode:
interface CheckNumber {
    public boolean isEven(int value);
} 

public class EvenOrOdd {

    public static void main(String[] args) {
	    CheckNumber checker = (value) -> value % 2 == 0;
	    System.out.println(checker.isEven(4));
    }
}
 
 * */

package lambda;

interface CheckNumber {
    public boolean isEven(int value);
} 

public class EvenOrOdd {

    public static void main(String[] args) {
	    CheckNumber checker = (value) -> value % 2 == 0;
	    System.out.println(checker.isEven(4));
    }
}
