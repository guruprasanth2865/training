/*
Requirements:
    - which one of these is a valid lambda expression? and why?:
        (int x, int y) -> x+y; or (x, y) -> x + y; 
        
Job to be done:
    - Check whether given code is valid or not

 Correct code : (x , y) -> x + y;
 
 This is because, the above code does not contain data-type definition

 * */

