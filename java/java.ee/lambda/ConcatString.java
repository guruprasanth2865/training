/*
Requirements:
    - +  write a Lambda expression program with a single method interface. To concatenate two strings 
    
Entities:
    - ConcatString
    - StringAdder
    
Function Declaration
    - apply()
    - public static void main(String[] args)

Job to be done:
    - Create Interface StringAdder
    - Declare a function adder - with two String arguments
    - Create class ConcatString
    - Declare and define main method 
    - Create an instance of Interface and write lambda function to return the added string
    - Invoke the apply function and print the returned value
 
Pseudocode:
interface StringAdder{
	public String apply(String first, String second);
}

public class ConcatString {
    
	public static void main(String[] args) {
		StringAdder addValues = (String first, String second) -> first.concat(second);
		System.out.println(addValues.apply("Hello ", "World"));
	}
}
 
 
 * */

package lambda;

interface StringAdder{
	public String apply(String first, String second);
}

public class ConcatString {
    
	public static void main(String[] args) {
		StringAdder addValues = (String first, String second) -> first.concat(second);
		System.out.println(addValues.apply("Hello ", "World"));
	}
}
