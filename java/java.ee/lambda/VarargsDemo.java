/*
Requirement:
    - Code a functional program that can return the sum of elements of varArgs, passed into the method of functional interface. 

Entities:
    - Calculator
    
Function Definition:
    - public static void main(String[] args)
    - adder
    
Job to be done:
    - Create a Interface- AdditionCalc
    - Declare a function adder(int... values) inside interface of return type-int
    - Create class VarargsDemo
    - Declare and define main method
    - Create a object-sumValues for interface-AdditionCalc and assign a lambda function to get a array and return their sum
    - Invoke the function-adder of the object sumValues and print the returned value

Pseudocode:
interface AdditionCalc {
	public int adder(int... values);
}

public class VarargsDemo {

	public static void main(String[] args) {
		AdditionCalc sumValues = (int... values) -> {
			int result = 0;
			for(int value : values) {
				result+=value;
			}
			return result;
		};
		System.out.println(sumValues.adder(1,2,3,4,5,6));
	}
}

 * */

package lambda;

interface AdditionCalc {
	public int adder(int... values);
}

public class VarargsDemo {

	public static void main(String[] args) {
		AdditionCalc sumValues = (int... values) -> {
			int result = 0;
			for (int value : values) {
				result+=value;
			}
			return result;
		};
		System.out.println(sumValues.adder(1,2,3,4,5,6));
	}
}
