/*
Requirements:
    - write a program to print difference of two numbers using lambda expression and the single method interface 
    
Entities:
    - DifferenceFinder
    - NumberDifferentiator
    
Function Declaration:
    - public static void main(String[] args)
    - public Integer apply(Integer first, Integer Second)

Job to be done:
    - Create a Interface and Declare a method-apply(Integer first, Integer second) 
    - Create a class DifferenceFinder
    - Declare and Define main method
    - Create a object-subract for Instance and define apply method
    - Invoke the apply method of arrer and print the returned value
    
Pseudocode:
interface NumberDifferentiator {
    public int apply(int first, int second);
}

public class DifferenceFinder {

    public static void main(String[] args) {
    	NumberDifferentiator subract = (first, second) -> first-second;
    	System.out.println(subract.apply(5, 2));
    }
}

  
 * */

package lambda;

interface NumberDifferentiator {
    public int apply(int first, int second);
}

public class DifferenceFinder {

    public static void main(String[] args) {
    	NumberDifferentiator subract = (first, second) -> first-second;
    	System.out.println(subract.apply(5, 2));
    }
}
