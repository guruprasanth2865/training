/*
Requirements:
    +  Convert this following code into a simple lambda expression
        int getValue(){
            return 5;
        }
        
Job to be done:
    - Simplify the code
    
Answer:
    getValue = () -> 5;
 
 * */

package lambda;

public class SimpleProgram {

}
