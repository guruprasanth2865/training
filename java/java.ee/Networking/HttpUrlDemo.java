/*
Requirements:
    - 6.Create a program using HttpUrlconnection in networking.

Entities:
    - HttpUrlconnectionDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class MttpUrlDemo
    - Declare and define main method
    - Create a http-url connection with HttpUrlDemo with HttpURLConnection class and its method openConnection()
    - Print the header fields with getHeaderField() method
    
Psudocode:
public class HttpUrlDemo {

    public static void main(String[] args) {
		URL url;
		try {
			url = new URL("https://www.youtube.com/");
			HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();    	
			for (int i = 1; i <= 8; i++) {
				System.out.println(httpConnection.getHeaderFieldKey(i) + " = " + httpConnection.getHeaderField(i));
			}
			httpConnection.disconnect();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    }
}

 
 * */

package Networking;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpUrlDemo {

    public static void main(String[] args) {
		URL url;
		try {
			url = new URL("https://www.youtube.com/");
			HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();    	
			for (int i = 1; i <= 8; i++) {
				System.out.println(httpConnection.getHeaderFieldKey(i) + " = " + httpConnection.getHeaderField(i));
			}
			httpConnection.disconnect();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    }
}
