/*
Requiremetns:
    - - Create a program  for url class and url connection class in networking.
    
Entities:
    - UrlConnectionDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class UrlConnectionDemo
    - Declare and define main method
    - Create  object of type URL with value - http://3.7.211.213/
    - Print the protocol, hostname, port number and filename with methods
        getProtocol(), getHost(), getPort(), getFile()
        
Psudocode:
import java.net.MalformedURLException;
import java.net.URL;

public class UrlConnectionDemo {

    public static void main(String[] args) {
    	try {
			URL link = new URL("http://3.7.211.213/");
			System.out.println("Protocol : " + link.getProtocol());
			System.out.println("Port number : " + link.getPort());
			System.out.println("Host name : " + link.getHost());
			System.out.println("File name : " + link.getFile());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}

 
 * */

package Networking;

import java.net.MalformedURLException;
import java.net.URL;

public class UrlConnectionDemo {

    public static void main(String[] args) {
    	try {
			URL link = new URL("http://3.7.211.213/");
			System.out.println("Protocol : " + link.getProtocol());
			System.out.println("Port number : " + link.getPort());
			System.out.println("Host name : " + link.getHost());
			System.out.println("File name : " + link.getFile());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
