/*
Requirements:
    - 4.Find out the IP Address and host name of your local computer.
    
Entities:
    - LocalAddress
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class LocalAddress
    - declare and define main method
    - Open a try block
    - In the try block, create a object of type InetAddress and use its getLocalHost-method
    - Get the host address and host name with methods - getHostAddress() and getHostName()
    - use getByName() method 

Psudocode:
public class LocalAddress {

    public static void main(String[] args) {
    	try {
    		InetAddress address = InetAddress.getLocalHost();
    		System.out.println("Local Host : " + address + "\nIP Address : " + address.getHostAddress() + "\nHost Name : " + address.getHostName());
    		address = InetAddress.getByName("google.com");
			System.out.println("Google inetaddress is : " + address);
    	} catch (UnknownHostException e) {
    		e.printStackTrace();
		}
    }
}

 
 * */

package Networking;


import java.net.InetAddress;
import java.net.UnknownHostException;

public class LocalAddress {

    public static void main(String[] args) {
    	try {
    		InetAddress address = InetAddress.getLocalHost();
    		System.out.println("Local Host : " + address + "\nIP Address : " + address.getHostAddress() + "\nHost Name : " + address.getHostName());
    		address = InetAddress.getByName("google.com");
			System.out.println("Google inetaddress is : " + address);
    	} catch (UnknownHostException e) {
    		e.printStackTrace();
		}
    }
}
