/*
Requirements:
    - Build a client side program using ServerSocket class for Networking. 
    
Entities:
    - ClientSideDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class ClientSideDemo
    - Declare and define main method
    - Create a socket at port 3333
    - Create a dataOutputStream for the socket and write content in it with writeUTF() method
    
Psudocode:

public class ClientSideDemo {

    public static void main(String[] args) {
    	try {
			Socket port = new Socket("localhost", 3333);
			DataOutputStream portWriter = new DataOutputStream(port.getOutputStream());
			portWriter.writeUTF("Helo Everyone...");
			portWriter.flush();
			portWriter.close();
			port.close();
			System.out.println("Content is written...");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
}

  
 * */

package Networking;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientSideDemo {

    public static void main(String[] args) {
    	try {
			Socket port = new Socket("localhost", 3333);
			DataOutputStream portWriter = new DataOutputStream(port.getOutputStream());
			portWriter.writeUTF("Helo Everyone...");
			portWriter.flush();
			portWriter.close();
			port.close();
			System.out.println("Content is written...");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
}
