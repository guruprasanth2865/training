/*
Requirements:
    - 1.Build a server side program using Socket class for Networking.
    
Entities:
    - ServerSideDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class ServerSideDemo
    - Declare and define main method
    - Create a object of type ServerSocket and set value - 3333
    - Establish the connection with accept() method
    - Create a object of type dataInputStream and use readURL() method to read 
    - Print the read content

Psudocode:
public class ServerSideDemo {
	
    public static void main(String[] args) {
    	try {
    		ServerSocket socket = new ServerSocket(3333);
			Socket netSocket = socket.accept();
			DataInputStream dataInput = new DataInputStream(netSocket.getInputStream());
			String content = (String) dataInput.readUTF();
			System.out.println(content);
			netSocket.close();
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}  
 
 * */

package Networking;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSideDemo {
	
    public static void main(String[] args) {
    	try {
    		ServerSocket socket = new ServerSocket(3333);
			Socket netSocket = socket.accept();
			DataInputStream dataInput = new DataInputStream(netSocket.getInputStream());
			String content = (String) dataInput.readUTF();
			System.out.println(content);
			netSocket.close();
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}  