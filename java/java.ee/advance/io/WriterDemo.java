/*
Requirements:
    - 5. Write some String content using Writer
    
Entities:
    - WriterDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class WriterDemo
    - Declare and define main method
    - Create a Writer object with a text-file path
    - Invoke write() method to write the content to the file
    - After writing, close the file with close() method
    
Psudo code:
public class WriterDemo {

    public static void main(String[] args) {
    	Writer fileWriter = null;
        try {
        	fileWriter = new FileWriter("E:\\test\\advance.io.txt");
        	fileWriter.write("Hello World... ....");
        	fileWriter.close();
            System.out.println("File is written...");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
  
 * */


package advance.io;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class WriterDemo {

    public static void main(String[] args) {
    	Writer fileWriter = null;
        try {
        	fileWriter = new FileWriter("E:\\test\\advance.io.txt");
        	fileWriter.write("Hello World... ....");
        	fileWriter.close();
            System.out.println("File is written...");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
