/*
Requirements:
    - 2. Reading a file using Reader
    
Entities:
    - FileReaderDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a File and pass it as argument to a Reader 
    - Invoke read() method to read the file
    - Invoke close() method to close the Reader
 
Psudocode:
public class FileReaderDemo {

    public static void main(String[] args) throws IOException {
        File file = new File("E:\\test\\advance.nio.txt");
        Reader fileReader = new FileReader(file);
        char[] content = new char[(int) file.length()];
        fileReader.read(content);
        System.out.println(new String(content));
        fileReader.close();
    }

}
 
 
 * */


package advance.io;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class FileReaderDemo {

    public static void main(String[] args) throws IOException {
        File file = new File("E:\\test\\advance.io.txt");
        Reader fileReader = new FileReader(file);
        char[] content = new char[(int) file.length()];
        fileReader.read(content);
        System.out.println(new String(content));
        fileReader.close();
    }

}