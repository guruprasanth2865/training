/*
Requirements:
    - 4. Write some String content using OutputStream
    
Entities:
    - OutputStreamDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class OutputStreamDemo
    - Declare and define main method
    - Create a object of type-FileOutputStream with a filepath in its argument
    - Invoke write() method to write content to the file
    - After writing, use close() method to close the file
    
Psudocode:
public class OutputStreamDemo {

	public static void main(String[] args) {
		OutputStream outStream = null;
        try {
            outStream = new FileOutputStream("E:\\test\\advance.io.txt");
            String content = "Hell0 World... ....";
            byte[] byteContent = content.getBytes();
            outStream.write(byteContent);
            outStream.close();
            System.out.println("File is written...");
        } catch (FileNotFoundException fileNotFound) {
            fileNotFound.getMessage();
        } catch (IOException ioe) {
            ioe.getMessage();
        }
    }

}
 
 * */


package advance.io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class OutputStreamDemo {

	public static void main(String[] args) {
		OutputStream outStream = null;
        try {
            outStream = new FileOutputStream("E:\\test\\advance.io.txt");
            String content = "Hell0 World... ....";
            byte[] byteContent = content.getBytes();
            outStream.write(byteContent);
            outStream.close();
            System.out.println("File is written...");
        } catch (FileNotFoundException fileNotFound) {
            fileNotFound.getMessage();
        } catch (IOException ioe) {
            ioe.getMessage();
        }
    }

}
