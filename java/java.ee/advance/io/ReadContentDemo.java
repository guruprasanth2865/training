/*
Requirements:
    - 3. Read a any text file using BufferedReader and print the content of the file 
    
Entities:
    - ReadContentDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class ReadContentDemo
    - Declare and define main method
    - Create a BufferReader object with the path of the text file 
    - Invoke readLine()method to read the line and print it
    - Invoke close() method to close the reader
    
Psudocode:
public class ReadContentDemo {
	
    public static void main(String[] args) {
    	BufferedReader fileReader;
		try {
			fileReader = new BufferedReader(new FileReader("E:\\test\\advance.io.txt"));
			String content;
	        while ((content = fileReader.readLine()) != null) {
	            System.out.println(content);
	        }
	        fileReader.close();
		} catch (FileNotFoundException e) { 
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }
}
 
 * */


package advance.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;    
import java.io.FileReader;
import java.io.IOException;

public class ReadContentDemo {
	
    public static void main(String[] args) {
    	BufferedReader fileReader;
		try {
			fileReader = new BufferedReader(new FileReader("E:\\test\\advance.io.txt"));
			String content;
	        while ((content = fileReader.readLine()) != null) {
	            System.out.println(content);
	        }
	        fileReader.close();
		} catch (FileNotFoundException e) { 
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }
}
