/*
Requirements:
    - 1. Reading a file using InputStream 
    
Entities:
    - InputStreamDemo
    
Function Declaration:
    - public static void main(String[] args)

Job to be done:
    - Create class InputStreamDemo
    - Declare and define main method
    - Create a object of type-FileInputStream  to read a file 
    - Use scanner to read the file
    - With a while loop, check whether the file has next line ( with fileRead.hasNextLine() ) and print it with nextLine() method
    
Psudocode:
public class InputStreamDemo {

	public static void main(String[] args) throws FileNotFoundException {
    	FileInputStream file = new FileInputStream("E:\\test\\advance.io.txt");
    	Scanner fileReader = new Scanner(file);
    	while(fileReader.hasNextLine()) {
    		System.out.println(fileReader.nextLine());
    	}
    	fileReader.close();
    }
}
 
 * */


package advance.io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class InputStreamDemo {

	public static void main(String[] args) throws FileNotFoundException {
    	FileInputStream file = new FileInputStream("E:\\test\\advance.io.txt");
    	Scanner fileReader = new Scanner(file);
    	while(fileReader.hasNextLine()) {
    		System.out.println(fileReader.nextLine());
    	}
    	fileReader.close();
    }
}
