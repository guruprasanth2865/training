/*
Requirements:
    - 6. Read a file using java.io.File

Entities:
    - ReadFileDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a File-object and set the path
    - Invoke length() method and getPath() method to fimmd the length and path of the file
    
Pseudocode:
public class ReadFileDemo {

    public static void main(String[] args) {
        File file = new File("E:\\test");
        System.out.println(file.length());
        System.out.println(file.getPath());
        System.out.println(file);
    }
}
 
 * */

package advance.io;

import java.io.File;

public class ReadFileDemo {

    public static void main(String[] args) {
        File file = new File("E:\\test");
        System.out.println(file.length());
        System.out.println(file.getPath());
        System.out.println(file);
    }
}
