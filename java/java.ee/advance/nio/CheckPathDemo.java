/*
Requirements:
    - 15. Create two paths and test whether they represent same path
    
Entities:
    - CheckPathDemo
    
Function Declaration:
    - public static void main(|String[] args)
    
Job to be done:
    - Create class CheckPathDemo
    - Declare and define main method
    - Create two Path objects with pathin the arguments
    - Check whether the two paths are same with isSameFile() method
    - If it is true, print yes, else print no
    
Pseudo code:
public class CheckPathDemo {

    public static void main(String[] args) {
    	Path fileOne = Paths.get("E:\\test\\advance.nio.txt");
        Path fileTwo = Paths.get("E:\\test\\advance.nio.txt");
        try {
			if (Files.isSameFile(fileOne, fileTwo) == true) {
				System.out.println("Both files are same");
			} else {
				System.out.println("Both files are anot same");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
 
 * */


package advance.nio;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.IOException;
import java.nio.file.Files;

public class CheckPathDemo {

    public static void main(String[] args) {
    	Path fileOne = Paths.get("E:\\test\\advance.nio.txt");
        Path fileTwo = Paths.get("E:\\test\\advance.nio.txt");
        try {
			if (Files.isSameFile(fileOne, fileTwo) == true) {
				System.out.println("Both files are same");
			} else {
				System.out.println("Both files are anot same");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
