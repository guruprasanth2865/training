/*
Requiremetns:
    - 8. Reading a CSV file using java.nio.Files API as List string with each row in CSV as a String
    
Entities:
    - CsvFileReaderDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class CsvReaderDemo
    - Declare and define main method
    - Invoke readAllLines() method to red all lines from csv file
    - Create a list and add contents from a csv file
    - Print the elements from the list using for loop
    
Pseudocode:
public class CsvReaderDemo {
	
	public static void main(String[] args) throws IOException {
		List<String> contents = Files.readAllLines(Paths.get("E:\\test\\csv.reader.csv"));
		for (String content : contents) {
			System.out.println(content);
		}
	}
}

 
 * */


package advance.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class CsvReaderDemo {
	
	public static void main(String[] args) throws IOException {
		List<String> contents = Files.readAllLines(Paths.get("E:\\test\\csv.reader.csv"));
		for (String content : contents) {
			System.out.println(content);
		}
	}
}
