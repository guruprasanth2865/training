/**
Requirements:
    -  12. Number of files in a directory and number of directories in a directory
    
Entities:
    - FileDirectoryView
    
Function Declaation:
    - public static void main(String[] args)
    
Job to be done:
    - Create class FileDirectoryView
    - Declare and deploy main mehtod
    - Create a File object with a directory
    - Check the file count with length() method
    - Print the count of files
    
Pseudocode:
public class FileDirectoryView {
	
	public static void main(String args[]) {
		File folderPath = new File("E:\\test");
		System.out.println("File Count:" + folderPath.list().length);
	}
}
 
 */


package advance.nio;

import java.io.File;

public class FileDirectoryView {
	
	public static void main(String args[]) {
		File folderPath = new File("E:\\test");
		System.out.println("File Count:" + folderPath.list().length);
	}
}
