/*
Requirements:
    - 14. Get the file names of all file with specific extension in a directory
    
Entities:
    - SpecificFileDemo
   
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class SpecificFileDemo
    - Declare and define main method
    - Create a File object with path in argument
    - Create a String array and override method-accept()
    - Print the cvalues from the string-array extension
    
Psudocode:
public class SpecificFileDemo {

	public static void main(String a[]) {
		File file = new File("E:\\test\\nio");
		String[] extension = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if (name.toLowerCase().endsWith(".txt")) {
					return true;
				} else {
					return false;
				}
			}
		});
		for (String value : extension) {
			System.out.println(value);
		}
	}
}
 
 * */


package advance.nio;

import java.io.File;
import java.io.FilenameFilter;

public class SpecificFileDemo {

	public static void main(String a[]) {
		File file = new File("E:\\test\\nio");
		String[] extension = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if (name.toLowerCase().endsWith(".txt")) {
					return true;
				} else {
					return false;
				}
			}
		});
		for (String value : extension) {
			System.out.println(value);
		}
	}
}
