/*
Requirements:
    - 7. Read a file using java.nio.Files using Paths
    
Entities;
    - PathDemo
    
Function Declaration:
    - public staic void main(String[] args)
    
Job t be done:
    - Create class PathDemo
    - Declare and define main method
    - Create a Path object and read the content with readAllBytes() method and store in a byte array
    - Convert the content fron byteArray to String and print them. 
 
Psudocode:
public class PathDemo {

	public static void main(String[] args) {
		Path path = Paths.get("E:\\test\\advance.nio.txt");
		try {
			byte[] byteReader = Files.readAllBytes(path);
			System.out.println("Read content : \n" + new String(byteReader));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}


 * */


package advance.nio;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathDemo {

	public static void main(String[] args) {
		Path path = Paths.get("E:\\test\\advance.nio.txt");
		try {
			byte[] byteReader = Files.readAllBytes(path);
			System.out.println("Read content : \n" + new String(byteReader));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
