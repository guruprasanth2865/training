/*
 Requirements;
     -  9. Given a path, check if path is file or directory
     - 10. InputStream to String and vice versa
     - 13. Get the permission allowed for a file

Entities:
    - FileOrDirectoryDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class FileOrDirectoryDemo
    - Declare and deploy main method
    - Create a file object with a path
    - Check whether the path is file or not with method - isFile()
        - If it is true, print - path as file
        - Else, print it as a directory
    - If it is a file, check whether  permission is allowed with canExecute(), canRead(), canWrite() method
    - Create a InputStreamReader and set the filePath to read
    - Read the content from the file using BufferReader
    - Print the red content

Psudocode:
public class FileOrDirectoryDemo {

    public static void main(String[] args) {
    	File filePath = new File("E:\\test\\advance.nio.txt");
		if (filePath.isFile()) {
			System.out.println(filePath + " is a  file");
			System.out.println("Execute status : " + filePath.canExecute());
			System.out.println("Readable status : " + filePath.canRead());
			System.out.println("Writable status : " + filePath.canWrite());
			try {
			    InputStream streamReader = new FileInputStream(filePath);
				InputStreamReader fileReader = new InputStreamReader(streamReader);
				BufferedReader reader = new BufferedReader(fileReader);
			    StringBuffer strReader = new StringBuffer();
			    String content = null;
			    while ((content = reader.readLine())!= null) {
			    	strReader.append(content);
			    }
			    System.out.println(strReader.toString());
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out.println(filePath + " is a directory");
		}
    }
}
 
 * */


package advance.nio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileOrDirectoryDemo {

    public static void main(String[] args) {
    	File filePath = new File("E:\\test\\advance.nio.txt");
		if (filePath.isFile()) {
			System.out.println(filePath + " is a  file");
			System.out.println("Execute status : " + filePath.canExecute());
			System.out.println("Readable status : " + filePath.canRead());
			System.out.println("Writable status : " + filePath.canWrite());
			try {
			    InputStream streamReader = new FileInputStream(filePath);
				InputStreamReader fileReader = new InputStreamReader(streamReader);
				BufferedReader reader = new BufferedReader(fileReader);
			    StringBuffer strReader = new StringBuffer();
			    String content = null;
			    while ((content = reader.readLine())!= null) {
			    	strReader.append(content);
			    }
			    System.out.println(strReader.toString());
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out.println(filePath + " is a directory");
		}
    }
}
