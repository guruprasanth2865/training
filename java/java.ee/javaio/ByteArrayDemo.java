/*
Requirements:
    - write a program for ByteArrayInputStream class to read byte array as input stream.
    
Entities:
    - ByteArrayDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class ByteArrayDemo
    - Declare and define main method
    - Declare a byte array and add some integers to it
    - Add the byte-array to a object of type-BByteArrayInputStream as convertedValues
    - Read that convertedValues with read() method and print them after converting to character. 
    
Psudocode:
public class ByteArrayDemo {
    
	public static void main(String[] args) throws IOException {  
	    byte[] biteValues = { 97, 75, 70, 80 };  
		ByteArrayInputStream convertedValues = new ByteArrayInputStream(biteValues);  
		int check = -1;  
		while ((check = convertedValues.read()) != -1) {  
		    char letter = (char) check;  
		    System.out.println("ASCII value :" + check + "   Character : " + letter);  
		}  
	}  
}  
 
 * */

package javaio;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class ByteArrayDemo {
    
	public static void main(String[] args) throws IOException {  
	    byte[] biteValues = { 97, 75, 70, 80 };  
		ByteArrayInputStream convertedValues = new ByteArrayInputStream(biteValues);  
		int check = -1;  
		while ((check = convertedValues.read()) != -1) {  
		    char letter = (char) check;  
		    System.out.println("ASCII value :" + check + "   Character : " + letter);  
		}  
	}  
}  