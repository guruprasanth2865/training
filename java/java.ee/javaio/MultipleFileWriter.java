/*
Requirements:
    - 2.write a program that allows to write data to multiple files together using bytearray outputstream  
    
Entities:
    - MultipleFileWriter
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class MultipleFileWriter
    - Declare and define main method
    - Create a OutputStream to write in a text file
    - Write a content to the file with writeTo() method
    - After writing, invoke flush() method and close() method to save and exit
    
Psudocode:
public class MultipleFileWriter {

    public static void main(String[] args) throws IOException {
    	
    	FileOutputStream firstFile = new FileOutputStream("E:\\test\\First.txt");
        FileOutputStream secondFile = new FileOutputStream("E:\\test\\Second.txt");
        ByteArrayOutputStream byteWriter = new ByteArrayOutputStream();
        String str = "Hello Everyone!...";
        byte[] byteValues = str.getBytes();
        byteWriter.write(byteValues);
        byteWriter.writeTo(firstFile);
        byteWriter.writeTo(secondFile);
        byteWriter.flush();
        System.out.println("File Written successfully");
        firstFile.close();
        secondFile.close();
        byteWriter.close();
    }
}

 
 * */

package javaio;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class MultipleFileWriter {

    public static void main(String[] args) throws IOException {
    	
    	FileOutputStream firstFile = new FileOutputStream("E:\\test\\First.txt");
        FileOutputStream secondFile = new FileOutputStream("E:\\test\\Second.txt");
        ByteArrayOutputStream byteWriter = new ByteArrayOutputStream();
        String str = "Hello Everyone!...";
        byte[] byteValues = str.getBytes();
        byteWriter.write(byteValues);
        byteWriter.writeTo(firstFile);
        byteWriter.writeTo(secondFile);
        byteWriter.flush();
        System.out.println("File Written successfully");
        firstFile.close();
        secondFile.close();
        byteWriter.close();
    }
}
