/*
Requirements:
     - 1.How to read the file contents  line by line in streams with example
     
Entities:
    - ReaderDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class ReaderDemo
    - Declare and define main method
    - Create a object of type-FileInputStream  to read a file 
    - Use scanner to read the file
    - With a while loop, check whether the file has next line ( with fileRead.hasNextLine() ) and print it with nextLine() method
    
Psudocode:
public class ReaderDemo {

    public static void main(String[] args) throws FileNotFoundException {
    	FileInputStream file = new FileInputStream("E:\\downloads\\list.set.exercise.txt");
    	Scanner fileReader = new Scanner(file);
    	while(fileReader.hasNextLine()) {
    		System.out.println(fileReader.nextLine());
    	}
    	fileReader.close();
    }
}
 
 * */

package javaio;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReaderDemo {

    public static void main(String[] args) throws FileNotFoundException {
    	FileInputStream file = new FileInputStream("E:\\downloads\\list.set.exercise.txt");
    	Scanner fileReader = new Scanner(file);
    	while(fileReader.hasNextLine()) {
    		System.out.println(fileReader.nextLine());
    	}
    	fileReader.close();
    }
}
