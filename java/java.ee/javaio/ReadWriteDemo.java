/*
Requirements:
    - 3.write a Java program reads data from a particular file using FileReader and writes it to another, using FileWriter.
    
Entities:
    - ReadWriteDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class ReadWriteDemo
    - Declare and define main method
    - Create a object of type FileReader to read a file
    - Create another object of type FileWriter to write a file
    - Read the file with read() method and write in another file with write() method using Loop 
    
Psudocode:
public class ReadWriteDemo {

    public static void main(String[] args) {
    	FileReader reader = null;
    	FileWriter writer = null;
    	try {
			reader = new FileReader("E:\\test\\readingFile.txt");
			writer = new FileWriter("E:\\test\\writingFile.txt");
			int read = -1;
			while ((read = reader.read()) != -1) {
				writer.write(read);
			}
			writer.flush();
			writer.close();
			reader.close();
			System.out.println("File is read and written successfully...");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}

 
 * */


package javaio;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadWriteDemo {

    public static void main(String[] args) {
    	FileReader reader = null;
    	FileWriter writer = null;
    	try {
			reader = new FileReader("E:\\test\\readingFile.txt");
			writer = new FileWriter("E:\\test\\writingFile.txt");
			int read = -1;
			while ((read = reader.read()) != -1) {
				writer.write(read);
			}
			writer.flush();
			writer.close();
			reader.close();
			System.out.println("File is read and written successfully...");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
