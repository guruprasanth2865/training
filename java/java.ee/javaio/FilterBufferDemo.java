/*
Requirements:
    - 5.Java program to write and read a file using filter and buffer input and output streams.
    
Entities:
    - FilterBufferDemo
    
Function Declaration:
    public static void main(String[] args)
    
Job to be done:
    - Create a class FilterBufferDemo
    - Declare and define main method
    - Create instances of type FileInputStream and FileOutputStream for reading and writing file
    - Use FilterInputStream and FilterOutputStream and their methods read() and write() to read and write in file
    - Use BufferedInputStream and BufferedOutputStream and their methods read() and write() to read and write in file
    
Psudocode:
public class FilterBufferDemo {

    public static void main(String[] args) {
    	try {
			FileInputStream file = new FileInputStream("E:\\test\\filter.and.buffer.txt");
			FileOutputStream outputFile = new FileOutputStream("E:\\test\\filter.buffer.output.txt");  
			FilterInputStream filterFile = new BufferedInputStream(file);
			FilterOutputStream filterOutput = new FilterOutputStream(outputFile);  
			BufferedInputStream bufferFile = new BufferedInputStream(new FileInputStream("E:\\test\\First.txt"));    
			BufferedOutputStream bufferOutput =new BufferedOutputStream(new FileOutputStream("E:\\test\\Second.txt")); 
			int value = -1;
			while ((value = filterFile.read()) != -1) {
				filterOutput.write((char) value);
				System.out.println((char) value);
			}
			System.out.println("File is read and written using FilterOutputStream");
			while ((value = bufferFile.read()) != -1) {
				bufferOutput.write((char) value);
				System.out.println((char) value);
			}
			System.out.println("File is read and written using Buffer");
			filterFile.close();
			filterOutput.flush();
			filterOutput.close();
			bufferOutput.flush();
			bufferOutput.close();
			bufferFile.close();
			file.close();
			outputFile.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}

 
 * */

package javaio;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;

public class FilterBufferDemo {

    public static void main(String[] args) {
    	try {
			FileInputStream file = new FileInputStream("E:\\test\\filter.and.buffer.txt");
			FileOutputStream outputFile = new FileOutputStream("E:\\test\\filter.buffer.output.txt");  
			FilterInputStream filterFile = new BufferedInputStream(file);
			FilterOutputStream filterOutput = new FilterOutputStream(outputFile);  
			BufferedInputStream bufferFile = new BufferedInputStream(new FileInputStream("E:\\test\\First.txt"));    
			BufferedOutputStream bufferOutput =new BufferedOutputStream(new FileOutputStream("E:\\test\\Second.txt")); 
			int value = -1;
			while ((value = filterFile.read()) != -1) {
				filterOutput.write((char) value);
				System.out.println((char) value);
			}
			System.out.println("File is read and written using FilterOutputStream");
			while ((value = bufferFile.read()) != -1) {
				bufferOutput.write((char) value);
				System.out.println((char) value);
			}
			System.out.println("File is read and written using Buffer");
			filterFile.close();
			filterOutput.flush();
			filterOutput.close();
			bufferOutput.flush();
			bufferOutput.close();
			bufferFile.close();
			file.close();
			outputFile.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
