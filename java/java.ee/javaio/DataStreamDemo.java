/*
Requirements:
    - 6.program to write primitive datatypes to file using by dataoutputstream.
    
Entities:
    - DataStreamDemo
    
Function Declaration:
    - public static void main(String[] args)

Job to be done:
    - Create class DataStreamDemo
    - Declare and define main method
    - Create a object of type-FileOutputStream and refer a text file in it
    - Create another object of type-DataOutputStream  with referance to the above object created
    - With the method writeInt()- add some integers in the file 
    
Psudocode:
public class DataStreamDemo {

    public static void main(String[] args) {
    	try {
			FileOutputStream textFile = new FileOutputStream("E:\\test\\data.txt");
			DataOutputStream data = new DataOutputStream(textFile);
			data.writeInt(70);
			data.flush();
			System.out.println("Data is written...");
			data.close();
			textFile.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}

  * */

package javaio;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class DataStreamDemo {

    public static void main(String[] args) {
    	try {
			FileOutputStream textFile = new FileOutputStream("E:\\test\\data.txt");
			DataOutputStream data = new DataOutputStream(textFile);
			data.writeInt(70);
			data.flush();
			System.out.println("Data is written...");
			data.close();
			textFile.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
