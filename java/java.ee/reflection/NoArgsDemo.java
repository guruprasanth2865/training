/*
Problem Statement:
To Create a constructor of type noargconstructor.
-----------------------------------WBS------------------------------------------
1.Requirements:
   - Create a constructor of type noargconstructor.
2.Entity:
   - Private Constructor
3.Function Declaration:
   - public static void main(String[] args)
4.Jobs to be done:
   1. Create an No Argument Constructor.
   2. Display the output.
-----------------------------------Program Code------------------------------------------
*/
package reflection;

class NoArgsDemo {

	int value;
	private static NoArgsDemo obj;

	private NoArgsDemo() {
		value = 5;
		System.out.println("Object created and value = " + value);
	}

	public static void main(String[] args) {
		setObj(new NoArgsDemo());
	}

	public static NoArgsDemo getObj() {
		return obj;
	}

	public static void setObj(NoArgsDemo obj) {
		NoArgsDemo.obj = obj;
	}
}





