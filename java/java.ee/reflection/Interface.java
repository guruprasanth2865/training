package reflection;

/*
Problem Statement:
3)Create a Program to demonstrate @Target annotation (@Target) 
using interface annotation (@interface).

----------------------------------------WBS-----------------------------------------
1.Requirement:
    - Program to demonstrate Target annotation (@Target) using interface annotation (@interface)

2.Entity:
    - MyClass

3.Method Signature:
    - public static void main(String args[])

4.Jobs to be done:
    1.Import annotation for performing ElementType and Target.
    2.Define the target type as METHOD.
    3.MyCustomAnnotation is created by using @interface.
    4.Apply MyCustomAnnotation to class MyClass.
    5.Annotation are applied to methods using target annotation.

Psuedo Code:
''''''''''''

@Target(ElementType.METHOD)
@interface MyCustomAnnotation {
	int studentAge() default 18;

	String studentName();
	String stuAddress();
	String stuStream() default "CSE";
}

public class Interface {

	@MyCustomAnnotation(studentName = "Santheesh", stuAddress = "Tirupur")
	public static void main(String[] args) {
		System.out.println("This function is annotated");
	}
}
----------------------------------------Program Code-----------------------------------------
*/

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@interface MyCustomAnnotation {
	int studentAge() default 18;

	String studentName();
	String stuAddress();
	String stuStream() default "CSE";
}

public class Interface {

	@MyCustomAnnotation(studentName = "Santheesh", stuAddress = "Tirupur")
	public static void main(String[] args) {
		System.out.println("This function is annotated");
	}
}

//Output:
//This function is annotated