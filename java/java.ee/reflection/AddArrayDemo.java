/*
Requirments:
    - Program java array using reflect.array class and add elements using the appropriate method.
 
Entity:
    - AddArrayDemo 
   
Function Declaration:
    - public static void main(String[] args)
    
Jobs to be Done:
    - Create class AddArrayDemo
    - Declare and define main method
    - Declare a integer - size with a number
    - Create a integer array with Array.newInstance() method
    - Add elements using setInt() method
    
Pseudo Code:
public class AddArrayDemo {
	public static void main(String[] args) {
		int size = 5;
		int[] values = (int[]) Array.newInstance(int.class, size);
		Array.setInt(values, 0, 2);
		Array.setInt(values, 1, 4);
		Array.setInt(values, 2, 6);
		Array.setInt(values, 3, 8);
		Array.setInt(values, 4, 10);
		System.out.println(Arrays.toString(values));
	}
}
*/


package reflection;

import java.lang.reflect.Array;
import java.util.Arrays;

public class AddArrayDemo {
	public static void main(String[] args) {
		int size = 5;
		int[] values = (int[]) Array.newInstance(int.class, size);
		Array.setInt(values, 0, 2);
		Array.setInt(values, 1, 4);
		Array.setInt(values, 2, 6);
		Array.setInt(values, 3, 8);
		Array.setInt(values, 4, 10);
		System.out.println(Arrays.toString(values));
	}
}
