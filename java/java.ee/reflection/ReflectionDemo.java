/*
Write a java program to demonstrate the use of reflection.

Answer:
'''''''
      * Java Reflection is a process of examining or modifying the run time behavior of 
a class at run time.
      * The java.lang.Class class provides many methods that can be used to get metadata, 
examine and change the run time behavior of a class.
      * The java.lang and java.lang.reflect packages provide classes for java reflection.

     * forName() method of Class class is used to load the class dynamically.
     * It returns the instance of Class class.
     * It should be used if you know the fully qualified name of class.
	 * This cannot be used for primitive types.

Example:
--------
class Simple{}  
  
class ReflectionDemo{  
 public static void main(String args[]){  
  Class c = Class.forName("Simple"); //ClassName 
  System.out.println(c.getName());  
 }  
}   
 
 * */


package reflection;

public class ReflectionDemo {

}
