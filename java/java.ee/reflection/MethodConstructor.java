/*
Method :
    - A method is used to expose the behaviour of an object.
    - A method name may or may not be the same name name as of the class.
    - It has return type.
    - Method is not provided by compiler.
	  
	  

Constructors :
    - A constructor is used to initialise the state of an object.
    - A constructor name should be same as the class name.
    - It does not return any values.
    - The java compiler provides a default contructor if you don't have one. 
 
 * */

package reflection;

public class MethodConstructor {

}
