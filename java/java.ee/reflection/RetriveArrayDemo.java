/*
Requirement:
    - Program to retrieve the added elements using the appropriate method.

Entity:
     - ArrayRetrieveElement

Function Declaration
    - public static void main(String[] args) 

Jobs to be done:
    - Create an array using newIntance method and store its size.
    - Add elements to array using setInt method
    - Get elements of array using getInt method.
   
Pseudo Code:


*/


package reflection;

import java.lang.reflect.Array;
import java.util.Arrays;

public class RetriveArrayDemo {
	public static void main(String[] args) {
		int size = 5;
		int[] values = (int[]) Array.newInstance(int.class, size);
		Array.setInt(values, 0, 0);
		Array.setInt(values, 1, 10);
		Array.setInt(values, 2, 20);
		Array.setInt(values, 3, 30);
		Array.setInt(values, 4, 40);
		System.out.println(Arrays.toString(values));
		System.out.println("Value at index 0: " + Array.getInt(values, 0));
		System.out.println("Value at index 1: " + Array.getInt(values, 1));
		System.out.println("Value at index 2: " + Array.getInt(values, 2));
		System.out.println("Value at index 3: " + Array.getInt(values, 3));
		System.out.println("Value at index 4: " + Array.getInt(values, 4));
	}
}