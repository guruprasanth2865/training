/*
Requirements:
    - Write test cases for PersonService class 
 
Entities:
    - PersonServiceTest
    - Person
    - PersonService
    - Address
    
Function Declaration:
    - setup
    - insertPersonTest
    - readPersonTest
    - readPersonAddressTest
    - readAllPersonTest
    - updatePersonTest
    - deletePersonTest
    - insertPersonAddressTest
    - insertPersonSecondTest
    
Job to be done:
    - Create class PersonServiceTest
    - Declare Person Object - person
    - Declare Person Object - newPerson
    - Declare Address Object - newAddress
    - Declare PersonService Object - service
    - Declare id of type Long
    
    //setup()
    1) Create a new Address Object with value and store it in newAddress
    2) Create a new PersonService Object and store it in service
    3) Create a new Person Object and store it in service
    4) Set id and person to null
    
    
    //insertPersonTest()
    1) Invoke create() method of service with person as argument
    2) Store the returned value in id
    3) Invoke read method with id as argument
    4) Store the returned value in person
    5) Invoke assertNotNull() method with person as argument
    
    
    //readPersonTest()
    1) Invoke read(id, false) method with id as argument
    2) Store the returned value in person
    3) Invoke assertNotNull() method with person as argument
    
    
    //readPersonAddressTest()
    1) Invoke read(id, true) method with id as argument
    2) Store the returned value in person
    3) Invoke assertTrue() method and check whether returned values are not null
    
    
    //readAllPersonTest()
    1) Invoke readAll() method from service
    2) Store the returned value in persons
    3) Invoke assertNotNull() method and pass argument as persons
    
    
    //updatePersonTest()
    1) Create new Person with values and store it to newPerson
    2) Invoke update() method with newPerson and id
    3) Invoke read() method with the same id value
    4) Store the returned value in person
    5) Invoke assertTrue() method. In its argument, check whether the values in person and newPerson are same
    
    
    //deletePersonTest()
    1) Invoke delete() method with id in argument
    
    //insertPersonAddressTest()
    1) Create new Address with value and store it in newAddress
    2) Create new Person with value and and store it in newPerson
    3) Invoke create() method, pass newAddress and newPerson as argument
    4) Store the returned value in id
    5) Invoke assertNotNull method and pass id as parameter to check whether returned value is null
    
    
    //insertPersonSecondTest()
    1) Create new Address with value and store it in newAddress
    2) Create new Person with value and and store it in newPerson
    3) Invoke create() method, pass newAddress and newPerson as argument
    4) Store the returned value in id
    5) Invoke assertNotNull method and pass id as parameter to check whether returned value is null
    
  
Pseudo code:
public class PersonServiceTest {

	private Person newPerson;
	private Person person;
	private Address newAddress;
	private PersonService service;
	List<Person> persons;
	Long id;
	
	@BeforeClass
	public void setup() {
		newAddress = new Address(1L);
		newPerson = new Person("Prasanth", "prasanth2865@gmail.com", newAddress, "2000-09-30");
		service = new PersonService();
		id = null;
		person = null;
	}
	
	
	@Test(priority=1, description="Insert Person")
	public void insertPersonTest() {
		id = service.create(newPerson);
		person = service.read(id, false);
		Assert.assertNotNull(person);
	}
	
	@Test(priority=2, description="Read Person with address")
	public void readPersonTest() {
		person = service.read(1L, false);
		Assert.assertNotNull(person);
	}
	
	@Test(priority=3, description="Read Person with Address")
	public void readPersonAddressTest() {
		person = null;
		person = service.read(1L, true);
	    Assert.assertTrue(person != null & person.getAddress().getPincode() != 0);
	}
	
	@Test(priority=4, description="Read All Person")
	public void readAllPersonTest() {
		persons = service.readAll();
		Assert.assertNotNull(persons);
	}
	
	@Test(priority=5, description="Update Person")
	public void updatePersonTest() {
		newPerson = new Person("Vishnu", "1@1company@gmail.com", newAddress, "2000-06-15");
		service.update(newPerson, person.getId());
		person = service.read(person.getId(), false);
		Assert.assertTrue(person.getName() == newPerson.getName() &
				          person.getEmail() == newPerson.getEmail() &
				          person.getAddress().getId() == newPerson.getAddress().getId() &
				          person.getBirthDate() == newPerson.getBirthDate());
	}
	
	@Test(priority=6, description="Delete Person")
	public void deletePersonTest() {
		service.delete(person.getId());
	}
	
	@Test(priority=7, description="Insert Person and Address")
	public void insertPersonAddressTest() {
		newPerson = new Person("Sarath", "vishnusarath@gmail.com", "2001-05-10");
		newAddress = new Address("Bose street", "Trichy", 641146);
		id = service.create(newPerson, newAddress);
		Assert.assertNotNull(id);
	}
	
	@Test(priority=8, description="Try Creating person with duplicate email", 
			expectedExceptions = { AppException.class }, 
			expectedExceptionsMessageRegExp = "Person with the same email already exists")
	public void insertPersonSecondTest() {
		id = null;
		newAddress = new Address(1L, "DEF Street", "Covai", 654321);
		newPerson = new Person("Vishnu", "vishnusarath@gmail.com", newAddress, "2000-06-15");
		id = service.create(newPerson);
		Assert.assertNotNull(id);
	}
}

    

 * */


package com.kpriet.training.jdbc.service;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.kpriet.training.jdbc.connection.ConnectionService;
import com.kpriet.training.jdbc.model.Address;
import com.kpriet.training.jdbc.model.Person;


public class PersonServiceTest {

	ConnectionService connectionService;
	private PersonService personService;
	private Person person;
	private Person readPerson;
	List<Person> persons;
	private Person updatePerson;
	private Address newAddress;
	private Person deletePerson;
	private Person newPerson;
	Date date;
	long id;
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	private String CSVPath;
	
	@BeforeClass
	public void setup() throws Exception {

		personService = new PersonService();
		connectionService = new ConnectionService();
		date = dateFormat.parse("30-09-2000");
		newAddress = new Address(1L);
		person = new Person("Barath", "Raju", "barathraj4@gmail.com", newAddress, date);
		id = 0;
		updatePerson = new Person("Barath", "Kumar", "barathkumaran546@gmail.com", newAddress, date);
		
		newAddress = new Address("Bose street", "Trichy", 641146);
		newPerson = new Person("Ram", "Charan", "ramcharan001@gmail.com", newAddress, date);
		
		CSVPath = "C:\\1Training\\training\\java\\java-jdbc\\person.file.csv";
	}
	
	
	@Test(priority=1, description="Insert Person")
	public void createPersonTest() {
		id = personService.create(person);
		readPerson = personService.read(id, false);
		System.out.println(readPerson.toString());
		person.setId(id);
		person.setCreatedDate(readPerson.getCreatedDate());
		System.out.println(person.toString());
		Assert.assertEquals(person.toString(), readPerson.toString());
		if (person.toString().equals(readPerson.toString())) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}
	
	
	@Test(priority=2, description="Read Person with address")
	public void readPersonTest() {
		readPerson = personService.read(person.getId(), false);
		Assert.assertEquals(person.toString(), readPerson.toString());
	}
	
	@Test(priority=3, description="Read Person with Address")
	public void readPersonAddressTest() {
		readPerson = personService.read(person.getId(), true);
		person.setAddress(new Address(1L, "DEF Street", "Covai", 654321));
		Assert.assertEquals(person.toString(), readPerson.toString());
	}
	
	@Test(priority=4, description="Read All Person")
	public void readAllPersonTest() {
		persons = personService.readAll();
		Assert.assertTrue(persons.size() > 0);
	}
	
	@Test(priority=5, description="Update Person")
	public void updatePersonTest() {
		updatePerson.setId(person.getId());
		personService.update(updatePerson);
		person = personService.read(person.getId(), false);
		updatePerson.setCreatedDate(person.getCreatedDate());
		Assert.assertEquals(person.toString(), updatePerson.toString());
		if (person.toString().equals(updatePerson.toString())) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}
	
	@Test(priority=6, description="Delete Person")
	public void deletePersonTest() {
		personService.delete(person.getId());
		deletePerson = personService.read(person.getId(), false);
		if (deletePerson == null) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}
	
	@Test(priority=7, description="Insert Person and Address")
	public void createPersonAddressTest() {
		id = personService.create(newPerson);
		newPerson.setId(id);
		readPerson = personService.read(id, true);
		newPerson.setCreatedDate(readPerson.getCreatedDate());
		newPerson.getAddress().setId(readPerson.getAddress().getId());
		Assert.assertEquals(newPerson.toString(), readPerson.toString());
		if (newPerson.toString().equals(readPerson.toString())) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}
	
	
	@Test(priority=8, description="Insert person from CSV")
	public void createPersonFromCSV() {
		persons = personService.createPersonWithCSV(CSVPath);
		if (persons.size() > 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}
	
	/*
	@Test(priority=9, description="Try Creating person with duplicate email", expectedExceptions = { AppException.class }, 
		  expectedExceptionsMessageRegExp = "Person with the same email already exists")
	public void insertPersonSecondTest() {
		newPerson = new Person("Vishnu", "Ram",  "ramcharan001@gmail.com", newAddress, date);
		id = personService.create(newPerson);
		//Assert.assertNotNull(id);
	}
	*/
}
