/*
Requirements:
    - Write test cases for AddressService class
    
Entities:
    - AddressServiceTest
    - Address
    - AddressService
    
Function Declaration:
    - setup
    - insertAddressTest
    - readAddressTest
    - readAllAddressTest
    - updateAddressTest
    - DeleteAddressTest
    - insertAddressSecondTest
    
Job to be done:
    - Declare ConnectionService Object - conectionService
    - Declare addressService Object - addressService
    - Declare Address Object- address
    - Declare Address Object - newAddress
    - Declare Address Object - updateAddress
    - Declare Address Object - deleteAddress
    - Declare a ArrayList Object - addresses
    - Declare a ArrayList Object - searchContent
    
    //setup()
    1) Set a new ConnectionService Object to the connectionService
    2) Set a new AddressService object to addressService
    3) Set address with a new Address Object with values init
    4) Set updateAddress with a new Address Object with values init
    5) Set searchContent with a string value init
    
    //createAddressTest() 
    1) Invoke create(address) method
    2) Store the returned value in id
    3) With the id, Invoke read() method
    4) Store the returned Address Object in address
    5) Invoke assertEquals() method of Assert Class to compare and inform status
  
    //readAddressTest()
    1) Invoke the read() method of addressService
    2) Store the returned value in address
    3) Invoke assertEquals() method of Assert Class to compare and inform status
    
    //readAllAddressTest()
    1) Invoke readAll() method of addressService 
    2) Store the returned value in addresses list
    3) Invoke assertTrue() method of Assert Class to inform status
    
    //updateAddressTest()
    1) Invoke update() method of addressService 
    2) Invoke read() method of addressService and store the returned Address Object in address
    3) Invoke assertEquals() method to compare address and inform status
    
    //DeleteAddressTest()
    1) Invoke delete() method with id in its argument
    2) Invoke read() method of addressService and store the returned value in address
    3) Invoke assertNull() method to check the read value
    4) 
   
    //Create Address without pincode
    1) Create a new Address object without pincode
    2) Invoke create() method of addressService 
    3) Store the returned value in id
    4) Invoke assertNotNull() method and check whether returned value is null
    
Pseudo code:
public class AddressServiceTest {
	
	private Address newAddress;
	private Address address;
	private AddressService addressService;
	Long id;
	List<Address> addresses;
	
	
	@BeforeClass
	public void setup() {
		newAddress = new Address("DEF Street", "Covai", 654321);
		addressService = new AddressService();
		id = null;
		addresses = null;
	}
	

	@Test(priority=1, description="Insert address")
	public void insertAddressTest() {
		id = addressService.create(newAddress);
		address = addressService.read(id);
		Assert.assertNotNull(address);
	}
	
	@Test(priority=2, description="Read address")
	public void readAddressTest() {
		address = addressService.read(id);
		Assert.assertNotNull(address.getId());
	}
	
	@Test(priority=3, description="Read All Address")
	public void readAllAddressTest() {
		addresses = addressService.readAll();
		Assert.assertTrue(addresses.size() > 0);
	}
	
	@Test(priority=4, description="Update Address")
	public void updateAddressTest() {
		newAddress = new Address(1L, "ABC Street", "Tirupur", 123456);
		addressService.update(newAddress, id);
		address = addressService.read(1L);
		Assert.assertTrue(address.getStreet() == newAddress.getStreet() & 
				          address.getCity() == newAddress.getCity() & 
				          address.getPincode() == newAddress.getPincode());
	}
	
	@Test(priority=5, description="Delete Address")
	public void deleteAddressTest() {
		addressService.delete(id);
	}
	
	@Test(priority=6, description="Try insert without pincode")
	public void insertAddressSecondTest() {
		id = null;
		newAddress = new Address("GHI Street", "Chennai");
		id = addressService.create(newAddress);
		Assert.assertTrue(address.getId() != 0L);
	}
}
 
 * */



package com.kpriet.training.jdbc.service;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.kpriet.training.jdbc.connection.ConnectionService;
import com.kpriet.training.jdbc.model.Address;


public class AddressServiceTest {
	
	ConnectionService connectionService;
	AddressService addressService;
	Address address;
	Address newAddress;
	Address updateAddress;
	Address deleteAddress;
	List<Address> addresses;
	List<Address> searchResults;
	String searchContent;
	
	@BeforeClass
	public void setup() {
	    connectionService = new ConnectionService();
	    addressService = new AddressService();
		address = new Address("Bose nagar", "Coimbatore", 641146);
		updateAddress = new Address("Subash nagar", "Chennai", 641146);
		searchContent = "Bose nagar, Coimbatore, 641146";
	}
	
	
	@Test(priority=1, description="Insert address")
	public void createAddressTest() {
		
		address.setId(addressService.create(address));
		newAddress = addressService.read(address.getId());
		Assert.assertEquals(address.toString(), newAddress.toString());
		if (address.toString().equals(newAddress.toString())) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}
	
	@Test(priority=2, description="Read address")
	public void readAddressTest() {
		newAddress = addressService.read(address.getId());
		Assert.assertEquals(address.toString(), newAddress.toString());
	}
	
	@Test(priority=3, description="Read All Address")
	public void readAllAddressTest() {
		addresses = addressService.readAll();
		Assert.assertTrue(addresses.size() > 0);
	}
	
	@Test(priority=4, description="Update Address")
	public void updateAddressTest() {
		updateAddress.setId(newAddress.getId());
		addressService.update(updateAddress);
		newAddress = addressService.read(updateAddress.getId());
		Assert.assertEquals(updateAddress.toString(), newAddress.toString());
		if (updateAddress.toString().equals(newAddress.toString())) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}
	
	@Test(priority=5, description="Delete Address")
	public void deleteAddressTest() {
		addressService.delete(newAddress.getId());
		deleteAddress = addressService.read(newAddress.getId());
		Assert.assertNull(deleteAddress);
		if (deleteAddress == null) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}
		
	@Test(priority=6, description="Search address")
	public void searchAddress() {
		searchResults = addressService.search(searchContent);
		System.out.println(searchResults.size());
	}

	/*
	@Test(priority=7, 
		  description="Try insert without pincode",
		  expectedExceptions = { AppException.class }, 
		  expectedExceptionsMessageRegExp = "Pincode is Empty")
	public void insertAddressSecondTest() {
		
	}
	*/
	
}
