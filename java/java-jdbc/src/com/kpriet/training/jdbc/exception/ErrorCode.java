/*
Requirements:
    - Create a enum class with errorcodes
    
Entities:
    - ErrorCode
    
Function Declaration:
    - ErrorCode(String detail)
    - public String getDetail()
    
Job to be done:
    1) Create a enum block
    2) Declare enum constant with value in argument
    3) Declare a string - detail;
    4) Declare a Constructor - ErrorCode(String detail)
    5) Get the value from parameter and set it to the enum constant
    6) Declare a method - getDetail();
    7) Return the detail.
  
 
 * */


package com.kpriet.training.jdbc.exception;

public enum ErrorCode {
	
	ERR01("Connection is not created"),
	ERR02("Connection is not commited"),
	ERR03("Connection is not closed"),
	ERR04("Address search operation is failed"),
	ERR05("Address is not created"),
	ERR06("Address is not read"),
	ERR07("Addresses are not read"),
	ERR08("Address is not updated"),
	ERR09("Address is not deleted"),
	ERR10("All informations are not entered"),
	ERR11("Email already exists"),
	ERR12("Name already exists"),
	ERR13("Person is not created"),
	ERR14("Person is not read"),
	ERR15("Persons are not read"),
	ERR16("Person is not updated"),
	ERR17("Person is not deleted"),
	ERR18("Date is in invalid format"),
	ERR19("CSV File is not read");

	String detail;
	
	ErrorCode(String detail) {
		this.detail = detail;
	}
	
	public String getDetail() {
		return this.detail;
	}
}
