CREATE DATABASE jdbc_demo;
USE jdbc_demo;
CREATE TABLE Address (
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT
    ,street VARCHAR(100)
    ,city VARCHAR(15)
    ,postal_code INT NOT NULL
);
CREATE INDEX idx_addressid ON Address(id);
CREATE TABLE Person (
    id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT
    ,first_name VARCHAR(50) NOT NULL
    ,last_name VARCHAR(50) NOT NULL
    ,email VARCHAR(100) UNIQUE NOT NULL
    ,address_id BIGINT NOT NULL
    ,FOREIGN KEY(address_id) REFERENCES Address(id)
    ,birth_date DATE NOT NULL
    ,created_date DATETIME DEFAULT CURRENT_TIMESTAMP 
    ,user_id BIGINT NOT NULL
    ,FOREIGN KEY(user_id) REFERENCES User(id) 
);
CREATE INDEX idx_personid ON Person(id, address_id);
CREATE INDEX idx_userid ON Person(id, user_id);

CREATE TABLE User (
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT
    ,username VARCHAR(100) NOT NULL
    ,password VARCHAR(200) NOT NULL
    ,role VARCHAR(10) NOT NULL
);

/*DROP TABLE Address;*/
DROP TABLE Person;
DROP TABLE Address;

INSERT INTO Address(street, city, postal_code) 
    VALUES ("ABC main road", "Arasur", 641407);
    
INSERT INTO Address(street, city, postal_code) 
    VALUES ("Arasur", "ABC main road", 641407);

SELECT id, street, city, postal_code FROM ADDRESS;

SELECT id, street, city, postal_code FROM ADDRESS
    WHERE id=1;
TRUNCATE TABLE Person;
TRUNCATE TABLE Address;
UPDATE Address 
    SET street = "XYZ Street"
        ,city = "Chennai"
        ,postal_code = 600001
	    WHERE id = 2;

DELETE FROM Address
    WHERE id = 9; 
    
DELETE FROM Address
    WHERE id = 18;
    
SELECT * FROM Address;

SELECT * FROM Person;

SELECT * FROM User;

INSERT INTO Person(first_name, last_name, email, address_id, birth_date)
    VALUES ("Sundhar", "Raj", "sundarraj@gmail.com", 2, "10-01-1970");
    
SELECT id, first_name, last_name, email, address_id, birth_date, created_date 
    FROM Person;
    
SELECT id, name, email, address_id, birth_date, created_date 
    FROM Person
        WHERE id = 3; 
        
UPDATE Person 
    SET name = "prasanth.S"
        ,email = "prasanth2865@gmail.com"
        ,address_id = 2
        ,birth_date = "2000-01-01"
            WHERE id = 3;
            
DELETE FROM Person 
    WHERE id = 9;
    
SELECT id FROM Person 
    WHERE email = "sundarraj@gmail.com" OR (first_name ="Vishnu" AND last_name="Sarath")
    HAVING id != 1; 
    
SELECT id FROM Person 
    WHERE first_name ="Vishnu" AND last_name="Sarath"
    HAVING id != 1; 

SELECT id, street, city, postal_code
    FROM Address
        WHERE street = "DEF Street";
 
SELECT id, street, city, postal_code
    FROM Address;

SELECT id, street, city, postal_code
    FROM Address
        WHERE (street LIKE  "%%"
			  AND city LIKE "%%") OR
              (street LIKE "%%"
			  AND city LIKE "%%")
              AND postal_code LIKE "%%";
              
SELECT id, street, city, postal_code
    FROM Address
        WHERE (street LIKE CONCAT('%', 'Bose nagae', '%') 
			  AND city LIKE CONCAT('%', 'Coimbatore', '%') ) OR
              (street LIKE CONCAT('%', 'Coimbatore', '%') 
			  AND city LIKE CONCAT('%', 'Bose nagar', '%') )
              AND postal_code LIKE CONCAT('%', '', '%') ;

SELECT id FROM Person
    WHERE address_id = 2
        HAVING id != 2;
        
        
INSERT INTO User(username, password, role) 
    VALUES ("guruprasanth2865", "guruprasanth2865", "Admin");
    
SELECT id, username, password, role FROM User;

SELECT * FROM Person;