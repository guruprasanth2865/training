/*
Requirements:
    - Create a enum class with errorcodes
    
Entities:
    - ErrorCode
    
Function Declaration:
    - ErrorCode(String detail)
    - public String getDetail()
    
Job to be done:
    1) Create a enum block
    2) Declare enum constant with value in argument
    3) Declare a string - detail;
    4) Declare a Constructor - ErrorCode(String detail)
    5) Get the value from parameter and set it to the enum constant
    6) Declare a method - getDetail();
    7) Return the detail.
  
 
 * */


package com.kpriet.training.exception;

public enum ErrorCode {
	
	ERR01("Connection is not created", 400),
	ERR02("Connection is not commited", 400),
	ERR03("Connection is not closed", 400),
	ERR04("Address search operation is failed", 400),
	ERR05("Address is not created", 400),
	ERR06("Address is not read", 400),
	ERR07("Addresses are not read", 400),
	ERR08("Address is not updated", 400),
	ERR09("Address is not deleted", 400),
	ERR10("All informations are not entered", 400),
	ERR11("Email already exists", 400),
	ERR12("Name already exists", 400),
	ERR13("Person is not created", 400),
	ERR14("Person is not read", 400),
	ERR15("Persons are not read", 400),
	ERR16("Person is not updated", 400),
	ERR17("Person is not deleted", 400),
	ERR18("Date is in invalid format", 400),
	ERR19("CSV File is not read", 400);

	String detail;
	int statusCode;
	
	ErrorCode(String detail, int statusCode) {
		this.detail = detail;
		this.statusCode = statusCode;
	}
	
	public String getDetail() {
		return this.detail;
	}
	
	public int getStatusCode() {
		return this.statusCode;
	}
}
