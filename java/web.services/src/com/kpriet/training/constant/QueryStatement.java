/*
Requirements:
    - Create a class QueryStatement to store query statemants
    
Entities:
    - QueryStatement
    
Function Declaration:
    - 
    
Jobs to be done:
    - Create class QueryStatement
    - Declare all query statements as static variables
 
 
 * */

package com.kpriet.training.constant;

public class QueryStatement {
	
	public static String CREATE_ADDRESS   = new StringBuilder("INSERT INTO Address (street       ")
			                                          .append("                    ,city         ")
			                                          .append("                    ,postal_code) ")
		                                              .append("VALUES (?, ?, ?)                  ")
		                                              .toString();
	
	public static String READ_ALL_ADDRESS = new StringBuilder("SELECT id          ")
			                                          .append("      ,street      ")
			                                          .append("      ,city        ")
			                                          .append("      ,postal_code ")
		                                              .append("  FROM Address     ")
		                                              .toString();
	
	public static String READ_ADDRESS     = new StringBuilder("SELECT id          ")
			                                          .append("      ,street      ")
			                                          .append("      ,city        ")
			                                          .append("      ,postal_code ")
			                                          .append("  FROM Address     ")
			                                          .append(" WHERE id = ?      ")
			                                          .toString();
	
	public static String UPDATE_ADDRESS   = new StringBuilder("UPDATE Address         ")
				                                      .append("   SET street = ?      ")
				                                      .append("      ,city = ?        ")
			                                          .append("      ,postal_code = ? ") 
				                                      .append(" WHERE id = ?          ")
				                                      .toString();
	
	public static String DELETE_ADDRESS   = new StringBuilder("DELETE FROM Address ")
				                                      .append(" WHERE id = ?       ")
				                                      .toString();
	
	public static String SEARCH_ADDRESS   = new StringBuilder("SELECT id                                  ")
			                                          .append("      ,street                              ")
			                                          .append("      ,city                                ")
			                                          .append("      ,postal_code                         ")
			                                          .append("  FROM Address                             ")
			                                          .append(" WHERE (street LIKE CONCAT('%', ?, '%')    ")
			                                          .append("   AND city LIKE CONCAT('%', ?, '%')) OR   ")
			                                          .append("      (street LIKE CONCAT('%', ?, '%')     ")
			                                          .append("   AND city LIKE CONCAT('%', ?, '%'))      ")
			                                          .append("   AND postal_code LIKE CONCAT('%', ?, '%')")
			                                          .toString();
	
	public static String CREATE_PERSON    = new StringBuilder("INSERT INTO Person(first_name  ")
			                                          .append("                  ,last_name   ")
			                                          .append("                  ,email       ")
			                                          .append("                  ,address_id  ")
			                                          .append("                  ,birth_date) ")
				                                      .append("VALUES (?, ?, ?, ?, ?)         ")
				                                      .toString();
	
	public static String READ_ALL_PERSON  = new StringBuilder("SELECT id           ")
			                                          .append("      ,first_name   ")
			                                          .append("      ,last_name    ")
			                                          .append("      ,email        ")
			                                          .append("      ,address_id   ")
			                                          .append("      ,birth_date   ")
			                                          .append("      ,created_date ")
				                                      .append("  FROM Person       ")
				                                      .toString();
	
	public static String READ_PERSON      = new StringBuilder("SELECT id           ")
			                                          .append("      ,first_name   ")
			                                          .append("      ,last_name    ")
			                                          .append("      ,email        ")
			                                          .append("      ,address_id   ")
			                                          .append("      ,birth_date   ")
			                                          .append("      ,created_date ")
			                                          .append("  FROM Person       ")
			                                          .append(" WHERE id = ?       ")
			                                          .toString();
	
	public static String UPDATE_PERSON    = new StringBuilder("UPDATE Person         ")
			                                          .append("   SET first_name = ? ")
			                                          .append("      ,last_name = ?  ")
			                                          .append("      ,email = ?      ")
			                                          .append("      ,address_id = ? ")
			                                          .append("      ,birth_date = ? ")
			                                          .append(" WHERE id = ?         ")
			                                          .toString();
	
	public static String DELETE_PERSON    = new StringBuilder("DELETE FROM Person ")
			                                          .append( "WHERE id = ?      ")
			                                          .toString();
	
	public static String GET_ADDRESS_ID   = new StringBuilder("SELECT id              ")
			                                          .append("  FROM address         ")
			                                          .append(" WHERE street = ?      ")
			                                          .append("   AND city = ?        ")
			                                          .append("   AND postal_code = ? ")
			                                          .toString();
	
	public static String GET_WITH_EMAIL   = new StringBuilder("SELECT id        ")
			                                          .append("  FROM Person    ")
			                                          .append(" WHERE email = ? ")
			                                          .append("HAVING id != ?   ")
			                                          .toString();
	
	public static String GET_WITH_NAME    = new StringBuilder("SELECT id             ")
			                                          .append("  FROM Person         ")
			                                          .append(" WHERE first_name = ? ")
			                                          .append("   AND last_name = ?  ")
			                                          .append("HAVING id != ?        ")
			                                          .toString();
	
	public static String CHECK_ADDRESS    = new StringBuilder("SELECT id             ")
			                                          .append("  FROM Person         ")
			                                          .append(" WHERE address_id = ? ")
			                                          .toString();
}
