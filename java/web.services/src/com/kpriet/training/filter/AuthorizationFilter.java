package com.kpriet.training.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class AuthorizationFilter implements Filter{
	
	  
    public void init(FilterConfig arg) {  
    }  
      
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) {  
        System.out.println("Done !");  
    }  
      
    public void destroy() {  
    }  
}
