/*
Requirements:
-------------
    - Create a ErrorFilter to write status code, error code and error message for the thrown exceptions (Negative scenario)
    
Entity:
-------
    - ErrorFilter
    
Function Declaration: 
---------------------
    - init()
    - doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
    - destroy()
    
Jobs to be done: 
----------------
    1) Create a class ErrorFilter that implements Filter 
    2) Declare and define init() method (with parameter - FilterConfig object) 
    3) Declare and define destroy() method
    4) Declare and define doFilter() method with parameters (request, response, filterChain) to filter the request and response
    5) Open a try block 
    6) Invoke filterChain.doFilter() method to pass the request and response along filter
    7) Catch the exception if any thrown
        7.1) Set the respective status code in response from exception
        7.2) Set the error code in the response from exception
        7.3) Set the error message in the response from the exception
        
        
pseudo code:
------------

class ErrorFilter implements Filter {

    public void init(FilterConfig arg) {
    }
    
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) {
    
        try {
            filterChain.doFilter(req, res);
        } catch (Exception e) {
            res.setStatus();
        }
    }

}
 
 * */


package com.kpriet.training.filter;

import javax.servlet.http.HttpServletResponse;

import com.kpriet.training.exception.AppException;


import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class ErrorFilter implements Filter{
	
	private FilterConfig filterConfig = null;

    private static class ByteArrayServletStream extends ServletOutputStream
    {
        ByteArrayOutputStream baos;

        ByteArrayServletStream(ByteArrayOutputStream baos)
        {
            this.baos = baos;
        }

        public void write(int param) throws IOException
        {
            baos.write(param);
        }
    }

    private static class ByteArrayPrintWriter
    {

        private ByteArrayOutputStream baos = new ByteArrayOutputStream();

        private PrintWriter pw = new PrintWriter(baos);

        private ServletOutputStream sos = new ByteArrayServletStream(baos);

        public PrintWriter getWriter()
        {
            return pw;
        }

        public ServletOutputStream getStream()
        {
            return sos;
        }

        byte[] toByteArray()
        {
            return baos.toByteArray();
        }
    }

    public class CharResponseWrapper extends HttpServletResponseWrapper
    {
        private ByteArrayPrintWriter output;
        private boolean usingWriter;

        public CharResponseWrapper(HttpServletResponse response)
        {
            super(response);
            usingWriter = false;
            output = new ByteArrayPrintWriter();
        }

        public byte[] getByteArray()
        {
            return output.toByteArray();
        }

        @Override
        public ServletOutputStream getOutputStream() throws IOException
        {
            // will error out, if in use
            if (usingWriter) {
                super.getOutputStream();
            }
            usingWriter = true;
            return output.getStream();
        }

        @Override
        public PrintWriter getWriter() throws IOException
        {
            // will error out, if in use
            if (usingWriter) {
                super.getWriter();
            }
            usingWriter = true;
            return output.getWriter();
        }

        public String toString()
        {
            return output.toString();
        }
    }

    public void init(FilterConfig filterConfig) throws ServletException
    {
        this.filterConfig = filterConfig;
    }

    public void destroy()
    {
        filterConfig = null;
    }
 
      
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) { 
    	
    	CharResponseWrapper wrappedResponse = new CharResponseWrapper(
                (HttpServletResponse)response);

        try {
			filterChain.doFilter(request, wrappedResponse);
			byte[] bytes = wrappedResponse.getByteArray();
			response.setContentType("application/json");
			response.getOutputStream().write(bytes);
		} catch (AppException er) {
			try {
				//byte[] bytes = wrappedResponse.getByteArray();
				//wrappedResponse.setContentType("application/json");
				response.setContentType("application/json");
                
		        response.getOutputStream().write(new StringBuilder("{ \"errorCode\" : \"")
		        		                                   .append(er.err.toString())
		        		                                   .append("\",")
		        		                                   .append(" \"errorMessage\" : \"")
		        		                                   .append(er.err.getDetail())
		        		                                   .append("\"  }").toString().getBytes());
		        
		        //response.getOutputStream().write(("{ \"error\" : \"" + er.getCause().getMessage() + "\"  }").getBytes());
		        wrappedResponse.setStatus(er.err.getStatusCode());
		        
			} catch (Exception err) {
				System.out.println(err.getCause().getMessage());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		} 
    }  
    
}
