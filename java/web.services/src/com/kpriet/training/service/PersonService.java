/*
Requirements:
    - CRUD OPeration for Person Class
     
Entities:
    - PersonService
    
Function Declaration:
    - public void setValue(PreparedStatement preparedStatement, String firstName, String lastName, String email, long addressId, Date birthDate)
    - public Person getValue(ResultSet result)
    - public void isUnique(String email, String firstName, String lastName, Long id, Connection connection)
    - public void checkAddressId(long id, Connection connection)
    - public long create(Person person, Connection connection)
    - public Person read(long id, boolean includeAddress, Connection connection)
    - public List<Person> readAll(Connection connection)
    - public void update(Person person, Connection connection)
    - public void delete(long id,  Connection connection)
    
Jobs to be done:
    - Create class PersonService
    
    
    
    //setValue method
    1) Declare setValue() method with parameters (PreparedStatement preparedStatement, String firstName, String lastName, String email, long addressId, Date birthDate)
    2) Invoke setString(), setLong(), setDate() method to set the values to preparedStatement
    
    
    //getValue method
    1) Declare getVlaue method with parameter (ResultSet)
    2) Get the values with getString(), getDate() and getLong() methods
    3) Create a Person Object with the values and return the person object
    
    
    //isUnique method
    1) Declare isUnique() method with parameters - (String email, String firstName, String lastName, Long id, Connection connection)
    2) Invoke prepareStatement() method to get PreparedStatement object
    3) Store the returned PreparedStatement object in preparedStatement
    4) Set the email and id value to the preparedStatement
    5) Invoke executeQuery() method and next() method
    6) If values are returned, throw AppException
        6.1) Else, Create another preparedStatement with GET_WITH_NAME query and store it in preparedStatement
        6.2) Set the values of first_name and last_name to the preparedStatement 
        6.3) Invoke executeQuery() method and next() method
        6.4) If values are returned, throw AppException 
    7) Close the preparedStatemnet
    
    
    
    //checkAddressId method
    1) Declare checkAddress method with parameter(long id, Connection connection)
    2) Invoke prepareStatement() method with CHECK_ADDRESS query 
    3) Store the returned value in preparedStatement
    4) Invoke setLong() method
    5) Invoke executeQuery() method and next() method
    6) Check if next() method returned false
        6.1) Create a AddressService Object 
        6.2) Invoke delete() method of AddressService object with address-id 
    
    
    
    //Create Person
    1) Create create() method with parameters- (Person person, Connection connection)
    2) Declare id of type Long and set value as 0
    3) Invoke isUnique(email, first-name, last-name, 0, connection) method to check whether the values are correct
    4) Check if address id is not provided
        4.1) Create AddressService object addressService
        4.2) Invoke isPresent() method
        4.3) If isPresent method provided value-0
            4.3.1) Invoke AddressService.create() method
    5) Invoke prepareStatement(INSERT_PERSON, RETURN_GENERATED_KEYS)
    6) Store the returned PreparedStatement object in statement
    7) Invoke setValue(preparedStatement, first_name, last_name, email, addressId, birth_date) to set the values to preparedStatement
    8) After setting values, check if executeUpdate() is equal to 0 and throw AppException with error code
    9) Invoke getGeneratedKeys() method and store the returned value in ResultSet Object-result
    10) Invoke next() method of result to get the first row of data
    11) Invoke result.getLong(1) method to get the stored id value and store it in id 
    12) Invoke close() method of PreparedStatement
    13) return id value 
    
    
    
    //read Person - with id and includeAddress
    1) Create read() method - with parameters- (long id, boolean includeAddress, Connection connection)
    2) Create a Person Object-person and assign null
    3) Invoke prepareStatement(READ_PERSON, RETURN_GENERATED_KEYS) 
    4) Store the returned value in PreparedStatement Object-statement
    5) Set the person-id value in statement with setLong(1, id)
    6) Invoke executeQuery() method and store the returned value in ResultSet Object-result
    7) Invoke getValue() method to get the values from ResultSet object
    8) Check if includeAddress is equal to true 
        8.1) Create a object for AddressService class
        8.2) Invoke read(id, connection) method
        8.3) Store the returned value in person.address
    9) Invoke close() method of PerparedStatement
    10) return person
    
    
    //readAll Person 
    1) Create read() method - with parameters (Connection connection)
    2) Create a List<Person> - persons and assign null
    3) Invoke prepareStatement(READ_ALL_PERSON, RETURN_GENERATED_KEYS) 
    4) Store the returned value in PreparedStatement Object-statement
    5) Set the person-id value in statement with setLong(1, id)
    6) Invoke executeQuery() method and store the returned value in ResultSet Object-result
    7) Open a while loop with condition result.next()
        7.1) Invoke getValue(result) method to get the values
        7.2) Create new AddressService object - addressService
        7.3) Invoke read(person.getAddress().g4etId(), connection) method 
        7.4) Store the returned value in person.address
    8) Invoke close() method of PreparedStatements
    9) return person
    
    
    //Update Person
    1) Create update() method - with parameter (Person person, Long id)
    2) Invoke isUnique(email, first_name, last_name, id, connection) method
    3) Invoke prepareStatement(UPDATE_PERSON, RETURN_GENERATED_KEYS) 
    4) Store the returned value in PreparedStatement Object - statement
    5) Invoke setValue() method to set the values
    6)  Invoke executeUpdate() method 
    7) Invoke close() method of PreparedStatement
    
    
    //Delete Person
    1) Create delete() method with parameter - (Long id)
    2) Invoke read() method to read the person
    3) Invoke checkAddressId() method to check address_id in Person
    4) Invoke prepareStatement(DELETE_PERSON) 
    5) Store the returned value in PreparedStatement
    6) Set the id value in statement with setLong() method
    7) Invoke executeUpdate() method 
    8) Invoke close() method of PreparedStatement
     

Pseudo code:
public class PersonService {
	
	Person person;
	
	public void setValue(PreparedStatement preparedStatement, Person person) throws Exception {
		
	    preparedStatement.setString(1, person.getFirstName());
		preparedStatement.setString(2, person.getLastName());
		preparedStatement.setString(3, person.getEmail());
		preparedStatement.setLong(4, person.getAddress().getId());
		preparedStatement.setDate(5, dateConverter(person.getBirthDate()));
	}
	
	public Person getValue(ResultSet resultSet) throws Exception {
		
		Person person = null;
		person = new Person(resultSet.getLong(1)
					       ,resultSet.getString(Constant.FIRST_NAME)
					       ,resultSet.getString(Constant.LAST_NAME)
					       ,resultSet.getString(Constant.EMAIL)
					       ,new Address(resultSet.getLong(Constant.ADDRESS_ID))
					       ,new java.util.Date(resultSet.getDate(Constant.BIRTH_DATE).getTime()) 
					       ,new java.util.Date(resultSet.getDate(Constant.CREATED_DATE).getTime()));
		return person;
	}
	
    public void personValidator(String email, String firstName, String lastName, long id) {

			try {
				PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.GET_WITH_EMAIL);
				preparedStatement.setString(1, email);
				preparedStatement.setLong(2, id);
		        
				if (preparedStatement.executeQuery().next()) {
		            throw new AppException(ErrorCode.ERR11);
		        }
				
		        preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.GET_WITH_NAME);
		        preparedStatement.setString(1, firstName);
		        preparedStatement.setString(2, lastName);
		        preparedStatement.setLong(3, id);
		        
		        if (preparedStatement.executeQuery().next()) {
		            throw new AppException(ErrorCode.ERR12);
		        }
		        
		        preparedStatement.close();
			} catch (Exception e) {
				e.printStackTrace();
				throw new AppException(ErrorCode.ERR14, e);
			}
			
    }
    
    public void checkAddressId(long id) throws Exception {

			PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.CHECK_ADDRESS);
			preparedStatement.setLong(1, id);
			
			if (preparedStatement.executeQuery().next() == false) {
				AddressService addressService = new AddressService();
				addressService.delete(id);
			}
			
			preparedStatement.close();
    }
    
    
    public Date dateValidator(String value) {
    	
    	DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        formatter.setLenient(false);
        
        try {
			return formatter.parse(value);
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR18, e);
		}
	}
    
    
    public java.sql.Date dateConverter(java.util.Date date) {
    	return new java.sql.Date(date.getTime());
    }
	
    
	public long create(Person person) {
		
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.CREATE_PERSON 
                ,Statement.RETURN_GENERATED_KEYS);) {
			long addressId = person.getAddress().getId();
			personValidator(person.getEmail(), person.getFirstName(), person.getLastName(), 0);
			
			if (person.getAddress().getId() == 0) {
				
				AddressService addressService = new AddressService();
				addressId = addressService.getAddressId(person.getAddress());
				
				if (addressId == 0) {
					addressId = addressService.create(person.getAddress());
				} 
			}
			
			setValue(preparedStatement, new Person( person.getFirstName(), person.getLastName(), person.getEmail(), new Address(addressId), person.getBirthDate()));
			ResultSet resultSet;
			
			if (preparedStatement.executeUpdate() == 1 && (resultSet = preparedStatement.getGeneratedKeys()).next()) {
				return resultSet.getLong(Constant.GENERATED_ID);
			} else {
				throw new AppException(ErrorCode.ERR13);
			}
			
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR13, e);
		}
	}
	
	
	public Person read(long id, boolean includeAddress) {
	   	
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.READ_PERSON)) {
			
			preparedStatement.setLong(1, id);
			ResultSet resultSet;
			
			if ((resultSet = preparedStatement.executeQuery()).next()) {
			    person = getValue(resultSet);
			    
		        if (includeAddress == true) {
		            AddressService addressService = new AddressService();
					person.setAddress(addressService.read(person.getAddress().getId()));
		        }	
		        
			}
			
			return person;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR14, e);
		}
	}
	
	
	public List<Person> readAll() {
	    
	    List<Person> persons = new ArrayList<>();
	    
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.READ_ALL_PERSON);) {
			
			ResultSet resultSet = preparedStatement.executeQuery();
		    
			while (resultSet.next()) {
		        
				person = getValue(resultSet);
		        AddressService addressService = new AddressService();
		        person.setAddress(addressService.read(person.getAddress().getId()));	
		        persons.add(person);
		    }
			
			return persons;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR15, e);
		}
	}
	
	
	public void update(Person person) {
	    
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.UPDATE_PERSON);) {
			
			personValidator(person.getEmail(), person.getFirstName(), person.getLastName(), person.getId());
			setValue(preparedStatement, person);
			preparedStatement.setLong(6, person.getId());
			
			if (preparedStatement.executeUpdate() == 0) {
				throw new AppException(ErrorCode.ERR16);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppException(ErrorCode.ERR16, e);
		}
	}
	
	
	public void delete(long id) {
	    
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.DELETE_PERSON);) {
			
			person = read(id, false);
			checkAddressId(person.getAddress().getId());
			preparedStatement.setLong(1, id);
			
			if (preparedStatement.executeUpdate() < 1) {
				throw new AppException(ErrorCode.ERR17);
			}
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR17, e);
		}
	}
	
	
	
	public List<Person> createPersonWithCSV(String csvfile) {
		
		List<Person> persons = new ArrayList<>();
        FileReader fileReader;
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.CREATE_PERSON 
                ,Statement.RETURN_GENERATED_KEYS);) {
			fileReader = new FileReader(csvfile);
			CSVParser parser = new CSVParser(fileReader, CSVFormat.DEFAULT
                                                                  .withFirstRecordAsHeader()
                                                                  .withIgnoreHeaderCase()
                                                                  .withTrim());
			for (CSVRecord data : parser) {
				
				person = new Person(data.get(Constant.FIRST_NAME)
						           ,data.get(Constant.LAST_NAME)
						           ,data.get(Constant.EMAIL)
						           ,dateValidator(data.get(Constant.BIRTH_DATE)));
				
				String street = data.get(Constant.STREET);
				String city = data.get(Constant.CITY);
				int pincode = Integer.parseInt(data.get(Constant.POSTAL_CODE));
				
				if (street != null && street != "" && city != null && city != "" && pincode != 0) {
					person.setAddress(new Address(street, city, pincode));
				}
				
				long addressId = person.getAddress().getId();
				personValidator(person.getEmail(), person.getFirstName(), person.getLastName(), 0);
					
				if (person.getAddress().getId() == 0) {
						
					AddressService addressService = new AddressService();
					addressId = addressService.getAddressId(person.getAddress());
						
					if (addressId == 0) {
						addressId = addressService.create(person.getAddress());
					} 
				}
					
				setValue(preparedStatement, new Person( person.getFirstName(), person.getLastName(), person.getEmail(), new Address(addressId), person.getBirthDate()));
				ResultSet resultSet;
					
				if (preparedStatement.executeUpdate() == 1 && (resultSet = preparedStatement.getGeneratedKeys()).next()) {
						person.setId(resultSet.getLong(Constant.GENERATED_ID)); 
				} else {
						throw new AppException(ErrorCode.ERR13);
			    }	
				persons.add(person);
			}
			
			fileReader.close();
			parser.close();
			return persons;
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppException(ErrorCode.ERR19, e);
		}
		
		
		
	}
}

 
 * */


package com.kpriet.training.service;

import java.io.FileReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.kpriet.training.connection.ConnectionService;
import com.kpriet.training.constant.Constant;
import com.kpriet.training.constant.QueryStatement;
import com.kpriet.training.exception.AppException;
import com.kpriet.training.exception.ErrorCode;
import com.kpriet.training.model.Address;
import com.kpriet.training.model.Person;

public class PersonService {
	
	Person person;
	
	public void setValue(PreparedStatement preparedStatement, Person person) throws Exception {
		
	    preparedStatement.setString(1, person.getFirstName());
		preparedStatement.setString(2, person.getLastName());
		preparedStatement.setString(3, person.getEmail());
		preparedStatement.setLong(4, person.getAddress().getId());
		preparedStatement.setDate(5, dateConverter(person.getBirthDate()));
	}
	
	public Person getValue(ResultSet resultSet) throws Exception {
		
		Person person = null;
		person = new Person(resultSet.getLong(1)
					       ,resultSet.getString(Constant.FIRST_NAME)
					       ,resultSet.getString(Constant.LAST_NAME)
					       ,resultSet.getString(Constant.EMAIL)
					       ,new Address(resultSet.getLong(Constant.ADDRESS_ID))
					       ,new java.util.Date(resultSet.getDate(Constant.BIRTH_DATE).getTime()) 
					       ,new java.util.Date(resultSet.getDate(Constant.CREATED_DATE).getTime()));
		return person;
	}
	
    public void personValidator(String email, String firstName, String lastName, long id) {

			try {
				PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.GET_WITH_EMAIL);
				preparedStatement.setString(1, email);
				preparedStatement.setLong(2, id);
		        
				if (preparedStatement.executeQuery().next()) {
		            throw new AppException(ErrorCode.ERR11);
		        }
				
		        preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.GET_WITH_NAME);
		        preparedStatement.setString(1, firstName);
		        preparedStatement.setString(2, lastName);
		        preparedStatement.setLong(3, id);
		        
		        if (preparedStatement.executeQuery().next()) {
		            throw new AppException(ErrorCode.ERR12);
		        }
		        
		        preparedStatement.close();
			} catch (Exception e) {
				e.printStackTrace();
				throw new AppException(ErrorCode.ERR14, e);
			}
			
    }
    
    public void checkAddressId(long id) throws Exception {

			PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.CHECK_ADDRESS);
			preparedStatement.setLong(1, id);
			
			if (preparedStatement.executeQuery().next() == false) {
				AddressService addressService = new AddressService();
				addressService.delete(id);
			}
			
			preparedStatement.close();
    }
    
    
    public Date dateValidator(String value) {
    	
    	DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        formatter.setLenient(false);
        
        try {
			return formatter.parse(value);
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR18, e);
		}
	}
    
    
    public java.sql.Date dateConverter(java.util.Date date) {
    	return new java.sql.Date(date.getTime());
    }
	
    
	public long create(Person person) {
		
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.CREATE_PERSON 
                ,Statement.RETURN_GENERATED_KEYS);) {
			long addressId = person.getAddress().getId();
			personValidator(person.getEmail(), person.getFirstName(), person.getLastName(), 0);
			
			if (person.getAddress().getId() == 0) {
				
				AddressService addressService = new AddressService();
				addressId = addressService.getAddressId(person.getAddress());
				
				if (addressId == 0) {
					addressId = addressService.create(person.getAddress());
				} 
			}
			
			setValue(preparedStatement, new Person( person.getFirstName(), person.getLastName(), person.getEmail(), new Address(addressId), person.getBirthDate()));
			ResultSet resultSet;
			
			if (preparedStatement.executeUpdate() == 1 && (resultSet = preparedStatement.getGeneratedKeys()).next()) {
				ConnectionService.commitRollback(true);
				return resultSet.getLong(Constant.GENERATED_ID);
			} else {
				throw new AppException(ErrorCode.ERR13);
			}
			
		} catch (Exception e) {
			ConnectionService.commitRollback(false);
			throw new AppException(ErrorCode.ERR13, e);
		}
	}
	
	
	public Person read(long id, boolean includeAddress) {
	   	
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.READ_PERSON)) {
			
			preparedStatement.setLong(1, id);
			ResultSet resultSet;
			
			if ((resultSet = preparedStatement.executeQuery()).next()) {
			    person = getValue(resultSet);
			    
		        if (includeAddress == true) {
		            AddressService addressService = new AddressService();
					person.setAddress(addressService.read(person.getAddress().getId()));
		        }	
		        
			}
			
			return person;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR14, e);
		}
	}
	
	
	public List<Person> readAll() {
	    
	    List<Person> persons = new ArrayList<>();
	    
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.READ_ALL_PERSON);) {
			
			ResultSet resultSet = preparedStatement.executeQuery();
		    
			while (resultSet.next()) {
		        
				person = getValue(resultSet);
		        AddressService addressService = new AddressService();
		        person.setAddress(addressService.read(person.getAddress().getId()));	
		        persons.add(person);
		    }
			
			return persons;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR15, e);
		}
	}
	
	
	public void update(Person person) {
	    
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.UPDATE_PERSON);) {
			
			personValidator(person.getEmail(), person.getFirstName(), person.getLastName(), person.getId());
			setValue(preparedStatement, person);
			preparedStatement.setLong(6, person.getId());
			
			if (preparedStatement.executeUpdate() == 0) {
				throw new AppException(ErrorCode.ERR16);
			}
			ConnectionService.commitRollback(true);
		} catch (Exception e) {
			ConnectionService.commitRollback(false);
			//e.printStackTrace();
			System.out.println(e.getCause().getMessage());
			throw new AppException(ErrorCode.ERR16, e);
		}
	}
	
	
	public void delete(long id) {
	    
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.DELETE_PERSON);) {
			
			person = read(id, false);
			checkAddressId(person.getAddress().getId());
			preparedStatement.setLong(1, id);
			
			if (preparedStatement.executeUpdate() < 1) {
				throw new AppException(ErrorCode.ERR17);
			}
			ConnectionService.commitRollback(true);
		} catch (Exception e) {
			ConnectionService.commitRollback(false);
			throw new AppException(ErrorCode.ERR17, e);
		}
	}
	
	
	
	public List<Person> createPersonWithCSV(String csvfile) {
		
		List<Person> persons = new ArrayList<>();
        FileReader fileReader;
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.CREATE_PERSON 
                ,Statement.RETURN_GENERATED_KEYS);) {
			fileReader = new FileReader(csvfile);
			CSVParser parser = new CSVParser(fileReader, CSVFormat.DEFAULT
                                                                  .withFirstRecordAsHeader()
                                                                  .withIgnoreHeaderCase()
                                                                  .withTrim());
			for (CSVRecord data : parser) {
				
				person = new Person(data.get(Constant.FIRST_NAME)
						           ,data.get(Constant.LAST_NAME)
						           ,data.get(Constant.EMAIL)
						           ,dateValidator(data.get(Constant.BIRTH_DATE)));
				
				String street = data.get(Constant.STREET);
				String city = data.get(Constant.CITY);
				int pincode = Integer.parseInt(data.get(Constant.POSTAL_CODE));
				
				if (street != null && street != "" && city != null && city != "" && pincode != 0) {
					person.setAddress(new Address(street, city, pincode));
				}
				
				long addressId = person.getAddress().getId();
				personValidator(person.getEmail(), person.getFirstName(), person.getLastName(), 0);
					
				if (person.getAddress().getId() == 0) {
						
					AddressService addressService = new AddressService();
					addressId = addressService.getAddressId(person.getAddress());
						
					if (addressId == 0) {
						addressId = addressService.create(person.getAddress());
					} 
				}
					
				setValue(preparedStatement, new Person( person.getFirstName(), person.getLastName(), person.getEmail(), new Address(addressId), person.getBirthDate()));
				ResultSet resultSet;
					
				if (preparedStatement.executeUpdate() == 1 && (resultSet = preparedStatement.getGeneratedKeys()).next()) {
						person.setId(resultSet.getLong(Constant.GENERATED_ID)); 
				} else {
						throw new AppException(ErrorCode.ERR13);
			    }	
				persons.add(person);
			}
			
			fileReader.close();
			parser.close();
			ConnectionService.commitRollback(true);
			return persons;
		} catch (Exception e) {
			ConnectionService.commitRollback(false);
			e.printStackTrace();
			throw new AppException(ErrorCode.ERR19, e);
		}
		
		
		
	}
}
