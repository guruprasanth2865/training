/*
Requirements:
    - Define AddressService class for CRUD (create, read, readAll, update and delete) operations for Address using JDBC
  use PreparedStatement appropriately
 
Entities:
    - AddressService
    
Function Declaration:
    - public void setValue(PreparedStatement preparedStatement, Address address)
    - public Address getValue(ResultSet result)
    - public void validateAddress(Address address)
    - public long isPresent(Address address, Connection connection)
    - public List<Address> search(Address address, Connection connection)
    - public Long create(Address address, Connection connection)
    - public Address read(Long id, Connection connection)
    - public List<Address> readAll(Connection connection)
    - public void update(Address address, Connection connection)
    - public void delete(long id, Connection connection)
  
Job to be done:
    - Create a class AddressService
    
    //Create method
    1) Declare a  create() method with parameter (Address address, Connection connection)
    2) Declare id of type long and set value as 0
    3) Invoke validateAddress(address) method 
    4) Invoke prepareStatement(INSERT_ADDRESS, RETURN_GENERATED_KEYS)
    5) Store the returned PreparedStatement object in preparedStatement
    6) Invoke setValue(preparedStatement, address) method 
    7) check if executeUpdate() is equal to 1 and invoke getGeneratedKeys() method
    8) Invoke getGeneratedKeys() method and store the returned value in ResultSet Object-result
    9) Invoke next() method of result to get the first row of data
    10) Invoke result.getLong(1) method to get the stored id value and store it in id 
    11) Invoke close() method of PreparedStatement
    12) return id value
    
    
    
    //Read method with id
    1) Create read() method with parameters (Long id, Connection connection)
    2) Create a Address object-address and set it to null
    5) Invoke prepareStatement(READ_ADDRESS, RETURN_GENERATED_KEYS)
    6) Store the returned PreparedStatement object in preparedStatement
    7) Invoke setLong() method to set the id value in prepared statement
    8) Invoke executeQuery() method and store the returned value in result of type ResultSet
    9) Invoke next() method to get the first row of fetched data
    10) Invoke getValue(preparedStatement) method to get the values
    11) Store the returned object in a Address object
    12) Invoke close() method of PreparedStatement 
    13) Return address
    
    
    
    
    //Read All method
    1) Create readAll() method with parameters (Connection connection)
    2) Create a List<Address> - addresses and set it as empty
    5) Invoke prepareStatement(READ_ALL_ADDRESS, RETURN_GENERATED_KEYS)
    6) Store the returned PreparedStatement object in preparedStatement
    7) Invoke executeQuery() method and store the returned value in ResultSet-result
    8) Open a while loop with condition - result.next()
        8.1) Invoke getValue(result) method to get the values and add them to list
    9) Invoke close() method from PreparedStatement
    10) return list-addresses
    
    
    //Update method with id
    1) create update() method with parameter (Address address, Connection connection)
    2) Invoke validateAddress(address) method 
    3) Invoke prepareStatement(UPDATE_ADDRESS, RETURN_GENERATED_KEYS) 
    4) Store the returned value in PreparedStatements
    5) Invoke setValue(preparedStatement, address) method to set the values to preparedStatement 
    6) Invoke executeUpdate() method 
    7) Invoke close() method of PreparedStatement and ConnectionService
    
    
    
    //Delete method with id
    1) Create delete() method with parameter - (Long id)
    2) Invoke prepareStatement(DELETE_ADDRESS) 
    3) Store the returned value in PreparedStatement
    4) Set the id value in statement with setLong() method
    5) Invoke executeUpdate() method 
    6) Invoke close() method of PreparedStatement and ConnectionService
    
    
    
    
    //Search method with values
    1) Declare a search method with parameters (Address address, Connection connection)
    2) Create a list of type Address 
    3) Invoke prepareStatement(SEARCH_ADDRESS, RETURN_GENERATED_KEYS) 
    4) Store the returned value in PreparedStatements
    5) Set the values top preparedStatement
    6) Invoke ececuteQuery() method and store the returned value in ResultSet object
    7) Invoke next() method and Invoke getValue(result) method 
    8) Store the returned object in list
    9) return list
    
    
    
    
    //setValue method 
    1) Declare setValue method with parameters (PreparedStatement preparedStatement, Address address)
    2) Set the values of address to preparedStatement with setString(), setInt() methods
    
    
    
    //getValue method
    1) Declare getValue method with parameter (ResultSet result)
    2) Get the values with getString(), getLong() and getString() method
    3) Create a new Address object with values and return the object
    
    
    
    //validateAddress method
    1) Declare validateAddress() method with parameter (Address address) 
    2) Check if street, city and pincode are empty and throw AppException 
    
    
    //isPresent method
    1) Declare isPresent method 
    2) Invoke prepareStatement() method with Query statement
    3) Store the returned PreparedStatement object in preparedStatement
    4) Invoke setValue(preparedStatement, address) method to set the values to preparedStatement
    5) Invoke Invoke executeQuery() and next() method to get the id value
    6) return the id value
    
    
    
Pseudocode:

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.kpriet.training.jdbc.connection.ConnectionService;
import com.kpriet.training.jdbc.constant.Constant;
import com.kpriet.training.jdbc.constant.QueryStatement;
import com.kpriet.training.jdbc.exception.AppException;
import com.kpriet.training.jdbc.exception.ErrorCode;
import com.kpriet.training.jdbc.model.Address;


public class AddressService {
	
	private Address address;
	
	public void setValue(PreparedStatement preparedStatement, Address address) throws Exception {
		preparedStatement.setString(1, address.getStreet());
		preparedStatement.setString(2, address.getCity());
		preparedStatement.setInt(3, address.getPostalCode());	
	}
	
	public Address getValue(ResultSet resultSet) throws Exception {
		address = new Address(resultSet.getLong(Constant.ID)
				             ,resultSet.getString(Constant.STREET)
				             ,resultSet.getString(Constant.CITY)
				             ,resultSet.getInt(Constant.POSTAL_CODE));
		return address;
	}
	
	public void validateAddress(Address address) {
	    
		if (address.getPostalCode() == 0 
	        || address.getStreet() == "" 
	        || address.getCity() == ""
	        || address.getStreet() == null
	        || address.getCity() == null) {
	        throw new AppException(ErrorCode.ERR10);
	    }
	}
	
	public long getAddressId(Address address) throws Exception {
		ResultSet resultSet;
		
	    try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.GET_ADDRESS_ID);) {
	    	setValue(preparedStatement, address);
			
	    	if ((resultSet = preparedStatement.executeQuery()).next()) {
			    return resultSet.getLong(Constant.ID);
			}
	    	
		    return 0;
	    } catch (Exception e) {
	    	throw new AppException();
	    }
	}
	
	public List<Address> search(String content) {
		
		List<Address> addresses = new ArrayList<>();
		
        if (content == null || content == "" ) {
            return null;
        }
        
        StringBuilder string1 = new StringBuilder("");
        StringBuilder string2 = new StringBuilder("");
        StringBuilder pincode = new StringBuilder("");
        
        for (String word : content.split(",")) {
        	
            if (Pattern.matches(Constant.PINCODE_PATTERN, word.trim())) {
            	pincode.append(word.trim());
            } else if (string1.toString().equals("")) {
            	string1.append(word.trim());
            } else {
            	string2.append(word.trim());
            }
        }
        
        try(PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.SEARCH_ADDRESS)) {
     
        	preparedStatement.setString(1, string1.toString());
            preparedStatement.setString(2, string2.toString());
            preparedStatement.setString(3, string2.toString());
            preparedStatement.setString(4, string1.toString());
            preparedStatement.setString(5, pincode.toString());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
            	addresses.add(getValue(resultSet));
            }
            
            return addresses;
        }
		catch (Exception e) {
			e.printStackTrace();
		    throw new AppException(ErrorCode.ERR04, e);
		}
	}
	
	public long create(Address address) {
		
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.CREATE_ADDRESS
					                                                                 ,Statement.RETURN_GENERATED_KEYS);) {
			
			validateAddress(address);
			setValue(preparedStatement, address);
			ResultSet resultSet;
			
			if(preparedStatement.executeUpdate() == 1  && (resultSet = preparedStatement.getGeneratedKeys()).next()) {
				
				return resultSet.getLong(Constant.GENERATED_ID);
			} else {
				
				throw new AppException(ErrorCode.ERR05);
			}
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR05, e);
		}
	}
	
	public Address read(long id) {
		
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.READ_ADDRESS);) {
			
			preparedStatement.setLong(1, id);
			ResultSet resultSet;
			
			if ((resultSet = preparedStatement.executeQuery()).next()) {
			    return getValue(resultSet);  
			} 
			
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppException(ErrorCode.ERR06, e);
		}
	}
	
	
	public List<Address> readAll() {
		
		List<Address> addresses = new ArrayList<>();
		
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.READ_ALL_ADDRESS)) {
			
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while (resultSet.next()) {
			    addresses.add(getValue(resultSet));
		    }
			
			return addresses;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR07, e);
		}
	}
	
	
	public void update(Address address) {
		
		validateAddress(address);
		
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.UPDATE_ADDRESS)) {
			
			setValue(preparedStatement, address);
			preparedStatement.setLong(4, address.getId());
            
			if (preparedStatement.executeUpdate() < 1) {
				throw new AppException(ErrorCode.ERR08);
			}
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR08, e);
		}
	}
	
	
	public void delete(long id) {
		
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.DELETE_ADDRESS)) {
			
			preparedStatement.setLong(1, id);
		    
			if (preparedStatement.executeUpdate() < 1) {
				throw new AppException(ErrorCode.ERR09);
			}
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR09, e);
		}
	}
}

    
 * */



package com.kpriet.training.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.kpriet.training.connection.ConnectionService;
import com.kpriet.training.constant.Constant;
import com.kpriet.training.constant.QueryStatement;
import com.kpriet.training.exception.AppException;
import com.kpriet.training.exception.ErrorCode;
import com.kpriet.training.model.Address;


public class AddressService {
	
	private Address address;
	
	public void setValue(PreparedStatement preparedStatement, Address address) throws Exception {
		preparedStatement.setString(1, address.getStreet());
		preparedStatement.setString(2, address.getCity());
		preparedStatement.setInt(3, address.getPostalCode());	
	}
	
	public Address getValue(ResultSet resultSet) throws Exception {
		address = new Address(resultSet.getLong(Constant.ID)
				             ,resultSet.getString(Constant.STREET)
				             ,resultSet.getString(Constant.CITY)
				             ,resultSet.getInt(Constant.POSTAL_CODE));
		return address;
	}
	
	public void validateAddress(Address address) {
	    
		if (address.getPostalCode() == 0 
	        || address.getStreet() == "" 
	        || address.getCity() == ""
	        || address.getStreet() == null
	        || address.getCity() == null) {
	        throw new AppException(ErrorCode.ERR10);
	    }
	}
	
	public long getAddressId(Address address) throws Exception {
		ResultSet resultSet;
		
	    try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.GET_ADDRESS_ID);) {
	    	setValue(preparedStatement, address);
			
	    	if ((resultSet = preparedStatement.executeQuery()).next()) {
			    return resultSet.getLong(Constant.ID);
			}
	    	
		    return 0;
	    } catch (Exception e) {
	    	throw new AppException(ErrorCode.ERR10);
	    }
	}
	
	public List<Address> search(String content) {
		
		List<Address> addresses = new ArrayList<>();
		
        if (content == null || content == "" ) {
            return null;
        }
        
        StringBuilder string1 = new StringBuilder("");
        StringBuilder string2 = new StringBuilder("");
        StringBuilder pincode = new StringBuilder("");
        
        for (String word : content.split(",")) {
        	
            if (Pattern.matches(Constant.PINCODE_PATTERN, word.trim())) {
            	pincode.append(word.trim());
            } else if (string1.toString().equals("")) {
            	string1.append(word.trim());
            } else {
            	string2.append(word.trim());
            }
        }
        
        try(PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.SEARCH_ADDRESS)) {
     
        	preparedStatement.setString(1, string1.toString());
            preparedStatement.setString(2, string2.toString());
            preparedStatement.setString(3, string2.toString());
            preparedStatement.setString(4, string1.toString());
            preparedStatement.setString(5, pincode.toString());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
            	addresses.add(getValue(resultSet));
            }
            
            return addresses;
        }
		catch (Exception e) {
			e.printStackTrace();
		    throw new AppException(ErrorCode.ERR04, e);
		}
	}
	
	public long create(Address address) {
		
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.CREATE_ADDRESS
					                                                                 , Statement.RETURN_GENERATED_KEYS);) {
			
			validateAddress(address);
			setValue(preparedStatement, address);
			ResultSet resultSet;
			
			if(preparedStatement.executeUpdate() != 0  && (resultSet = preparedStatement.getGeneratedKeys()).next()) {
				ConnectionService.commitRollback(true);
				//ConnectionService.get().commit();
				return resultSet.getLong(Constant.GENERATED_ID);
			} else {
				ConnectionService.commitRollback(false);
				throw new AppException(ErrorCode.ERR05);
			}
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR05, e);
		}
	}
	
	public Address read(long id) {
		
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.READ_ADDRESS);) {
			
			preparedStatement.setLong(1, id);
			ResultSet resultSet;
			
			if ((resultSet = preparedStatement.executeQuery()).next()) {
			    return getValue(resultSet);  
			}
			return null;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR06, e);
		}
	}
	
	
	public List<Address> readAll() {
		
		List<Address> addresses = new ArrayList<>();
		
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.READ_ALL_ADDRESS)) {
			
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while (resultSet.next()) {
			    addresses.add(getValue(resultSet));
		    }
			
			return addresses;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR07, e);
		}
	}
	
	
	public void update(Address address) {
		
		
		
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.UPDATE_ADDRESS)) {
			validateAddress(address);
			setValue(preparedStatement, address);
			preparedStatement.setLong(4, address.getId());
            
			if (preparedStatement.executeUpdate() < 1) {
				throw new AppException(ErrorCode.ERR08);
			}
			ConnectionService.commitRollback(true);
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			ConnectionService.commitRollback(false);
			throw new AppException(ErrorCode.ERR08, e);
		}
	}
	
	
	public void delete(long id) {
		
		try (PreparedStatement preparedStatement = ConnectionService.get().prepareStatement(QueryStatement.DELETE_ADDRESS)) {
			
			preparedStatement.setLong(1, id);
		    
			if (preparedStatement.executeUpdate() < 1) {
				throw new AppException(ErrorCode.ERR09);
			}
			ConnectionService.commitRollback(true);
		} catch (Exception e) {
			ConnectionService.commitRollback(false);
			throw new AppException(ErrorCode.ERR09, e);
		}
	}
	
	/*
	public static void main(String[] args) {
		AddressService addressService = new AddressService();
		ConnectionService connectionService = new ConnectionService();
		Address address = addressService.read(1);
		System.out.println(address.getStreet());
	}
	*/
}
