/*
Requirements: 
    - Create a class to  Parse jsonString into object based on class type and object into jsonString
    
Entity:
    - Js
 
 * */


package com.kpriet.training.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonUtil {
	
	public static Gson gson = new Gson();

	public static Object toObject(String jsonString, Class<?> type) {
		
		return new GsonBuilder().setDateFormat("yyyy-MM-dd").create().fromJson(jsonString, type);
	}
	
	public static String toJson(Object object) {
		
		return new GsonBuilder().setDateFormat("yyyy-MM-dd").create().toJson(object);
		 
	}
}
