/*
Requirements:
    - POJO class for Address 
    
Entities:
    - Address
    
Function Declaration:
    - public Address(Long id, String street, String city, int pincode)
    - public Address(String street, String city, int pincode)
    - public Address(String street, String city)
    - public long getId()
    - public String getStreet()
    - public String getCity()
    - public int getPincode() 
    - public void setId(Long id)
    - public void setStreet(String street)
    - public void setCity(String city) 
    - public void setPincode(int pincode)
    
Job to be done:
    1) Create a class Address
    2) Set 4 properties to it. - -d, street, city and pincode
    3) Declare a Address constructor method to get id, street, city and pincode
        3.1) Set the values of properties with the parameter values
    4) Declare a Address constructor method to get street, city and pincode
        4.1) Set the values of properties with the parameter values
    5) Declare a Address constructor method to get id, street, city 
        5.1) Set the values of properties with the parameter values
    6) Declare a getId() method to return id
    7) Declare a getStreet() method to return street
    8) Declare a getCity() method to return city
    9) Declare a getPincode() method to return pincode
    10) Declare a setId(Long id) method to set the value of id
    11) Declare a setStreet(String street) method to set the value od street
    12) Declare a setCity(String city) method to set the value of city
    13) Declare a setPincode(int pincode) method to set the value of pincode

Pseudo code:
public class Address {

	private long id;
	private String street;
	private String city;
	private int pincode;
	
	public Address(Long id, String street, String city, int pincode) {
		this.id = id;
		this.street = street;
		this.city = city;
		this.pincode = pincode;
	}
	
	public Address(String street, String city, int pincode) {
		this.street = street;
		this.city = city;
		this.pincode = pincode;
	}
	
	public Address(String street, String city) {
		this.street = street;
		this.city = city;
	}
	
	public long getId() {
		return this.id;
	}
	
	public String getStreet() {
		return this.street;
	}
	
	public String getCity() {
		return this.city;
	}
	
	public int getPincode() {
		return this.pincode;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setPincode(int pincode) {
		this.pincode = pincode;
		
	}
}
 
 * */


package com.kpriet.training.model;

public class Address {

	private long id;
	private String street;
	private String city;
	private int postalCode;
	
	public Address(long id, String street, String city, int postalCode) {
		this.id = id;
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
	}
	
	public Address(String street, String city, int postalCode) {
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
	}
	
	public Address(long id) {
		this.id = id;
	}
	
	public long getId() {
		return this.id;
	}
	
	public String getStreet() {
		return this.street;
	}
	
	public String getCity() {
		return this.city;
	}
	
	public int getPostalCode() {
		return this.postalCode;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
		
	}

	@Override
	public String toString() {
		
		/*
		if (id == 0 && (street == null || street == "") && (city == null || city == "")) {
		    return null;
		}
		*/
		
		return new StringBuilder("Address [id=")
				         .append(id)
				         .append(", street=")
				         .append(street)
				         .append(", city=")
				         .append(city)
				         .append(", postalCode=")
				         .append(postalCode)
				         .append("]").toString();
	}
}
