/*
Requirements:
    - Create a class named ConnectionService with methods to connect with database
    
Entities:
    - ConnectionService
    
Function Declaration:
    - public ConnectionService()
    - public void init()
    - public Connection get()
    - public void commitRollback(boolean condition)
    - public void release()

Job to be done:
    1) Create class ConnectionService
    2) Declare DataSource Object - "dataSource"
    3) Declare ThreadLocal Object of Connection Object - "thread" 
    
    
    //public ConnectionService() method - constructor
    1) Invoke init() method
    
    
    //init() method - public void init()
    1) Create a Properties object - "dbDetails"
    2) Create a HikariConfig Object - "config" 
    3) Invoke getResourceAsStream() method inside load() method to get the values from properties file and 
       load them in Properties Object - dbDetails
    3) Get the values of url, username and password from dbDetails with getProperty(key) method
    4) Set url with setJdbcUrl(url) method.
    5) Set username with setUsername(username) method.
    6) Set password with setPassword(password) method.
    7) Set maximum pool size as 3 with setMaximumPoolSize(value) method
    8) Add dataSource properties to HikariConfig Object.
    9) Create new HikariDataSource object and assign it to dataSource 
 
    
    //get() method - public Connection get()
    1) Check if ThreadLocal Value is null
      1.1) Invoke dataSource.getConnection() method to get the connection and set in ThreadLocal.
    2) return ThreadLocal.get() 
    
    
    //commitRollback() method - public void commitRollback(boolean condition)
    1) Check if condition is equal to true
        1.1) Invoke commit() method to commit changes in db
        1.2) Else Invoke rollback() method to rollback
    
    
    //release method - public void release()
    1) Invoke ThreadLocal.get().close() method
    2) Invoke ThreadLocal.remove() method
    
    
Pseudo code:
public class ConnectionService {
	
	ThreadLocal<Connection> threadLocal = new ThreadLocal<Connection>();
	
	public ConnectionService() {
		init();
	}

    public void init() {

        try {
			Properties dbDetails = new Properties();
			HikariConfig config = new HikariConfig();
			dbDetails.load(ConnectionService.class.getClassLoader().getResourceAsStream("db.properties"));
			config.setJdbcUrl()
			config.setUsername();
			config.setPassword();
			config.setMaximumPoolSize();
			config.setAutoComit(false)
	        
	        
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR01, e);
		}
    }
    
    public Connection get() {
        return threadLocal.get();
    }
    
    public void commitRollback(boolean condition) {
    	if (condition == true) {
    		try {
				threadLocal.get().commit();
			} catch (Exception e) {
				throw new AppException(ErrorCode.ERR02, e);
			}
    	} else {
    		try {
    			threadLocal.get().rollback();
			} catch (SQLException e) {
				throw new AppException(ErrorCode.ERR02, e);
			}
    	}
    }
    
    public void release() {
    	try {
			threadLocal.get().close();
			threadLocal.remove();
		} catch (SQLException e) {
			throw new AppException(ErrorCode.ERR03, e);
		}
    }
}
 
 * */


package com.kpriet.training.connection;



import com.kpriet.training.constant.Constant;
import com.kpriet.training.exception.AppException;
import com.kpriet.training.exception.ErrorCode;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.util.Properties;

public class ConnectionService {
	
	public static HikariDataSource dataSource;
	public static HikariConfig config = new HikariConfig();
	public static ThreadLocal<Connection> thread = new ThreadLocal<Connection>();

    public static void init() {

        try {
			Properties dbDetails = new Properties();
	        dbDetails.load(ConnectionService.class.getClassLoader().getResourceAsStream("db.properties"));
	        config.setJdbcUrl(dbDetails.getProperty(Constant.URL));
	        config.setUsername(dbDetails.getProperty(Constant.USERNAME));
	        config.setPassword(dbDetails.getProperty(Constant.PASSWORD));
	        config.setMaximumPoolSize(Constant.CONNECTION_SIZE);
	        config.setAutoCommit(false);
	        dataSource = new HikariDataSource(config);
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR01, e);
		}
    }
    
    public static Connection get() {
        if (thread.get() == null) {
        	try {
        		ConnectionService.init();
				thread.set(dataSource.getConnection());
			} catch (Exception e) {
				throw new AppException(ErrorCode.ERR01, e);
			}
        }
        return thread.get();
    }
    
    public static void commitRollback(boolean condition) {
    	if (condition == true) {
    		try {
				thread.get().commit();
			} catch (Exception e) {
				throw new AppException(ErrorCode.ERR02, e);
			}
    	} else {
    		try {
    			thread.get().rollback();
			} catch (Exception e) {
				throw new AppException(ErrorCode.ERR02, e);
			}
    	}
    }
    
    public static void release() {
    	try {
			thread.get().close();
			thread.remove();
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR03, e);
		}
    }
    
    
    /*
    public static void main(String[] args) {
    	
    	ConnectionService connectionService = new ConnectionService();
    	System.out.println(connectionService.get());
    }
    */
}
