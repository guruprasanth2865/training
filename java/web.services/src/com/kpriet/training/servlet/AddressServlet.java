/*
Requirement:-
    - Implement the method doGet(), doPost(), doPut(), doDelete() for address servlet
  
Entity :-
    - AddressServlet  class
    - HttpServlet
  
Function Signature :-
    - protected void doGet (HttpServletRequest req, HttpServletResponse res) throws AppException()
    - protected void doPost (HttpServletRequest req, HttpServletResponse res) throws AppException()
    - protected void doPut (HttpServletRequest req, HttpServletResponse res) throws AppException()
    - protected void doDelete (HttpServletRequest req, HttpServletResponse res) throws AppException()
 
Jobs to be Done :-

    1) Create class AddressServlet that extends HttpServlet
    2) Create a JsonUtil object as a static variable
    3) Create a AddressService object as a static variable
    
    doPost
    ------
    1) Declare doPost() method with 2 parameters - HttpServletRequest req and HttpServletResponse res
    2) Open a try block and create a PrintWriter object for response
    3) Create a BufferedReader object to read json data form the request
    4) Invoke JsonUtil.toObject() method to convert json to respective object
    5) Invoke addressService.create() method and pass the converted object as an argument to create an address
    6) Store the generated id in addressId
    7) Invoke toJson() method to parse the addressId to json string
    8) Set the json string to HttpServletResponse res body 

    doGet
    -----
    1) Declare doGet() method with 2 parameters - HttpServletRequest req and HttpServletResponse res
    2) Get the id of address from HttpServletRequest req with getParameter() method
    3) Invoke addressService.read() method and pass the id as an argument 
    4) Store the returned Address Object in address
    5) Invoke toJson() method to parse the address object to json string
    6) Set the json string to HttpServletResponse res body
     
    doPut
    -----
    1) Declare doPut() method with 2 parameters - HttpServletRequest req and HttpServletResponse res
    2) Open a try block and create a PrintWriter object for response
    3) Create a BufferedReader object to read json data form the request
    4) Invoke JsonUtil.toObject() method to convert json to respective object
    5) Invoke addressService.update() method and pass the converted object as an argument to update an address
    6) Set the success message to HttpServletResponse res body 
    
    doDelete
    --------
    1) Declare doDelete() method with 2 parameters - HttpServletRequest req and HttpServletResponse res
    2) Get the id of address from HttpServletRequest req with getParameter() method
    3) Invoke addressService.delete() method and pass the id as an argument 
    4) Set the success message to HttpServletResponse res body 
  
 pseudo code :-
 
 class AddressServlet extends HttpServlet {
 
     String line = null;
 
     protected void doGet(HttpServletRequest req, HttpServletResponse res) {
         
         try (PrintWriter writer = res.getWriter();) {
             writer.append(JsonUtil.toJson(addressService.read((long)(req.getParameter("id"))));
         } catch ( Exception e) {
             e.printStackTrace();
         }
     }
     
     protected void doPost(HttpServletRequest req, HttpServletResponse res) {
         
         try (BufferedReader reader = new BufferedReader();
             PrintWriter writer = res.getWriter();) {
              
             StringBuffer jsonContent = new StringBuffer();
             
             while((line = reader.readLine()) != null) {
                 jsonContent.append(line);
             }
             
             long addressId = addressService.create(JsonUtil.toObject(jsonContent, Address))
             
             writer.append(addressId);
         }
     } 
     
     protected void doPut(HttpServletRequest req, HttpServletResponse res) {
         
         try (BufferedReader reader = new BufferedReader();
             PrintWriter writer = res.getWriter();) {
            
             StringBuffer jsonContent = new StringBuffer();
             
             while((line = reader.readLine()) != null) {
                 jsonContent.append(line);
             }
             
             addressService.update(JsonUtil.toObject(jsonContent, Address));
             
             writer.append("Address Updated Successfully");
         }
     } 
     
     protected void doDelete(HttpServletRequest req, HttpServletResponse res) {
         
         try (BufferedReader reader = new BufferedReader();
             PrintWriter writer = res.getWriter();) {
              
             StringBuffer jsonContent = new StringBuffer();
             
             while((line = reader.readLine()) != null) {
                 jsonContent.append(line);
             }
             
             addressService.delete((long)(jsonContent));
             
             writer.append("Deleted successfully");
         }
     }  
 } 
 * */


package com.kpriet.training.servlet;

import java.io.PrintWriter;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kpriet.training.exception.AppException;
import com.kpriet.training.exception.ErrorCode;
import com.kpriet.training.model.Address;
import com.kpriet.training.service.AddressService;
import com.kpriet.training.util.JsonUtil;

public class AddressServlet extends HttpServlet {
	 
	private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest req, HttpServletResponse res) {
        
        try (PrintWriter writer = res.getWriter()) {
        	String addressId = req.getParameter("id"); 
        	if (addressId != "" && addressId != null) {
        		writer.append(JsonUtil.toJson(new AddressService().read(Long.parseLong(addressId))));
        	} else {
        		writer.append(JsonUtil.toJson(new AddressService().readAll()));
        	}
        } catch (AppException e) {
        	throw e;
		} catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void doPost(HttpServletRequest req, HttpServletResponse res) {
        
        try (PrintWriter writer = res.getWriter();) {
            StringBuffer jsonContent = new StringBuffer().append(req.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
            writer.append(JsonUtil.toJson(new AddressService().create((Address)JsonUtil.toObject(jsonContent.toString(), Address.class))));
        } catch (AppException e) {
        	throw e;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR04, e);
		}
    } 
    
    public void doPut(HttpServletRequest req, HttpServletResponse res) {
            
        try {
            StringBuffer jsonContent = new StringBuffer().append(req.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
			new AddressService().update((Address) JsonUtil.toObject(jsonContent.toString(), Address.class));
		} catch (AppException e) {
        	throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
    } 
    
    public void doDelete(HttpServletRequest req, HttpServletResponse res) {
        
        try {
        	new AddressService().delete(Long.parseLong(req.getParameter("id")));
        } catch (AppException e) {
        	throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
    }  
}
