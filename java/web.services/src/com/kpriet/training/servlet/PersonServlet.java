/*
Requirements: 
    - Implement the methods doGet(), doPost(), doPut(), doDelete() method for PersonServlet
    
Entity:
    PersonServlet
    HttpServlet
    
Function Declaration:
    - public void doGet(HttpServletRequest req, HttpServletResopnse res)
    - public void doPost(HttpServletRequest req, HttpServletResopnse res)
    - public void doPut(HttpServletRequest req, HttpServletResopnse res)
    - public void doDelete(HttpServletRequest req, HttpServletResopnse res)
    
Jobs to be done:
    
    1) To write a PersonServlet class that extends HttpServlet
    2) To create a JsonUtil static variable 
    3) To declare a string line
    4) To create a static PersonServlet Object
    
    doGet() 
    -------
    1) Declare doGet() method with two parameters - HttpServletRequest req, HttpServletResponse res
    2) Get the id of the person from HttpServletRequest object ( req ) with getParameter() method
    3) Invoke the PersonService.read() method to read the person
    4) Store the returned Person object to person
    5) Invoke toJson() method to parse the Person object to json string
    6) Set the json string to json string to res
    
    doPost() 
    --------
    1) Declare doPost() method with two parameters - HttpServletRequest req, HttpServletResponse res
    2) Open a try block and create a PrintWriter object for response
    3) Create a BufferReader Object to read the json object from req
    4) Invoke JsonUtil.toObject() method to convert json to respective Person Object
    5) Invoke PersonService.create() method to create the person and store the returned id in personId
    6) Invoke toJson() method to convert the id to json string
    7) Set the json string to HttpResponseServlet res body
    
    doPut() 
    -------
    1) Declare doPut() method with 2 parameters - HttpServletRequest req, HttpSerevletResponse res
    2) Open a try block with a PrintWriter object to write response
    3) Create a BufferedReader object to read json data form the request
    4) Invoke JsonUtil.toObject() method to convert json to respective object
    5) Invoke personService.update() method and pass the converted object as an argument to update a person
    6) Set the success message to HttpServletResponse res body 
    
    doDelete()
    ----------
    1) Declare doDelete() method with 2 parameters - HttpServletRequest req and HttpServletResponse res
    2) Get the id of address from HttpServletRequest req with getParameter() method
    3) Invoke personService.delete() method and pass the id as an argument 
    4) Set the success message to HttpServletResponse res body 


pseudo code
-----------
public class PersonServlet extends HttpServlet{
	
		
	private static final long serialVersionUID = -6062858640508950336L;
	
	JsonUtil jsonUtil = new JsonUtil();
	PersonService personService = new PersonService();
	ConnectionService connectionService = new ConnectionService();

	public void doGet(HttpServletRequest req, HttpServletResponse res) {
        
        try (PrintWriter writer = res.getWriter()) {
        	String personId = req.getParameter("id"); 
        	String includeAddress = req.getParameter("include_address");
        	if (personId != "" && personId != null) {
        		writer.append(JsonUtil.toJson(new PersonService().read(Long.parseLong(personId), Boolean.parseBoolean(includeAddress))));
        	} else {
        		writer.append(JsonUtil.toJson(new PersonService().readAll()));
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void doPost(HttpServletRequest req, HttpServletResponse res) {
        
        try (PrintWriter writer = res.getWriter()) {
            StringBuffer jsonContent = new StringBuffer().append(req.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
            writer.append(JsonUtil.toJson(new PersonService().create((Person)JsonUtil.toObject(jsonContent.toString(), Person.class))));
        } catch (Exception e) {
			e.printStackTrace();
		}
    } 
    
    public void doPut(HttpServletRequest req, HttpServletResponse res) {
            
        try {
            StringBuffer jsonContent = new StringBuffer().append(req.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
			new PersonService().update((Person) JsonUtil.toObject(jsonContent.toString(), Person.class));
		} catch (Exception e) {
			e.printStackTrace();
		}
    } 
    
    public void doDelete(HttpServletRequest req, HttpServletResponse res) {
        
        try {
        	new PersonService().delete(Long.parseLong(req.getParameter("id")));
        } catch (Exception e) {
			e.printStackTrace();
		}
    }  
	 

}

 
 * */


package com.kpriet.training.servlet;

import java.io.PrintWriter;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kpriet.training.connection.ConnectionService;
import com.kpriet.training.model.Person;
import com.kpriet.training.service.PersonService;
import com.kpriet.training.util.JsonUtil;

public class PersonServlet extends HttpServlet{
	
	private static final long serialVersionUID = -6062858640508950336L;
	
	JsonUtil jsonUtil = new JsonUtil();
	PersonService personService = new PersonService();
	ConnectionService connectionService = new ConnectionService();

	public void doGet(HttpServletRequest req, HttpServletResponse res) {
        
        try (PrintWriter writer = res.getWriter()) {
        	String personId = req.getParameter("id"); 
        	String includeAddress = req.getParameter("include_address");
        	if (personId != "" && personId != null) {
        		writer.append(JsonUtil.toJson(new PersonService().read(Long.parseLong(personId), Boolean.parseBoolean(includeAddress))));
        	} else {
        		writer.append(JsonUtil.toJson(new PersonService().readAll()));
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void doPost(HttpServletRequest req, HttpServletResponse res) {
        
        try (PrintWriter writer = res.getWriter()) {
            StringBuffer jsonContent = new StringBuffer().append(req.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
            writer.append(JsonUtil.toJson(new PersonService().create((Person)JsonUtil.toObject(jsonContent.toString(), Person.class))));
        } catch (Exception e) {
			e.printStackTrace();
		}
    } 
    
    public void doPut(HttpServletRequest req, HttpServletResponse res) {
            
        try {
            StringBuffer jsonContent = new StringBuffer().append(req.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
			new PersonService().update((Person) JsonUtil.toObject(jsonContent.toString(), Person.class));
		} catch (Exception e) {
			e.printStackTrace();
		}
    } 
    
    public void doDelete(HttpServletRequest req, HttpServletResponse res) {
        
        try {
        	new PersonService().delete(Long.parseLong(req.getParameter("id")));
        } catch (Exception e) {
			e.printStackTrace();
		}
    }  
	 

}
