let swapContainer = (idValue, btnValue) => {
  document.querySelectorAll(".details-container").forEach(element => element.style.display="none")
  document.getElementById(idValue).style.display = "block"; 
  document.querySelectorAll(".category-btn").forEach(element => element.style.backgroundColor="#f8697e")
  document.getElementById(btnValue).style.backgroundColor = "#d73d63";
}

let validateInput = (obj) => {
  if (obj.name == "" || obj.area == "" || obj.id == "") {
    throw "Enter all values";
  }
}