let clearStudentFormData = () => {
  document.getElementById("student-name").value = "";
  document.getElementById("student-area").value = "";
  document.getElementById("student-id").value = "";
  document.getElementById("student-name").focus();
  document.getElementById("new-student-heading").innerHTML = "New Student";
  document.getElementById("student-add-btn").innerHTML = "Add";
  document.getElementById("student-add-btn").onclick = async () => await createStudent();
}

let setStudentFormData = (studentData) => {
  document.getElementById("student-name").value = studentData.name;
  document.getElementById("student-area").value = studentData.area;
  document.getElementById("student-id").value = studentData.id;
  document.getElementById("new-student-heading").innerHTML = "Edit student";
  document.getElementById("student-add-btn").innerHTML = "Edit";
  document.getElementById("student-add-btn").onclick = () => updateStudent(studentData.id);
}

let renderStudentGridData = (listData) => {
  let studentDetail = document.getElementById("student-details");
  studentDetail.innerHTML = "";
  let columnName = document.createElement("div");
  let columnArea = document.createElement("div");
  let columnID = document.createElement("div");
  let columnAction = document.createElement("div");

  columnName.innerHTML = "Name";
  columnArea.innerHTML = "Area";
  columnID.innerHTML = "ID";
  columnAction.innerHTML = "Actions";

  columnName.classList.add("column-name");
  columnName.classList.add("text-align-center");
  columnArea.classList.add("column-name");
  columnArea.classList.add("text-align-center");
  columnID.classList.add("column-name");
  columnID.classList.add("text-align-center");
  columnAction.classList.add("column-name");
  columnAction.classList.add("text-align-center");

  studentDetail.appendChild(columnName);
  studentDetail.appendChild(columnArea);
  studentDetail.appendChild(columnID);
  studentDetail.appendChild(columnAction);

  listData.forEach(studentData => {

    let name = document.createElement("div");
    let area = document.createElement("div");
    let id = document.createElement("div");
    let actions = document.createElement("div");
    let deleteLink = document.createElement("a");

    actions.classList.add("text-align-center");
    actions.classList.add("action-container");
    deleteLink.classList.add("delete");

    name.innerHTML = studentData.name;
    area.innerHTML = studentData.area;
    id.innerHTML = studentData.id;
    deleteLink.innerHTML = "Delete";
    deleteLink.onclick = async () => {
      await remove("http://localhost:3002/student", studentData.id);
      await reloadStudent();
      clearFormData();
    };
    name.onclick = () => setStudentFormData(studentData);
    area.onclick = () => setStudentFormData(studentData);
    id.onclick = () => setStudentFormData(studentData);
    actions.onclick = () => setStudentFormData(studentData);

    actions.appendChild(deleteLink);
    studentDetail.appendChild(name);
    studentDetail.appendChild(area);
    studentDetail.appendChild(id);
    studentDetail.appendChild(actions);
  });
}

let createStudent = async () => {

  let student = {
    name: document.getElementById("student-name").value.trim(),
    area: document.getElementById("student-area").value.trim(),
    id: document.getElementById("student-id").value.trim()
  }

  try {
    validateInput(student);
    await create("http://localhost:3002/student", student);
    await reloadStudent();
    clearStudentFormData();
  } catch (err) {
    alert(err);
  }
}

let updateStudent = async (id) => {
  let student = {
    name: document.getElementById("student-name").value.trim(),
    area: document.getElementById("student-area").value.trim(),
    id: document.getElementById("student-id").value.trim()
  }

  try {
    validateInput(student);
    await update("http://localhost:3002/student", student, id);
    await reloadStudent();
  } catch (err) {
    alert(err);
  }
}


const reloadStudent = async () => {
  let studentData = await read("http://localhost:3002/student");
  renderStudentGridData(studentData);
}

const loadStudent = (async () => {
  let studentData = await read("http://localhost:3002/student");
  renderStudentGridData(studentData);
  if (studentData.length > 0) setStudentFormData(studentData[0]);
})();
