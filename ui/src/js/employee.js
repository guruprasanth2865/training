let setFormData = (employeeData) => {
  document.getElementById("name").value = employeeData.name;
  document.getElementById("area").value = employeeData.area;
  document.getElementById("id").value = employeeData.id;
  document.getElementById("new-employee-heading").innerHTML = "Edit Employee";
  document.getElementById("employee-add-btn").innerHTML = "Edit";
  document.getElementById("employee-add-btn").onclick = () => updateEmployee(employeeData.id);
}

let updateEmployee = async (id) => {
  let employee = {
    name: document.getElementById("name").value.trim(),
    area: document.getElementById("area").value.trim(),
    id: document.getElementById("id").value.trim()
  }

  try {
    validateInput(employee);
    await update("http://localhost:3002/employee", employee, id);
    await reloadEmployee();
  } catch (err) {
    alert(err);
  }
}

let clearFormData = () => {
  document.getElementById("name").value = "";
  document.getElementById("area").value = "";
  document.getElementById("id").value = "";
  document.getElementById("name").focus();
  document.getElementById("new-employee-heading").innerHTML = "New Employee";
  document.getElementById("employee-add-btn").innerHTML = "Add";
  document.getElementById("employee-add-btn").onclick = async () => await createEmployee();
}

let renderEmployeeGridData = (listData) => {
  let employeeDetail = document.getElementById("employee-details");
  employeeDetail.innerHTML = "";
  let columnName = document.createElement("div");
  let columnArea = document.createElement("div");
  let columnID = document.createElement("div");
  let columnAction = document.createElement("div");

  columnName.innerHTML = "Name";
  columnArea.innerHTML = "Area";
  columnID.innerHTML = "ID";
  columnAction.innerHTML = "Actions";

  columnName.classList.add("column-name");
  columnName.classList.add("text-align-center");
  columnArea.classList.add("column-name");
  columnArea.classList.add("text-align-center");
  columnID.classList.add("column-name");
  columnID.classList.add("text-align-center");
  columnAction.classList.add("column-name");
  columnAction.classList.add("text-align-center");

  employeeDetail.appendChild(columnName);
  employeeDetail.appendChild(columnArea);
  employeeDetail.appendChild(columnID);
  employeeDetail.appendChild(columnAction);
  listData.forEach(employeeData => {

    let name = document.createElement("div");
    let area = document.createElement("div");
    let id = document.createElement("div");
    let actions = document.createElement("div");
    let deleteLink = document.createElement("a");

    actions.classList.add("text-align-center");
    actions.classList.add("action-container");
    deleteLink.classList.add("delete");

    name.innerHTML = employeeData.name;
    area.innerHTML = employeeData.area;
    id.innerHTML = employeeData.id;
    deleteLink.innerHTML = "Delete";
    deleteLink.onclick = async () => {
      await remove("http://localhost:3002/employee", employeeData.id);
      await reloadEmployee();
      clearFormData();
    };
    name.onclick = () => setFormData(employeeData);
    area.onclick = () => setFormData(employeeData);
    id.onclick = () => setFormData(employeeData);
    actions.onclick = () => setFormData(employeeData);

    actions.appendChild(deleteLink);
    employeeDetail.appendChild(name);
    employeeDetail.appendChild(area);
    employeeDetail.appendChild(id);
    employeeDetail.appendChild(actions);
  });
}

let createEmployee = async () => {

  let employee = {
    name: document.getElementById("name").value.trim(),
    area: document.getElementById("area").value.trim(),
    id: document.getElementById("id").value.trim()
  }

  try {
    validateInput(employee);
    await create("http://localhost:3002/employee", employee);
    await reloadEmployee();
    clearFormData();
  } catch (err) {
    alert(err);
  }
}

const reloadEmployee = async () => {
  let employeeData = await read("http://localhost:3002/employee");
  renderEmployeeGridData(employeeData);
}

const loadEmployee = (async () => {
  let employeeData = await read("http://localhost:3002/employee");
  renderEmployeeGridData(employeeData);
  if (employeeData.length > 0) setFormData(employeeData[0]);
})();
